/**
 * \file drawable.hpp
 *
 * Encapsulates the abstract base class Drawable.
 */

#ifndef __DRAWABLE_HPP__
#define __DRAWABLE_HPP__

#include <glibmm.h>
#include <gdkmm/rectangle.h>
#include <sigc++/signal.h>
#include <glibmm/object.h>
#include <cairomm/context.h>
#include <cairomm/pattern.h>

#include "color.hpp"

class TreeModel;

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The abstract base class Drawable.
 *
 * This class represents an entity which is drawable. Anything to be drawn onto
 * the canvas should inherit from this base class.
 */
class Drawable : public Glib::Object
{
public:
	/**
	 * \brief The changed signal type.
	 *
	 * This represents the signal type of a changed signal of a drawable entity.
	 */
	typedef sigc::signal<void, Drawable *, bool> changed_signal_t;

	/**
	 * \brief Constructor.
	 */
	Drawable(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	virtual ~Drawable(void);

	/**
	 * \brief Gets the x-coordinate of the top-left corner of the extents.
	 * \return The x-coordinate.
	 */
	virtual gdouble get_extents_x(void) const;

	/**
	 * \brief Gets the y-coordinate of the top-left corner of the extents.
	 * \return The y-coordinate.
	 */
	virtual gdouble get_extents_y(void) const;

	/**
	 * \brief Gets the width of the extents.
	 * \return The width.
	 */
	virtual gdouble get_extents_width(void) const;

	/**
	 * \brief Gets the height of the extents.
	 * \return The height.
	 */
	virtual gdouble get_extents_height(void) const;

	/**
	 * \brief Gets the region.
	 * \return The region.
	 */
	virtual const Gdk::Rectangle &get_rect(void) const;

	/**
	 * \brief Gets the changed signal.
	 *
	 * This signal will be emitted when the coordinate or the size of the
	 * Drawable entity changes, so that some other entities who calculates their
	 * coordinates based on these two parameters can take corresponding actions.
	 *
	 * \return The changed signal.
	 */
	virtual changed_signal_t get_changed_signal(void) const;

	/**
	 * \brief Gets the dirty signal.
	 *
	 * This signal will be emitted when the Drawable entity needs to be redrawn.
	 *
	 * \return The dirty signal.
	 */
	virtual changed_signal_t get_dirty_signal(void) const;

	/**
	 * \brief Tells whether or not it is visible.
	 * \return <code>true</code> if visible.
	 */
	virtual bool is_visible(void) const;

	/**
	 * \brief Gets the color.
	 * \return The color.
	 */
	virtual const Color &get_color(void) const;

	/**
	 * \brief Gets the highlight color.
	 * \return The highlight color.
	 */
	virtual const Color &get_highlight_color(void) const;

	/**
	 * \brief Sets the color in normal mode.
	 * \param color The color object.
	 */
	virtual void set_color(Color color);

	/**
	 * \brief Sets the color in highlight mode.
	 * \param color The color object.
	 */
	virtual void set_highlight_color(Color color);

	/**
	 * \brief Toggles the visibility.
	 * \param value <code>true</code> will show the entity.
	 */
	virtual void toggle_visibility(bool value);

	/**
	 * \brief Draws the entity.
	 * \param cr The Cairo context.
	 */
	virtual void draw(Cairo::RefPtr<Cairo::Context> cr) = 0;

	/**
	 * \brief Checks if the given coordinate pair is in the extents region.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \return <code>true</code> if the given coordinate pair is in the extents
	 * region.
	 */
	virtual bool is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) const = 0;

	/**
	 * \brief Highlights the entity.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	virtual void highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) = 0;

	/**
	 * \brief Removes highlight.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	virtual void remove_highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) = 0;
protected:
	/**
	 * \brief Updates the extents coordinate and size.
	 * \param emit <code>true</code> if you want to set the entity as dirty, so
	 * that it will be redrawn.
	 */
	virtual void _update_extents(bool emit = true) = 0;

	/**
	 * \brief Gets the x-coordinate of the top-left corner of the extents.
	 * \return The x-coordinate.
	 */
	gdouble _get_x(void) const;

	/**
	 * \brief Gets the y-coordinate of the top-left corner of the extents.
	 * \return The y-coordinate.
	 */
	gdouble _get_y(void) const;

	/**
	 * \brief Gets the width of the extents.
	 * \return The width.
	 */
	gdouble _get_width(void) const;

	/**
	 * \brief Gets the height of the extents.
	 * \return The height.
	 */
	gdouble _get_height(void) const;

	/**
	 * \brief Sets the x-coordinate of the top-left corner of the extents.
	 * \param x The x-coordinate.
	 */
	void _set_x(gdouble x);

	/**
	 * \brief Sets the y-coordinate of the top-left corner of the extents.
	 * \param y The y-coordinate.
	 */
	void _set_y(gdouble y);

	/**
	 * \brief Sets the width of the extents.
	 * \param width The width.
	 */
	void _set_width(gdouble width);

	/**
	 * \brief Sets the height of the extents.
	 * \param height The height.
	 */
	void _set_height(gdouble height);

	/**
	 * \brief Sets the region.
	 */
	virtual void _set_rect(void);

	/**
	 * \brief Emits the changed signal.
	 * \param immediate Reserved.
	 */
	virtual void _set_changed(bool immediate = false);

	/**
	 * \brief Emits the dirty signal.
	 * \param immediate <code>true</code> if the entity should be redrawn
	 * immediately.
	 */
	virtual void _set_dirty(bool immediate = false);

	Cairo::Path *_path;			//!< The Cairo path of the entity.
	Color _normal_color;		//!< Normal color.
	Color _highlight_color;		//!< Highlight color.
	const TreeModel &_parent;		  //!< The parent widget.
private:
	gdouble _x;		//!< The x-coordinate of the top-left corner of the extents.
	gdouble _y;		//!< The y-coordinate of the top-left corner of the extents.
	gdouble _width;				//!< The width of the extents.
	gdouble _height;			//!< The height of the extents.
	Gdk::Rectangle *_rect;		//!< The region.
	bool _is_visible;			//!< <code>true</code> if it is visible.

	changed_signal_t _changed_signal; //!< The changed signal.
	changed_signal_t _dirty_signal;	  //!< The dirty signal.
};

inline gdouble Drawable::get_extents_x(void) const
{
	return _x;
}

inline gdouble Drawable::get_extents_y(void) const
{
	return _y;
}

inline gdouble Drawable::get_extents_width(void) const
{
	return _width;
}

inline gdouble Drawable::get_extents_height(void) const
{
	return _height;
}

inline const Gdk::Rectangle &Drawable::get_rect(void) const
{
	return *_rect;
}

inline Drawable::changed_signal_t Drawable::get_changed_signal(void) const
{
	return _changed_signal;
}

inline Drawable::changed_signal_t Drawable::get_dirty_signal(void) const
{
	return _dirty_signal;
}

inline bool Drawable::is_visible(void) const
{
	return _is_visible;
}

inline const Color &Drawable::get_color(void) const
{
	return _normal_color;
}

inline const Color &Drawable::get_highlight_color(void) const
{
	return _highlight_color;
}

inline gdouble Drawable::_get_x(void) const
{
	return Drawable::get_extents_x();
}

inline gdouble Drawable::_get_y(void) const
{
	return Drawable::get_extents_y();
}

inline gdouble Drawable::_get_width(void) const
{
	return Drawable::get_extents_width();
}

inline gdouble Drawable::_get_height(void) const
{
	return Drawable::get_extents_height();
}

inline void Drawable::_set_changed(bool immediate)
{
	_changed_signal.emit(this, immediate);
}

inline void Drawable::_set_dirty(bool immediate)
{
	_dirty_signal.emit(this, immediate);
}

/**
 * \}
 */

#endif
