/**
 * \defgroup Views Views and Controllers
 * \brief UI-related classes and functions.
 */

/**
 * \mainpage Subset of Gtkmm used in AVT.
 *
 * - Cairo Context
 * - Cairo Pattern (for colors in Cairo)
 * - Sigc++ signal (for all the signals)
 * - Glibmm ustring (for handling UTF-8 text)
 * - Pango Layout (for fonts)
 * - Gtkmm Widget
 * - Gtkmm EventBox (for handling X events like key release events)
 * - Gtkmm Entry (for editing text in place)
 * - Gtkmm Layout (for allowing Gtkmm Entry to be placed at an arbitrary
 *                 location on the canvas)
 * - Gtkmm TreeView (for displaying additional information)
 * - Gtkmm ListStore (for organizing the additional information)
 * - Gtkmm Window (for the main window)
 * - Gtkmm FileChooserDialog
 * - Gtkmm Main (the main loop)
 * - Glademm (for interpreting the glade xml file)
 */

/**
 * \file main_window.hpp
 *
 * Encapsulates the MainWindow class.
 */

#ifndef __MAIN_WINDOW_HPP__
#define __MAIN_WINDOW_HPP__

#include <string>
#include <gtkmm/window.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/combobox.h>
#include <gtkmm/scale.h>
#include <libglademm.h>

#include "list_view.hpp"
#include "parser.hpp"
#include "alignment.hpp"
#include "tree_model.hpp"

using namespace Gnome;

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The MainWindow class which represents the main window.
 */
class MainWindow : public Gtk::Window
{
public:
	/**
	 * \brief Constructor.
	 * \param cobject The C object.
	 * \param glade The glade object.
	 */
	MainWindow(GtkWindow *cobject, Glib::RefPtr<Glade::Xml> &glade);

	/**
	 * \brief Destructor.
	 */
	~MainWindow(void);

	/**
	 * \brief Quits.
	 *
	 * Quits the program.
	 */
	void quit(void);

	/**
	 * \brief Clears the current contexts.
	 */
	void clear(void);

	/**
	 * \brief Shows the Open File Chooser dialog.
	 */
	void show_open_dialog(void);

	/**
	 * \brief Shows the opacity adjustment dialog.
	 */
	void show_opacity_adjustment_dialog(void);

	/**
	 * \brief Reads the alignment file.
	 * \param filename The file name.
	 */
	void read_file(const std::string &filename);
protected:
	/**
	 * \brief Key release event handler.
	 * \param event The key release event.
	 * \return <code>true</code> if the event is handled.
	 */
	bool on_key_release_event(GdkEventKey *event);

	/**
	 * \brief Opacity adjustment value changed event handler.
	 */
	virtual void on_opacity_adjustment_value_changed(void);

	/**
	 * \brief Opacity presets changed event handler.
	 */
	virtual void on_opacity_presets_changed(void);
private:
	/**
	 * \brief Initializes the actions.
	 */
	void _init_actions(void);

	/**
	 * \brief Initializes the user interface.
	 */
	void _init_ui(void);

	/**
	 * \brief Reads general information of the alignment file.
	 */
	void _read_header(void);

	/**
	 * \brief Reads the language_models.
	 */
	void _read_language_models(void);

	/**
	 * \brief Reads the alignment information.
	 */
	void _read_alignment(void);

	/**
	 * \brief Reads the translation probabilities.
	 */
	void _read_translation(void);

	/**
	 * \brief Reads the distortion probabilities.
	 */
	void _read_distortion(void);

	/**
	 * \brief Reads the N-gram probabilities.
	 */
	void _read_ngram(void);

	/**
	 * \brief Reads the fertility probabilities.
	 */
	void _read_fertility(void);

	/**
	 * \brief Shows the corresponding information of a terminal.
	 * \param terminal The terminal.
	 */
	void _show_terminal_info(const Terminal &terminal);

	Parser _parser;				//!< The parser.
	Alignment _alignment;		//!< The alignment model.

	Glib::RefPtr<Glade::Xml> _glade; //!< The glade object.

	Gtk::FileChooserDialog *_file_chooser_dialog; //!< The file chooser dialog.
	Gtk::Dialog *_opacity_adjustment_dialog;	  //!< The opacity adjustment dialog;

	Gtk::EventBox *_canvas_eventbox; //!< The event box for the canvas.
	TreeModel *_tree_model;			 //!< The tree model.
	Gtk::ComboBox *_opacity_presets; //!< The opacity presets combo box.
	Gtk::HScale *_opacity_scale;	 //!< The opacity scale.
	Gtk::Adjustment *_opacity_adjustment; //!< The opacity adjustment.

	Gtk::HBox *_info_box;		//!< The information pane.
	ListView *_stats_listview;	//!< The general information display.
	ListView *_trans_listview;	//!< The translation probabilities display.
	ListView *_dist_listview;	//!< The distortion probabilities display.
	ListView *_ngram_listview;	//!< The N-gram probabilities display.
	ListView *_fert_listview;	//!< The fertility probabilities display.
};

/**
 * \}
 */

#endif
