/**
 * \file tree_model.hpp
 *
 * Encapsulates the TreeModel class.
 */

#ifndef __TREE_MODEL_HPP__
#define __TREE_MODEL_HPP__

#include <list>
#include <gtkmm/eventbox.h>
#include <gtkmm/layout.h>
#include <gtkmm/entry.h>
#include <gtkmm/uimanager.h>
#include <gtkmm/actiongroup.h>
#include <gtkmm/toggleaction.h>
#include <gtkmm/radioaction.h>
#include <gtkmm/menu.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/dialog.h>
#include <cairomm/context.h>
#include <libglademm.h>

#include "alignment.hpp"
#include "terminal.hpp"
#include "drawable.hpp"
#include "edge.hpp"
#include "draggable_edge.hpp"
#include "leaf.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The TreeModel class.
 *
 * This class represents the canvas which all the drawing operations take place
 * on. It visualizes the tree model.
 */
class TreeModel : public Gtk::Layout
{
public:
	/**
	 * \brief The source angle changed signal type.
	 */
	typedef sigc::signal<void, gdouble> source_angle_changed_signal_t;

	/**
	 * \brief The target angle changed signal type.
	 */
	typedef sigc::signal<void, gdouble> target_angle_changed_signal_t;

	/**
	 * \brief The source flipped signal type.
	 */
	typedef sigc::signal<void, bool> source_flipped_signal_t;

	/**
	 * \brief The target flipped signal type.
	 */
	typedef sigc::signal<void, bool> target_flipped_signal_t;

	/**
	 * \brief The correspondence visibility toggled signal type.
	 */
	typedef sigc::signal<void, bool> correspondence_visibility_toggled_signal_t;

	/**
	 * \brief The tree visibility toggled signal type.
	 */
	typedef sigc::signal<void, bool> tree_visibility_toggled_signal_t;

	/**
	 * \brief The bracket visibility toggled signal type.
	 */
	typedef sigc::signal<void, bool> bracket_visibility_toggled_signal_t;

	/**
	 * \brief The opacity adjustment visibility toggled signal type.
	 */
	typedef sigc::signal<void> opacity_adjustment_selected_signal_t;

	/**
	 * \brief The opacity changed signal type.
	 */
	typedef sigc::signal<void, gdouble> opacity_changed_signal_t;

	/**
	 * \brief The level spacing changed signal type.
	 */
	typedef sigc::signal<void, gdouble> level_spacing_changed_signal_t;

	/**
	 * \brief The terminal selected signal type.
	 */
	typedef sigc::signal<void, const Terminal &> terminal_selected_signal_t;

	/**
	 * \brief The tree edge cornered signal type.
	 */
	typedef sigc::signal<void, bool> tree_edge_cornered_signal_t;

	/**
	 * \brief The node position signal type.
	 */
	typedef sigc::signal<void, Node::position_t> node_position_changed_signal_t;

	/**
	 * \brief Constructor.
	 * \param cobject The C object.
	 * \param glade The glade object.
	 */
	TreeModel(GtkLayout *cobject, Glib::RefPtr<Gnome::Glade::Xml> &glade);

	/**
	 * \brief Destructor.
	 */
	~TreeModel(void);

	/**
	 * \brief Clears the current contexts.
	 */
	void clear(void);

	/**
	 * \brief Gets the opacity.
	 * \return The opacity.
	 */
	gdouble get_opacity(void) const;

	/**
	 * \brief Gets the leaf before the given leaf in the sentence.
	 * \param leaf The current leaf.
	 * \return The leaf before it, or <code>end</code> if the given leaf is the
	 * first one.
	 */
	std::list<Node *>::iterator get_previous_leaf(const Node &leaf);

	/**
	 * \brief Gets the leaf after the given leaf in the sentence.
	 * \param leaf The current leaf.
	 * \return The leaf after it, or <code>end</code> if the given leaf is the
	 * last one.
	 */
	std::list<Node *>::iterator get_next_leaf(const Node &leaf);

	/**
	 * \brief Sets the Gtk::EventBox for receiving signals.
	 * \param eventbox The Gtk::EventBox.
	 */
	void set_eventbox(Gtk::EventBox &eventbox);

	/**
	 * \brief Adds an Alignment object.
	 * \param alignment The Alignment object.
	 */
	void add_alignment(Alignment &alignment);

	/**
	 * \brief Horizontally scales the canvas up by a certain amount.
	 */
	void increase_hscale(void);

	/**
	 * \brief Vertically scales the canvas up by a certain amount.
	 */
	void increase_vscale(void);

	/**
	 * \brief Horizontally scales the canvas down by a certain amount.
	 */
	void decrease_hscale(void);

	/**
	 * \brief Vertically scales the canvas down by a certain amount.
	 */
	void decrease_vscale(void);

	/**
	 * \brief Changes the opacity.
	 * \param opacity The new opacity.
	 */
	void set_opacity(gdouble opacity);

	/**
	 * \brief Gets the terminal selected signal.
	 * \return The terminal selected signal.
	 */
	terminal_selected_signal_t get_terminal_selected_signal(void) const;

	/**
	 * \brief Gets the opacity adjustment selected signal.
	 * \return The opacity adjustment selected signal.
	 */
	opacity_adjustment_selected_signal_t get_opacity_adjustment_selected_signal(void) const;

	/**
	 * \brief Converts a canvas x-coordinate to device x-coordinate.
	 * \param x The x-coordinate to convert.
	 * \return The device x-coordinate.
	 */
	gdouble _c2d_x(gdouble x) const;

	/**
	 * \brief Converts a canvas y-coordinate to device y-coordinate.
	 * \param y The y-coordinate to convert.
	 * \return The device y-coordinate.
	 */
	gdouble _c2d_y(gdouble y) const;

	/**
	 * \brief Converts a canvas width to device width.
	 * \param w The width to convert.
	 * \return The device width.
	 */
	gdouble _c2d_w(gdouble w) const;

	/**
	 * \brief Converts a canvas height to device height.
	 * \param h The height to convert.
	 * \return The device height.
	 */
	gdouble _c2d_h(gdouble h) const;

	/**
	 * \brief Converts a device x-coordinate to canvas x-coordinate.
	 * \param x The x-coordinate to convert.
	 * \return The canvas x-coordinate.
	 */
	gdouble _d2c_x(gdouble x) const;

	/**
	 * \brief Converts a devicey y-coordinate to canvas y-coordinate.
	 * \param y The y-coordinate to convert.
	 * \return The canvas y-coordinate.
	 */
	gdouble _d2c_y(gdouble y) const;

	/**
	 * \brief Converts a device width to canvas width.
	 * \param w The width to convert.
	 * \return The canvas width.
	 */
	gdouble _d2c_w(gdouble w) const;

	/**
	 * \brief Converts a device height to canvas height.
	 * \param h The height to convert.
	 * \return The canvas height.
	 */
	gdouble _d2c_h(gdouble h) const;
protected:
	/**
	 * \brief Initializes the UI.
	 */
	virtual void _init_ui(void);

	/**
	 * \brief Initializes the menus.
	 */
	virtual void _init_menus(void);

	/**
	 * \brief Initializes the dialogs.
	 */
	virtual void _init_dialogs(Glib::RefPtr<Gnome::Glade::Xml> &glade);

	/**
	 * \brief Expose event handler.
	 * \param event The expose event.
	 * \return <code>true</code> if the event is handled.
	 */
	bool on_expose_event(GdkEventExpose *event);

	/**
	 * \brief Size allocation event handler.
	 * \param allocation The size allocation.
	 */
	void on_size_allocate(Gtk::Allocation& allocation);

	/**
	 * \brief Button press event handler.
	 * \param event The button press event.
	 * \return <code>true</code> if the event is handled.
	 */
	bool on_eventbox_button_press_event(GdkEventButton *event);

	/**
	 * \brief Button release event handler.
	 * \param event The button release event.
	 * \return <code>true</code> if the event is handled.
	 */
	bool on_eventbox_button_release_event(GdkEventButton *event);

	/**
	 * \brief Motion notify event handler.
	 * \param event The motion notify event.
	 * \return <code>true</code> if the event is handled.
	 */
	bool on_eventbox_motion_notify_event(GdkEventMotion *event);

	/**
	 * \brief Key release event handler.
	 * \param event The key release event.
	 * \return <code>true</code> if the event is handled.
	 */
	bool on_eventbox_key_release_event(GdkEventKey *event);

	/**
	 * \brief Toggles the visibility of correspondence edges.
	 */
	virtual void on_correspondence_visibility_toggled(void);

	/**
	 * \brief Entry box key release event handler.
	 */
	virtual bool on_entry_key_release_event(GdkEventKey *event);

	/**
	 * \brief Toggles the visibility of trees.
	 */
	virtual void on_tree_visibility_toggled(void);

	/**
	 * \brief Toggles the visibility of brackets.
	 */
	virtual void on_bracket_visibility_toggled(void);

	/**
	 * \brief Shows the opacity adjustment.
	 */
	virtual void show_opacity_adjustment(void);

	/**
	 * \brief Shows the level spacing adjustment dialog.
	 */
	virtual void show_level_spacing_dialog(void);

	/**
	 * \brief Level spacing value changed event handler.
	 */
	virtual void on_level_spacing_value_changed(void);

	/**
	 * \brief Toggles the tree edge cornered property.
	 */
	virtual void on_tree_edge_cornered_toggled(void);

	/**
	 * \brief Changes the node position.
	 */
	virtual void on_node_position_changed(void);
private:
	/**
	 * \brief Calculates the position of the objects on the canvas.
	 * \param center <code>true</code> to auto-center the sentences.
	 */
	void _calculate_position(bool center = false);

	/**
	 * \brief Sets the target rotation.
	 * \param rotation The target rotation.
	 */
	void _set_target_rotation(gdouble rotation);

	/**
	 * \brief On friend changed signal handler.
	 *
	 * This handler will be called when a node's friend has changed. It will
	 * create or delete the corresponding edge.
	 *
	 * \param s The emitting NonTerminal.
	 * \param t The friend NonTerminal.
	 * \param val <code>true</code> if the friend is added, otherwise it's
	 * deleted.
	 */
	void _on_friend_changed(NonTerminal *s, NonTerminal *t, bool val);

	/**
	 * \brief On parent changed signal handler.
	 *
	 * This handler will be called when a node's parent has changed. It will
	 * create or delete the corresponding edge.
	 *
	 * \param s The emitting NonTerminal.
	 * \param t The parent NonTerminal.
	 * \param val <code>true</code> if the parent is added, otherwise it's
	 * deleted.
	 */
	void _on_parent_changed(NonTerminal *s, NonTerminal *t, bool val);

	/**
	 * \brief On child changed signal handler.
	 *
	 * This handler will be called when a node's child has changed. It will
	 * create or delete the corresponding edge.
	 *
	 * \param s The emitting NonTerminal.
	 * \param t The child NonTerminal.
	 * \param val <code>true</code> if the child is added, otherwise it's
	 * deleted.
	 */
	void _on_child_changed(NonTerminal *s, NonTerminal *t, bool val);

	/**
	 * \brief The common parts for all the transformable creating functions.
	 * \param transformable The new transformable object.
	 */
	void _new_transformable_common(Transformable *transformable);

	/**
	 * \brief The common parts for all the edge creating functions.
	 * \param edge The new edge created.
	 * \param source The source.
	 * \param target The target.
	 */
	void _new_edge_common(DraggableEdge *edge, Node *source, Node *target);

	/**
	 * \brief Creates new virtual edges from a leaf to other leaves.
	 * \param leaf The leaf.
	 */
	void _new_virtual_edges(Node *leaf);

	/**
	 * \brief Creates a new virtual node.
	 * \param node The node to copy from.
	 * \return The new virtual node.
	 */
	Node *_new_virtual_node(Node *node);

	/**
	 * \brief Creates a new branch.
	 * \param type The type of the branch.
	 * \return The created branch, or <code>NULL</code> if failed.
	 */
	Node *_new(LanguageModel::language_model_type_t type);

	/**
	 * \brief Creates a new edge.
	 * \param source The source.
	 * \param target The target.
	 * \return The created edge, or <code>NULL</code> if failed.
	 */
	template <typename T>
	T *_new(Node *source, Node *target);

	/**
	 * \brief Creates a new permutation edge.
	 * \param node The Node which has the permutability.
	 * \return The created permutation edge, or <code>NULL</code> if failed.
	 */
	Edge *_new(const Node *node);

	/**
	 * \brief Deletes the virtual edges connecting to a leaf.
	 * \param leaf The leaf.
	 */
	void _delete_virtual_edges(Node *leaf);

	/**
	 * \brief Deletes the Node.
	 * \param l The list which contains the Node.
	 * \param iter The iterator to the Node.
	 */
	void _delete(std::list<Node *> &l, std::list<Node *>::iterator iter);

	/**
	 * \brief Deletes the Node.
	 * \param node The Node to delete.
	 */
	void _delete(Node *node);

	/**
	 * \brief Deletes the permutation edge.
	 * \param edge The permutation edge to delete.
	 */
	void _delete(Edge *edge);

	/**
	 * \brief Deletes the alignment edge.
	 * \param edge The alignment edge to delete.
	 */
	void _delete(DraggableEdge *edge);

	/**
	 * \brief Deletes and clears a list.
	 * \param list The list.
	 */
	template <typename T>
	void _delete_and_clear(T &list);

	/**
	 * \brief Clears the selected list.
	 */
	void _clear_selected(void);

	/**
	 * \brief Groups the selected Node objects together.
	 *
	 * A new Node object will be created as the parent of all the Node objects
	 * selected. The parent Node will center itself base on the coordinates of
	 * the children.
	 *
	 * \note The selected list will not be cleared after this method call. You
	 * also have to invalidate the window to get the changes updated onto the
	 * canvas.
	 *
	 * \return The created Node, or <code>NULL</code> if failed.
	 */
	Node *_group_selected(void);

	/**
	 * \brief Collapses the selected nodes.
	 * \return The node representing the collapsed nodes.
	 */
	Node *_collapse(void);

	/**
	 * \brief Expands the selected nodes.
	 */
	void _expand(void);

	/**
	 * \brief Adds a list of nodes to a group.
	 * \param branch The parent node.
	 * \param l The list of nodes to add.
	 */
	Node *_add_to_group(Node &branch, std::list<Node *> &l);

	/**
	 * \brief Adds the source LanguageModel.
	 *
	 * New Leaf objects for the source LanguageModel are created at this time.
	 */
	void _add_source(void);

	/**
	 * \brief Adds the target LanguageModel.
	 *
	 * New Leaf objects for the target LanguageModel are created at this time.
	 */
	void _add_target(void);

	/**
	 * \brief Adds a new Terminal.
	 *
	 * It creates a new Leaf object for the terminal.
	 *
	 * \param terminal The terminal to add.
	 * \param source <code>true</code> if this terminal belongs to the source
	 * LanguageModel.
	 */
	void _add_terminal(Terminal &terminal, bool source);

	/**
	 * \brief Adds all the DraggableEdge objects based on the alignment information.
	 */
	void _add_edges(void);

	/**
	 * \brief Merges two nodes.
	 * \param first The first node.
	 * \param second The second node.
	 * \return The merged node.
	 */
	std::list<Node *>::iterator _merge(std::list<Node *>::iterator first, std::list<Node *>::iterator second);

	/**
	 * \brief Merges a node with either the node before it or after it.
	 * \param node The node.
	 * \param before <code>true</code> if to merge the node with the node before
	 * it, otherwise to merge it with the node after it.
	 * \return The merged node.
	 */
	std::list<Node *>::iterator _merge(Node &node, bool before);

	/**
	 * \brief Splits a node into two.
	 *
	 * If the node is empty. Nothing will happen.
	 *
	 * \param node The node to split.
	 * \param pos The position of the split.
	 * \return The iterator to the latter node.
	 */
	std::list<Node *>::iterator _split(Node &node, gint pos);

	/**
	 * \brief Invalidates the whole canvas so that it will be redrawn.
	 */
	void _invalidate(void);

	/**
	 * \brief Invalidates the Drawable.
	 * \param v The Drawable to be redrawn.
	 * \param immediate <code>true</code> if it needs to be redrawn immediately.
	 */
	void _invalidate(Drawable *v, bool immediate);

	/**
	 * \brief Invalidates the region.
	 * \param rect The region.
	 * \param immediate <code>true</code> if it needs to be redrawn immediately.
	 */
	void _invalidate(const Gdk::Rectangle &rect, bool immediate);

	/**
	 * \brief Changes or deletes an edge.
	 * \param edge The edge to change.
	 * \param x The x-coordinate of the point.
	 * \param y The y-coordinate of the point.
	 */
	void _change_edge(DraggableEdge &edge, gdouble x, gdouble y);

	/**
	 * \brief Finds an item in the list.
	 * \param obj The list containing the item.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \return The iterator to the item, or <code>obj.end()</code> if not found.
	 */
	template <typename T>
	typename std::list<T>::iterator _find(std::list<T> &obj, gdouble x, gdouble y) const;

	/**
	 * \brief Finds a Node in the list.
	 * \param obj The list containing the item.
	 * \param x The x-coordinate.
	 * \return The iterator to the item, or <code>obj.end()</code> if not found.
	 */
	std::list<Node *>::iterator _find_x(std::list<Node *> &obj, gdouble x) const;

	/**
	 * \brief Finds a Node in the list.
	 * \param obj The list containing the item.
	 * \param y The y-coordinate.
	 * \return The iterator to the item, or <code>obj.end()</code> if not found.
	 */
	std::list<Node *>::iterator _find_y(std::list<Node *> &obj, gdouble y) const;

	/**
	 * \brief Finds a Node in the list other than <i>node</i>.
	 * \param obj The list containing the item.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \param node The node to skip.
	 * \return The iterator to the item, or <code>obj.end()</code> if not found.
	 */
	std::list<Node *>::iterator _find(std::list<Node *> &obj, gdouble x, gdouble y, const Node &node) const;

	/**
	 * \brief Starts editing Node text
	 * \param node The Node to edit.
	 * \param pos The position to place the cursor.
	 */
	void _start_editing(Node &node, gint pos = 0);

	/**
	 * \brief Stops editing.
	 */
	void _stop_editing(void);

	terminal_selected_signal_t _terminal_selected_signal; //!< The terminal selected signal.
	source_angle_changed_signal_t _source_angle_changed_signal;  //!< The source angle changed signal.
	target_angle_changed_signal_t _target_angle_changed_signal;  //!< The target angle changed signal.
	source_flipped_signal_t _source_flipped_signal;				 //!< The source flipped signal.
	target_flipped_signal_t _target_flipped_signal;				 //!< The target flipped signal.
	correspondence_visibility_toggled_signal_t _correspondence_visibility_toggled_signal; //!< The correspondence visibility toggled signal.
	tree_visibility_toggled_signal_t _tree_visibility_toggled_signal; //!< The tree visibility toggled signal.
	bracket_visibility_toggled_signal_t _bracket_visibility_toggled_signal; //!< The bracket visibility toggled signal.
	opacity_adjustment_selected_signal_t _opacity_adjustment_selected_signal; //!< The opacity adjustment selected signal.
	opacity_changed_signal_t _opacity_changed_signal; //!< The opacity changed signal.
	level_spacing_changed_signal_t _level_spacing_changed_signal; //!< The level spacing changed signal.
	tree_edge_cornered_signal_t _tree_edge_cornered_signal;		  //!< The tree edge cornered signal.
	node_position_changed_signal_t _node_position_changed_signal; //!< The position changed signal.

	Glib::RefPtr<Gtk::UIManager> _ui_manager;
	Glib::RefPtr<Gtk::ActionGroup> _ctx_menu_action_group;
	Gtk::Menu *_ctx_menu;
	Glib::ustring _ctx_menu_ui; //!< The context menu UI description.

	Gtk::Dialog *_level_spacing_dialog;
	Gtk::SpinButton *_level_spacing_spinbutton;

	// Layout details
	const gdouble _SENTENCE_SPACING;   //!< The language_model spacing.
	const gdouble _TREE_LEVEL_SPACING; //!< The tree level spacing.
	const gdouble _WORD_SPACING;	   //!< The terminal spacing.
	const gdouble _SCALING_STEP;	   //!< The scaling step.
	gdouble _xc;					   //!< The device x-coordinate of the center.
	gdouble _yc;					   //!< The device y-coordinate of the center.
	gdouble _dx;					   //!< The difference between the x-coordinate of the original center and the mouse pointer.
	gdouble _dy;					   //!< The difference between the y-coordinate of the original center and the mouse pointer.
	bool _is_centering;				   //!< <code>true</code> if is centering by dragging.
	bool _is_draw_rotating;			   //!< <code>true</code> to draw the rotating sign.
	bool _is_rotating;				   //!< <code>true</code> if the language_model is rotating.
	Glib::RefPtr<Gtk::ToggleAction> _is_show_correspondences; //!< Whether or not to show the correspondences.
	Glib::RefPtr<Gtk::ToggleAction> _is_show_reference_lines; //!< Whether or not to show the reference lines.
	Glib::RefPtr<Gtk::ToggleAction> _is_show_trees; //!< Whether or not to show the trees.
	Glib::RefPtr<Gtk::ToggleAction> _is_show_brackets; //!< Whether or not to show the brackets.
	Glib::RefPtr<Gtk::ToggleAction> _is_tree_edge_cornered; //!< Whether or not the tree edges are cornered.
	Gtk::RadioButton::Group _position_button_group;			//!< Position buttons group.
	Glib::RefPtr<Gtk::RadioAction> _position_begin;	//!< Position at the beginning.
	Glib::RefPtr<Gtk::RadioAction> _position_end;	//!< Position at the end.
	Glib::RefPtr<Gtk::RadioAction> _position_center; //!< Position at the center.
	bool _source_flip;				   //!< <code>true</code> if to draw the tree below the sentence.
	bool _target_flip;				   //!< <code>true</code> if to draw the tree below the sentence.
	gdouble _source_rotation;		   //!< The angle of rotation in radians.
	gdouble _target_rotation;		   //!< The angle of rotation in radians.
	gdouble _hscale;				   //!< The horizontal scale.
	gdouble _vscale;				   //!< The vertical scale.
	gdouble _opacity;				   //!< The opacity.

	Node *_editing_node;			   //!< The editing node.
	Gtk::Entry *_editor;			   //!< The cell text editor.
	Cairo::RefPtr<Cairo::Context> _cr; //!< The Cairo context.
	Alignment *_alignment;			   //!< The alignment model.
	std::list<Node *> _source;		   //!< The source leaves.
	std::list<Node *> _target;		   //!< The target leaves.
	std::list<Node *> _branches;	   //!< The branches
	std::list<DraggableEdge *> _edges; //!< The edges.
	std::list<Edge *> _permutations;   //!< The permutation indicators.
	Drawable *_highlighted;			   //!< The highlighted object.
	std::list<Node *> _selected;	   //!< The selected objects.
	Node *_virtual_node;			   //!< The virtual node.
	Node *_dragging_node;			   //!< The dragging node.
	Node *_changing_node;			   //!< The changing node.
	DraggableEdge *_dragging_edge;	   //!< The dragging edge.
	bool _is_draw_indicator;		   //!< Whether or not to draw the indicator.
	std::list<Node *>::iterator _indicator_pos; //!< The node after the insertion indicator.
};

inline gdouble TreeModel::get_opacity(void) const
{
	return _opacity;
}

inline void TreeModel::increase_hscale(void)
{
	_hscale += _SCALING_STEP;

	_invalidate();
}

inline void TreeModel::increase_vscale(void)
{
	_vscale += _SCALING_STEP;

	_invalidate();
}

inline void TreeModel::decrease_hscale(void)
{
	if ((_hscale - _SCALING_STEP) < 0.1) {
		return;
	}

	_hscale -= _SCALING_STEP;

	_invalidate();
}

inline void TreeModel::decrease_vscale(void)
{
	if ((_vscale - _SCALING_STEP) < 0.1) {
		return;
	}

	_vscale -= _SCALING_STEP;

	_invalidate();
}

inline TreeModel::terminal_selected_signal_t
TreeModel::get_terminal_selected_signal(void) const
{
	return _terminal_selected_signal;
}

inline TreeModel::opacity_adjustment_selected_signal_t
TreeModel::get_opacity_adjustment_selected_signal(void) const
{
	return _opacity_adjustment_selected_signal;
}

inline gdouble TreeModel::_c2d_x(gdouble x) const
{
	gdouble y = 0;

	_cr->user_to_device(x, y);

	return x;
}

inline gdouble TreeModel::_c2d_y(gdouble y) const
{
	gdouble x = 0;

	_cr->user_to_device(x, y);

	return y;
}

inline gdouble TreeModel::_c2d_w(gdouble w) const
{
	gdouble h = 0;

	_cr->user_to_device_distance(w, h);

	return w;
}

inline gdouble TreeModel::_c2d_h(gdouble h) const
{
	gdouble w = 0;

	_cr->user_to_device_distance(w, h);

	return h;
}

inline gdouble TreeModel::_d2c_x(gdouble x) const
{
	gdouble y = 0;

	_cr->device_to_user(x, y);

	return x;
}

inline gdouble TreeModel::_d2c_y(gdouble y) const
{
	gdouble x = 0;

	_cr->device_to_user(x, y);

	return y;
}

inline gdouble TreeModel::_d2c_w(gdouble w) const
{
	gdouble h = 0;

	_cr->device_to_user_distance(w, h);

	return w;
}

inline gdouble TreeModel::_d2c_h(gdouble h) const
{
	gdouble w = 0;

	_cr->device_to_user_distance(w, h);

	return h;
}

/**
 * \}
 */

#endif
