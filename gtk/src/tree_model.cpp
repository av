#include <algorithm>
#include <functional>
#include <gdk/gdkkeysyms.h>
#include <gtkmm/stockid.h>
#include <gtkmm/stock.h>

#include "tree_model.hpp"
#include "tree_edge.hpp"
#include "virtual_edge.hpp"

template <>
DraggableEdge *TreeModel::_new<DraggableEdge>(Node *source, Node *target)
{
	DraggableEdge *e = NULL;

	e = new DraggableEdge(*this);
	_new_edge_common(e, source, target);

	_correspondence_visibility_toggled_signal.connect(sigc::mem_fun(*(Drawable *)e, &Drawable::toggle_visibility));

	return e;
}

template <>
TreeEdge *TreeModel::_new<TreeEdge>(Node *source, Node *target)
{
	TreeEdge *e = NULL;

	e = new TreeEdge(*this);
	_new_edge_common(e, source, target);

	_tree_edge_cornered_signal.connect(sigc::mem_fun(*e, &TreeEdge::set_is_cornered));

	return e;
}

TreeModel::TreeModel(GtkLayout *cobject,
                     Glib::RefPtr<Gnome::Glade::Xml> &glade) :
	Gtk::Layout(cobject),
	_ctx_menu(NULL),
	_level_spacing_dialog(NULL),
	_level_spacing_spinbutton(NULL),
	_SENTENCE_SPACING(100),
	_TREE_LEVEL_SPACING(80),
	_WORD_SPACING(15),
	_SCALING_STEP(0.2),
	_xc(0),
	_yc(0),
	_dx(0),
	_dy(0),
	_is_centering(false),
	_is_draw_rotating(false),
	_is_rotating(false),
	_source_flip(false),
	_target_flip(false),
	_source_rotation(0),
	_target_rotation(0),
	_hscale(1),
	_vscale(1),
	_opacity(0),
	_editing_node(NULL),
	_editor(Gtk::manage(new Gtk::Entry)),
	_alignment(NULL),
	_highlighted(NULL),
	_virtual_node(NULL),
	_dragging_node(NULL),
	_changing_node(NULL),
	_dragging_edge(NULL),
	_is_draw_indicator(false)
{
	_init_ui();
	_init_menus();
	_init_dialogs(glade);
}

TreeModel::~TreeModel(void)
{
	clear();

	delete _ctx_menu;
	delete _level_spacing_dialog;
}

void TreeModel::_init_ui(void)
{
	_editor->signal_key_release_event().connect(sigc::mem_fun(*this, &TreeModel::on_entry_key_release_event));
	_editor->set_no_show_all();

	add(*_editor);
}

void TreeModel::_init_menus(void)
{
	_ctx_menu_action_group = Gtk::ActionGroup::create();
	_ctx_menu_action_group->add(Gtk::Action::create("context_menu",
	                                                "Context Menu"));

	_is_show_correspondences = Gtk::ToggleAction::create("toggle_correspondences", "Show Correspondences", "Whether or not to show the correspondence edges", true);
	_ctx_menu_action_group->add(_is_show_correspondences, sigc::mem_fun(*this, &TreeModel::on_correspondence_visibility_toggled));
	_is_show_reference_lines = Gtk::ToggleAction::create("toggle_reference_lines", "Show Reference Lines", "Whether or not to show the reference lines", true);
	_ctx_menu_action_group->add(_is_show_reference_lines, sigc::mem_fun<void>(*this, &TreeModel::_invalidate));
	_is_show_trees = Gtk::ToggleAction::create("toggle_trees", "Show Trees", "Whether or not to show the trees", true);
	_ctx_menu_action_group->add(_is_show_trees, sigc::mem_fun(*this, &TreeModel::on_tree_visibility_toggled));
	_is_tree_edge_cornered = Gtk::ToggleAction::create("toggle_tree_edge_cornered", "Tree Edges Cornered", "Whether or not to make the tree edges cornered", false);
	_ctx_menu_action_group->add(_is_tree_edge_cornered, sigc::mem_fun(*this, &TreeModel::on_tree_edge_cornered_toggled));
	_is_show_brackets = Gtk::ToggleAction::create("toggle_brackets", "Show Brackets", "Whether or not to show the brackets", true);
	_ctx_menu_action_group->add(_is_show_brackets, sigc::mem_fun(*this, &TreeModel::on_bracket_visibility_toggled));
	_ctx_menu_action_group->add(Gtk::Action::create("show_opacity_adjustment", "Show Opacity Adjustment"), sigc::mem_fun(*this, &TreeModel::show_opacity_adjustment));

	_ctx_menu_action_group->add(Gtk::Action::create("set_level_spacing", "Set Level Spacing", "Set the spacing between levels"), sigc::mem_fun(*this, &TreeModel::show_level_spacing_dialog));

	_position_begin = Gtk::RadioAction::create(_position_button_group, "set_node_position_begin", "Begin");
	_position_end = Gtk::RadioAction::create(_position_button_group, "set_node_position_end", "End");
	_position_center = Gtk::RadioAction::create(_position_button_group, "set_node_position_center", "Center");
	_position_begin->property_value() = Node::BEGIN;
	_position_end->property_value() = Node::END;
	_position_center->property_value() = Node::CENTER;
	_ctx_menu_action_group->add(_position_begin, sigc::mem_fun(*this, &TreeModel::on_node_position_changed));
	_ctx_menu_action_group->add(_position_end, sigc::mem_fun(*this, &TreeModel::on_node_position_changed));
	_ctx_menu_action_group->add(_position_center, sigc::mem_fun(*this, &TreeModel::on_node_position_changed));
	_position_center->set_current_value(_position_center->property_value());

	_ctx_menu_ui =
		"<ui>"
		"  <popup name='context_menu'>"
		"    <menuitem action='toggle_reference_lines'/>"
		"    <separator/>"
		"    <menuitem action='toggle_correspondences'/>"
		"    <menuitem action='toggle_trees'/>"
		"    <menuitem action='toggle_tree_edge_cornered'/>"
		"    <menuitem action='toggle_brackets'/>"
		"    <separator/>"
		"    <menuitem action='show_opacity_adjustment'/>"
		"    <menuitem action='set_level_spacing'/>"
		"    <separator/>"
		"    <menuitem action='set_node_position_begin'/>"
		"    <menuitem action='set_node_position_end'/>"
		"    <menuitem action='set_node_position_center'/>"
		"  </popup>"
		"</ui>";
	_ui_manager = Gtk::UIManager::create();
	_ui_manager->insert_action_group(_ctx_menu_action_group);
	_ui_manager->add_ui_from_string(_ctx_menu_ui);

	_ctx_menu = dynamic_cast<Gtk::Menu *>(_ui_manager->get_widget("/context_menu"));
}

void TreeModel::_init_dialogs(Glib::RefPtr<Gnome::Glade::Xml> &glade)
{
	_level_spacing_dialog = glade->get_widget("level_spacing_dialog",
	                                          _level_spacing_dialog);
	g_assert(_level_spacing_dialog);

	_level_spacing_dialog->add_button(Gtk::StockID(Gtk::Stock::OK),
	                                  Gtk::RESPONSE_OK);
	_level_spacing_dialog->add_button(Gtk::StockID(Gtk::Stock::CANCEL),
	                                  Gtk::RESPONSE_CANCEL);

	_level_spacing_spinbutton = glade->get_widget("level_spacing_spinbutton",
	                                              _level_spacing_spinbutton);
	g_assert(_level_spacing_spinbutton);

	_level_spacing_spinbutton->signal_value_changed().connect(sigc::mem_fun(*this, &TreeModel::on_level_spacing_value_changed));
}

void TreeModel::clear(void)
{
	_delete_and_clear(_permutations);
	_delete_and_clear(_edges);
	_delete_and_clear(_branches);
	_delete_and_clear(_source);
	_delete_and_clear(_target);

	_xc = 0;
	_yc = 0;
	_is_draw_rotating = false;
	_is_rotating = false;
	_source_rotation = 0.0;
	_target_rotation = 0.0;
	_hscale = 1;
	_vscale = 1;
	_highlighted = NULL;
	_selected.clear();
	_dragging_node = NULL;
	_dragging_edge = NULL;
}

std::list<Node *>::iterator TreeModel::get_previous_leaf(const Node &leaf)
{
	std::list<Node *> *l = NULL;
	std::list<Node *>::iterator iter;

	if (leaf.get_type() == LanguageModel::SOURCE) {
		l = &_source;
	} else {
		l = &_target;
	}

	if (!leaf.get_nonterminal().is_leaf()) {
		return l->end();
	}

	iter = std::find(l->begin(), l->end(), &leaf);

	if (iter != l->begin()) {
		return --iter;
	} else {
		return l->end();
	}
}

std::list<Node *>::iterator TreeModel::get_next_leaf(const Node &leaf)
{
	std::list<Node *> *l = NULL;
	std::list<Node *>::iterator iter;

	if (leaf.get_type() == LanguageModel::SOURCE) {
		l = &_source;
	} else {
		l = &_target;
	}

	if (!leaf.get_nonterminal().is_leaf()) {
		return l->end();
	}

	iter = std::find(l->begin(), l->end(), &leaf);

	if (++iter != l->end()) {
		return iter;
	} else {
		return l->end();
	}
}

void TreeModel::set_eventbox(Gtk::EventBox &eventbox)
{
	eventbox.signal_button_press_event().connect(sigc::mem_fun(*this, &TreeModel::on_eventbox_button_press_event), false);
	eventbox.signal_button_release_event().connect(sigc::mem_fun(*this, &TreeModel::on_eventbox_button_release_event), false);
	eventbox.signal_motion_notify_event().connect(sigc::mem_fun(*this, &TreeModel::on_eventbox_motion_notify_event), false);
	eventbox.signal_key_release_event().connect(sigc::mem_fun(*this, &TreeModel::on_eventbox_key_release_event), false);
}

void TreeModel::add_alignment(Alignment &alignment)
{
	_alignment = &alignment;

	clear();

	_add_source();
	_add_target();
	_add_edges();

	// For testing purpose only
	_selected.push_back(list_at(_source, 0));
	_selected.push_back(list_at(_source, 1));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_source, 2));
	_selected.push_back(list_at(_source, 3));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_source, 4));
	_selected.push_back(list_at(_source, 5));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 0));
	_selected.push_back(list_at(_branches, 1));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 2));
	_selected.push_back(list_at(_branches, 3));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 4));
	_selected.push_back(list_at(_source, 6));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_target, 0));
	_selected.push_back(list_at(_target, 1));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_target, 4));
	_selected.push_back(list_at(_target, 5));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_target, 6));
	_selected.push_back(list_at(_target, 7));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 7));
	_selected.push_back(list_at(_target, 3));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 9));
	_selected.push_back(list_at(_target, 2));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 8));
	_selected.push_back(list_at(_branches, 10));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 11));
	_selected.push_back(list_at(_branches, 6));
	_group_selected();
	_selected.clear();

	_selected.push_back(list_at(_branches, 12));
	_selected.push_back(list_at(_target, 8));
	_group_selected();
	_selected.clear();

	list_at(_source, 0)->get_nonterminal().add_friend(*list_at(_branches, 7));
	list_at(_branches, 7)->get_nonterminal().add_friend(*list_at(_source, 0));

	list_at(_branches, 0)->get_nonterminal().add_friend(*list_at(_branches, 9));
	list_at(_branches, 9)->get_nonterminal().add_friend(*list_at(_branches, 0));

	list_at(_branches, 0)->get_nonterminal().add_friend(*list_at(_branches, 10));
	list_at(_branches, 10)->get_nonterminal().add_friend(*list_at(_branches, 0));

	list_at(_branches, 2)->get_nonterminal().add_friend(*list_at(_branches, 6));
	list_at(_branches, 6)->get_nonterminal().add_friend(*list_at(_branches, 2));

	list_at(_branches, 1)->get_nonterminal().add_friend(*list_at(_branches, 8));
	list_at(_branches, 8)->get_nonterminal().add_friend(*list_at(_branches, 1));

	list_at(_branches, 3)->get_nonterminal().add_friend(*list_at(_branches, 11));
	list_at(_branches, 11)->get_nonterminal().add_friend(*list_at(_branches, 3));

	list_at(_branches, 4)->get_nonterminal().add_friend(*list_at(_branches, 12));
	list_at(_branches, 12)->get_nonterminal().add_friend(*list_at(_branches, 4));

	list_at(_branches, 5)->get_nonterminal().add_friend(*list_at(_branches, 13));
	list_at(_branches, 13)->get_nonterminal().add_friend(*list_at(_branches, 5));

	_calculate_position(true);
	_invalidate();
}

void TreeModel::set_opacity(gdouble opacity)
{
	_opacity = opacity;

	_opacity_changed_signal.emit(_opacity);
}

bool TreeModel::on_expose_event(GdkEventExpose *event)
{
	gdouble x, y;
	gdouble start_x, start_y, end_x, end_y;
	guint width = 0;
	guint height = 0;
	Leaf *leaf = NULL;
	std::list<DraggableEdge *>::iterator eiter;
	std::list<Edge *>::iterator piter;
	std::list<Node *>::iterator niter;
	Glib::RefPtr<Gdk::Window> window = get_bin_window();
	Gtk::Allocation allocation = get_allocation();

	if (window) {
		_cr = window->create_cairo_context();

		width = allocation.get_width();
		height = allocation.get_height();

		// Only redraw the region which has changed
		if (event) {
			_cr->rectangle(event->area.x, event->area.y,
			               event->area.width, event->area.height);
			_cr->clip();
		}

		_cr->translate(_xc, _yc);
		_cr->scale(_hscale, _vscale);

		// Paint background
		_cr->save();
		_cr->set_source_rgb(1, 1, 1); // white
		_cr->paint();

		// Draw all children leaves
		for (niter = _source.begin(); niter != _source.end(); niter++) {
			leaf = dynamic_cast<Leaf *>(*niter);
			Color color = leaf->get_line_color();

			color.set_alpha(_target_rotation / (M_PI / 2));
			leaf->set_line_color(color);

			gdouble center_x = _c2d_x(leaf->get_extents_x()) +
				leaf->get_extents_width() / 2 * _hscale;
			gdouble center_y = _c2d_y(leaf->get_extents_y()) +
				leaf->get_extents_height() / 2 * _vscale;
			if (_source_rotation == 0.0) {
				start_x = _d2c_x(center_x);
				start_y = _d2c_y(0.0);

				end_x = _d2c_x(center_x);
				end_y = _d2c_y(height);
			} else if (_source_rotation == M_PI / 2) {
				start_x = _d2c_x(0.0);
				start_y = _d2c_y(center_y);

				end_x = _d2c_x(width);
				end_y = _d2c_y(center_y);
			} else {
				start_x = _d2c_x(0.0);
				start_y = _d2c_y(center_y + center_x / tan(_source_rotation));

				end_y = _d2c_y(0.0);
				end_x = _d2c_x(center_x + center_y * tan(_source_rotation));
			}

			if (!_dragging_node || _dragging_node != *niter) {
				if (_is_show_reference_lines->get_active()) {
					leaf->draw_line(_cr, start_x, start_y, end_x, end_y);
				}
				leaf->draw(_cr);
			}
		}

		for (niter = _target.begin(); niter != _target.end(); niter++) {
			leaf = dynamic_cast<Leaf *>(*niter);
			Color color = leaf->get_line_color();

			color.set_alpha(_target_rotation / (M_PI / 2));
			leaf->set_line_color(color);

			gdouble center_x = _c2d_x(leaf->get_extents_x()) +
				leaf->get_extents_width() / 2 * _hscale;
			gdouble center_y = _c2d_y(leaf->get_extents_y()) +
				leaf->get_extents_height() / 2 * _vscale;
			if (_target_rotation <= 0.05) {
				start_x = _d2c_x(center_x);
				start_y = _d2c_y(0.0);

				end_x = _d2c_x(center_x);
				end_y = _d2c_y(height);
			} else if ((M_PI / 2 - _target_rotation) <= 0.05) {
				start_x = _d2c_x(0.0);
				start_y = _d2c_y(center_y);

				end_x = _d2c_x(width);
				end_y = _d2c_y(center_y);
			} else {
				start_x = _d2c_x(0.0);
				start_y = _d2c_y(center_y + center_x / tan(_target_rotation));

				end_y = _d2c_y(0.0);
				end_x = _d2c_x(center_x + center_y * tan(_target_rotation));
			}

			if (!_dragging_node || _dragging_node != *niter) {
				if (_is_show_reference_lines->get_active()) {
					leaf->draw_line(_cr, start_x, start_y, end_x, end_y);
				}
				leaf->draw(_cr);
			}
		}

		if (_is_draw_rotating || _is_rotating) {
			x = leaf->get_extents_x() + leaf->get_extents_width() +
				_WORD_SPACING + 6;
			y = leaf->get_extents_y() + leaf->get_extents_height() / 2;

			_cr->save();
			_cr->arc(x, y, 7.0, 0.0, 2 * M_PI);
			_cr->set_source_rgb(0.0, 0.0, 0.0);
			_cr->set_line_width(3.5);
			_cr->stroke();
			_cr->restore();
		}

		// Draw all branches
		for (niter = _branches.begin(); niter != _branches.end(); niter++) {
			(*niter)->draw(_cr);
		}

		// Draw all edges
		for (eiter = _edges.begin(); eiter != _edges.end(); eiter++) {
			(*eiter)->draw(_cr);
		}

		// Draw all permutations
		for (piter = _permutations.begin(); piter != _permutations.end();
		     piter++) {
			(*piter)->draw(_cr);
		}

		if (_virtual_node) {
			_virtual_node->draw(_cr);
		}

		// Draw insertion indicator and dragging node
		if (_dragging_node) {
			if (_is_draw_indicator) {
				niter = _indicator_pos;

				if (_indicator_pos == _source.end() ||
				    _indicator_pos == _target.end()) {
					niter--;

					x = (*niter)->get_extents_x() +
						(*niter)->get_extents_width() + _WORD_SPACING / 2;
				} else {
					x = (*niter)->get_extents_x() - _WORD_SPACING / 2;
				}
				y = (*niter)->get_extents_y();

				_cr->save();
				_cr->move_to(x, y - 5);
				_cr->line_to(x, y + (*niter)->get_extents_height() + 5);
				_cr->set_source_rgba(0, 0, 0, 0.5);
				_cr->set_line_width(_WORD_SPACING / 4);
				_cr->stroke();
				_cr->restore();
			}

			_dragging_node->draw(_cr);
		}

		_cr->restore();
	}

	return true;
}

void TreeModel::on_size_allocate(Gtk::Allocation& allocation)
{
	Gtk::Layout::on_size_allocate(allocation);

	_calculate_position();
}

bool TreeModel::on_eventbox_button_press_event(GdkEventButton *event)
{
	Node *n = NULL;
	std::list<Node *>::iterator iter;
	std::list<DraggableEdge *>::iterator eiter;
	gdouble x = _d2c_x(event->x);
	gdouble y = _d2c_y(event->y);
	Glib::ustring response;

	if (event->button == 3) {
		if (_ctx_menu) {
			_ctx_menu->popup(event->button, event->time);
		}

		return true;
	}

	if ((iter = _find(_source, x, y)) != _source.end() ||
		(iter = _find(_target, x, y)) != _target.end()) {
		n = *iter;
	} else if ((iter = _find(_branches, x, y)) != _branches.end()) {
		n = *iter;
	}

	if (n) {
		if (n->is_drag()) {
			_changing_node = n;

			return true;
		}

		iter = std::find(_selected.begin(), _selected.end(), n);

		// Multi-select.
		if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) {
			if (iter == _selected.end()) {
				n->select(_cr, x, y);
				_selected.push_back(n);
			} else {
				(*iter)->deselect();
				_selected.erase(iter);

				return true;
			}
		} else if (iter == _selected.end()) {
			_clear_selected();

			n->select(_cr, x, y);
			_selected.push_back(n);
		}

		// Double clicked.
		if (event->type == GDK_2BUTTON_PRESS) {
			_start_editing(*n);
		} else if (n->get_nonterminal().is_leaf()) {
			_terminal_selected_signal.emit(dynamic_cast<Terminal &>
			                               (n->get_nonterminal()));
		}
	} else if ((eiter = _find(_edges, x, y)) != _edges.end()) {
		_dragging_edge = *eiter;
		_dragging_edge->drag(x, y);
	} else {
		if (!_target.empty()) {
			n = *_target.rbegin();
			// Start rotating the language_model
			if (x > n->get_extents_x() && y > n->get_extents_y() &&
			    y < (n->get_extents_y() + n->get_extents_height())) {
				_is_rotating = true;
			}
		}

		if (!_is_rotating) {
			_is_centering = true;

			_dx = event->x;
			_dy = event->y;

			_clear_selected();
		}
	}

	return true;
}

bool TreeModel::on_eventbox_button_release_event(GdkEventButton *event)
{
	Node *node = NULL;
	DraggableEdge *edge = NULL;
	gdouble x = _d2c_x(event->x);
	gdouble y = _d2c_y(event->y);
	gdouble fixed_y;
	std::list<Node *>::iterator iter;

	if (_is_centering) {
		_is_centering = false;

		_invalidate();
		return true;
	}

	if (_changing_node) {
		_changing_node->stop_drag(x, y);
		_changing_node->update_position();
		_changing_node = NULL;

		_invalidate();
		return true;
	}

	if (!_dragging_edge) {
		if (_dragging_node && _dragging_node->get_nonterminal().is_leaf()) {
			fixed_y = _dragging_node->get_extents_y() +
				_dragging_node->get_extents_height() / 2;
			dynamic_cast<Leaf *>(_dragging_node)->stop_drag();

			if (_is_draw_indicator) {
			    if (_dragging_node->get_type() == LanguageModel::SOURCE) {
					iter = std::find(_source.begin(), _source.end(),
					                 _dragging_node);
					node = *iter;

					_source.erase(iter);
					_source.insert(_indicator_pos, node);
				} else {
					iter = std::find(_target.begin(), _target.end(),
					                 _dragging_node);
					node = *iter;

					_target.erase(iter);
					_target.insert(_indicator_pos, node);
				}
			}

			_calculate_position();
			_invalidate();
		} else if (_is_rotating) {
			_is_rotating = false;

			if ((M_PI / 2 - _target_rotation) < 0.2) {
				_set_target_rotation(M_PI / 2);
			} else if (_target_rotation < 0.2) {
				_set_target_rotation(0.0);
			}

			_target_angle_changed_signal.emit(_target_rotation);

			_invalidate();
		}

		_dragging_node = NULL;

		if (!_editing_node) {
			grab_focus();
		}

		return true;
	}

	if ((edge = dynamic_cast<VirtualEdge *>(_highlighted))) {
		edge->get_source()->get_nonterminal().add_friend(*(edge->get_target()));
		edge->get_target()->get_nonterminal().add_friend(*(edge->get_source()));
		_delete(edge);

		_highlighted = NULL;
		_dragging_edge = NULL;

		_invalidate();

		return true;
	}

	_change_edge(*_dragging_edge, x, y);

	_dragging_node = NULL;
	_dragging_edge = NULL;

	// There must be a better way to clear the dragging traces
	_invalidate();

	return true;
}

bool TreeModel::on_eventbox_motion_notify_event(GdkEventMotion *event)
{
	std::list<DraggableEdge *>::iterator iter;
	std::list<Edge *>::iterator piter;
	Node *node = NULL;
	gdouble x = _d2c_x(event->x);
	gdouble y = _d2c_y(event->y);
	gdouble dx, dy;

	if (_is_centering) {
		_xc += event->x - _dx;
		_yc += event->y - _dy;
		_dx = event->x;
		_dy = event->y;

		_invalidate();

		return true;
	}

	if (_is_draw_rotating) {
		_is_draw_rotating = false;
		_invalidate();
	}

	if (_changing_node) {
		_changing_node->drag(x, y);

		_invalidate();
	} else if (_dragging_edge) {
		_dragging_edge->drag(x, y);

		_invalidate();
	} else if (_dragging_node) {
		if (!_dragging_node->get_nonterminal().is_leaf()) {
			return true;
		}

		_is_draw_indicator = true;
		dynamic_cast<Leaf *>(_dragging_node)->drag(x, y);

		if (_dragging_node->get_type() == LanguageModel::SOURCE) {
			_indicator_pos = _find(_source, x, y, *_dragging_node);

			if (_indicator_pos == _source.end()) {
			    if ((*(--_indicator_pos))->get_extents_x() < x) {
					_indicator_pos++;
				} else {
					_is_draw_indicator = false;
				}
			}
		} else {
			_indicator_pos = _find(_target, x, y, *_dragging_node);

			if (_indicator_pos == _target.end()) {
			    if ((*(--_indicator_pos))->get_extents_x() < x) {
					_indicator_pos++;
				} else {
					_is_draw_indicator = false;
				}
			}
		}

		_invalidate();
	} else if (_is_rotating) {
		if (_target.empty()) {
			_is_rotating = false;
		} else {
			node = *_target.begin();
			dx = x - node->get_extents_x();
			dy = y - node->get_extents_y();

			if (dx >= 0.0) {
				if (dy >= 0.0) {
					_set_target_rotation(atan(dy / dx));
				} else {
					_set_target_rotation(0.0);
				}
			} else {
				_set_target_rotation(M_PI / 2.0);
			}

			_target_angle_changed_signal.emit(_target_rotation);

			_invalidate();
		}
	} else if ((event->state & GDK_BUTTON1_MASK) == GDK_BUTTON1_MASK) {
		if (_selected.size() == 1 &&
		    (*_selected.begin())->is_in_region(_cr, x, y)) {
			_dragging_node = *_selected.begin();
		} else {
			_xc = _xc + event->x;
			_yc = _yc + event->y;
		}
	} else {
		if (_highlighted) {
			_highlighted->remove_highlight(_cr, x, y);
			_highlighted = NULL;
		}

		if ((iter = _find(_edges, x, y)) != _edges.end()) {
			_highlighted = *iter;
			_highlighted->highlight(_cr, x, y);
		} else if ((piter = _find(_permutations, x, y)) !=
				   _permutations.end()) {
			_highlighted = *piter;
			_highlighted->highlight(_cr, x, y);
		}

		if (!_highlighted && !_target.empty()) {
			node = *_target.rbegin();
			if (x > node->get_extents_x() && y > node->get_extents_y() &&
			    y < (node->get_extents_y() + node->get_extents_height())) {
				_is_draw_rotating = true;

				_invalidate();
			}
		}
	}

	return true;
}

bool TreeModel::on_eventbox_key_release_event(GdkEventKey *event)
{
	DraggableEdge *e = NULL;
	Node *n = NULL;
	Node *source = NULL;
	Node *target = NULL;
	std::list<Node *>::iterator iter, prev;

	if (event->keyval == GDK_Delete) {
		if (_highlighted) {
			if ((e = dynamic_cast<TreeEdge *>(_highlighted))) {
				_delete((TreeEdge *)e);
			} else if ((e = dynamic_cast<DraggableEdge *>(_highlighted))) {
				source = e->get_source();
				target = e->get_target();

				if (source == _virtual_node || target == _virtual_node) {
					source = NULL;
					target = NULL;
				}

				_delete(e);

				if (source && target) {
					_new<VirtualEdge>(source, target);

					_invalidate();
				}
			}

			_highlighted = NULL;
		} else if (!_selected.empty()) {
			for (iter = _selected.begin(); iter != _selected.end();) {
				prev = iter++;

				_delete(*prev);

				_calculate_position();
			}

			_selected.clear();
		} else {
			return false;
		}

		_invalidate();
	} else if (event->keyval == GDK_Return && _selected.size() == 1) {
		n = *_selected.begin();

		_start_editing(*n);
	} else if (event->keyval == GDK_f) {
		_source_flip = !_source_flip;
		_target_flip = !_target_flip;

		_source_flipped_signal.emit(_source_flip);
		_target_flipped_signal.emit(_target_flip);

		_calculate_position();
		_invalidate();
	} else if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) {
		if (event->keyval == GDK_g && (n = _group_selected())) {
			_clear_selected();

			n->select();
			_selected.push_back(n);

			_invalidate();
		} else if (event->keyval == GDK_e) {
			if (_selected.size() == 1) {
				n = *_selected.begin();

				if (n->get_type() == LanguageModel::SOURCE) {
					e = _new<DraggableEdge>(n, NULL);
					e->drag(DraggableEdge::TARGET, 0.0, 0.0);
				} else {
					e = _new<DraggableEdge>(NULL, n);
					e->drag(DraggableEdge::SOURCE, 0.0, 0.0);
				}

				_dragging_edge = e;

				_invalidate();
			}
		} else if (event->keyval == GDK_l) {
			_invalidate();
		} else if (event->keyval == GDK_c) {
			n = _collapse();
			_clear_selected();
			if (n) {
				n->select();
				_selected.push_back(n);
			}

			_calculate_position();
			_invalidate();
		} else if (event->keyval == GDK_p) {
			_expand();
			_clear_selected();

			_calculate_position();
			_invalidate();
		} else {
			return false;
		}
	} else if ((event->state & GDK_MOD1_MASK) == GDK_MOD1_MASK) {
		if (event->keyval == GDK_e) {
			if (_selected.size() == 1) {
				n = *_selected.begin();

				e = _new<TreeEdge>(NULL, n);
				e->drag(DraggableEdge::SOURCE, 0.0, 0.0);

				_dragging_edge = e;

				_invalidate();
			}
		} else {
			return false;
		}
	} else {
		return false;
	}

	return true;
}

void TreeModel::on_correspondence_visibility_toggled(void)
{
	_correspondence_visibility_toggled_signal.emit(_is_show_correspondences->get_active());
}

bool TreeModel::on_entry_key_release_event(GdkEventKey *event)
{
	Glib::ustring response;
	std::list<Node *>::iterator iter;
	gint pos = 0;
	Node *node = NULL;

	if (!_editing_node) {
		return false;
	}

	if (event->keyval == GDK_Return) {
		response = _editor->get_text();

		if (_editing_node->get_text().compare(response) != 0) {
			_editing_node->set_text(response);

			_invalidate();
		}

		_stop_editing();

		return true;
	} else if (event->keyval == GDK_Escape) {
		_stop_editing();

		return true;
	}

	if (!_editing_node->get_nonterminal().is_leaf()) {
		return true;
	}
	if (event->keyval == GDK_Left && _editor->get_position() == 0) {
		iter = get_previous_leaf(*_editing_node);
		if (iter != _source.end() && iter != _target.end()) {
			node = *iter;
		}

		if (node) {
			_stop_editing();
			_start_editing(*node, node->get_text().size());
		}
	} else if (event->keyval == GDK_Right &&
	           _editor->get_position() == _editor->get_text_length()) {
		iter = get_next_leaf(*_editing_node);
		if (iter != _source.end() && iter != _target.end()) {
			node = *iter;
		}

		if (node) {
			_stop_editing();
			_start_editing(*node);
		}
	} else if (event->keyval == GDK_BackSpace && _editor->get_position() == 0) {
		pos = _editing_node->get_text().size();
		iter = _merge(*_editing_node, true);

		if (iter != _source.end() && iter != _target.end()) {
			_stop_editing();
			_start_editing(**iter, (*iter)->get_text().size() - pos);
		}

		_invalidate();
	} else if (event->keyval == GDK_Delete &&
	           _editor->get_position() == _editor->get_text_length()) {
		pos = _editing_node->get_text().size();
		iter = _merge(*_editing_node, false);

		if (iter != _source.end() && iter != _target.end()) {
			_stop_editing();
			_start_editing(**iter, pos);
		}

		_invalidate();
	} else if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK &&
	           event->keyval == GDK_s) {
		_editing_node->set_text(_editor->get_text());

		iter = _split(*_editing_node, _editor->get_position());

		_calculate_position();

		if (iter != _source.end() && iter != _target.end()) {
			_stop_editing();
			_start_editing(**iter);
		}

		_invalidate();
	} else {
		return true;
	}

	return true;
}

void TreeModel::on_tree_visibility_toggled(void)
{
	_tree_visibility_toggled_signal.emit(_is_show_trees->get_active());
}

void TreeModel::on_bracket_visibility_toggled(void)
{
	_bracket_visibility_toggled_signal.emit(_is_show_brackets->get_active());
}

void TreeModel::show_opacity_adjustment(void)
{
	_opacity_adjustment_selected_signal.emit();
}

void TreeModel::show_level_spacing_dialog(void)
{
	gdouble original;
	gint response;

	g_assert(_level_spacing_dialog);

	original = (*_branches.begin())->get_level_spacing();
	_level_spacing_spinbutton->set_value(original);
	response = _level_spacing_dialog->run();

	if (response == Gtk::RESPONSE_CANCEL) {
		_level_spacing_spinbutton->set_value(original);
	}

	_level_spacing_dialog->hide();
}

void TreeModel::on_level_spacing_value_changed(void)
{
	_level_spacing_changed_signal.emit(_level_spacing_spinbutton->get_value());

	_invalidate();
}

void TreeModel::on_tree_edge_cornered_toggled(void)
{
	_tree_edge_cornered_signal.emit(_is_tree_edge_cornered->get_active());

	_invalidate();
}

void TreeModel::on_node_position_changed(void)
{
	_node_position_changed_signal.emit((Node::position_t)_position_begin->get_current_value());

	_invalidate();
}

void TreeModel::_calculate_position(bool center)
{
	gdouble sbearing, tbearing;
	gdouble x, y;
	gdouble dx, dy;
	guint width = 0;
	guint height = 0;
	guint highest = 0;
	guint lowest = 0;
	gdouble twidest = 0.0;
	Leaf *leaf = NULL;
	std::list<DraggableEdge *>::iterator eiter;
	std::list<Edge *>::iterator piter;
	std::list<Node *>::iterator niter;

	Gtk::Allocation allocation = get_allocation();
	width = allocation.get_width();
	height = allocation.get_height();

	// Calculate left and right bearings to center the source sentence.
	sbearing = 0;
	y = -(_SENTENCE_SPACING / 2) * _vscale;
	for (niter = _source.begin(); niter != _source.end(); niter++) {
		if (!(*niter)->is_visible()) {
			continue;
		}

		sbearing += ((*niter)->get_extents_width() +
		             (*niter)->get_spacing_before() +
		             (*niter)->get_spacing_after()) * _hscale;

		highest = guint(ceil(fabs((y - (*niter)->get_extents_height()) *
		                          _vscale)));
	}

	// Calculate left and right bearings to center the target sentence.
	tbearing = 0;
	y = (_SENTENCE_SPACING / 2) * _vscale *
		(1 - _target_rotation / (M_PI / 2));
	for (niter = _target.begin(); niter != _target.end(); niter++) {
		if (!(*niter)->is_visible()) {
			continue;
		}

		tbearing += ((*niter)->get_extents_width() +
		             (*niter)->get_spacing_before()) * _hscale;

		if ((*niter)->get_extents_width() > twidest) {
			twidest = (*niter)->get_extents_width();
		}

		if (y > lowest) {
			lowest = guint(ceil(fabs((y + (*niter)->get_extents_height()) *
			                         _vscale)));
		}

		dy = ((*niter)->get_extents_height() + (*niter)->get_spacing_before() +
		      (*niter)->get_spacing_after()) * _vscale;
		y += dy * sin(_target_rotation);
	}

	sbearing += twidest * _hscale * (_target_rotation / (M_PI / 2));
	if (sbearing > width) {
		width = guint(sbearing + _WORD_SPACING * _hscale);
		sbearing = _WORD_SPACING * _hscale;
	} else {
		sbearing = (width - (sbearing + _WORD_SPACING * _hscale)) / 2;
	}
	sbearing += twidest * _hscale * (_target_rotation / (M_PI / 2));

	if (tbearing > width) {
		width = guint(tbearing + _WORD_SPACING * _hscale);
		tbearing = _WORD_SPACING * _hscale;
	} else {
		tbearing = (width - (tbearing + _WORD_SPACING * _hscale)) / 2;
	}
	tbearing = tbearing * (1 - _target_rotation / (M_PI / 2)) +
		(sbearing - twidest * _hscale) * (_target_rotation / (M_PI / 2));

	// Set coordinates.
	x = -(width / 2 - sbearing) / _hscale;
	y = -(_SENTENCE_SPACING / 2) * _vscale * (1 - _target_rotation / (M_PI / 2));
	for (niter = _source.begin(); niter != _source.end(); niter++) {
		if (!(*niter)->is_visible()) {
			continue;
		}

		leaf = dynamic_cast<Leaf *>(*niter);
		x += leaf->get_spacing_before();

		leaf->set_coordinates(x, y - leaf->get_extents_height());

		x += leaf->get_extents_width() + leaf->get_spacing_after();
	}

	x = -(width / 2 - tbearing) / _hscale;
	y = (_SENTENCE_SPACING / 2) * _vscale * (1 - _target_rotation / (M_PI / 2));
	for (niter = _target.begin(); niter != _target.end(); niter++) {
		if (!(*niter)->is_visible()) {
			continue;
		}

		leaf = dynamic_cast<Leaf *>(*niter);
		x += leaf->get_spacing_before() * cos(_target_rotation);
		y += leaf->get_spacing_before() * sin(_target_rotation);

		leaf->set_coordinates(x, y);

		dx = leaf->get_extents_width() + leaf->get_spacing_after();
		dy = leaf->get_extents_height() + leaf->get_spacing_after();
		x += dx * cos(_target_rotation);
		y += dy * sin(_target_rotation);
	}

	_source_flipped_signal.emit(false);
	_target_flipped_signal.emit(false);
	for (niter = _branches.begin(); niter != _branches.end(); niter++) {
		if (!(*niter)->is_visible()) {
			continue;
		}

		y = (*niter)->get_extents_y();

		if ((*niter)->get_type() == LanguageModel::SOURCE) {
			y = y * _vscale;
		} else {
			y = (y + (*niter)->get_extents_height()) * _vscale;
		}

		if (y < 0 && -y > highest) {
			highest = guint(ceil(-y));
		} else if (y > 0 && y > lowest) {
			lowest = guint(ceil(y));
		}
	}

	// Set coordinates.
	if (_source_flip) {
		x = -(width / 2 - sbearing) / _hscale;
		y = -gdouble(highest) * (1 - _target_rotation / (M_PI / 2)) / _vscale;
		for (niter = _source.begin(); niter != _source.end(); niter++) {
			if (!(*niter)->is_visible()) {
				continue;
			}

			leaf = dynamic_cast<Leaf *>(*niter);
			x += leaf->get_spacing_before();

			leaf->set_coordinates(x, y - leaf->get_extents_height() *
			                      _target_rotation / (M_PI / 2));

			x += leaf->get_extents_width() + leaf->get_spacing_after();
		}
	}

	if (_target_flip) {
		x = -(width / 2 - tbearing) / _hscale;
		y = gdouble(lowest) * (1 - _target_rotation / (M_PI / 2)) / _vscale;
		for (niter = _target.begin(); niter != _target.end(); niter++) {
			if (!(*niter)->is_visible()) {
				continue;
			}

			leaf = dynamic_cast<Leaf *>(*niter);
			x += leaf->get_spacing_before() * cos(_target_rotation);
			y += leaf->get_spacing_before() * sin(_target_rotation);

			leaf->set_coordinates(x, y - leaf->get_extents_height() *
			                      (1 - _target_rotation / (M_PI / 2)));

			dx = leaf->get_extents_width() + leaf->get_spacing_after();
			dy = leaf->get_extents_height() + leaf->get_spacing_after();
			x += dx * cos(_target_rotation);
			y += dy * sin(_target_rotation);
		}
	}
	_source_flipped_signal.emit(_source_flip);
	_target_flipped_signal.emit(_target_flip);

	if (center) {
		_xc = width / 2;
		if (height < (lowest + highest)) {
			height = lowest + highest;
			_yc = highest;
		} else if ((height / 2) < highest) {
			_yc = highest;
		} else if ((height / 2) < lowest) {
			_yc = height - lowest;
		} else {
			_yc = height / 2;
		}
	}
}

void TreeModel::_set_target_rotation(gdouble rotation)
{
	if (fabs(rotation - _target_rotation) < 0.001) {
		return;
	}

	_target_rotation = rotation;

	_calculate_position();
}

void TreeModel::_on_friend_changed(NonTerminal *s, NonTerminal *t, bool val)
{
	Node *source = s->get_node();
	Node *target = t->get_node();
	std::list<DraggableEdge *>::iterator iter;

	if (source->get_type() == LanguageModel::TARGET) {
		std::swap(source, target);
	}

	for (iter = _edges.begin(); iter != _edges.end(); iter++) {
		if (source == (*iter)->get_source() && target == (*iter)->get_target()) {
			break;
		}
	}

	if (val) {
		if (iter == _edges.end()) {
			_new<DraggableEdge>(source, target);
		}
	} else {
		if (iter != _edges.end()) {
			_delete(*iter);
		}
	}
}

void TreeModel::_on_parent_changed(NonTerminal *s, NonTerminal *t, bool val)
{
	Node *source = t->get_node();
	Node *target = s->get_node();
	TreeEdge *edge = NULL;
	std::list<DraggableEdge *>::iterator iter;

	for (iter = _edges.begin(); iter != _edges.end(); iter++) {
		if (source == (*iter)->get_source() && target == (*iter)->get_target()) {
			break;
		}
	}

	if (val) {
		if (iter == _edges.end()) {
			edge = _new<TreeEdge>(source, target);
			_tree_visibility_toggled_signal.connect(sigc::mem_fun(*(Drawable *)edge, &Drawable::toggle_visibility));
		}
	} else {
		if (iter != _edges.end()) {
			_delete(*iter);
		}
	}
}

void TreeModel::_on_child_changed(NonTerminal *s, NonTerminal *t, bool val)
{
	Node *source = s->get_node();
	Node *target = t->get_node();
	TreeEdge *edge = NULL;
	std::list<DraggableEdge *>::iterator iter;

	for (iter = _edges.begin(); iter != _edges.end(); iter++) {
		if (source == (*iter)->get_source() && target == (*iter)->get_target()) {
			break;
		}
	}

	if (val) {
		if (iter == _edges.end()) {
			edge = _new<TreeEdge>(source, target);
			_tree_visibility_toggled_signal.connect(sigc::mem_fun(*(Drawable *)edge, &Drawable::toggle_visibility));
		}
	} else {
		if (iter != _edges.end()) {
			_delete(*iter);
		}
	}
}

void TreeModel::_new_transformable_common(Transformable *transformable)
{
	transformable->change_source_angle(_source_rotation);
	transformable->change_target_angle(_target_rotation);

	transformable->get_dirty_signal().connect(sigc::mem_fun<Drawable *, bool>(*this, &TreeModel::_invalidate));
	_source_angle_changed_signal.connect(sigc::mem_fun(*transformable, &Transformable::change_source_angle));
	_target_angle_changed_signal.connect(sigc::mem_fun(*transformable, &Transformable::change_target_angle));
}

void TreeModel::_new_edge_common(DraggableEdge *edge, Node *source, Node *target)
{
	edge->change_source(source);
	edge->change_target(target);
	edge->set_opacity_weight(_opacity);
	_edges.push_back(edge);

	_opacity_changed_signal.connect(sigc::mem_fun(*edge, &DraggableEdge::set_opacity_weight));

	_new_transformable_common(edge);
}

void TreeModel::_new_virtual_edges(Node *leaf)
{
	std::list<Node *> *l = NULL;
	std::list<Node *>::iterator iter;

	if (!leaf) {
		return;
	}

	if (leaf->get_type() == LanguageModel::SOURCE) {
		l = &_target;
	} else {
		l = &_source;
	}

	for (iter = l->begin(); iter != l->end(); iter++) {
		if (!leaf->get_nonterminal().is_friend(**iter)) {
			if (leaf->get_type() == LanguageModel::SOURCE) {
				_new<VirtualEdge>(leaf, *iter);
			} else {
				_new<VirtualEdge>(*iter, leaf);
			}
		}
	}
}

Node *TreeModel::_new_virtual_node(Node *node)
{
	std::list<DraggableEdge *> edges;
	std::list<TreeEdge *> tree_edges;
	std::list<DraggableEdge *>::iterator iter;
	std::list<TreeEdge *>::iterator titer;
	Color color;

	if (!node || _virtual_node) {
		return NULL;
	}

	color = node->get_color();
	color.set("red");
	_virtual_node = _new(node->get_type());
	_virtual_node->set_text(node->get_text());
	_virtual_node->set_color(color);
	_virtual_node->set_coordinates(node->get_extents_x(),
	                               node->get_extents_y() -
	                               node->get_extents_height() - 5);

	edges = node->get_edges();
	tree_edges = node->get_tree_edges();

	for (iter = edges.begin(); iter != edges.end(); iter++) {
		if ((*iter)->get_source() == node) {
			if (!(*iter)->change_source(_virtual_node)) {
				_delete(*iter);
			}
		} else {
			if (!(*iter)->change_target(_virtual_node)) {
				_delete(*iter);
			}
		}
	}

	for (titer = tree_edges.begin(); titer != tree_edges.end(); titer++) {
		if ((*titer)->get_source() == node) {
			if (!(*titer)->change_source(_virtual_node)) {
				_delete(*titer);
			}
		} else {
			if (!(*titer)->change_target(_virtual_node)) {
				_delete(*titer);
			}
		}
	}

	return _virtual_node;
}

Node *TreeModel::_new(LanguageModel::language_model_type_t type)
{
	LanguageModel *language_model = NULL;
	NonTerminal *nonterminal = NULL;
	Node *branch = NULL;

	if (!_alignment) {
		return NULL;
	}

	if (type == LanguageModel::SOURCE) {
		language_model = &_alignment->get_source();
	} else {
		language_model = &_alignment->get_target();
	}

	nonterminal = new NonTerminal(*language_model);
	language_model->add(nonterminal);

	branch = new Node(*this, *nonterminal);

	nonterminal->get_friend_changed_signal().connect(sigc::mem_fun(this, &TreeModel::_on_friend_changed));
	nonterminal->get_parent_changed_signal().connect(sigc::mem_fun(this, &TreeModel::_on_parent_changed));
	nonterminal->get_child_changed_signal().connect(sigc::mem_fun(this, &TreeModel::_on_child_changed));
	_tree_visibility_toggled_signal.connect(sigc::mem_fun(*(Drawable *)branch, &Drawable::toggle_visibility));
	_bracket_visibility_toggled_signal.connect(sigc::mem_fun(*branch, &Node::set_mono_visibility));
	_level_spacing_changed_signal.connect(sigc::mem_fun(*branch, &Node::set_level_spacing));
	_node_position_changed_signal.connect(sigc::mem_fun(*branch, &Node::set_position));
	_new_transformable_common(branch);

	if (type == LanguageModel::SOURCE) {
		branch->set_flipped(_source_flip);
		_source_flipped_signal.connect(sigc::mem_fun(*branch, &Node::set_flipped));
	} else {
		branch->set_flipped(_target_flip);
		_target_flipped_signal.connect(sigc::mem_fun(*branch, &Node::set_flipped));
	}

	branch->set_level_spacing(_level_spacing_spinbutton->get_value_as_int());
	branch->change_source_angle(_source_rotation);
	branch->change_target_angle(_target_rotation);

	return branch;
}

template <typename T>
T *TreeModel::_new(Node *source, Node *target)
{
	T *e = NULL;

	e = new T(*this);
	_new_edge_common(e, source, target);

	return e;
}

Edge *TreeModel::_new(const Node *node)
{
	const TreeEdge *const_edge = NULL;
	Edge *permutation = new Edge(*this);

	if (!node) {
		return NULL;
	}

	const_edge = node->get_leftmost_tree_edge();
	if (const_edge) {
		const_edge->get_changed_signal().connect(sigc::mem_fun<Drawable *, bool>(*permutation, &Edge::set_start));
	}

	const_edge = node->get_rightmost_tree_edge();
	if (const_edge) {
		const_edge->get_changed_signal().connect(sigc::mem_fun<Drawable *, bool>(*permutation, &Edge::set_end));
	}

	_permutations.push_back(permutation);

	return permutation;
}

void TreeModel::_delete_virtual_edges(Node *leaf)
{
	std::list<DraggableEdge *>::iterator prev, iter;

	for (iter = _edges.begin(); iter != _edges.end();) {
		prev = iter++;

		if (((*prev)->get_source() == leaf || (*prev)->get_target() == leaf) &&
		    dynamic_cast<VirtualEdge *>(*prev)) {
			_delete(*prev);
		}
	}
}

void TreeModel::_delete(std::list<Node *> &l, std::list<Node *>::iterator iter)
{
	std::list<DraggableEdge *> edges;
	std::list<TreeEdge *> tree_edges;
	std::list<DraggableEdge *>::iterator eiter;
	std::list<TreeEdge *>::iterator titer;
	std::list<Node *>::iterator niter;
	Node *node = *iter;

	l.erase(iter);

	edges = node->get_edges();
	tree_edges = node->get_tree_edges();
	NonTerminal &nonterminal = node->get_nonterminal();

	niter = std::find(_selected.begin(), _selected.end(), node);
	if (niter != _selected.end()) {
		_selected.erase(niter);
	}

	nonterminal.set_node(NULL);

	for (eiter = edges.begin(); eiter != edges.end(); eiter++) {
		_delete(*eiter);
	}

	for (titer = tree_edges.begin(); titer != tree_edges.end(); titer++) {
			_delete(*titer);
	}

	_delete_virtual_edges(node);

	if (node->get_type() == LanguageModel::SOURCE) {
		_alignment->get_source().remove(&nonterminal);
	} else {
		_alignment->get_target().remove(&nonterminal);
	}

	delete node;
}

void TreeModel::_delete(Node *node)
{
	std::list<Node *> *l = NULL;
	std::list<Node *>::iterator iter;

	if (!node) {
		return;
	}

	if (node->get_nonterminal().is_leaf()) {
		if (node->get_type() == LanguageModel::SOURCE) {
			l = &_source;
		} else {
			l = &_target;
		}
	} else {
		l = &_branches;
	}

	iter = std::find(l->begin(), l->end(), node);
	if (iter != l->end()) {
		_delete(*l, iter);
	}
}

void TreeModel::_delete(Edge *edge)
{
	std::list<Edge *>::iterator iter;

	if (!edge) {
		return;
	}

	if ((iter = std::find(_permutations.begin(), _permutations.end(), edge)) !=
	    _permutations.end()) {
		_permutations.erase(iter);
	}

	delete edge;
}

void TreeModel::_delete(DraggableEdge *edge) {
	std::list<DraggableEdge *>::iterator iter;
	Node *source = edge->get_source();
	Node *target = edge->get_target();

	if (!edge) {
		return;
	}

	edge->delete_from_nodes();

	if ((source == _virtual_node || target == _virtual_node) &&
	    _virtual_node->get_edges().empty() &&
	    _virtual_node->get_tree_edges().empty()) {
		_delete(_virtual_node);
		_virtual_node = NULL;
	}

	if ((iter = std::find(_edges.begin(), _edges.end(), edge)) != _edges.end()) {
		_edges.erase(iter);
	}

	if (_highlighted == edge) {
		_highlighted = NULL;
	}
	if (_dragging_edge == edge) {
		_dragging_edge = NULL;
	}

	delete edge;
}

template <typename T>
void TreeModel::_delete_and_clear(T &list)
{
	typename T::iterator iter;

	for (iter = list.begin(); iter != list.end(); iter++) {
		delete *iter;
	}

	list.clear();
}

void TreeModel::_clear_selected(void)
{
	std::list<Node *>::iterator iter;

	if (_selected.empty()) {
		return;
	}

	for (iter = _selected.begin(); iter != _selected.end(); iter++) {
		(*iter)->deselect();
	}

	_selected.clear();
}

Node *TreeModel::_group_selected(void)
{
	Node *branch = NULL;
	std::list<Node *>::iterator iter;
	LanguageModel::language_model_type_t type;
	std::pair<gdouble, gdouble> anchor;

	type = (*_selected.begin())->get_type();
	branch = _new(type);
	if (!branch) {
		return NULL;
	}

	for (iter = _selected.begin(); iter != _selected.end(); iter++) {
		if ((*iter)->get_type() != type) {
			break;
		}


		branch->get_nonterminal().add_child(**iter);
		(*iter)->get_nonterminal().set_parent(branch);
	}

	if (iter == _selected.begin()) {
		_delete(branch);

		return NULL;
	}

	if (branch->get_nonterminal().get_permutability()) {
		_new(branch);
	}

	_branches.push_back(branch);

	return branch;
}

Node *TreeModel::_collapse(void)
{
	Node *node = *_selected.begin();
	std::list<Node *>::iterator iter;
	std::list<NonTerminal *>::iterator i;
	LanguageModel::language_model_type_t type;

	if (_selected.size() < 2) {
		std::list<NonTerminal *> l;
		node->get_nonterminal().get_descendants(l);

		if (l.empty()) {
			return NULL;
		}

		for (i = l.begin(); i != l.end(); i++) {
			_selected.push_back((*i)->get_node());
		}
	}

	type = node->get_type();

	for (iter = _selected.begin(); iter != _selected.end(); iter++) {
		if ((*iter)->get_type() != type || (*iter)->get_collapsed()) {
			return NULL;
		}

		if ((*iter)->get_nonterminal().get_height() >
		    node->get_nonterminal().get_height()) {
			node = *iter;
		}
	}

	for (iter = _selected.begin(); iter != _selected.end(); iter++) {
		(*iter)->set_collapsed(node);

		if (*iter != node) {
			node->add_collapsed_node(**iter);
		}
	}

	return node;
}

void TreeModel::_expand(void)
{
	std::list<Node *>::iterator iter;
	std::list<Node *>::const_iterator citer;

	for (iter = _selected.begin(); iter != _selected.end(); iter++) {
		if ((*iter)->get_collapsed()) {
			for (citer = (*iter)->get_collapsed_nodes().begin();
			     citer != (*iter)->get_collapsed_nodes().end(); citer++) {
				(*citer)->set_collapsed(NULL);
			}

			(*iter)->set_collapsed(NULL);
		}
	}
}

Node *TreeModel::_add_to_group(Node &branch, std::list<Node *> &l)
{
	TreeEdge *edge = NULL;
	std::list<Node *>::iterator iter;
	LanguageModel::language_model_type_t type;
	std::pair<gdouble, gdouble> anchor;

	type = (*l.begin())->get_type();

	for (iter = l.begin(); iter != l.end(); iter++) {
		if ((*iter)->get_type() != type) {
			break;
		}

		edge = _new<TreeEdge>(&branch, *iter);
		_tree_visibility_toggled_signal.connect(sigc::mem_fun(*(Drawable *)edge, &Drawable::toggle_visibility));
	}

	return &branch;
}

void TreeModel::_add_source(void)
{
	std::list<Terminal *> &terminals = _alignment->get_source().get_terminals();
	std::list<Terminal *>::iterator iter;

	for (iter = terminals.begin(); iter != terminals.end(); iter++) {
		_add_terminal(**iter, true);
	}
}

void TreeModel::_add_target(void)
{
	std::list<Terminal *> &terminals = _alignment->get_target().get_terminals();
	std::list<Terminal *>::iterator iter;

	for (iter = terminals.begin(); iter != terminals.end(); iter++) {
		_add_terminal(**iter, false);
	}
}

void TreeModel::_add_terminal(Terminal &terminal, bool source)
{
	Leaf *v;

	if (source) {
		v = new Leaf(*this, terminal);

		_source.push_back(v);
	} else {
		v = new Leaf(*this, terminal);

		_target.push_back(v);
	}

	terminal.get_friend_changed_signal().connect(sigc::mem_fun(this, &TreeModel::_on_friend_changed));
	terminal.get_parent_changed_signal().connect(sigc::mem_fun(this, &TreeModel::_on_parent_changed));

	_new_transformable_common(v);
}

void TreeModel::_add_edges(void)
{
	const alignment_t *source = NULL;
	const alignment_t *target = NULL;
	Node *s = NULL;
	Node *t = NULL;
	alignment_t::const_iterator iter;
	std::list<Node *>::iterator i;

	_alignment->get_alignment(&source, &target);

	for (iter = source->begin(); iter != source->end(); iter++) {
		if (iter->first < 0 || iter->second < 0) {
			continue;
		}

		s = list_at(_source, iter->second);
		t = list_at(_target, iter->first);

		if (s && t) {
			s->get_nonterminal().add_friend(*t);
			t->get_nonterminal().add_friend(*s);
		}
	}

	for (iter = target->begin(); iter != target->end(); iter++) {
		if (iter->first < 0 || iter->second < 0) {
			continue;
		}

		s = list_at(_source, iter->second);
		t = list_at(_target, iter->first);

		if (s && t) {
			s->get_nonterminal().add_friend(*t);
			t->get_nonterminal().add_friend(*s);
		}
	}

	for (i = _source.begin(); i != _source.end(); i++) {
		_new_virtual_edges(*i);
	}
}

std::list<Node *>::iterator
TreeModel::_merge(std::list<Node *>::iterator first,
                  std::list<Node *>::iterator second)
{
	Node *node = *second;
	std::list<DraggableEdge *> edges;
	std::list<TreeEdge *> tree_edges;
	std::list<DraggableEdge *>::iterator iter;
	std::list<TreeEdge *>::iterator titer;

	if (!(*first)->get_nonterminal().is_leaf() ||
	    !node->get_nonterminal().is_leaf()) {
		// Raise an exception.
	}

	edges = node->get_edges();
	tree_edges = node->get_tree_edges();

	for (iter = edges.begin(); iter != edges.end(); iter++) {
		if ((*iter)->get_source() == node) {
			if (!(*iter)->change_source(*first)) {
				_delete(*iter);
			}
		} else {
			if (!(*iter)->change_target(*first)) {
				_delete(*iter);
			}
		}
	}

	for (titer = tree_edges.begin(); titer != tree_edges.end(); titer++) {
		if ((*titer)->get_source() == node) {
			if (!(*titer)->change_source(*first)) {
				_delete(*titer);
			}
		} else {
			if (!(*titer)->change_target(*first)) {
				_delete(*titer);
			}
		}
	}

	_delete_virtual_edges(node);

	(*first)->set_text((*first)->get_text().append(node->get_text()));

	_delete(node);

	return first;
}

std::list<Node *>::iterator TreeModel::_merge(Node &node, bool before)
{
	std::list<Node *>::iterator first, second;

	if (node.get_type() == LanguageModel::SOURCE) {
		if (!_new_virtual_node(&node)) {
			return _source.end();
		}

		first = std::find(_source.begin(), _source.end(), &node);

		if (first == _source.end()) {
			return first;
		}
	} else {
		if (!_new_virtual_node(&node)) {
			return _target.end();
		}

		first = std::find(_target.begin(), _target.end(), &node);

		if (first == _target.end()) {
			return first;
		}
	}

	if (before) {
		second = first;
		first = get_previous_leaf(node);
	} else {
		second = get_next_leaf(node);
	}

	return _merge(first, second);
}

std::list<Node *>::iterator TreeModel::_split(Node &node, gint pos)
{
	std::list<Node *> *l = NULL;
	std::list<Node *>::iterator next;
	Glib::ustring text = node.get_text();
	LanguageModel *lm = NULL;
	Terminal *term = NULL;
	Leaf *leaf = NULL;

	if (node.get_type() == LanguageModel::SOURCE) {
		lm = &(_alignment->get_source());
		l = &_source;
	} else {
		lm = &(_alignment->get_target());
		l = &_target;
	}

	if (!node.get_nonterminal().is_leaf() || !_new_virtual_node(&node)) {
		return l->end();
	}

	next = std::find(l->begin(), l->end(), &node);
	if (next == l->end()) {
		return next;
	}

	(*next)->set_text(text.substr(pos));

	term = new Terminal(node.get_nonterminal().get_language_model(),
	                    text.substr(0, pos));
	lm->add(term, (Terminal *)&((*next)->get_nonterminal()));

	leaf = new Leaf(*this, *term);
	_new_transformable_common(leaf);
	_new_virtual_edges(leaf);

	return ++(l->insert(next, leaf));
}

void TreeModel::_invalidate(void)
{
	guint w, h;
	gdouble width, height;

	get_size(w, h);
	width = gdouble(w);
	height = gdouble(h);

	if (_editing_node) {
		move(*_editor, gint(_c2d_x(_editing_node->get_extents_x())),
		     gint(_c2d_y(_editing_node->get_extents_y())));
	}

	if (width < abs(get_allocation().get_width())) {
		width = abs(get_allocation().get_width());
	}
	if (height < abs(get_allocation().get_height())) {
		height = abs(get_allocation().get_height());
	}

	_invalidate(Gdk::Rectangle(0, 0, gint(ceil(width)),
	                           gint(ceil(height))), false);
}

void TreeModel::_invalidate(Drawable *v, bool immediate)
{
	if (!v) {
		return;
	}

	if (v == _editing_node) {
		move(*_editor, gint(_c2d_x(_editing_node->get_extents_x())),
		     gint(_c2d_y(_editing_node->get_extents_y())));
	}

	_invalidate(v->get_rect(), immediate);
}

void TreeModel::_invalidate(const Gdk::Rectangle &rect, bool immediate)
{
	Glib::RefPtr<Gdk::Window> window = get_bin_window();
	if (window) {
		window->invalidate_rect(rect, false);

		if (immediate) {
			window->process_updates(false);
		}
	}
}

void TreeModel::_change_edge(DraggableEdge &edge, gdouble x, gdouble y)
{
	Node *source = edge.get_source();
	Node *target = edge.get_target();
	Node *node = NULL;
	bool result = false;
	std::list<Node *>::iterator iter;
	bool is_tree_edge = false;
	DraggableEdge::drag_type_t drag = edge.get_drag();

	if (dynamic_cast<TreeEdge *>(&edge)) {
		is_tree_edge = true;
	}

	if ((iter = _find(_source, x, y)) != _source.end()) {
		node = *iter;
	} else if ((iter = _find(_target, x, y)) != _target.end()) {
		node = *iter;
	} else if ((iter = _find(_branches, x, y)) != _branches.end()) {
		node = *iter;
	}

	if (node && source && target) {
		if (is_tree_edge) {
			source->get_nonterminal().delete_child(*target);
		} else {
			source->get_nonterminal().delete_friend(*target);
			target->get_nonterminal().delete_friend(*source);
		}
	}

	if (drag == DraggableEdge::SOURCE) {
		if (node && target) {
			if (is_tree_edge) {
				result = target->get_nonterminal().set_parent(node);

				if (result) {
					result = node->get_nonterminal().add_child(*target);
				} else {
					std::cout << "oops 1" << std::endl;
				}
			} else {
				result = target->get_nonterminal().add_friend(*node);

				if (result) {
					result = node->get_nonterminal().add_friend(*target);
				} else {
					std::cout << "oops 2" << std::endl;
				}
			}
		}
	} else if (drag == DraggableEdge::TARGET) {
		if (node && source) {
			if (is_tree_edge) {
				result = source->get_nonterminal().add_child(*node);

				if (result) {
					result = node->get_nonterminal().set_parent(source);
				} else {
					std::cout << "oops 3" << std::endl;
				}
			} else {
				result = source->get_nonterminal().add_friend(*node);

				if (result) {
					result = node->get_nonterminal().add_friend(*source);
				} else {
					std::cout << "oops 4" << std::endl;
				}
			}
		}
	}

	if (!result && !edge.restore()) {
		std::cout << "fuck" << std::endl;
		return;
	}

	if (source && target &&
	    (source == _virtual_node || target == _virtual_node) &&
	    _virtual_node->get_edges().empty() &&
	    _virtual_node->get_tree_edges().empty()) {
		_delete(_virtual_node);
		_virtual_node = NULL;
	}
}

template <typename T>
typename std::list<T>::iterator TreeModel::_find(std::list<T> &obj,
                                                 gdouble x, gdouble y) const
{
	typename std::list<T>::iterator iter;

	for (iter = obj.begin(); iter != obj.end(); iter++) {
		if ((*iter)->is_in_region(_cr, x, y)) {
			return iter;
		}
	}

	return obj.end();
}

std::list<Node *>::iterator TreeModel::_find_x(std::list<Node *> &obj,
                                               gdouble x) const
{
	std::list<Node *>::iterator iter;

	for (iter = obj.begin(); iter != obj.end(); iter++) {
		if ((*iter)->is_x_in_region(_cr, x)) {
			return iter;
		}
	}

	return obj.end();
}

std::list<Node *>::iterator TreeModel::_find_y(std::list<Node *> &obj,
                                               gdouble y) const
{
	std::list<Node *>::iterator iter;

	for (iter = obj.begin(); iter != obj.end(); iter++) {
		if ((*iter)->is_y_in_region(_cr, y)) {
			return iter;
		}
	}

	return obj.end();
}

std::list<Node *>::iterator TreeModel::_find(std::list<Node *> &obj,
                                             gdouble x, gdouble y,
                                             const Node &node) const
{
	std::list<Node *>::iterator iter;

	for (iter = obj.begin(); iter != obj.end(); iter++) {
		if (!node.get_nonterminal().is_self(**iter) &&
		    (*iter)->is_in_region(_cr, x, y)) {
			return iter;
		}
	}

	return obj.end();
}

void TreeModel::_start_editing(Node &node, gint pos)
{
	_editing_node = &node;

	_editor->set_text(_editing_node->get_text());
	_editor->set_size_request(gint(_c2d_w(_editing_node->get_extents_width())),
	                          gint(_c2d_h(_editing_node->get_extents_height())));
	_editor->set_has_frame(false);
	_editor->grab_focus();

	if (pos <= _editor->get_text_length()) {
		_editor->set_position(pos);
	}

	move(*_editor, gint(_c2d_x(_editing_node->get_extents_x())),
	     gint(_c2d_y(_editing_node->get_extents_y())));
	_editor->show();
}

void TreeModel::_stop_editing(void)
{
	_editing_node = NULL;
	_editor->hide();

	grab_focus();
}
