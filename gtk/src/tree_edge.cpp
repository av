#include <utility>

#include "tree_edge.hpp"

TreeEdge::TreeEdge(const TreeModel &parent) :
	Drawable(parent),
	DraggableEdge(parent)
{

}

TreeEdge::~TreeEdge(void)
{

}

void TreeEdge::draw(Cairo::RefPtr<Cairo::Context> cr)
{
	gdouble x1, y1, x2, y2;
	gdouble angle = 0;
	gdouble offset = 0;

	if (_source) {
		if (_source->get_type() == LanguageModel::SOURCE) {
			angle = _source_angle;
			offset = 5.0;
		} else {
			angle = _target_angle;
			offset = -5.0;
		}
	} else {
		if (_target->get_type() == LanguageModel::SOURCE) {
			angle = _source_angle;
			offset = 5.0;
		} else {
			angle = _target_angle;
			offset = -5.0;
		}
	}

	if ((_source && !_source->is_visible()) ||
	    (_target && !_target->is_visible())) {
		toggle_visibility(false);
		set_matrix_visibility(false);
	} else {
		toggle_visibility(true);
		set_matrix_visibility(true);
	}

	if (is_visible()) {
		if (_drag == NONE) {
			_set_source_anchor(false);
			_set_target_anchor(false);
		}

		if (get_is_cornered()) {
			cr->save();
			cr->begin_new_path();

			if (angle == 0.0) {
				cr->move_to(get_start().first, get_start().second);
				cr->line_to(get_start().first, get_start().second + offset);
				cr->line_to(get_end().first, get_start().second + offset);
				cr->line_to(get_end().first, get_end().second);
			} else if (angle == M_PI / 2) {
				cr->move_to(get_start().first, get_start().second);
				cr->line_to(get_start().first - offset, get_start().second);
				cr->line_to(get_start().first - offset, get_end().second);
				cr->line_to(get_end().first, get_end().second);
			}

			delete _path;
			_path = cr->copy_path();

			if (_is_highlight) {
				cr->set_line_width(_WIDTH + _HIGHLIGHT_WIDTH);
				cr->set_source(_highlight_color.get_cairo_pattern());
				cr->stroke_preserve();
			}

			cr->set_source(_normal_color.get_cairo_pattern());
			cr->set_line_cap(Cairo::LINE_CAP_ROUND);
			cr->set_line_width(_WIDTH);

			cr->get_stroke_extents(x1, y1, x2, y2);
			_set_width(x2 - x1);
			_set_height(y2 - y1);

			cr->stroke();
			cr->restore();
		} else {
			Edge::draw(cr);
		}
	}
}

std::pair<gdouble, gdouble> TreeEdge::get_source_side_anchor(void) const
{
	gdouble arctangent = fabs((get_start().first - get_end().first) /
	                          (get_start().second - get_end().second));
	gdouble x, y;

	y = 10;
	x = arctangent * y;

	if (get_start().first < get_end().first) {
		x = get_start().first + x;
	} else {
		x = get_start().first - x;
	}
	if (get_start().second < get_end().second) {
		y = get_start().second + y;
	} else {
		y = get_start().second - y;
	}

	return std::make_pair<gdouble, gdouble>(x, y);
}

std::pair<gdouble, gdouble> TreeEdge::get_target_side_anchor(void) const
{
	gdouble arctangent = fabs((get_start().first - get_end().first) /
	                          (get_start().second - get_end().second));
	gdouble x, y;

	y = 10;
	x = arctangent * y;

	if (get_start().first < get_end().first) {
		x = get_end().first - x;
	} else {
		x = get_end().first + x;
	}
	if (get_start().second < get_end().second) {
		y = get_end().second - y;
	} else {
		y = get_end().second + y;
	}

	return std::make_pair<gdouble, gdouble>(x, y);
}

void TreeEdge::set_is_cornered(bool val)
{
	_is_cornered = val;

	_set_dirty();
}

void TreeEdge::set_opacity_weight(gdouble weight)
{

}

bool TreeEdge::change_source(Node *source)
{
	if (_target && source && _target->get_type() != source->get_type()) {
		return false;
	}

	if (!_set_source(source)) {
		return false;
	}

	_drag = NONE;

	return true;
}

bool TreeEdge::change_target(Node *target)
{
	if (_source && target && _source->get_type() != target->get_type()) {
		return false;
	}

	if (!_set_target(target)) {
		return false;
	}

	_drag = NONE;

	return true;
}

void TreeEdge::change_target_angle(gdouble angle)
{
	Transformable::change_target_angle(angle);
}

void TreeEdge::delete_from_nodes(void)
{
	if (_source && _target) {
		_source->delete_edge(*this);
		_target->delete_edge(*this);
	}
}

bool TreeEdge::_add_to_nodes(void)
{
	if (_source && _target) {
		if (!_source->add_edge(*this) || !_target->add_edge(*this)) {
			return false;
		}
	}

	return true;
}

void TreeEdge::_set_source_anchor(bool emit)
{
	gdouble x, y;

	if (!_source || _drag == SOURCE) {
		return;
	}

	if (_source->get_type() == LanguageModel::SOURCE) {
		_source->get_bottom_anchor(x, y);
	} else {
		if (_target_angle < M_PI / 4) {
			_source->get_top_anchor(x, y);
		} else {
			_source->get_right_anchor(x, y);
		}
	}

	set_start(x, y, emit);
}

void TreeEdge::_set_target_anchor(bool emit)
{
	gdouble x, y;

	if (!_target || _drag == TARGET) {
		return;
	}

	if (_target->get_type() == LanguageModel::SOURCE) {
		_target->get_top_anchor(x, y);
	} else {
		if (_target_angle < M_PI / 4) {
			_target->get_bottom_anchor(x, y);
		} else {
			_target->get_left_anchor(x, y);
		}
	}

	set_end(x, y, emit);
}
