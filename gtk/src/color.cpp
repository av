#include "color.hpp"

Color::Color(void) :
	_alpha(1),
	_color(Gdk::Color())
{

}

Color::Color(gushort red, gushort green, gushort blue, gdouble alpha) :
	_alpha(alpha),
	_color(Gdk::Color())
{
	set_rgb(red, green, blue);
}

Color::Color(gdouble red, gdouble green, gdouble blue, gdouble alpha) :
	_alpha(alpha),
	_color(Gdk::Color())
{
	set_rgb_p(red, green, blue);
}

Color::Color(const Glib::ustring &value) :
	_alpha(1),
	_color(Gdk::Color(value))
{
	_set_cairo_pattern();
}

Color::Color(const Color &other) :
	_alpha(other._alpha),
	_color(Gdk::Color(other._color)),
	_pattern(other._pattern)
{

}

Color::~Color(void)
{

}

Color &Color::operator=(const Color &other)
{
	_alpha = other._alpha;
	_color = other._color;
	_pattern = other._pattern;

	return *this;
}

void Color::set_blue(gushort value)
{
	_color.set_blue(value);
	_set_cairo_pattern();
}

void Color::set_blue_p(gdouble value)
{
	_color.set_blue((gushort)(value * 65535.0));
	_set_cairo_pattern();
}

void Color::set_green(gushort value)
{
	_color.set_green(value);
	_set_cairo_pattern();
}

void Color::set_green_p(gdouble value)
{
	_color.set_green((gushort)(value * 65535.0));
	_set_cairo_pattern();
}

void Color::set_red(gushort value)
{
	_color.set_red(value);
	_set_cairo_pattern();
}

void Color::set_red_p(gdouble value)
{
	_color.set_red((gushort)(value * 65535.0));
	_set_cairo_pattern();
}

void Color::set_alpha(gdouble value)
{
	_alpha = value;
	_set_cairo_pattern();
}

void Color::set_rgb(gushort red, gushort green, gushort blue)
{
	_color.set_rgb(red, green, blue);
	_set_cairo_pattern();
}

void Color::set_rgb_p(gdouble red, gdouble green, gdouble blue)
{
	_color.set_rgb_p(red, green, blue);
	_set_cairo_pattern();
}

void Color::set_rgba(gushort red, gushort green, gushort blue, gdouble alpha)
{
	_alpha = alpha;
	_color.set_rgb(red, green, blue);
	_set_cairo_pattern();
}

void Color::set_rgba_p(gdouble red, gdouble green, gdouble blue, gdouble alpha)
{
	_alpha = alpha;
	_color.set_rgb_p(red, green, blue);
	_set_cairo_pattern();
}

void Color::set(const Glib::ustring &value)
{
	_color.set(value);
	_set_cairo_pattern();
}

void Color::_set_cairo_pattern(void)
{
	_pattern = Cairo::SolidPattern::create_rgba(_color.get_red_p(),
	                                            _color.get_green_p(),
	                                            _color.get_blue_p(), _alpha);
}
