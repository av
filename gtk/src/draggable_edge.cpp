#include <cmath>

#include "draggable_edge.hpp"

bool _is_in_matrix = false;

DraggableEdge::DraggableEdge(const TreeModel &parent) :
	Drawable(parent),
	Edge(parent),
	_SPACING(5),
	_BORDER_WIDTH(3),
	_drag(NONE),
	_border_width(_BORDER_WIDTH),
	_opacity_weight(0),
	_topleft_x(0),
	_topleft_y(0),
	_topright_x(0),
	_topright_y(0),
	_bottomleft_x(0),
	_bottomleft_y(0),
	_bottomright_x(0),
	_bottomright_y(0),
	_source(NULL),
	_target(NULL)
{

}

DraggableEdge::~DraggableEdge(void)
{

}

void DraggableEdge::draw(Cairo::RefPtr<Cairo::Context> cr)
{
	if ((_source && !_source->is_visible()) ||
	    (_target && !_target->is_visible())) {
		toggle_visibility(false);
		set_matrix_visibility(false);
	} else {
		toggle_visibility(true);
		set_matrix_visibility(true);
	}

	if (is_visible() && get_is_tree_model()) {
		if (_drag == NONE) {
			_set_source_anchor(false);
			_set_target_anchor(false);
		}

		Edge::draw(cr);
	}
}

void DraggableEdge::drag(gdouble x, gdouble y)
{
	gdouble sdist = 0.0;
	gdouble tdist = 0.0;

	if (_drag == NONE) {
		sdist = sqrt(pow((x - get_start().first), 2) +
		             pow((y - get_start().second), 2));
		tdist = sqrt(pow((x - get_end().first), 2) +
		             pow((y - get_end().second), 2));
	}

	// If the point is closer to the source, then change the source. Otherwise,
	// change the target
	if ((sdist - tdist) > 0 || _drag == TARGET) {
		change_target_follow_mouse(x, y);
	} else {
		change_source_follow_mouse(x, y);
	}
}

void DraggableEdge::drag(DraggableEdge::drag_type_t type, gdouble x, gdouble y)
{
	_drag = type;
	drag(x, y);
}

void DraggableEdge::set_opacity_weight(gdouble weight)
{
	_opacity_weight = weight;

	_calculate_opacity();

	_set_dirty();
}

bool DraggableEdge::change_source(Node *source)
{
	if (_target && source && _target->get_type() == source->get_type()) {
		return false;
	}

	if (!_set_source(source)) {
		return false;
	}

	_drag = NONE;

	return true;
}

bool DraggableEdge::change_target(Node *target)
{
	if (_source && target && _source->get_type() == target->get_type()) {
		return false;
	}

	if (!_set_target(target)) {
		return false;
	}

	_drag = NONE;

	return true;
}

bool DraggableEdge::restore(void)
{
	if (_drag == NONE) {
		return true;
	}

	if (!_source || !_target) {
		return false;
	}

	if (_drag == SOURCE) {
		_set_source_anchor();
	} else if (_drag == TARGET) {
		_set_target_anchor();
	}

	_drag = NONE;

	return true;
}

void DraggableEdge::change_source_follow_mouse(gdouble x, gdouble y)
{
	_drag = SOURCE;

	set_start(x, y);
}

void DraggableEdge::change_target_follow_mouse(gdouble x, gdouble y)
{
	_drag = TARGET;

	set_end(x, y);
}

void DraggableEdge::delete_from_nodes(void)
{
	if (_source && _target) {
		_source->delete_edge(*this);
		_target->delete_edge(*this);
	}
}

void DraggableEdge::_calculate_opacity(void)
{
	guint height = 0;
	guint max = 0;
	gdouble weight = 0;
	Color color = get_color();
	Color highlight_color = get_highlight_color();

	if (_target_angle > 0) {
		Edge::_calculate_opacity();

		return;
	}

	if (_source) {
		height += _source->get_nonterminal().get_height();
		max += _source->get_nonterminal().get_root()->get_height();
	}
	if (_target) {
		height += _target->get_nonterminal().get_height();
		max += _target->get_nonterminal().get_root()->get_height();
	}

	if (_opacity_weight < 0.0) {
		weight = _opacity_weight;
	} else if (_opacity_weight > 0) {
		height = max - height;
		weight = -_opacity_weight;
	}

	color.set_alpha(pow(pow(height + 1, 2), weight * 2));
	highlight_color.set_alpha(color.get_alpha());
	set_color(color);
	set_highlight_color(highlight_color);
}

bool DraggableEdge::_add_to_nodes(void)
{
	if (_source && _target) {
		if (!_source->add_edge(*this) || !_target->add_edge(*this)) {
			return false;
		}
	}

	return true;
}

void DraggableEdge::_set_source_anchor(bool emit)
{
	gdouble x, y;

	if (!_source || _drag == TARGET) {
		return;
	}

	_source->get_bottom_anchor(x, y);
	set_start(x, y, emit);
}

void DraggableEdge::_set_target_anchor(bool emit)
{
	gdouble x, y;

	if (!_target || _drag == SOURCE) {
		return;
	}

	_target->get_top_anchor(x, y);
	set_end(x, y, emit);
}

bool DraggableEdge::_set_source(Node *source)
{
	Node *original = _source;

	delete_from_nodes();

	_source = source;

	if (!_add_to_nodes()) {
		_source = original;
		_add_to_nodes();

		return false;
	}

	_set_source_anchor();

	return true;
}

bool DraggableEdge::_set_target(Node *target)
{
	Node *original = _target;

	delete_from_nodes();

	_target = target;

	if (!_add_to_nodes()) {
		_target = original;
		_add_to_nodes();

		return false;
	}

	_set_target_anchor();

	return true;
}

void DraggableEdge::_update_extents(bool emit)
{
	if (get_is_tree_model()) {
		Edge::_update_extents(emit);
	}

	if (emit) {
		_set_dirty();
	}

	_set_changed();
}
