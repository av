#include <cairomm/pattern.h>

#include "transformable.hpp"
#include "tree_model.hpp"

Transformable::Transformable(const TreeModel &parent) :
	Drawable(parent),
	_matrix_path(NULL),
	_matrix_x(0),
	_matrix_y(0),
	_matrix_width(0),
	_matrix_height(0),
	_source_angle(0),
	_target_angle(0),
	_is_tree_model(true),
	_is_matrix_model(false),
	_is_visible(true),
	_rect(NULL)
{

}

Transformable::~Transformable(void)
{
	delete _matrix_path;
	delete _rect;
}

void Transformable::set_is_tree_model(bool value)
{
	_is_tree_model = value;
}

void Transformable::set_is_matrix_model(bool value)
{
	_is_matrix_model = value;
}

void Transformable::set_color(Color color)
{
	Drawable::set_color(color);

	color.set_alpha(1 - color.get_alpha());
	_matrix_normal_color = color;
}

void Transformable::set_highlight_color(Color color)
{
	Drawable::set_highlight_color(color);

	color.set_alpha(1 - color.get_alpha());
	_matrix_highlight_color = color;
}

void Transformable::set_matrix_visibility(bool value)
{
	_is_visible = value;
}

void Transformable::change_source_angle(gdouble angle)
{
	_source_angle = angle;

	_update_extents();
}

void Transformable::change_target_angle(gdouble angle)
{
	_target_angle = angle;

	_update_extents();
}

void Transformable::_set_rect(void)
{
	Drawable::_set_rect();

	delete _rect;
	_rect = new Gdk::Rectangle(gint(_parent._c2d_x(floor(_matrix_x))),
	                           gint(_parent._c2d_y(floor(_matrix_y))),
	                           gint(_parent._c2d_w(ceil(_matrix_width))),
	                           gint(_parent._c2d_h(ceil(_matrix_height))));
	_rect->join(Drawable::get_rect());
}
