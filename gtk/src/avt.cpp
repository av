#include <iostream>

#include <gtkmm/main.h>
#include <glibmm/error.h>

#include "main_window.hpp"

int main(int argc, char **argv)
{
	Gtk::Main ml(argc, argv);
	MainWindow *win = NULL;

	try {
		Glib::RefPtr<Glade::Xml> glade = Glade::Xml::create("glade/avt.glade");
		glade->get_widget_derived("avt", win);

		ml.run(*win);
	} catch (Glib::Error &e) {
		std::cerr << "An error occurred: " << e.what() << std::endl;
		return 1;
	}

	delete win;

	return 0;
}
