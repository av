/**
 * \file leaf.hpp
 *
 * Encapsulates the Leaf class.
 */

#ifndef __LEAF_HPP__
#define __LEAF_HPP__

#include <vector>
#include <gtkmm/widget.h>
#include <pangomm/layout.h>
#include <cairomm/context.h>

#include "node.hpp"
#include "terminal.hpp"
#include "language_model.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The Leaf class.
 *
 * This class represents the terminals of the language_models on the canvas.
 */
class Leaf : public Node
{
public:
	/**
	 * \brief Constructor.
	 * \param parent The parent widget which contains the canvas.
	 * \param terminal The Terminal object.
	 */
	Leaf(TreeModel &parent, Terminal &terminal);

	/**
	 * \brief Destructor.
	 */
	~Leaf(void);

	/**
	 * \brief Gets the Terminal object.
	 * \return The Terminal object.
	 */
	Terminal &get_terminal(void) const;

	/**
	 * \brief Gets the line color.
	 * \return The line color.
	 */
	const Color &get_line_color(void) const;

	/**
	 * \brief Sets the coordinate of the top-left corner.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void set_coordinates(gdouble x, gdouble y);

	/**
	 * \brief Sets the color of the reference line.
	 * \param color The color object.
	 */
	virtual void set_line_color(Color color);

	/**
	 * \brief Draws the reference line of the node.
	 * \param cr The Cairo context.
	 * \param start_x The x-coordinate of the starting point.
	 * \param start_y The y-coordinate of the starting point.
	 * \param end_x The x-coordinate of the ending point.
	 * \param end_y The y-coordinate of the ending point.
	 */
	void draw_line(Cairo::RefPtr<Cairo::Context> cr, gdouble start_x, gdouble start_y, gdouble end_x, gdouble end_y);

	/**
	 * \brief Drags the leaf.
	 * \param x The x-coordinate of the new point.
	 * \param y The y-coordinate of the new point.
	 */
	virtual void drag(gdouble x, gdouble y);

	/**
	 * \brief Stops dragging.
	 *
	 * This stops the previous dragging operation. If the leaf is not in
	 * dragging status, nothing will be changed.
	 */
	virtual void stop_drag(void);
private:
	Color _line_normal_color;	//!< Normal color of the reference line.

	const gdouble _LINE_WIDTH;	 //!< The reference line width.
	bool _is_drag;				 //!< <code>true</code> if the leaf is being dragged.
};

inline Terminal &Leaf::get_terminal(void) const
{
	return dynamic_cast<Terminal &>(_nonterminal);
}

inline const Color &Leaf::get_line_color(void) const
{
	return _line_normal_color;
}

/**
 * \}
 */

#endif
