/**
 * \file node.hpp
 *
 * Encapsulates the Node class.
 */

#ifndef __NODE_HPP__
#define __NODE_HPP__

#include <list>
#include <pangomm/layout.h>

#include "transformable.hpp"
#include "selectable.hpp"
#include "language_model.hpp"
#include "nonterminal.hpp"

class DraggableEdge;
class TreeEdge;

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The Node base class.
 *
 * This class represents the view of a node in the tree structure.
 */
class Node : public Selectable, public Transformable
{
public:
	/**
	 * \brief The position type.
	 */
	typedef enum {
		BEGIN,					//!< At the beginning of the children.
		END,					//!< At the end of the children.
		CENTER,					//!< At the center of the children.
		COG,					//!< At the center of gravity.
	} position_t;

	/**
	 * \brief Constructor.
	 * \param parent The parent widget which contains the canvas.
	 * \param n The NonTerminal.
	 */
	Node(TreeModel &parent, NonTerminal &n);

	/**
	 * \brief Destructor.
	 */
	virtual ~Node(void);

	/**
	 * \brief Gets the region.
	 * \return The region.
	 */
	const Gdk::Rectangle &get_rect(void) const;

	/**
	 * \brief Gets the type of the node.
	 * \return The type of the node.
	 */
	LanguageModel::language_model_type_t get_type(void) const;

	/**
	 * \brief Gets the NonTerminal.
	 * \return The NonTerminal.
	 */
	NonTerminal &get_nonterminal(void) const;

	/**
	 * \brief Gets the text.
	 * \return The text.
	 */
	Glib::ustring get_text(void) const;

	/**
	 * \brief Gets the Pango font description used by this node.
	 * \return The Pango::FontDescription.
	 */
	virtual const Pango::FontDescription &get_font_description(void) const;

	/**
	 * \brief Tells if the mono representation or the matrix representation is
	 * dragged.
	 * \return <code>true</code> if dragged.
	 */
	virtual bool is_drag(void) const;

	/**
	 * \brief Gets the anchor at the top for edge association.
	 * \param[out] x The x-coordinate of the anchor.
	 * \param[out] y The y-coordinate of the anchor.
	 */
	virtual void get_top_anchor(gdouble &x, gdouble &y) const;

	/**
	 * \brief Gets the anchor at the bottom for edge association.
	 * \param[out] x The x-coordinate of the anchor.
	 * \param[out] y The y-coordinate of the anchor.
	 */
	virtual void get_bottom_anchor(gdouble &x, gdouble &y) const;

	/**
	 * \brief Gets the anchor at the left for edge association.
	 * \param[out] x The x-coordinate of the anchor.
	 * \param[out] y The y-coordinate of the anchor.
	 */
	virtual void get_left_anchor(gdouble &x, gdouble &y) const;

	/**
	 * \brief Gets the anchor at the right for edge association.
	 * \param[out] x The x-coordinate of the anchor.
	 * \param[out] y The y-coordinate of the anchor.
	 */
	virtual void get_right_anchor(gdouble &x, gdouble &y) const;

	/**
	 * \brief Gets the anchor at the centroid for edge association.
	 * \param[out] x The x-coordinate of the anchor.
	 * \param[out] y The y-coordinate of the anchor.
	 */
	virtual void get_centroid_anchor(gdouble &x, gdouble &y) const;

	/**
	 * \brief Gets the top left coordinates of the matrix representation.
	 * \return The coordinates of the top left corner.
	 */
	virtual std::pair<gdouble, gdouble> get_topleft(void) const;

	/**
	 * \brief Gets the top right coordinates of the matrix representation.
	 * \return The coordinates of the top right corner.
	 */
	virtual std::pair<gdouble, gdouble> get_topright(void) const;

	/**
	 * \brief Gets the bottom left coordinates of the matrix representation.
	 * \return The coordinates of the bottom left corner.
	 */
	virtual std::pair<gdouble, gdouble> get_bottomleft(void) const;

	/**
	 * \brief Gets the bottom right coordinates of the matrix representation.
	 * \return The coordinates of the bottom right corner.
	 */
	virtual std::pair<gdouble, gdouble> get_bottomright(void) const;

	/**
	 * \brief Gets the spacing before the node.
	 * \return The spacing.
	 */
	virtual gdouble get_spacing_before(void) const;

	/**
	 * \brief Gets the spacing after the node.
	 * \return The spacing.
	 */
	virtual gdouble get_spacing_after(void) const;

	/**
	 * \brief Gets the spacing between levels.
	 * \return The level spacing.
	 */
	virtual gdouble get_level_spacing(void) const;

	/**
	 * \brief Gets the alignment edges this node associates with.
	 * \return The alignment edges.
	 */
	virtual const std::list<DraggableEdge *> get_edges(void) const;

	/**
	 * \brief Gets the tree edges this node has.
	 * \return The tree edges.
	 */
	virtual const std::list<TreeEdge *> get_tree_edges(void) const;

	/**
	 * \brief Gets the left-most tree edge.
	 * \return The left-most tree edge, or <code>NULL</code> if no tree edges.
	 */
	virtual const TreeEdge *get_leftmost_tree_edge(void) const;

	/**
	 * \brief Gets the right-most tree edge.
	 * \return The right-most tree edge, or <code>NULL</code> if no tree edges.
	 */
	virtual const TreeEdge *get_rightmost_tree_edge(void) const;

	/**
	 * \brief Tells whether it is collapsed.
	 * \return <code>true</code> if collapsed.
	 */
	virtual bool get_collapsed(void) const;

	/**
	 * \brief Gets the nodes which are collapsed under this node.
	 * \return The list of nodes.
	 */
	virtual const std::list<Node *> &get_collapsed_nodes(void) const;

	/**
	 * \brief Tells the visibility of the monolingual representation.
	 * \return <code>true</code> if visible.
	 */
	virtual bool get_is_mono_visible(void) const;

	/**
	 * \brief Tells if it is set to virtual temporarily.
	 * \return <code>true</code> if virtual.
	 */
	virtual bool get_is_virtual(void) const;

	/**
	 * \brief Gets the position.
	 * \return The position.
	 */
	virtual position_t get_position(void) const;

	/**
	 * \brief Sets the position.
	 * \param val The new position.
	 */
	virtual void set_position(position_t val);

	/**
	 * \brief Sets it to virtual temporarily.
	 * \param val <code>true</code> if virtual.
	 */
	virtual void set_virtuality(bool val);

	/**
	 * \brief Sets the visibility of the monolingual representation.
	 * \param val <code>true</code> if visible.
	 */
	virtual void set_mono_visibility(bool val);

	/**
	 * \brief Sets the text.
	 * \param text The new text.
	 */
	virtual void set_text(const Glib::ustring &text);

	/**
	 * \brief Sets the font.
	 * \param family The font family, ex. "Sans".
	 * \param size The font size, ex. 12.
	 */
	virtual void set_font(const Glib::ustring &family, gint size);

	/**
	 * \brief Sets the top-left coordinate of the node.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	virtual void set_coordinates(gdouble x, gdouble y);

	/**
	 * \brief Sets the coordinates of the matrix representaion.
	 * \param tl The top left corner.
	 * \param tr The top right corner.
	 * \param bl The bottom left corner.
	 * \param br The bottom right corner.
	 */
	virtual void set_matrix_coordinates(std::pair<gdouble, gdouble> tl, std::pair<gdouble, gdouble> tr, std::pair<gdouble, gdouble> bl, std::pair<gdouble, gdouble> br);

	/**
	 * \brief Sets the flipped property.
	 * \param flipped <code>true</code> if flipped.
	 *
	 * If it is flipped, the tree grows in the opposite direction as it
	 * originally would do.
	 */
	virtual void set_flipped(bool flipped);

	/**
	 * \brief Sets the spacing before levels.
	 * \param spacing The level spacing.
	 */
	virtual void set_spacing_before(gdouble spacing);

	/**
	 * \brief Sets the spacing after the node.
	 * \param spacing The spacing.
	 */
	virtual void set_spacing_after(gdouble spacing);

	/**
	 * \brief Sets the spacing between levels.
	 * \param spacing The level spacing, a non-positive value will set it to the
	 * default value.
	 */
	virtual void set_level_spacing(gdouble spacing);

	/**
	 * \brief Sets the collapsed variable.
	 * \param val The node which this node will synchronize position with,
	 * <code>NULL</code> if not collapsed.
	 */
	virtual void set_collapsed(Node *val);

	/**
	 * \brief Adds the collapsed nodes under this node.
	 * \param val The node which is collapsed under this node.
	 */
	virtual void add_collapsed_node(Node &val);

	/**
	 * \brief Adds an edge.
	 * \param edge The edge to add.
	 * \return <code>true</code> if successfully added.
	 */
	virtual bool add_edge(DraggableEdge &edge);

	/**
	 * \brief Adds a tree edge.
	 * \param edge The tree edge to add.
	 * \return <code>true</code> if successfully added.
	 */
	virtual bool add_edge(TreeEdge &edge);

	/**
	 * \brief Deletes an edge.
	 * \param edge The edge to delete.
	 */
	virtual void delete_edge(const DraggableEdge &edge);

	/**
	 * \brief Deletes a tree edge.
	 * \param edge The tree edge to delete.
	 */
	virtual void delete_edge(const TreeEdge &edge);

	/**
	 * \brief Recalculates the x-coordinate of the top-left corner.
	 * \param emit <code>true</code> if you want to set the entity as dirty, so
	 * that it will be redrawn.
	 */
	virtual void update_position(bool emit = false);

	/**
	 * \brief Recalculates the coordinates of the monolingual representation.
	 * \param emit <code>true</code> if you want to set the entity as dirty, so
	 * that it will be redrawn.
	 */
	virtual void update_mono_position(bool emit = false);

	/**
	 * \brief Draws the node.
	 * \param cr The Cairo context.
	 */
	void draw(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Draws the monolingual representation.
	 * \param cr The Cairo context.
	 */
	void draw_mono(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Draws the matrix representation.
	 * \param cr The Cairo context.
	 */
	void draw_matrix(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Checks if the given coordinate pair is in the node's extents.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	bool is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) const;

	/**
	 * \brief Checks if the given coordinate is in the node's extents.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 */
	bool is_x_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x) const;

	/**
	 * \brief Checks if the given coordinate is in the node's extents.
	 * \param cr The Cairo context.
	 * \param y The y-coordinate.
	 */
	bool is_y_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble y) const;

	/**
	 * \brief Highlights the node.
	 */
	virtual void highlight(void);

	/**
	 * \brief Highlights the node.
	 *
	 * This is the same as calling <code>highlight()</code>, it is kept for
	 * consistency reason.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y);

	/**
	 * \brief Removes highlight.
	 */
	virtual void remove_highlight(void);

	/**
	 * \brief Removes highlight.
	 *
	 * This is the same as calling <code>remove_highlight()</code>, it is kept
	 * for consistency reason.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void remove_highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y);

	/**
	 * \brief Drags the mono representation or the matrix representation.
	 * \param x The x-coordinate of the new point.
	 * \param y The y-coordinate of the new point.
	 */
	virtual void drag(gdouble x, gdouble y);

	/**
	 * \brief Stops dragging the mono representation or the matrix
	 * representation.
	 * \param x The x-coordinate of the new point.
	 * \param y The y-coordinate of the new point.
	 */
	virtual void stop_drag(gdouble x, gdouble y);

	/**
	 * \brief Selects the node.
	 */
	virtual void select(void);

	/**
	 * \brief Selects the node.
	 *
	 * This is the same as calling <code>select()</code>, it is kept for
	 * consistency reason.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void select(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y);

	/**
	 * \brief Deselects the node.
	 */
	virtual void deselect(void);

	/**
	 * \brief Deselects the node.
	 *
	 * This is the same as calling <code>deselect()</code>, it is kept for
	 * consistency reason.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void deselect(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y);

	/**
	 * \brief Changes the angle of the target language_model.
	 * \param angle The new angle.
	 */
	void change_target_angle(gdouble angle);
protected:
	/**
	 * \brief Sets the region.
	 */
	virtual void _set_rect(void);

	/**
	 * \brief Draws the brackets.
	 * \param cr The Cairo context.
	 * \param tl The coordinates of the top left corner.
	 * \param tr The coordinates of the top right corner.
	 * \param bl The coordinates of the bottom left corner.
	 * \param br The coordinates of the bottom right corner.
	 * \param[out] path The Cairo path to store the result to.
	 */
	virtual void _draw_brackets(Cairo::RefPtr<Cairo::Context> cr, std::pair<gdouble, gdouble> tl, std::pair<gdouble, gdouble> tr, std::pair<gdouble, gdouble> bl, std::pair<gdouble, gdouble> br, Cairo::Path **path = NULL);

	/**
	 * \brief Checks if the node already has the edge.
	 * \param edge The edge to check.
	 * \return <code>true</code> if the node already has the edge.
	 */
	virtual bool _has_edge(const DraggableEdge &edge) const;

	/**
	 * \brief Checks if the node already has the tree edge.
	 * \param edge The tree edge to check.
	 * \return <code>true</code> if the node already has the tree edge.
	 */
	virtual bool _has_tree_edge(const TreeEdge &edge) const;

	/**
	 * \brief Updates the extents coordinate and size.
	 * \param emit <code>true</code> if you want to set the entity as dirty, so
	 * that it will be redrawn.
	 */
	void _update_extents(bool emit = true);

	const gint _DEFAULT_WIDTH;	 //!< The default width for an empty cell.
	const gdouble _BORDER_WIDTH; //!< The border width.
	const gdouble _MONO_BRACKET_LENGTH; //!< The length of the bracket corner.
	const gint _PADDING;		 //!< The padding.
	const gdouble _SPACING;		 //!< The default spacing before and after the node.
	const gdouble _MONO_SPACING; //!< The spacing between different levels of brackets.
	const gdouble _LEVEL_SPACING;	 //!< The default spacing between levels.
	gdouble _descent;			 //!< The descent of the terminal.
	gdouble _spacing_before;	 //!< The spacing before the node.
	gdouble _spacing_after;		 //!< The spacing after the node.
	gdouble _level_spacing;		 //!< The level spacing.
	gdouble _mono_x;			 //!< The x-coordinate of the mono extents.
	gdouble _mono_y;			 //!< The y-coordinate of the mono extents.
	gdouble _mono_width;		 //!< The width of the mono extents.
	gdouble _mono_height;		 //!< The height of the mono extents.
	gdouble _topleft_x, _topleft_y;			//!< The topleft corner.
	gdouble _topright_x, _topright_y;		//!< The topright corner.
	gdouble _bottomleft_x, _bottomleft_y;	//!< The bottomleft corner.
	gdouble _bottomright_x, _bottomright_y; //!< The bottomright corner.
	gdouble _mtopleft_x, _mtopleft_y;		//!< The topleft corner.
	gdouble _mtopright_x, _mtopright_y;		//!< The topright corner.
	gdouble _mbottomleft_x, _mbottomleft_y;	//!< The bottomleft corner.
	gdouble _mbottomright_x, _mbottomright_y; //!< The bottomright corner.

	Glib::RefPtr<Pango::Layout> _layout; //!< The layout.
	Pango::FontDescription *_font;		 //!< The font description.
	Cairo::Path *_mono_path;			 //!< The Cairo path for monolingual representation.
	bool _is_highlight;		  //!< <code>true</code> if the node is highlighted.
	bool _is_flipped;			//!< <code>true</code> if it is flipped.
	std::list<DraggableEdge *> _edges; //!< The correspondence edges it associates with.
	std::list<TreeEdge *> _tree_edges; //!< The tree edges it has.

	NonTerminal &_nonterminal;	//!< The NonTerminal.
private:
	/**
	 * \brief The type of the current dragging end.
	 */
	typedef enum {
		NONE,					//!< Not being dragged.
		LEFT,
		RIGHT,
	} drag_type_t;

	drag_type_t _drag;			//!< The drag type.
	bool _force_mono_off;		//!< Monolingual representation visibility.
	bool _is_mono_visible;		//!< Monolingual representation visibility.
	bool _is_virtual;			//!< Virtuality.
	Gdk::Rectangle *_rect;		//!< The region.
	Node *_collapsed;			//!< The node to synchronize position with if collapsed.
	std::list<Node *> _collapsed_nodes; //!< The nodes which are collapsed under this node.
	position_t _position;				//!< The position.
};

inline const Gdk::Rectangle &Node::get_rect(void) const
{
	return *_rect;
}

inline LanguageModel::language_model_type_t Node::get_type(void) const
{
	return _nonterminal.get_language_model().get_type();
}

inline NonTerminal &Node::get_nonterminal(void) const
{
	return _nonterminal;
}

inline Glib::ustring Node::get_text(void) const
{
	return _nonterminal.get_text();
}

inline const Pango::FontDescription &Node::get_font_description(void) const
{
	return *_font;
}

inline std::pair<gdouble, gdouble> Node::get_topleft(void) const
{
	return std::make_pair(_topleft_x, _topleft_y);
}

inline std::pair<gdouble, gdouble> Node::get_topright(void) const
{
	return std::make_pair(_topright_x, _topright_y);
}

inline std::pair<gdouble, gdouble> Node::get_bottomleft(void) const
{
	return std::make_pair(_bottomleft_x, _bottomleft_y);
}

inline std::pair<gdouble, gdouble> Node::get_bottomright(void) const
{
	return std::make_pair(_bottomright_x, _bottomright_y);
}

inline gdouble Node::get_spacing_before(void) const
{
	return _spacing_before;
}

inline gdouble Node::get_spacing_after(void) const
{
	return _spacing_after;
}

inline gdouble Node::get_level_spacing(void) const
{
	return _level_spacing;
}

inline const std::list<DraggableEdge *> Node::get_edges(void) const
{
	return _edges;
}

inline const std::list<TreeEdge *> Node::get_tree_edges(void) const
{
	return _tree_edges;
}

inline bool Node::get_collapsed(void) const
{
	return _collapsed ? true : false;
}

inline const std::list<Node *> &Node::get_collapsed_nodes(void) const
{
	return _collapsed_nodes;
}

inline bool Node::get_is_mono_visible(void) const
{
	return _is_mono_visible;
}

inline bool Node::get_is_virtual(void) const
{
	return _is_virtual;
}

inline Node::position_t Node::get_position(void) const
{
	return _position;
}

/**
 * \}
 */

#endif
