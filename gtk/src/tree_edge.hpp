/**
 * \file tree_edge.hpp
 *
 * Encapsulates the TreeEdge class.
 */

#ifndef __TREE_EDGE_HPP__
#define __TREE_EDGE_HPP__

#include "draggable_edge.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The TreeEdge class.
 *
 * This class represents the tree edges.
 */
class TreeEdge : public DraggableEdge
{
public:
	/**
	 * \brief Constructor.
	 */
	TreeEdge(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	~TreeEdge(void);

	/**
	 * \brief Draws the edge.
	 * \param cr The Cairo context.
	 */
	void draw(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Gets the anchor which is close to the source.
	 * \return The anchor.
	 */
	std::pair<gdouble, gdouble> get_source_side_anchor(void) const;

	/**
	 * \brief Gets the anchor which is close to the target.
	 * \return The anchor.
	 */
	std::pair<gdouble, gdouble> get_target_side_anchor(void) const;

	/**
	 * \brief Tells it is cornered.
	 * \return <code>true</code> if it is.
	 */
	bool get_is_cornered(void) const;

	/**
	 * \brief Sets the cornered property.
	 * \param val <code>true</code> to make it cornered.
	 */
	void set_is_cornered(bool val);

	/**
	 * \brief Sets the matrix representation indicator.
	 * \param value <code>true</code> if in matrix representation.
	 */
	void set_is_matrix_model(bool value);

	/**
	 * \brief Changes the opacity weight.
	 *
	 * This method does not do anything as tree edges should be visible all the
	 * time.
	 *
	 * \param weight The opacity weight.
	 */
	void set_opacity_weight(gdouble weight);

	/**
	 * \brief Changes the source Node.
	 * \param source The new source Node, or <code>NULL</code> to clear the
	 * source Node.
	 * \return <code>true</code> if successful.
	 */
	bool change_source(Node *source);

	/**
	 * \brief Changes the target Node.
	 * \param target The new target Node, or <code>NULL</code> to clear the
	 * target Node.
	 * \return <code>true</code> if successful.
	 */
	bool change_target(Node *target);

	/**
	 * \brief Changes the angle of the target language_model.
	 * \param angle The new angle.
	 */
	void change_target_angle(gdouble angle);

	/**
	 * \brief Deletes this edge from the two Nodes at both ends.
	 *
	 * Both Nodes will also be removed from each other's friends list.
	 */
	void delete_from_nodes(void);
protected:
	/**
	 * \brief Adds this edge to the two Nodes at both ends.
	 *
	 * Both Nodes will also be added to each other's friends list.
	 *
	 * \return <code>true</code> if successful.
	 */
	bool _add_to_nodes(void);

	/**
	 * \brief Sets the source end anchor point based on the current source Node.
	 * \param emit <code>false</code> if you do not want to emit a changed
	 * signal.
	 */
	void _set_source_anchor(bool emit = true);

	/**
	 * \brief Sets the target end anchor point based on the current target Node.
	 * \param emit <code>false</code> if you do not want to emit a changed
	 * signal.
	 */
	void _set_target_anchor(bool emit = true);
private:
	bool _is_cornered;			//!< <code>true</code> if the edge is cornered.
};

inline bool TreeEdge::get_is_cornered(void) const
{
	return _is_cornered;
}

inline void TreeEdge::set_is_matrix_model(bool value)
{

}

/**
 * \}
 */

#endif
