#include "virtual_edge.hpp"

VirtualEdge::VirtualEdge(const TreeModel &parent) :
	Drawable(parent),
	DraggableEdge(parent)
{
	set_color(Color(0.74, 0.74, 0.74));
	DraggableEdge::toggle_visibility(false);
}

VirtualEdge::~VirtualEdge(void)
{

}

void VirtualEdge::draw(Cairo::RefPtr<Cairo::Context> cr)
{
	gdouble x1, y1, x2, y2;

	if ((_source && !_source->is_visible()) ||
	    (_target && !_target->is_visible())) {
		set_matrix_visibility(false);
	} else {
		set_matrix_visibility(true);
	}

	if (!get_is_matrix_visible() || !get_is_matrix_model() ||
	    !_source || !_target) {
		return;
	}

	_calculate_position();

	cr->save();
	cr->begin_new_path();

	cr->move_to(_topleft_x, _topleft_y);
	cr->line_to(_topright_x, _topright_y);
	cr->line_to(_bottomright_x, _bottomright_y);
	cr->line_to(_bottomleft_x, _bottomleft_y);
	cr->close_path();

	delete _matrix_path;
	_matrix_path = cr->copy_path();

	if (_is_highlight) {
		cr->set_source(_matrix_highlight_color.get_cairo_pattern());
	} else {
		cr->set_source(_matrix_normal_color.get_cairo_pattern());
	}
	cr->set_line_width(_border_width);

	cr->get_stroke_extents(x1, y1, x2, y2);
	_matrix_x = x1;
	_matrix_y = y1;
	_matrix_width = x2 - x1;
	_matrix_height = y2 - y1;

	cr->stroke();
	cr->restore();
}

bool VirtualEdge::is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x,
                               gdouble y) const
{
	bool result = false;

	if (get_is_matrix_model() && _matrix_path) {
		cr->save();
		cr->begin_new_path();
		cr->append_path(*_matrix_path);
		result = cr->in_stroke(x, y) || cr->in_fill(x, y);
		cr->restore();
	}

	return result;
}

void VirtualEdge::drag(gdouble x, gdouble y)
{

}

void VirtualEdge::change_source_follow_mouse(gdouble x, gdouble y)
{

}

void VirtualEdge::change_target_follow_mouse(gdouble x, gdouble y)
{

}

void VirtualEdge::delete_from_nodes(void)
{

}

bool VirtualEdge::_add_to_nodes(void)
{
	return true;
}

void VirtualEdge::_calculate_position(void)
{
	gdouble x, y;
	gdouble spacing;

	spacing = _SPACING * (_source->get_nonterminal().get_height() + 1);

	_source->get_centroid_anchor(x, y);
	_topleft_x = x - spacing;
	_bottomleft_x = _topleft_x;

	x = _source->get_extents_x();
	if (_topleft_x < x &&
	    (x - _topleft_x) > _source->get_spacing_before()) {
		_source->set_spacing_before(x - _topleft_x);
	}

	_source->get_centroid_anchor(x, y);
	_topright_x = x + spacing;
	_bottomright_x = _topright_x;

	x = _source->get_extents_x() +
		_source->get_extents_width();
	if (_topright_x > x &&
	    (_topright_x - x) > _source->get_spacing_after()) {
		_source->set_spacing_after(_topright_x - x);
	}

	spacing = _SPACING * (_target->get_nonterminal().get_height() + 1);

	_target->get_centroid_anchor(x, y);
	_topleft_y = y - spacing + (x - _topleft_x) / tan(_target_angle);
	_topright_y = y - spacing + (x - _topright_x) / tan(_target_angle);

	y = _target->get_extents_height() / 2;
	if (spacing > y &&
	    (spacing - y) > _target->get_spacing_before()) {
		_target->set_spacing_before(spacing - y);
	}

	_target->get_centroid_anchor(x, y);
	_bottomleft_y = y + spacing + (x - _bottomleft_x) / tan(_target_angle);
	_bottomright_y = y + spacing + (x - _bottomright_x) / tan(_target_angle);

	y = _target->get_extents_height() / 2;
	if (spacing > y &&
	    (spacing - y) > _target->get_spacing_after()) {
		_target->set_spacing_after(spacing - y);
	}
}
