/**
 * \file selectable.hpp
 *
 * Encapsulates the Selectable abstract base class.
 */

#ifndef __SELECTABLE_HPP__
#define __SELECTABLE_HPP__

#include "drawable.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The Selectable abstract base class.
 */
class Selectable : public virtual Drawable
{
public:
	/**
	 * \brief Constructor.
	 */
	Selectable(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	~Selectable(void);

	/**
	 * \brief Selects the entity.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	virtual void select(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) = 0;

	/**
	 * \brief Deselects the entity.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	virtual void deselect(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) = 0;
};

/**
 * \}
 */

#endif
