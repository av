/**
 * \file edge.hpp
 *
 * Encapsulates the base class Edge.
 */

#ifndef __EDGE_HPP__
#define __EDGE_HPP__

#include <utility>
#include <cairomm/context.h>

#include "transformable.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The base class Edge.
 *
 * This class represents an edge which has a starting point and an ending point.
 */
class Edge : public Transformable
{
public:
	/**
	 * \brief Constructor.
	 */
	Edge(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	~Edge(void);

	/**
	 * \brief Gets the starting point.
	 * \return The starting point.
	 */
	virtual const std::pair<gdouble, gdouble> &get_start(void) const;

	/**
	 * \brief Gets the ending point.
	 * \return The ending point.
	 */
	virtual const std::pair<gdouble, gdouble> &get_end(void) const;

	/**
	 * \brief Sets the starting point.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \param emit <code>false</code> if you do not want to emit a changed
	 * signal.
	 */
	virtual void set_start(gdouble x, gdouble y, bool emit = true);

	/**
	 * \brief Sets the starting point.
	 *
	 * This method is meant to be associated to the changed_signal_t signal.
	 *
	 * \param e The Drawable entity.
	 * \param immediate The parameter passed by the signal emitter.
	 */
	virtual void set_start(Drawable *e, bool immediate);

	/**
	 * \brief Sets the ending point.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \param emit <code>false</code> if you do not want to emit a changed
	 * signal.
	 */
	virtual void set_end(gdouble x, gdouble y, bool emit = true);

	/**
	 * \brief Sets the ending point.
	 *
	 * This method is meant to be associated to the changed_signal_t signal.
	 *
	 * \param e The Drawable entity.
	 * \param immediate The parameter passed by the signal emitter.
	 */
	virtual void set_end(Drawable *e, bool immediate);

	/**
	 * \brief Draws the edge.
	 * \param cr The Cairo context.
	 */
	void draw(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Calculates if the given coordinate pair is in the edge's extents.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \return <code>true</code> if the given coordinate pair is in the edge's
	 * extents.
	 */
	bool is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) const;

	/**
	 * \brief Highlights the edge.
	 */
	virtual void highlight(void);

	/**
	 * \brief Highlights the edge.
	 *
	 * This is the same as calling <code>highlight()</code>, it is kept for
	 * consistency reason.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y);

	/**
	 * \brief Removes highlight.
	 */
	virtual void remove_highlight(void);

	/**
	 * \brief Removes highlight.
	 *
	 * This is the same as calling <code>remove_highlight()</code>, it is kept
	 * for consistency reason.
	 *
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 */
	void remove_highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y);

	/**
	 * \brief Changes the angle of the target language_model.
	 * \param angle The new angle.
	 */
	void change_target_angle(gdouble angle);
protected:
	/**
	 * \brief Calculates the opacity based on current state.
	 */
	virtual void _calculate_opacity(void);

	/**
	 * \brief Updates the extents coordinate and size.
	 * \param emit <code>true</code> if you want to set the entity as dirty, so
	 * that it will be redrawn.
	 */
	void _update_extents(bool emit = true);

	gdouble _WIDTH;				//!< The width of the edge.
	gdouble _HIGHLIGHT_WIDTH;	//!< The highlighted width of the edge.
	bool _is_highlight;			//!< <code>true</code> if the edge is highlighted.
private:
	std::pair<gdouble, gdouble> _start; //!< The starting point.
	std::pair<gdouble, gdouble> _end;	//!< The ending point.
};

inline const std::pair<gdouble, gdouble> &Edge::get_start(void) const
{
	return _start;
}

inline const std::pair<gdouble, gdouble> &Edge::get_end(void) const
{
	return _end;
}

/**
 * \}
 */

#endif
