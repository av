/**
 * \file transformable.hpp
 *
 * Encapsulates the Transformable abstract base class.
 */

#ifndef __TRANSFORMABLE_HPP__
#define __TRANSFORMABLE_HPP__

#include "drawable.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The Transformable abstract base class.
 *
 * This class represents entities with different representations in different
 * models.
 */
class Transformable : public virtual Drawable
{
public:
	/**
	 * \brief Constructor.
	 */
	Transformable(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	~Transformable(void);

	/**
	 * \brief Gets the region.
	 * \return The region.
	 */
	const Gdk::Rectangle &get_rect(void) const;

	/**
	 * \brief Gets the tree representation indicator.
	 * \return <code>true</code> if in tree representation.
	 */
	virtual bool get_is_tree_model(void) const;

	/**
	 * \brief Gets the matrix representation indicator.
	 * \return <code>true</code> if in matrix representation.
	 */
	virtual bool get_is_matrix_model(void) const;

	/**
	 * \brief Gets the visibility of the matrix representation.
	 * \return <code>true</code> if visible.
	 */
	virtual bool get_is_matrix_visible(void) const;

	/**
	 * \brief Sets the tree representation indicator.
	 * \param value <code>true</code> if in tree representation.
	 */
	virtual void set_is_tree_model(bool value);

	/**
	 * \brief Sets the matrix representation indicator.
	 * \param value <code>true</code> if in matrix representation.
	 */
	virtual void set_is_matrix_model(bool value);

	/**
	 * \brief Sets the color in normal mode.
	 * \param color The color object.
	 */
	void set_color(Color color);

	/**
	 * \brief Sets the color in highlight mode.
	 * \param color The color object.
	 */
	void set_highlight_color(Color color);

	/**
	 * \brief Sets the visibility of the matrix representation.
	 * \param value <code>true</code> will show the matrix representation.
	 */
	virtual void set_matrix_visibility(bool value);

	/**
	 * \brief Changes the angle of the source language_model.
	 * \param angle The new angle.
	 */
	virtual void change_source_angle(gdouble angle);

	/**
	 * \brief Changes the angle of the target language_model.
	 * \param angle The new angle.
	 */
	virtual void change_target_angle(gdouble angle);
protected:
	/**
	 * \brief Sets the region.
	 */
	virtual void _set_rect(void);

	Color _matrix_normal_color; //!< Normal color in matrix representation.
	Color _matrix_highlight_color; //!< Highlight color in matrix representation.
	Cairo::Path *_matrix_path;			 //!< The Cairo path for matrix model.

	gdouble _matrix_x;			//!< The x-coordinate of the edge in matrix model.
	gdouble _matrix_y;			//!< The y-coordinate of the edge in matrix model.
	gdouble _matrix_width;		//!< The width of the edge in matrix model.
	gdouble _matrix_height;		//!< The height of the edge in matrix model.
	gdouble _source_angle;		//!< The angle of the source language_model.
	gdouble _target_angle;		//!< The angle of the target language_model.
private:
	bool _is_tree_model;	   //!< <code>true</code> if in tree representation.
	bool _is_matrix_model;	 //!< <code>true</code> if in matrix representation.
	bool _is_visible;			//!< <code>true</code> if visible.
	Gdk::Rectangle *_rect;		//!< The region.
};

inline const Gdk::Rectangle &Transformable::get_rect(void) const
{
	return *_rect;
}

inline bool Transformable::get_is_tree_model(void) const
{
	return _is_tree_model;
}

inline bool Transformable::get_is_matrix_model(void) const
{
	return _is_matrix_model;
}

inline bool Transformable::get_is_matrix_visible(void) const
{
	return _is_visible;
}

/**
 * \}
 */

#endif
