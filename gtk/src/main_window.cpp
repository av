#include <memory>
#include <gtkmm/main.h>
#include <gtkmm/stockid.h>
#include <gtkmm/stock.h>

#include "main_window.hpp"
#include "list.hpp"

MainWindow::MainWindow(GtkWindow *cobject, Glib::RefPtr<Glade::Xml> &glade) :
	Gtk::Window(cobject),
	_glade(glade),
	_file_chooser_dialog(NULL),
	_opacity_adjustment_dialog(NULL),
	_tree_model(NULL),
	_opacity_presets(NULL),
	_opacity_scale(NULL),
	_opacity_adjustment(NULL),
	_info_box(NULL),
	_stats_listview(NULL),
	_trans_listview(NULL),
	_dist_listview(NULL),
	_ngram_listview(NULL),
	_fert_listview(NULL)
{
	_init_actions();
	_init_ui();

	show_all();
}

MainWindow::~MainWindow(void)
{
	delete _file_chooser_dialog;
	delete _opacity_adjustment_dialog;
	delete _tree_model;
	delete _canvas_eventbox;
	delete _info_box;
}

void MainWindow::quit(void)
{
	Gtk::Main::quit();
}

void MainWindow::clear(void)
{
	_stats_listview->unset_model();
	_trans_listview->unset_model();
	_dist_listview->unset_model();
	_ngram_listview->unset_model();
	_fert_listview->unset_model();
}

void MainWindow::show_open_dialog(void)
{
	gint response;
	std::string filename;

	g_assert(_file_chooser_dialog);
	_file_chooser_dialog->set_current_folder(Glib::get_home_dir());
	response = _file_chooser_dialog->run();

	if (response == Gtk::RESPONSE_OK) {
		filename = _file_chooser_dialog->get_filename();
		read_file(filename);
	}

	_file_chooser_dialog->hide();
}

void MainWindow::show_opacity_adjustment_dialog(void)
{
	gdouble original;
	gint response;

	g_assert(_opacity_adjustment_dialog);

	original = _opacity_adjustment->get_value();
	response = _opacity_adjustment_dialog->run();

	if (response == Gtk::RESPONSE_CANCEL) {
		_opacity_adjustment->set_value(original);
	}

	_opacity_adjustment_dialog->hide();
}

void MainWindow::read_file(const std::string &filename)
{
	_parser.read_file(filename);

	clear();

	_read_header();
	_read_language_models();
	_read_alignment();
	_read_translation();
	_read_distortion();
	_read_ngram();
	_read_fertility();

	g_assert(_tree_model);
	_tree_model->add_alignment(_alignment);
}

bool MainWindow::on_key_release_event(GdkEventKey *event)
{
	if (Gtk::Window::on_key_release_event(event)) {
		return true;
	}

	if (event->keyval == GDK_h &&
	    (event->state & GDK_SHIFT_MASK) != GDK_SHIFT_MASK &&
	    (event->state & GDK_CONTROL_MASK) != GDK_CONTROL_MASK &&
	    (event->state & GDK_MOD1_MASK) != GDK_MOD1_MASK) {
		if (_info_box->is_visible()) {
			_info_box->hide();
		} else {
			_info_box->show();
		}
	} else if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) {
		if (event->keyval == GDK_Left) {
			_tree_model->decrease_hscale();
		} else if (event->keyval == GDK_Right) {
			_tree_model->increase_hscale();
		} else if (event->keyval == GDK_Up) {
			_tree_model->increase_vscale();
		} else if (event->keyval == GDK_Down) {
			_tree_model->decrease_vscale();
		} else {
			return false;
		}
	} else {
		return false;
	}

	return true;
}

void MainWindow::on_opacity_adjustment_value_changed(void)
{
	if (_opacity_adjustment && _tree_model) {
		_tree_model->set_opacity(_opacity_adjustment->get_value());
	}
}

void MainWindow::on_opacity_presets_changed(void)
{
	Glib::ustring val;

	g_assert(_opacity_adjustment);
	g_assert(_opacity_presets);

	_opacity_presets->get_active()->get_value(0, val);

	if (val.compare("Both") == 0) {
		_opacity_adjustment->set_value(0.0);
	} else if (val.compare("Leaves Only") == 0) {
		_opacity_adjustment->set_value(_opacity_adjustment->get_lower());
	} else if (val.compare("Root Only") == 0) {
		_opacity_adjustment->set_value(_opacity_adjustment->get_upper());
	}
}

void MainWindow::_init_actions(void)
{
	// Connect all the signals to the corresponding handlers
	_glade->connect_clicked("quit", sigc::mem_fun(*this, &MainWindow::quit));
	_glade->connect_clicked("open", sigc::mem_fun(*this, &MainWindow::show_open_dialog));
}

void MainWindow::_init_ui(void)
{
	// Get hold of the file chooser dialog.
	_file_chooser_dialog = _glade->get_widget("file_chooser_dialog",
	                                          _file_chooser_dialog);
	g_assert(_file_chooser_dialog);
	_file_chooser_dialog->add_button(Gtk::StockID(Gtk::Stock::OPEN),
	                                 Gtk::RESPONSE_OK);
	_file_chooser_dialog->add_button(Gtk::StockID(Gtk::Stock::CANCEL),
	                                 Gtk::RESPONSE_CANCEL);

	// Initialize the canvas.
	_canvas_eventbox = _glade->get_widget("canvas_eventbox", _canvas_eventbox);
	g_assert(_canvas_eventbox);
	_glade->get_widget_derived("canvas", _tree_model);
	g_assert(_tree_model);
	_tree_model->set_eventbox(*_canvas_eventbox);
	_tree_model->get_terminal_selected_signal().connect(sigc::mem_fun(*this, &MainWindow::_show_terminal_info));
	_tree_model->get_opacity_adjustment_selected_signal().connect(sigc::mem_fun(*this, &MainWindow::show_opacity_adjustment_dialog));

	// Get opacity adjustment dialog.
	_opacity_adjustment_dialog = _glade->get_widget("opacity_adjustment_dialog",
	                                                _opacity_adjustment_dialog);
	g_assert(_opacity_adjustment_dialog);
	_opacity_adjustment_dialog->add_button(Gtk::StockID(Gtk::Stock::OK),
	                                       Gtk::RESPONSE_OK);
	_opacity_adjustment_dialog->add_button(Gtk::StockID(Gtk::Stock::CANCEL),
	                                       Gtk::RESPONSE_CANCEL);
	_opacity_presets = _glade->get_widget("opacity_presets", _opacity_presets);
	g_assert(_opacity_presets);
	_opacity_presets->signal_changed().connect(sigc::mem_fun(*this, &MainWindow::on_opacity_presets_changed));
	_opacity_scale = _glade->get_widget("opacity_scale", _opacity_scale);
	g_assert(_opacity_scale);
	_opacity_adjustment = _opacity_scale->get_adjustment();
	_opacity_adjustment->set_value(_tree_model->get_opacity());
	_opacity_adjustment->signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_opacity_adjustment_value_changed));

	// Get hold of the widgets which will contain the information lists.
	_info_box = _glade->get_widget("info_box", _info_box);
	g_assert(_info_box);
	_glade->get_widget_derived("stats_treeview", _stats_listview);
	_glade->get_widget_derived("trans_treeview", _trans_listview);
	_glade->get_widget_derived("dist_treeview", _dist_listview);
	_glade->get_widget_derived("ngram_treeview", _ngram_listview);
	_glade->get_widget_derived("fert_treeview", _fert_listview);
	g_assert(_stats_listview);
	g_assert(_trans_listview);
	g_assert(_dist_listview);
	g_assert(_ngram_listview);
	g_assert(_fert_listview);
}

void MainWindow::_read_header(void)
{
	header_t *header = NULL;

	header = _parser.parse_header();

	g_assert(_stats_listview);

	if (header) {
		_alignment.set_stats(*header);

		delete header;

		const Glib::RefPtr<List> &list(_alignment.get_stats());
		_stats_listview->set_model(list);
		_stats_listview->append_column("Name", list->get_str_column());
		_stats_listview->append_column("Value", list->get_str_value_column());
	}
}

void MainWindow::_read_language_models(void)
{
	language_model_t *source = NULL;
	language_model_t *target = NULL;

	source = _parser.parse_source_language_model();
	target = _parser.parse_target_language_model();

	if (source) {
		_alignment.set_source_language_model(source);
	}
	if (target) {
		_alignment.set_target_language_model(target);
	}
}

void MainWindow::_read_alignment(void)
{
	alignment_t *source = new alignment_t;
	alignment_t *target = new alignment_t;

	_parser.parse_alignment(*source, *target);

	_alignment.set_alignment(source, target);
}

void MainWindow::_read_translation(void)
{
	translation_t *trans = NULL;

	trans = _parser.parse_translation();

	if (trans) {
		g_assert(_trans_listview);
		_alignment.set_translation(*trans);

		delete trans;
	}
}

void MainWindow::_read_distortion(void)
{
	distortion_t *dist = NULL;

	dist = _parser.parse_distortion();

	if (dist) {
		g_assert(_dist_listview);
		_alignment.set_distortion(*dist);

		delete dist;
	}
}

void MainWindow::_read_ngram(void)
{
	ngram_t *ngram = NULL;

	ngram = _parser.parse_ngram();

	if (ngram) {
		g_assert(_ngram_listview);
		_alignment.set_ngram(*ngram);

		delete ngram;
	}
}

void MainWindow::_read_fertility(void)
{
	fertility_t *fert = NULL;

	fert = _parser.parse_fertility();

	if (fert) {
		g_assert(_fert_listview);
		_alignment.set_fertility(*fert);

		delete fert;
	}
}

void MainWindow::_show_terminal_info(const Terminal &terminal)
{
	static bool is_trans_appended = false;
	static bool is_dist_appended = false;
	static bool is_ngram_appended = false;
	static bool is_fert_appended = false;

	const Glib::RefPtr<List> &trans_list(terminal.get_translation());
	_trans_listview->unset_model();
	if (trans_list) {
		_trans_listview->set_model(trans_list);
		if (!is_trans_appended) {
			_trans_listview->append_column("Translations", trans_list->get_str_column());
			_trans_listview->append_column("Probabilities", trans_list->get_double_value_column());
			is_trans_appended = true;
		}
	}

	const Glib::RefPtr<List> &dist_list(terminal.get_distortion());
	_dist_listview->unset_model();
	if (dist_list) {
		_dist_listview->set_model(dist_list);
		if (!is_dist_appended) {
			_dist_listview->append_column("Distortions", dist_list->get_int_column());
			_dist_listview->append_column("Probabilities", dist_list->get_double_value_column());
			is_dist_appended = true;
		}
	}

	const Glib::RefPtr<List> &ngram_list(terminal.get_ngram());
	_ngram_listview->unset_model();
	if (ngram_list) {
		_ngram_listview->set_model(ngram_list);
		if (!is_ngram_appended) {
			_ngram_listview->append_column("N-gram", ngram_list->get_str_column());
			_ngram_listview->append_column("Probabilities", ngram_list->get_double_value_column());
			is_ngram_appended = true;
		}
	}

	const Glib::RefPtr<List> &fert_list(terminal.get_fertility());
	_fert_listview->unset_model();
	if (fert_list) {
		_fert_listview->set_model(fert_list);
		if (!is_fert_appended) {
			_fert_listview->append_column("Fertilities", fert_list->get_int_column());
			_fert_listview->append_column("Probabilities", fert_list->get_double_value_column());
			is_fert_appended = true;
		}
	}
}
