/**
 * \file color.hpp
 *
 * Encapsulates the Color class.
 */

#ifndef __COLOR_HPP__
#define __COLOR_HPP__

#include <gdkmm/color.h>
#include <cairomm/pattern.h>

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The Color class.
 *
 * This can be directly feed to objects of type Drawable or its descendant.
 */
class Color
{
public:
	/**
	 * \brief Constructor.
	 */
	Color(void);

	/**
	 * \brief Constructor.
	 * \param red The red component.
	 * \param green The green component.
	 * \param blue The blue component.
	 * \param alpha The alpha value.
	 */
	Color(gushort red, gushort green, gushort blue, gdouble alpha = 1.0);

	/**
	 * \brief Constructor.
	 * \param red The red component.
	 * \param green The green component.
	 * \param blue The blue component.
	 * \param alpha The alpha value.
	 */
	Color(gdouble red, gdouble green, gdouble blue, gdouble alpha = 1.0);

	/**
	 * \brief Constructor.
	 * \param value The string specifying the color.
	 */
	Color(const Glib::ustring &value);

	/**
	 * \brief Constructor.
	 *
	 * This constructs a color the same as the given one.
	 *
	 * \param other Another color.
	 */
	Color(const Color &other);

	/**
	 * \brief Destructor.
	 */
	~Color(void);

	Color &operator=(const Color &other);

	/**
	 * \brief Gets the blue component.
	 * \return The blue component.
	 */
	gushort get_blue(void) const;

	/**
	 * \brief Gets the blue component.
	 * \return The blue component, as percentage.
	 */
	gdouble get_blue_p(void) const;

	/**
	 * \brief Gets the green component.
	 * \return The green component.
	 */
	gushort get_green(void) const;

	/**
	 * \brief Gets the green component.
	 * \return The green component, as percentage.
	 */
	gdouble get_green_p(void) const;

	/**
	 * \brief Gets the red component.
	 * \return The red component.
	 */
	gushort get_red(void) const;

	/**
	 * \brief Gets the red component.
	 * \return The red component, as percentage.
	 */
	gdouble get_red_p(void) const;

	/**
	 * \brief Gets the alpha value.
	 * \return The alpha value.
	 */
	gdouble get_alpha(void) const;

	/**
	 * \brief Gets the Cairo Solid Pattern.
	 * \return The Cairo Solid Pattern.
	 */
	Cairo::RefPtr<Cairo::SolidPattern> get_cairo_pattern(void) const;

	/**
	 * \brief Sets the blue component of the color.
	 * \param value The blue component.
	 */
	void set_blue(gushort value);

	/**
	 * \brief Sets the blue component of the color.
	 * \param value The blue component, as percentage.
	 */
	void set_blue_p(gdouble value);

	/**
	 * \brief Sets the green component of the color.
	 * \param value The green component.
	 */
	void set_green(gushort value);

	/**
	 * \brief Sets the green component of the color.
	 * \param value The green component, as percentage.
	 */
	void set_green_p(gdouble value);

	/**
	 * \brief Sets the red component of the color.
	 * \param value The red component.
	 */
	void set_red(gushort value);

	/**
	 * \brief Sets the red component of the color.
	 * \param value The red component, as percentage.
	 */
	void set_red_p(gdouble value);

	/**
	 * \brief Sets the alpha value.
	 * \param value The alpha value.
	 */
	void set_alpha(gdouble value);

	/**
	 * \brief Sets the color.
	 * \param red The red component.
	 * \param green The green component.
	 * \param blue The blue component.
	 */
	void set_rgb(gushort red, gushort green, gushort blue);

	/**
	 * \brief Sets the color.
	 * \param red The red component.
	 * \param green The green component.
	 * \param blue The blue component.
	 * \param alpha The alpha value.
	 */
	void set_rgba(gushort red, gushort green, gushort blue, gdouble alpha);

	/**
	 * \brief Sets the color, with components as percentages.
	 * \param red The red component.
	 * \param green The green component.
	 * \param blue The blue component.
	 */
	void set_rgb_p(gdouble red, gdouble green, gdouble blue);

	/**
	 * \brief Sets the color, with components as percentages.
	 * \param red The red component.
	 * \param green The green component.
	 * \param blue The blue component.
	 * \param alpha The alpha value.
	 */
	void set_rgba_p(gdouble red, gdouble green, gdouble blue, gdouble alpha);

	/**
	 * \brief Sets the color by name.
	 * \param value The color name.
	 */
	void set(const Glib::ustring &value);
private:
	/**
	 * \brief Sets the Cairo Pattern.
	 */
	void _set_cairo_pattern(void);

	gdouble _alpha;				//!< The alpha value.
	Gdk::Color _color;			//!< The Gdk Color.
	Cairo::RefPtr<Cairo::SolidPattern> _pattern; //!< The Cairo Pattern.
};

inline gushort Color::get_blue(void) const
{
	return _color.get_blue();
}

inline gdouble Color::get_blue_p(void) const
{
	return _color.get_blue_p();
}

inline gushort Color::get_green(void) const
{
	return _color.get_green();
}

inline gdouble Color::get_green_p(void) const
{
	return _color.get_green_p();
}

inline gushort Color::get_red(void) const
{
	return _color.get_red();
}

inline gdouble Color::get_red_p(void) const
{
	return _color.get_red_p();
}

inline gdouble Color::get_alpha(void) const
{
	return _alpha;
}

inline Cairo::RefPtr<Cairo::SolidPattern> Color::get_cairo_pattern(void) const
{
	return _pattern;
}

/**
 * \}
 */

#endif
