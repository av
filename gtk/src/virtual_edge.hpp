/**
 * \file virtual_edge.hpp
 *
 * Encapsulates the VirtualEdge class.
 */

#ifndef __VIRTUAL_EDGE_HPP__
#define __VIRTUAL_EDGE_HPP__

#include "draggable_edge.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The VirtualEdge class.
 *
 * This class represents the virtual edges. A virtual edges is an edge which
 * looks like a DraggableEdge, but on which the only operation you can perform
 * is clicking. And two nodes have no relationships if they are only connected
 * by a virtual edge.
 */
class VirtualEdge : public DraggableEdge
{
public:
	/**
	 * \brief Constructor.
	 */
	VirtualEdge(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	~VirtualEdge(void);

	/**
	 * \brief Draws the edge.
	 * \param cr The Cairo context.
	 */
	void draw(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Calculates if the given coordinate pair is in the edge's extents.
	 * \param cr The Cairo context.
	 * \param x The x-coordinate.
	 * \param y The y-coordinate.
	 * \return <code>true</code> if the given coordinate pair is in the edge's
	 * extents.
	 */
	bool is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) const;

	/**
	 * \brief Overrides the original method so that nodes will not be affected.
	 * \param x Dummy variable.
	 * \param y Dummy variable.
	 */
	void drag(gdouble x, gdouble y);

	/**
	 * \brief Overrides the original method so that nodes will not be affected.
	 * \param value Dummy variable.
	 */
	void toggle_visibility(bool value);

	/**
	 * \brief Sets the tree representation indicator.
	 *
	 * A virtual edge only has the matrix representation.
	 *
	 * \param value Dummy variable.
	 */
	void set_is_tree_model(bool value);

	/**
	 * \brief Sets the matrix representation indicator.
	 *
	 * A virtual edge only has the matrix representation.
	 *
	 * \param value Dummy variable.
	 */
	void set_is_matrix_model(bool value);

	/**
	 * \brief Overrides the original method so that nodes will not be affected.
	 * \param x Dummy variable.
	 * \param y Dummy variable.
	 */
	void change_source_follow_mouse(gdouble x, gdouble y);

	/**
	 * \brief Overrides the original method so that nodes will not be affected.
	 * \param x Dummy variable.
	 * \param y Dummy variable.
	 */
	void change_target_follow_mouse(gdouble x, gdouble y);

	/**
	 * \brief Overrides the original method so that nodes will not be affected.
	 */
	void delete_from_nodes(void);
protected:
	/**
	 * \brief Adds this edge to the two Nodes at both ends.
	 *
	 * Both Nodes will also be added to each other's friends list.
	 *
	 * \return <code>true</code> if successful.
	 */
	bool _add_to_nodes(void);

	/**
	 * \brief Recalculates the position of the matrix representation.
	 */
	virtual void _calculate_position(void);
};

inline void VirtualEdge::toggle_visibility(bool value)
{

}

inline void VirtualEdge::set_is_tree_model(bool value)
{
	DraggableEdge::set_is_tree_model(false);
}

inline void VirtualEdge::set_is_matrix_model(bool value)
{
	DraggableEdge::set_is_matrix_model(true);
}

/**
 * \}
 */

#endif
