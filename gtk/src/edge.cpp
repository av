#include <cmath>

#include "edge.hpp"
#include "tree_edge.hpp"

Edge::Edge(const TreeModel &parent) :
	Drawable(parent),
	Transformable(parent),
	_WIDTH(1.5),
	_HIGHLIGHT_WIDTH(4),
	_is_highlight(false)
{
	set_color(Color("black"));
	set_highlight_color(Color("cyan"));
}

Edge::~Edge(void)
{

}

void Edge::set_start(gdouble x, gdouble y, bool emit)
{
	_start = std::make_pair(x, y);

	_update_extents(emit);
}

void Edge::set_start(Drawable *e, bool immediate)
{
	TreeEdge *edge = dynamic_cast<TreeEdge *>(e);
	std::pair<gdouble, gdouble> anchor;

	anchor = edge->get_source_side_anchor();
	set_start(anchor.first, anchor.second);
}

void Edge::set_end(gdouble x, gdouble y, bool emit)
{
	_end = std::make_pair(x, y);

	_update_extents(emit);
}

void Edge::set_end(Drawable *e, bool immediate)
{
	TreeEdge *edge = dynamic_cast<TreeEdge *>(e);
	std::pair<gdouble, gdouble> anchor;

	anchor = edge->get_source_side_anchor();
	set_end(anchor.first, anchor.second);
}

void Edge::draw(Cairo::RefPtr<Cairo::Context> cr)
{
	gdouble x1, y1, x2, y2;

	if (cr) {
		cr->save();
		cr->begin_new_path();

		cr->move_to(_start.first, _start.second);
		cr->line_to(_end.first, _end.second);

		delete _path;
		_path = cr->copy_path();

		if (_is_highlight) {
			cr->set_line_width(_WIDTH + _HIGHLIGHT_WIDTH);
			cr->set_source(_highlight_color.get_cairo_pattern());
			cr->stroke_preserve();
		}

		cr->set_source(_normal_color.get_cairo_pattern());
		cr->set_line_cap(Cairo::LINE_CAP_ROUND);
		cr->set_line_width(_WIDTH);

		cr->get_stroke_extents(x1, y1, x2, y2);
		_set_width(x2 - x1);
		_set_height(y2 - y1);

		cr->stroke();
		cr->restore();
	}
}

bool Edge::is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x,
                        gdouble y) const
{
	bool result = false;

	if (_path) {
		cr->save();
		cr->begin_new_path();
		cr->append_path(*_path);
		cr->set_line_width(_WIDTH + _HIGHLIGHT_WIDTH);
		result = cr->in_stroke(x, y);
		cr->restore();
	}

	return result;
}

void Edge::highlight(void)
{
	highlight(Cairo::RefPtr<Cairo::Context>(), 0.0, 0.0);
}

void Edge::highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y)
{
	_is_highlight = true;

	_set_dirty();
}

void Edge::remove_highlight(void)
{
	remove_highlight(Cairo::RefPtr<Cairo::Context>(), 0.0, 0.0);
}

void Edge::remove_highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x,
                            gdouble y)
{
	_is_highlight = false;

	_set_dirty();
}

void Edge::change_target_angle(gdouble angle)
{
	Transformable::change_target_angle(angle);

	if (angle == (M_PI / 2)) {
		set_is_tree_model(false);
		set_is_matrix_model(true);
	} else if (angle == 0.0) {
		set_is_tree_model(true);
		set_is_matrix_model(false);
	} else {
		set_is_tree_model(true);
		set_is_matrix_model(true);
	}

	_calculate_opacity();
}

void Edge::_calculate_opacity(void)
{
	gdouble angle = 0;
	Color color = get_color();
	Color highlight_color = get_highlight_color();

	if (_source_angle > 0) {
		angle = _source_angle;
	}
	if (_target_angle > 0) {
		angle = _target_angle;
	}

	color.set_alpha(1 - angle / (M_PI / 2));
	highlight_color.set_alpha(color.get_alpha());
	set_color(color);
	set_highlight_color(highlight_color);
}

void Edge::_update_extents(bool emit)
{
	// Calculate the bounding box of the line
	if (_start.first < _end.first) {
		_set_x(_start.first - (_WIDTH + _HIGHLIGHT_WIDTH));
		_set_width(_end.first - _start.first +
		           (_WIDTH + _HIGHLIGHT_WIDTH) * 2);
	} else {
		_set_x(_end.first - (_WIDTH + _HIGHLIGHT_WIDTH));
		_set_width(_start.first - _end.first +
		           (_WIDTH + _HIGHLIGHT_WIDTH) * 2);
	}

	if (_start.second < _end.second) {
		_set_y(_start.second);
		_set_height(_end.second - _start.second +
		            (_WIDTH + _HIGHLIGHT_WIDTH) * 2);
	} else {
		_set_y(_end.second);
		_set_height(_start.second - _end.second +
		            (_WIDTH + _HIGHLIGHT_WIDTH) * 2);
	}

	if (emit) {
		_set_dirty();
	}

	_set_changed();
}
