#include "leaf.hpp"

Leaf::Leaf(TreeModel &parent, Terminal &terminal) :
	Drawable(parent),
	Node(parent, terminal),
	_LINE_WIDTH(1),
	_is_drag(false)
{
	set_text(terminal.get_text());
	set_line_color(Color(0.0, 0.0, 0.0));
}

Leaf::~Leaf(void)
{

}

void Leaf::set_coordinates(gdouble x, gdouble y)
{
	if (!_is_drag) {
		Node::set_coordinates(x, y);
		update_mono_position();
	}
}

void Leaf::set_line_color(Color color)
{
	_line_normal_color = color;
}

void Leaf::draw_line(Cairo::RefPtr<Cairo::Context> cr,
                     gdouble start_x, gdouble start_y,
                     gdouble end_x, gdouble end_y)
{
	std::valarray<double> dashes(1);
	dashes = 3;

	if (cr && is_visible()) {
		cr->save();

		cr->begin_new_path();

		cr->move_to(start_x, start_y);
		cr->line_to(end_x, end_y);

		cr->set_source(_line_normal_color.get_cairo_pattern());
		cr->set_line_width(_LINE_WIDTH);
		cr->set_dash(dashes, 0.0);
		cr->stroke();
		cr->restore();
	}
}

void Leaf::drag(gdouble x, gdouble y)
{
	const NonTerminal *parent = get_nonterminal().get_parent();

	_is_drag = true;

	_set_x(x - _get_width() / 2);
	_set_y(y);

	if (parent && parent->get_node()) {
		parent->get_node()->update_position();
	}

	_set_dirty();
}

void Leaf::stop_drag(void)
{
	Color color = get_color();

	_is_drag = false;

	color.set_alpha(1.0);
	set_color(color);
}
