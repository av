/**
 * \file list_view.hpp
 *
 * Encapsulates the ListView class.
 */

#ifndef __LIST_VIEW_HPP__
#define __LIST_VIEW_HPP__

#include <gtkmm/treeview.h>
#include <libglademm.h>

#include "list.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The ListView class which displays the List contents.
 */
class ListView : public Gtk::TreeView
{
public:
	/**
	 * \brief Constructor.
	 * \param cobject The C object.
	 * \param glade The glade object.
	 */
	ListView(GtkTreeView *cobject, Glib::RefPtr<Gnome::Glade::Xml> &glade);

	/**
	 * \brief Destructor.
	 */
	~ListView(void);
};

/**
 * \}
 */

#endif
