#include <valarray>

#include "node.hpp"
#include "draggable_edge.hpp"
#include "tree_edge.hpp"
#include "tree_model.hpp"

bool _is_in_mono = false; //!< <code>true</code> if dragging the mono representation or the matrix representation.

bool is_right_of(const TreeEdge *i, const TreeEdge *e)
{
	if (i && e) {
		if (i->get_start().first > e->get_start().first) {
			return true;
		}
	}

	return false;
}

Node::Node(TreeModel &parent, NonTerminal &n) :
	Drawable(parent),
	Selectable(parent),
	Transformable(parent),
	_DEFAULT_WIDTH(15),
	_BORDER_WIDTH(0.5),
	_MONO_BRACKET_LENGTH(3),
	_PADDING(2),
	_SPACING(3),
	_MONO_SPACING(4),
	_LEVEL_SPACING(50),
	_spacing_before(_SPACING * 2),
	_spacing_after(_SPACING * 2),
	_level_spacing(_LEVEL_SPACING),
	_mono_x(0),
	_mono_y(0),
	_mono_width(0),
	_mono_height(0),
	_topleft_x(0),
	_topleft_y(0),
	_topright_x(0),
	_topright_y(0),
	_bottomleft_x(0),
	_bottomleft_y(0),
	_bottomright_x(0),
	_bottomright_y(0),
	_mtopleft_x(0),
	_mtopleft_y(0),
	_mtopright_x(0),
	_mtopright_y(0),
	_mbottomleft_x(0),
	_mbottomleft_y(0),
	_mbottomright_x(0),
	_mbottomright_y(0),
	_layout(parent.create_pango_layout("")),
	_font(new Pango::FontDescription()),
	_mono_path(NULL),
	_is_highlight(false),
	_is_flipped(false),
	_nonterminal(n),
	_drag(NONE),
	_force_mono_off(false),
	_is_mono_visible(true),
	_is_virtual(false),
	_rect(NULL),
	_collapsed(NULL),
	_position(CENTER)
{
	_nonterminal.set_node(this);

	set_font("Sans", 12);
	set_color(Color(0.8, 0.5, 0.5));
	set_highlight_color(Color(0.6, 0.8, 0.6));

	_update_extents();
}

Node::~Node(void)
{
	delete _font;
	delete _mono_path;
	delete _rect;
}

bool Node::is_drag(void) const
{
	return _is_in_mono;
}

void Node::get_top_anchor(gdouble &x, gdouble &y) const
{
	x = _get_x() + _get_width() / 2;
	y = _get_y();
}

void Node::get_bottom_anchor(gdouble &x, gdouble &y) const
{
	x = _get_x() + _get_width() / 2;
	y = _get_y() + _get_height();
}

void Node::get_left_anchor(gdouble &x, gdouble &y) const
{
	x = _get_x();
	y = _get_y() + _get_height() / 2;
}

void Node::get_right_anchor(gdouble &x, gdouble &y) const
{
	x = _get_x() + _get_width();
	y = _get_y() + _get_height() / 2;
}

void Node::get_centroid_anchor(gdouble &x, gdouble &y) const
{
	x = _get_x() + _get_width() / 2;
	y = _get_y() + _get_height() / 2;
}

const TreeEdge *Node::get_leftmost_tree_edge(void) const
{
	if (!_tree_edges.empty()) {
		return *_tree_edges.begin();
	} else {
		return NULL;
	}
}

const TreeEdge *Node::get_rightmost_tree_edge(void) const
{
	if (!_tree_edges.empty()) {
		return *_tree_edges.rbegin();
	} else {
		return NULL;
	}
}

void Node::set_position(Node::position_t val)
{
	_position = val;

	update_position(true);
}

void Node::set_virtuality(bool val)
{
	_is_virtual = val;

	if (_is_virtual) {
		set_color(Color("red"));
	} else {
		set_color(Color(0.8, 0.5, 0.5));
	}

	_set_dirty();
}

void Node::set_mono_visibility(bool val)
{
	_force_mono_off = !val;
	_is_mono_visible = val;
}

void Node::set_text(const Glib::ustring &text)
{
	_nonterminal.set_text(text);
	_layout->set_text(text);

	_update_extents();
}

void Node::set_font(const Glib::ustring &family, gint size)
{
	if (family.size() > 0) {
		_font->set_family(family);
	}

	if (size > 0) {
		_font->set_size(size * Pango::SCALE);
	}

	_layout->set_font_description(*_font);
}

void Node::set_coordinates(gdouble x, gdouble y)
{
	const NonTerminal *parent = get_nonterminal().get_parent();

	_set_x(x);
	_set_y(y);

	if (parent && parent->get_node()) {
		parent->get_node()->update_position();
	}
}

void Node::set_matrix_coordinates(std::pair<gdouble, gdouble> tl,
                                  std::pair<gdouble, gdouble> tr,
                                  std::pair<gdouble, gdouble> bl,
                                  std::pair<gdouble, gdouble> br)
{
	if (get_is_virtual()) {
		return;
	}

	if (get_type() == LanguageModel::SOURCE) {
		_mtopleft_x = _topleft_x;
		_mtopright_x = _topright_x;
		_mbottomleft_x = _bottomleft_x;
		_mbottomright_x = _bottomright_x;
		_mtopleft_y = tl.second - (_topleft_x - tl.first) / tan(_target_angle);
		_mtopright_y = tr.second - (_topright_x - tr.first) / tan(_target_angle);
		_mbottomleft_y = bl.second - (_bottomleft_x - bl.first) / tan(_target_angle);
		_mbottomright_y = br.second - (_bottomright_x - br.first) / tan(_target_angle);
	} else {
		_mtopleft_x = tl.first;
		_mtopright_x = tr.first;
		_mbottomleft_x = bl.first;
		_mbottomright_x = br.first;
		_mtopleft_y = _topleft_y - (tl.first - _topleft_x) / tan(_target_angle);
		_mtopright_y = _topright_y - (tr.first - _topright_x) / tan(_target_angle);
		_mbottomleft_y = _bottomleft_y - (bl.first - _bottomleft_x) / tan(_target_angle);
		_mbottomright_y = _bottomright_y - (br.first - _bottomright_x) / tan(_target_angle);
	}
}

void Node::set_flipped(bool flipped)
{
	_is_flipped = flipped;

	update_position(false);
}

void Node::set_spacing_before(gdouble spacing)
{
	if (fabs(spacing - _spacing_before) < 0.1) {
		return;
	}

	if (spacing < (_SPACING * 2)) {
		_spacing_before = _SPACING * 2;
	} else {
		_spacing_before = spacing + _SPACING;
	}
}

void Node::set_spacing_after(gdouble spacing)
{
	if (fabs(spacing - _spacing_after) < 0.1) {
		return;
	}

	if (spacing < (_SPACING * 2)) {
		_spacing_after = _SPACING * 2;
	} else {
		_spacing_after = spacing + _SPACING;
	}
}

void Node::set_level_spacing(gdouble spacing)
{
	if (spacing <= 0) {
		_level_spacing = _LEVEL_SPACING;
	} else if (fabs(spacing - _level_spacing) < 0.1) {
		return;
	} else {
		_level_spacing = spacing;
	}

	update_position(true);
}

void Node::set_collapsed(Node *val)
{
	_collapsed = val;
	bool visibility = true;
	const std::list<NonTerminal *> *l = &get_nonterminal().get_friends();
	std::list<NonTerminal *>::const_iterator iter;
	const NonTerminal *parent = get_nonterminal().get_parent();
	Node *n = NULL;

	if (_collapsed) {
		if (_collapsed != this) {
			visibility = false;
		}
	} else {
		_collapsed_nodes.clear();
	}

	toggle_visibility(visibility);
	set_mono_visibility(visibility);
	set_matrix_visibility(visibility);

	for (iter = l->begin(); iter != l->end(); iter++) {
		n = (*iter)->get_node();
		n->set_matrix_visibility(visibility);
	}
	while (parent) {
		n = parent->get_node();
		l = &parent->get_friends();
		parent = parent->get_parent();
		n->set_matrix_visibility(visibility);
		for (iter = l->begin(); iter != l->end(); iter++) {
			n = (*iter)->get_node();
			n->set_matrix_visibility(visibility);
		}
	}

	update_position();
}

void Node::add_collapsed_node(Node &val)
{
	_collapsed_nodes.push_back(&val);
}

bool Node::add_edge(DraggableEdge &edge)
{
	if (!_has_edge(edge)) {
		_edges.push_back(&edge);
		return true;
	} else {
		return false;
	}
}

bool Node::add_edge(TreeEdge &edge)
{
	std::list<TreeEdge *>::iterator iter;

	if (!_has_tree_edge(edge)) {
		iter = std::find_if(_tree_edges.begin(), _tree_edges.end(),
		                    std::bind2nd(std::ptr_fun(is_right_of), &edge));

		_tree_edges.insert(iter, &edge);

		update_position();

		return true;
	} else {
		return false;
	}
}

void Node::delete_edge(const DraggableEdge &edge)
{
	std::list<DraggableEdge *>::iterator iter;

	iter = std::find(_edges.begin(), _edges.end(), &edge);

	if (iter != _edges.end()) {
		_edges.erase(iter);
	}
}

void Node::delete_edge(const TreeEdge &edge)
{
	std::list<TreeEdge *>::iterator iter;

	iter = std::find(_tree_edges.begin(), _tree_edges.end(), &edge);

	if (iter != _tree_edges.end()) {
		_tree_edges.erase(iter);

		update_position();
	}
}

void Node::update_position(bool emit)
{
	Node *left, *right;
	gdouble cx = 0;
	gdouble cy = 0;
	gdouble x, y;
	gdouble d, dx, dy;
	gdouble angle;
	gint sign = 1;
	gint height = get_nonterminal().get_height();

	if (!is_visible() || get_is_virtual() ||
	    get_nonterminal().get_children().empty()) {
		return;
	}

	if (_collapsed && _collapsed != this) {
		set_coordinates(_collapsed->get_extents_x(),
		                _collapsed->get_extents_y());

		return;
	}

	if (get_type() == LanguageModel::SOURCE) {
		angle = _source_angle;
	} else {
		angle = _target_angle;
	}

	if (_is_flipped) {
		sign = -sign;
	}

	left = get_nonterminal().get_leftmost_leaf()->get_node();
	right = get_nonterminal().get_rightmost_leaf()->get_node();

	if (_position == BEGIN || _position == CENTER) {
		cx += left->get_extents_x() + left->get_extents_width() * cos(angle) / 2;
		cy += left->get_extents_y();
	}
	if (_position == END || _position == CENTER) {
		cx += right->get_extents_x() +
		    right->get_extents_width() * cos(angle) / 2;
		cy += right->get_extents_y() +
		    right->get_extents_height() * cos(angle) / 2;
	}
	if (_position == CENTER) {
		cy += left->get_extents_height() * cos(angle) / 2;
		cx /= 2;
		cy /= 2;
	}
	d = sign * height * _level_spacing;
	dx = d * sin(angle);
	dy = d * cos(angle);

	_set_x(cx - dx - _get_width() / 2);
	if (get_type() == LanguageModel::SOURCE) {
		_set_y(cy - dy - (sign + 1) / 2 * _get_height());
	} else {
		_set_y(cy + dy - (1 - sign) / 2 * _get_height());
	}

	update_mono_position(false);

	if (emit) {
		_set_dirty();
	}
}

void Node::update_mono_position(bool emit)
{
	Node *left = get_nonterminal().get_leftmost_leaf()->get_node();
	Node *right = get_nonterminal().get_rightmost_leaf()->get_node();
	gdouble x, y;
	gdouble spacing = _MONO_SPACING * (get_nonterminal().get_height() + 1);
	gdouble w = left->get_extents_width() / 2;
	gdouble h = left->get_extents_height() / 2;
	gdouble angle = 0;

	if (get_is_virtual()) {
		return;
	}

	if (_force_mono_off || !left->is_visible() || !right->is_visible()) {
		_is_mono_visible = false;
	} else {
		_is_mono_visible = true;
	}

	if (get_type() == LanguageModel::SOURCE) {
		angle = _source_angle;
	} else {
		angle = _target_angle;
	}

	if (_drag == NONE) {
		left->get_centroid_anchor(x, y);
		_topleft_x = x - (spacing + w) * cos(angle) + spacing * sin(angle);
		_bottomleft_x = x - (spacing + w) * cos(angle) - spacing * sin(angle);
		_topleft_y = y - (spacing + h) * sin(angle) - spacing * cos(angle);
		_bottomleft_y = y - (spacing + h) * sin(angle) + spacing * cos(angle);

		if (spacing > left->get_spacing_before()) {
			left->set_spacing_before(spacing);
		}

		w = right->get_extents_width() / 2;
		h = right->get_extents_height() / 2;

		right->get_centroid_anchor(x, y);
		_topright_x = x + (spacing + w) * cos(angle) + spacing * sin(angle);
		_bottomright_x = x + (spacing + w) * cos(angle) - spacing * sin(angle);
		_topright_y = y + (spacing + h) * sin(angle) - spacing * cos(angle);
		_bottomright_y = y + (spacing + h) * sin(angle) + spacing * cos(angle);

		if (spacing > right->get_spacing_after()) {
			right->set_spacing_after(spacing);
		}
	}

	if (get_type() == LanguageModel::SOURCE) {
		const std::list<NonTerminal *> &l = get_nonterminal().get_friends();
		std::list<NonTerminal *>::const_iterator iter;
		for (iter = l.begin(); iter != l.end(); iter++) {
			Node *n = (*iter)->get_node();

			set_matrix_coordinates(n->get_bottomleft(), n->get_topleft(), n->get_bottomright(), n->get_topright());
			n->set_matrix_coordinates(get_topright(), get_bottomright(), get_topleft(), get_bottomleft());
		}
	} else {
		const std::list<NonTerminal *> &l = get_nonterminal().get_friends();
		std::list<NonTerminal *>::const_iterator iter;
		for (iter = l.begin(); iter != l.end(); iter++) {
			Node *n = (*iter)->get_node();

			set_matrix_coordinates(n->get_topright(), n->get_bottomright(), n->get_topleft(), n->get_bottomleft());
			n->set_matrix_coordinates(get_bottomleft(), get_topleft(), get_bottomright(), get_topright());
		}
	}

	if (emit) {
		_set_dirty();
	}
}

void Node::draw(Cairo::RefPtr<Cairo::Context> cr)
{
	gdouble x1, y1, x2, y2;
	Glib::RefPtr<Pango::LayoutLine> line;

	if (cr) {
		if (is_visible()) {
			// Draw a rectangular box around the text
			cr->save();

			cr->begin_new_path();
			cr->rectangle(_get_x() + _BORDER_WIDTH / 2,
			              _get_y() + _BORDER_WIDTH / 2,
			              _get_width() - _BORDER_WIDTH,
			              _get_height() - _BORDER_WIDTH);

			delete _path;
			_path = cr->copy_path();

			if (_is_highlight) {
				cr->set_source(_highlight_color.get_cairo_pattern());
				cr->fill_preserve();
				cr->set_source(_normal_color.get_cairo_pattern());
			} else {
				cr->set_source(_normal_color.get_cairo_pattern());
				cr->fill_preserve();
				cr->set_source(_highlight_color.get_cairo_pattern());
			}
			cr->set_line_width(_BORDER_WIDTH);

			cr->get_stroke_extents(x1, y1, x2, y2);
			_set_width(x2 - x1);
			_set_height(y2 - y1);

			cr->stroke();
			cr->restore();

			cr->save();

			if (_collapsed) {
				cr->begin_new_path();
				cr->move_to(_get_x() + _get_width() / 2,
				            _get_y() + _get_height() / 8);
				cr->line_to(_get_x() + _get_width() / 2,
				            _get_y() + _get_height() * 7 / 8);
				cr->move_to(_get_x() + _get_width() / 8,
				            _get_y() + _get_height() / 2);
				cr->line_to(_get_x() + _get_width() * 7 / 8,
				            _get_y() + _get_height() / 2);
				cr->set_line_width(_BORDER_WIDTH * 3);
				cr->stroke();
			} else {
				cr->translate(_get_x() + _PADDING + _BORDER_WIDTH / 2,
				              _get_y() + _get_height() - _BORDER_WIDTH / 2 -
				              _descent - _PADDING);

				line = _layout->get_line(0);
				if (line) {
					line->show_in_cairo_context(cr);
				}
			}

			cr->restore();

			draw_mono(cr);
		}

		draw_matrix(cr);
	}
}

void Node::draw_matrix(Cairo::RefPtr<Cairo::Context> cr)
{
	gdouble x1, y1, x2, y2;

	if (!get_is_matrix_visible() || _target_angle <= 0.05 ||
	    (_mtopright_x == _mbottomleft_x && _mtopleft_y == _mbottomright_y)) {
		return;
	}

	if (cr) {
		cr->save();

		_draw_brackets(cr, std::make_pair(_mtopleft_x, _mtopleft_y),
		               std::make_pair(_mtopright_x, _mtopright_y),
		               std::make_pair(_mbottomleft_x, _mbottomleft_y),
		               std::make_pair(_mbottomright_x, _mbottomright_y),
		               &_matrix_path);

		cr->get_stroke_extents(x1, y1, x2, y2);
		_matrix_x = x1;
		_matrix_y = y1;
		_matrix_width = x2 - x1;
		_matrix_height = y2 - y1;

		cr->begin_new_path();
		cr->restore();
	}
}

void Node::draw_mono(Cairo::RefPtr<Cairo::Context> cr)
{
	gdouble x1, y1, x2, y2;

	if (!is_visible() || !get_is_mono_visible() || get_nonterminal().is_leaf()) {
		return;
	}

	if (cr) {
		cr->save();

		_draw_brackets(cr, std::make_pair(_topleft_x, _topleft_y),
		               std::make_pair(_topright_x, _topright_y),
		               std::make_pair(_bottomleft_x, _bottomleft_y),
		               std::make_pair(_bottomright_x, _bottomright_y),
		               &_mono_path);

		cr->get_stroke_extents(x1, y1, x2, y2);
		_mono_x = x1;
		_mono_y = y1;
		_mono_width = x2 - x1;
		_mono_height = y2 - y1;

		cr->begin_new_path();
		cr->restore();
	}
}

void Node::_draw_brackets(Cairo::RefPtr<Cairo::Context> cr,
                          std::pair<gdouble, gdouble> tl,
                          std::pair<gdouble, gdouble> tr,
                          std::pair<gdouble, gdouble> bl,
                          std::pair<gdouble, gdouble> br,
                          Cairo::Path **path)
{
	gdouble angle = 0;

	if (get_type() == LanguageModel::SOURCE) {
		angle = _source_angle;
	} else {
		angle = _target_angle;
	}

	if (cr) {
		cr->begin_new_path();

		cr->move_to(tl.first + _MONO_BRACKET_LENGTH * cos(angle),
		            tl.second + _MONO_BRACKET_LENGTH * sin(angle));
		cr->line_to(tl.first, tl.second);
		cr->line_to(bl.first, bl.second);
		cr->line_to(bl.first + _MONO_BRACKET_LENGTH * cos(angle),
		            bl.second + _MONO_BRACKET_LENGTH * sin(angle));

		cr->move_to(tr.first - _MONO_BRACKET_LENGTH * cos(angle),
		            tr.second - _MONO_BRACKET_LENGTH * sin(angle));
		cr->line_to(tr.first, tr.second);
		cr->line_to(br.first, br.second);
		cr->line_to(br.first - _MONO_BRACKET_LENGTH * cos(angle),
		            br.second - _MONO_BRACKET_LENGTH * sin(angle));

		if (path) {
			delete *path;
			*path = cr->copy_path();
		}

		cr->set_line_width(_BORDER_WIDTH * 3);
		if (_is_highlight) {
			cr->set_source(_highlight_color.get_cairo_pattern());
		} else {
			cr->set_source(_normal_color.get_cairo_pattern());
		}
		cr->stroke_preserve();
	}
}

bool
Node::is_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y) const
{
	bool result = false;

	if (is_visible() && get_is_tree_model() && _path) {
		cr->save();
		cr->begin_new_path();
		cr->append_path(*_path);
		cr->set_line_width(_BORDER_WIDTH);
		result = cr->in_fill(x, y) || cr->in_stroke(x, y);
		cr->restore();

		if (!result && _mono_path) {
			cr->save();
			cr->begin_new_path();
			cr->append_path(*_mono_path);
			cr->set_line_width(_BORDER_WIDTH * 3);
			result = cr->in_stroke(x, y);
			_is_in_mono = result;
			cr->restore();
		}
	}

	if (!result && get_is_matrix_visible() && get_is_matrix_model() &&
	    _matrix_path) {
		cr->save();
		cr->begin_new_path();
		cr->append_path(*_matrix_path);
		cr->set_line_width(_BORDER_WIDTH * 3);
		result = cr->in_stroke(x, y);
		_is_in_mono = result;
		cr->restore();
	}

	return result;
}

bool Node::is_x_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble x) const
{
	bool result = false;

	if (is_visible() && _path) {
		cr->save();
		cr->begin_new_path();
		cr->append_path(*_path);
		cr->set_line_width(_BORDER_WIDTH);
		result = cr->in_fill(x, _get_y() + _get_height() / 2);
		cr->restore();
	}

	return result;
}

bool Node::is_y_in_region(Cairo::RefPtr<Cairo::Context> cr, gdouble y) const
{
	bool result = false;

	if (is_visible() && _path) {
		cr->save();
		cr->begin_new_path();
		cr->append_path(*_path);
		cr->set_line_width(_BORDER_WIDTH);
		result = cr->in_fill(_get_x() + _get_width() / 2, y);
		cr->restore();
	}

	return result;
}

void Node::highlight(void)
{
	highlight(Cairo::RefPtr<Cairo::Context>(), 0.0, 0.0);
}

void Node::highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y)
{
	std::list<DraggableEdge *>::iterator diter;
	std::list<TreeEdge *>::iterator titer;
	std::list<NonTerminal *>::const_iterator iter;

	if (!_is_highlight) {
		_is_highlight = true;

		for (diter = _edges.begin(); diter != _edges.end(); diter++) {
			(*diter)->highlight();
		}

		for (titer = _tree_edges.begin(); titer != _tree_edges.end(); titer++) {
			(*titer)->highlight();
		}

		for (iter = get_nonterminal().get_children().begin();
		     iter != get_nonterminal().get_children().end(); iter++) {
			(*iter)->get_node()->highlight();
		}

		for (iter = get_nonterminal().get_friends().begin();
		     iter != get_nonterminal().get_friends().end(); iter++) {
			(*iter)->get_node()->highlight();
		}

		_set_dirty();
	}
}

void Node::remove_highlight(void)
{
	remove_highlight(Cairo::RefPtr<Cairo::Context>(), 0.0, 0.0);
}

void
Node::remove_highlight(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y)
{
	std::list<DraggableEdge *>::iterator diter;
	std::list<TreeEdge *>::iterator titer;
	std::list<NonTerminal *>::const_iterator iter;

	if (_is_highlight) {
		_is_highlight = false;

		for (diter = _edges.begin(); diter != _edges.end(); diter++) {
			(*diter)->remove_highlight();
		}

		for (titer = _tree_edges.begin(); titer != _tree_edges.end(); titer++) {
			(*titer)->remove_highlight();
		}

		for (iter = get_nonterminal().get_children().begin();
		     iter != get_nonterminal().get_children().end(); iter++) {
			(*iter)->get_node()->remove_highlight();
		}

		for (iter = get_nonterminal().get_friends().begin();
		     iter != get_nonterminal().get_friends().end(); iter++) {
			(*iter)->get_node()->remove_highlight();
		}

		_set_dirty();
	}
}

void Node::drag(gdouble x, gdouble y)
{
	gdouble angle = 0;
	gdouble ldist, rdist;

	if (get_type() == LanguageModel::SOURCE) {
		angle = _source_angle;
	} else {
		angle = _target_angle;
	}

	if (_is_in_mono && _drag == NONE) {
		ldist = sqrt(pow(x - (_topleft_x + _bottomleft_x) / 2, 2) +
		             pow(y - (_topleft_y + _bottomleft_y) / 2, 2));
		rdist = sqrt(pow(x - (_topright_x + _bottomright_x) / 2, 2) +
		             pow(y - (_topright_y + _bottomright_y) / 2, 2));

		if (ldist < rdist) {
			_drag = LEFT;
		} else {
			_drag = RIGHT;
		}
	}

	if (_drag == LEFT) {
		_topleft_x += (x - _topleft_x) * cos(angle);
		_topleft_y += (y - _topleft_y) * sin(angle);
		_bottomleft_x += (x - _bottomleft_x) * cos(angle);
		_bottomleft_y += (y - _bottomleft_y) * sin(angle);

		_mtopleft_x += (x - _mtopleft_x) * cos(angle);
		_mtopleft_y += (y - _mtopleft_y) * sin(angle);
		_mbottomleft_x += (x - _mbottomleft_x) * cos(angle);
		_mbottomleft_y += (y - _mbottomleft_y) * sin(angle);
	} else if (_drag == RIGHT) {
		_topright_x += (x - _topright_x) * cos(angle);
		_topright_y += (y - _topright_y) * sin(angle);
		_bottomright_x += (x - _bottomright_x) * cos(angle);
		_bottomright_y += (y - _bottomright_y) * sin(angle);

		_mtopright_x += (x - _mtopright_x) * cos(angle);
		_mtopright_y += (y - _mtopright_y) * sin(angle);
		_mbottomright_x += (x - _mbottomright_x) * cos(angle);
		_mbottomright_y += (y - _mbottomright_y) * sin(angle);
	}
}

void Node::stop_drag(gdouble x, gdouble y)
{
	std::list<Terminal *> &terminals = get_nonterminal().get_language_model().get_terminals();
	const std::list<NonTerminal *> &children = get_nonterminal().get_children();
	std::list<Terminal *>::iterator iter;
	std::list<NonTerminal *>::const_iterator citer;
	std::list<Terminal *>::iterator lterminal = terminals.end();
	std::list<Terminal *>::iterator rterminal = terminals.end();
	NonTerminal *parent = NULL;
	NonTerminal *lprevious = NULL;
	NonTerminal *rprevious = NULL;
	Node *node = NULL;
	gdouble nx, ny, nw, nh;
	gdouble lx, rx;

	if (_drag == NONE) {
		return;
	}

	if (_drag == LEFT) {
		lx = x;
		rx = _topright_x;
	} else {
		lx = _topleft_x;
		rx = x;
	}

	for (iter = terminals.begin(); iter != terminals.end(); iter++) {
		node = (*iter)->get_node();

		nx = node->get_extents_x();
		ny = node->get_extents_y();
		nw = node->get_extents_width();
		nh = node->get_extents_height();

		if (lterminal == terminals.end() && lx <= nx) {
			lterminal = iter;
		}

		if (rterminal == terminals.end() && rx <= nx) {
			if (iter != terminals.begin()) {
				rterminal = iter;
				rterminal--;
			} else {
				rterminal = iter;
			}
		}

		if (lterminal != terminals.end() && rterminal != terminals.end()) {
			break;
		}
	}

	if (rterminal == terminals.end()) {
		rterminal = --iter;
	}

	for (parent = (*lterminal)->get_parent(), lprevious = *lterminal;
	     parent && parent != &get_nonterminal();
	     lprevious = parent, parent = parent->get_parent()) {
		if (parent->get_node()->get_topleft().first < lx) {
			break;
		}
	}

	for (parent = (*rterminal)->get_parent(), rprevious = *rterminal;
	     parent && parent != &get_nonterminal();
	     rprevious = parent, parent = parent->get_parent()) {
		if (parent->get_node()->get_topright().first > rx) {
			break;
		}
	}

	if (lprevious->is_ascendant(*rprevious) ||
	    rprevious->is_ascendant(*lprevious) ||
	    lprevious == rprevious) {
		_topleft_x = lx;
		_bottomleft_x = lx;
		_topright_x = rx;
		_bottomright_x = rx;

		set_virtuality(true);

		_drag = NONE;

		return;
	}

	if (get_is_virtual()) {
		set_virtuality(false);
	}

	for (citer = children.begin(); citer != children.end(); citer++) {
		(*citer)->set_parent(NULL);
	}

	get_nonterminal().delete_all_children();
	get_nonterminal().add_child(*lprevious);
	lprevious->set_parent(get_nonterminal());
	get_nonterminal().add_child(*rprevious);
	rprevious->set_parent(get_nonterminal());

	for (iter = lterminal, iter++; iter != rterminal; iter++) {
		if (!(*iter)->is_ascendant(get_nonterminal())) {
			parent = (*iter)->get_root();
			if (!get_nonterminal().is_ascendant(*parent)) {
				parent->set_parent(get_nonterminal());
				get_nonterminal().add_child(*parent);
			}
		}
	}

	for (parent = get_nonterminal().get_parent(); parent;
	     parent = parent->get_parent()) {
		parent->get_node()->update_position();
	}

	_drag = NONE;
}

void Node::select(void)
{
	select(Cairo::RefPtr<Cairo::Context>(), 0.0, 0.0);
}

void Node::select(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y)
{
	highlight();
}

void Node::deselect(void)
{
	deselect(Cairo::RefPtr<Cairo::Context>(), 0.0, 0.0);
}

void Node::deselect(Cairo::RefPtr<Cairo::Context> cr, gdouble x, gdouble y)
{
	remove_highlight(cr, x, y);
}

void Node::_set_rect(void)
{
	Transformable::_set_rect();

	delete _rect;
	_rect = new Gdk::Rectangle(gint(_parent._c2d_x(floor(_mono_x))),
	                           gint(_parent._c2d_y(floor(_mono_y))),
	                           gint(_parent._c2d_w(ceil(_mono_width))),
	                           gint(_parent._c2d_h(ceil(_mono_height))));
	_rect->join(Transformable::get_rect());
}

bool Node::_has_edge(const DraggableEdge &edge) const
{
	std::list<DraggableEdge *>::const_iterator iter;

	iter = std::find(_edges.begin(), _edges.end(), &edge);

	if (iter != _edges.end()) {
		return true;
	}

	return false;
}

bool Node::_has_tree_edge(const TreeEdge &edge) const
{
	std::list<TreeEdge *>::const_iterator iter;

	iter = std::find(_tree_edges.begin(), _tree_edges.end(), &edge);

	if (iter != _tree_edges.end()) {
		return true;
	}

	return false;
}

void Node::_update_extents(bool emit)
{
	Glib::RefPtr<Pango::LayoutLine> line;
	Pango::Rectangle ink_extents;
	Pango::Rectangle extents;

	line = _layout->get_line(0);
	if (line) {
		line->get_extents(ink_extents, extents);
		_descent = extents.get_descent() / Pango::SCALE;
		if (_layout->get_text().empty()) {
			_set_width(_DEFAULT_WIDTH);
		} else {
			_set_width(extents.get_width() / Pango::SCALE);
		}
		_set_width(_get_width() + _PADDING * 2 + _BORDER_WIDTH);
		_set_height(extents.get_height() / Pango::SCALE + _PADDING * 2 +
		            _BORDER_WIDTH);
	}

	update_position(emit);
}

void Node::change_target_angle(gdouble angle)
{
	Transformable::change_target_angle(angle);

	if (angle == 0.0) {
		set_is_matrix_model(false);
	} else {
		set_is_matrix_model(true);
	}

	update_mono_position();
}
