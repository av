/**
 * \defgroup Models Models
 * \brief Non-UI classes and functions.
 */

/**
 * \file alignment.hpp
 *
 * Encapsulates the Alignment class.
 */

#ifndef __ALIGNMENT_HPP__
#define __ALIGNMENT_HPP__

#include "language_model.hpp"
#include "list.hpp"
#include "parser.hpp"

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The alignment model which contains all the alignment information.
 */
class Alignment
{
public:
	/**
	 * \brief Constructor.
	 */
	Alignment(void);

	/**
	 * \brief Destructor.
	 */
	~Alignment(void);

	/**
	 * \brief Gets the general information of the alignment.
	 * \return The general information of the alignment.
	 */
	const Glib::RefPtr<List> &get_stats(void) const;

	/**
	 * \brief Gets the source language_model.
	 * \return The source language_model.
	 */
	LanguageModel &get_source(void);

	/**
	 * \brief Gets the target language_model.
	 * \return The target language_model.
	 */
	LanguageModel &get_target(void);

	/**
	 * \brief Gets the alignment information.
	 * \param[out] source The alignment information of the source language_model.
	 * \param[out] target The alignment information of the target language_model.
	 */
	void get_alignment(const alignment_t **source, const alignment_t **target) const;

	/**
	 * \brief Sets the general information of the alignment.
	 * \param header The general information.
	 */
	void set_stats(const header_t &header);

	/**
	 * \brief Sets the source language_model.
	 * \param source The source language_model.
	 */
	void set_source_language_model(language_model_t *source);

	/**
	 * \brief Sets the target language_model.
	 * \param target The target language_model.
	 */
	void set_target_language_model(language_model_t *target);

	/**
	 * \brief Sets the alignment information.
	 * \param source The alignment information of the source language_model.
	 * \param target The alignment information of the target language_model.
	 */
	void set_alignment(alignment_t *source, alignment_t *target);

	/**
	 * \brief Sets the translation probabilities.
	 * \param trans The translation probabilities.
	 */
	void set_translation(const translation_t &trans);

	/**
	 * \brief Sets the distortion probabilities.
	 * \param dist The distortion probabilities.
	 */
	void set_distortion(const distortion_t &dist);

	/**
	 * \brief Sets the N-gram probabilities.
	 * \param ngram The N-gram probabilities.
	 */
	void set_ngram(const ngram_t &ngram);

	/**
	 * \brief Sets the fertility probabilities.
	 * \param fert The fertility probabilities.
	 */
	void set_fertility(const fertility_t &fert);
private:
	Glib::RefPtr<List> _ref_stats_list; //!< The general information of the alignment.

	LanguageModel *_source;			//!< The source language_model.
	LanguageModel *_target;			//!< The target language_model.
	alignment_t *_source_alignment; //!< The alignment information of the source language_model.
	alignment_t *_target_alignment; //!< The alignment information of the target language_model.
};

inline const Glib::RefPtr<List> &Alignment::get_stats(void) const
{
	return _ref_stats_list;
}

inline LanguageModel &Alignment::get_source(void)
{
	return *_source;
}

inline LanguageModel &Alignment::get_target(void)
{
	return *_target;
}

inline void Alignment::get_alignment(const alignment_t **source,
                                     const alignment_t **target) const
{
	if (source) {
		*source = _source_alignment;
	}

	if (target) {
		*target = _target_alignment;
	}
}

/**
 * \}
 */

#endif
