#include "alignment.hpp"

Alignment::Alignment(void) :
	_source(NULL),
	_target(NULL),
	_source_alignment(NULL),
	_target_alignment(NULL)
{

}

Alignment::~Alignment(void)
{
	_ref_stats_list.clear();
	delete _source;
	delete _target;
	delete _source_alignment;
	delete _target_alignment;
}

void Alignment::set_stats(const header_t &header)
{
	header_t::const_iterator iter;
	List::str_str_t header_list;

	for (iter = header.begin(); iter != header.end(); iter++) {
		header_list.push_back(*iter);
	}

	_ref_stats_list = List::create(header_list);
}

void Alignment::set_source_language_model(language_model_t *source)
{
	if (_source) {
		delete _source;
	}

	_source = new LanguageModel(source, LanguageModel::SOURCE);
}

void Alignment::set_target_language_model(language_model_t *target)
{
	if (_target) {
		delete _target;
	}

	_target = new LanguageModel(target, LanguageModel::TARGET);
}

void Alignment::set_alignment(alignment_t *source, alignment_t *target)
{
	if (_source_alignment) {
		delete _source_alignment;
	}
	if (_target_alignment) {
		delete _target_alignment;
	}

	_source_alignment = source;
	_target_alignment = target;
}

void Alignment::set_translation(const translation_t &trans)
{
	guint i;

	if (!_target) {
		return;
	}

	for (i = 0; i < trans.size(); i++) {
		const std::list<Terminal *> &w = _target->get_terminals();
		list_at(w, i)->set_translation(trans[i]);
	}
}

void Alignment::set_distortion(const distortion_t &dist)
{
	guint i;

	if (!_target) {
		return;
	}

	for (i = 0; i < dist.size(); i++) {
		const std::list<Terminal *> &w = _target->get_terminals();
		list_at(w, i)->set_distortion(dist[i]);
	}
}

void Alignment::set_ngram(const ngram_t &ngram)
{
	guint i;

	if (!_target) {
		return;
	}

	for (i = 0; i < ngram.size(); i++) {
		const std::list<Terminal *> &w = _target->get_terminals();
		list_at(w, i)->set_ngram(ngram[i]);
	}
}

void Alignment::set_fertility(const fertility_t &fert)
{
	guint i;

	if (!_target) {
		return;
	}

	for (i = 0; i < fert.size(); i++) {
		const std::list<Terminal *> &w = _target->get_terminals();
		list_at(w, i)->set_fertility(fert[i]);
	}
}
