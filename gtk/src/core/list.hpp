/**
 * \file list.hpp
 *
 * Encapsulates the List class.
 */

#ifndef __LIST_HPP__
#define __LIST_HPP__

#include <vector>
#include <utility>
#include <gtkmm/liststore.h>
#include <gtkmm/treemodelcolumn.h>

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The List class. It represents a list of information to be shown in a
 * TreeView widget, like translation probabilities of a terminal, etc.
 *
 * This class includes the Gtkmm ListStore widget model so that the information
 * of each terminal only needs to be populated once at the creation time of the
 * terminal, instead of creating the ListStore and populate the information every
 * time the terminal gets selected.
 */
class List : public Gtk::ListStore
{
public:
	/**
	 * \brief A list with two string value columns.
	 */
	typedef std::vector< std::pair<Glib::ustring, Glib::ustring> > str_str_t;
	/**
	 * \brief A list with a string value column and a double value column.
	 */
	typedef std::vector< std::pair<Glib::ustring, gdouble> > str_double_t;
	/**
	 * \brief A list with an integer value column and a double value column.
	 */
	typedef std::vector< std::pair<gint, gdouble> > int_double_t;

	/**
	 * \brief Constructor of a list with two string value columns.
	 * \param data The data of the list.
	 */
	List(const str_str_t &data);

	/**
	 * \brief Constructor of a list with a string value column and a double
	 * value column.
	 * \param data The data of the list.
	 */
	List(const str_double_t &data);

	/**
	 * \brief Constructor of a list with an integer value column and a double
	 * value column.
	 * \param data The data of the list.
	 */
	List(const int_double_t &data);

	/**
	 * \brief Destructor.
	 */
	~List(void);

	/**
	 * \brief Creates a List object with two string value columns.
	 * \param data The data of the list.
	 */
	static Glib::RefPtr<List> create(const str_str_t &data);

	/**
	 * \brief Creates a List object with a string value column and a double
	 * value column.
	 * \param data The data of the list.
	 */
	static Glib::RefPtr<List> create(const str_double_t &data);

	/**
	 * \brief Creates a List object with an integer value column and a double
	 * value column.
	 * \param data The data of the list.
	 */
	static Glib::RefPtr<List> create(const int_double_t &data);

	/**
	 * \brief Gets the string column model.
	 * \return The string column model.
	 */
	Gtk::TreeModelColumn<Glib::ustring> &get_str_column(void);

	/**
	 * \brief Gets the integer column model.
	 * \return The integer column model.
	 */
	Gtk::TreeModelColumn<gint> &get_int_column(void);

	/**
	 * \brief Gets the string value column model.
	 * \return The string value column model.
	 */
	Gtk::TreeModelColumn<Glib::ustring> &get_str_value_column(void);

	/**
	 * \brief Gets the double value column model.
	 * \return The double value column model.
	 */
	Gtk::TreeModelColumn<gdouble> &get_double_value_column(void);
private:
	/**
	 * \brief Creates a List object.
	 * \param data The data of the list.
	 * \param name_column The first column model.
	 * \param value_column The value column model.
	 */
	template <typename T, typename I, typename V>
	void _create(const T &data, I &name_column, V &value_column);

	Gtk::TreeModelColumn<Glib::ustring> _str_column; //!< The string column model.
	Gtk::TreeModelColumn<gint> _int_column;			 //!< The integer column model.
	Gtk::TreeModelColumn<Glib::ustring> _str_value_column; //!< The string value column model.
	Gtk::TreeModelColumn<gdouble> _double_value_column;	   //!< The double value column model.
};

inline Gtk::TreeModelColumn<Glib::ustring> &List::get_str_column(void)
{
	return _str_column;
}

inline Gtk::TreeModelColumn<gint> &List::get_int_column(void)
{
	return _int_column;
}

inline Gtk::TreeModelColumn<Glib::ustring> &List::get_str_value_column(void)
{
	return _str_value_column;
}

inline Gtk::TreeModelColumn<gdouble> &List::get_double_value_column(void)
{
	return _double_value_column;
}

/**
 * \}
 */

#endif
