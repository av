/**
 * \file language_model.hpp
 *
 * Encapsulates the LanguageModel class.
 */

#ifndef __LANGUAGE_MODEL_HPP__
#define __LANGUAGE_MODEL_HPP__

#include <list>

#include "terminal.hpp"
#include "parser.hpp"
#include "nonterminal.hpp"

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The LanguageModel class.
 */
class LanguageModel
{
public:
	/**
	 * \brief The language_model type.
	 */
	typedef enum {
		SOURCE,					//!< Source language_model type.
		TARGET,					//!< Target language_model type.
	} language_model_type_t;

	/**
	 * \brief Constructor.
	 * \param language_model The actual language_model. It is owned by this
	 * LanguageModel object, it will be deleted when this LanguageModel object
	 * destructs.
	 * \param type The language_model type.
	 */
	LanguageModel(language_model_t *language_model, language_model_type_t type);

	/**
	 * \brief Destructor.
	 */
	~LanguageModel(void);

	/**
	 * \brief Gets the language_model type.
	 * \return The language_model type.
	 */
	language_model_type_t get_type(void) const;

	/**
	 * \brief Gets the next NonTerminal ID.
	 *
	 * Use the ID returned by this method when you are creating new NonTerminals.
	 *
	 * \return The next NonTerminal ID.
	 */
	gint get_next_id(void) const;

	/**
	 * \brief Gets the NonTerminals (excluding the leaf nodes) in the
	 * language_model.
	 * \return The list of all the NonTerminals.
	 */
	const std::list<NonTerminal *> &get_nonterminals(void) const;

	/**
	 * \brief Gets all the terminals in the language_model.
	 * \return The list of all the terminals.
	 */
	std::list<Terminal *> &get_terminals(void);

	/**
	 * \brief Adds a NonTerminal.
	 * \param n The NonTerminal to add.
	 */
	void add(NonTerminal *n);

	/**
	 * \brief Inserts a Terminal.
	 * \param terminal The terminal to insert.
	 * \param loc The terminal will be inserted before loc, <code>NULL</code>
	 * will insert the terminal at the end.
	 */
	void add(Terminal *terminal, Terminal *loc);

	/**
	 * \brief Deletes a NonTerminal.
	 * \param n The NonTerminal to delete.
	 */
	void remove(NonTerminal *n);

	/**
	 * \brief Removes a Terminal.
	 * \param terminal The terminal to delete.
	 */
	void remove(Terminal *terminal);
private:
	/**
	 * \brief Inserts a value into the list.
	 * \param l The list.
	 * \param val The value to insert.
	 * \param loc The value will be inserted before loc, <code>NULL</code> will
	 * insert the value at the end.
	 */
	template <typename T>
	void _add(std::list<T *> &l, T *val, T *loc);

	/**
	 * \brief Removes a value from the list.
	 * \param l The list.
	 * \param val The value to remove.
	 */
	template <typename T>
	void _remove(std::list<T *> &l, T *val);

	language_model_t *_language_model;	//!< The actual language_model.
	language_model_type_t _type;		//!< The language_model type.
	std::list<NonTerminal *> _nonterminals; //!< The non-terminals excluding the terminals.
	std::list<Terminal *> _terminals;		  //!< The terminals.
};

inline LanguageModel::language_model_type_t LanguageModel::get_type(void) const
{
	return _type;
}

inline gint LanguageModel::get_next_id(void) const
{
	return _terminals.size() + _nonterminals.size();
}

inline const std::list<NonTerminal *> &
LanguageModel::get_nonterminals(void) const
{
	return _nonterminals;
}

inline std::list<Terminal *> &LanguageModel::get_terminals(void)
{
	return _terminals;
}

/**
 * \}
 */

#endif
