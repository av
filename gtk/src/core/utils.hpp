/**
 * \file utils.hpp
 *
 * Contains all the utility functions and function objects.
 */

#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <list>
#include <glibmm/ustring.h>

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The string comparison functor to be used in STL function templates.
 */
struct str_cmp
{
	/**
	 * \brief Compares two strings.
	 * \param s1 The string currently testing.
	 * \param s2 The string to compare to.
	 */
	bool operator()(Glib::ustring s1, Glib::ustring s2) const
		{
			return s1.compare(s2) < 0;
		}
};

/**
 * \brief The integer comparison functor to be used in STL function templates.
 */
struct int_cmp
{
	/**
	 * \brief Compares two integers.
	 * \param n1 The integer currently testing.
	 * \param n2 The integer to compare to.
	 */
	bool operator()(gint n1, gint n2) const
		{
			return n1 < n2;
		}
};

/**
 * \brief Returns the nth element in the STL list.
 *
 * This function runs in linear time.
 *
 * \param list The list that contains the element.
 * \param n The index number.
 * \return The element, or <code>NULL</code> if out of range.
 */
template <typename T>
T list_at(const std::list<T> &list, gint n)
{
	typename std::list<T>::const_iterator iter;
	gint i = 0;

	for (iter = list.begin(); iter != list.end(); iter++, i++) {
		if (i == n) {
			break;
		}
	}

	if (iter != list.end()) {
		return *iter;
	}

	return NULL;
}

/**
 * \brief Returns the index of the element in the STL list.
 *
 * This function runs in linear time.
 *
 * \param list The list that contains the element.
 * \param n The element.
 * \return The index, or <code>list.size()</code> if it does not exist.
 */
template <typename T>
guint list_index(const std::list<T> &list, const T n)
{
	typename std::list<T>::const_iterator iter;
	guint i = 0;

	for (iter = list.begin(); iter != list.end(); iter++, i++) {
		if (*iter == n) {
			break;
		}
	}

	if (iter != list.end()) {
		return i;
	}

	return list.size();
}

/**
 * \brief Inserts the element into the list if the predicate is true.
 *
 * This function runs in linear time.
 *
 * \param list The list to insert to.
 * \param element The elemnt.
 * \param pred The unary predicate.
 */
template <typename ListT, typename T, typename Pred>
typename ListT::iterator insert_if(ListT &list, T element, Pred pred)
{
	typename ListT::iterator iter;

	iter = std::find_if(list.begin(), list.end(), pred);

	return list.insert(iter, element);
}

/**
 * \brief Tells if an element is in the list.
 * \param list The list.
 * \param val The value to test.
 * \return <code>true</code> if in the list.
 */
template <typename ListT, typename T>
bool is_in_list(ListT &list, const T &val)
{
	typename ListT::const_iterator iter;

	iter = std::find(list.begin(), list.end(), val);

	if (iter != list.end()) {
		return true;
	}

	return false;
}

/**
 * \}
 */

#endif
