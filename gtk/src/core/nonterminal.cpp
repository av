#include <algorithm>
#include <functional>

#include "nonterminal.hpp"
#include "language_model.hpp"
#include "node.hpp"

static bool is_id_greater(const NonTerminal *i, const NonTerminal *n)
{
	if (i->get_id() > n->get_id()) {
		return true;
	}

	return false;
}

static void set_height_if_greater(NonTerminal *i, gint *height)
{
	if (i->get_height() >= *height) {
		*height = i->get_height() + 1;
	}
}

NonTerminal::NonTerminal(LanguageModel &language_model,
                         const Glib::ustring &text, bool permutability) :
	_language_model(language_model),
	_id(_language_model.get_next_id()),
	_text(text),
	_is_permutable(permutability),
	_parent(NULL),
	_node(NULL),
	_height(0)
{

}

NonTerminal::~NonTerminal(void)
{

}

NonTerminal *NonTerminal::get_root(void)
{
	if (!get_parent()) {
		return this;
	}

	return _parent->get_root();
}

void NonTerminal::get_descendants(std::list<NonTerminal *> &l)
{
	std::list<NonTerminal *>::iterator iter;
	std::list<NonTerminal *>::const_iterator i, j;

	iter = l.insert(l.end(), this);

	for (i = iter; i != l.end(); i++) {
		const std::list<NonTerminal *> &children = (*i)->get_children();

		for (j = children.begin(); j != children.end(); j++) {
			l.push_back(*j);
		}
	}

	l.erase(iter);
}

const NonTerminal *NonTerminal::get_leftmost_leaf(void) const
{
	std::list<NonTerminal *>::const_iterator iter;
	const NonTerminal *cur = NULL;
	const NonTerminal *leftmost = NULL;

	if (_children.empty()) {
		return this;
	} else {
		for (iter = _children.begin(); iter != _children.end(); iter++) {
			cur = (*iter)->get_leftmost_leaf();

			if (!leftmost || cur->get_id() < leftmost->get_id()) {
				leftmost = cur;
			}
		}

		return leftmost;
	}
}

const NonTerminal *NonTerminal::get_rightmost_leaf(void) const
{
	std::list<NonTerminal *>::const_iterator iter;
	const NonTerminal *cur = NULL;
	const NonTerminal *rightmost = NULL;

	if (_children.empty()) {
		return this;
	} else {
		for (iter = _children.begin(); iter != _children.end(); iter++) {
			cur = (*iter)->get_rightmost_leaf();

			if (!rightmost || cur->get_id() > rightmost->get_id()) {
				rightmost = cur;
			}
		}

		return rightmost;
	}
}

NonTerminal::sections_t NonTerminal::get_sections(void) const
{
	sections_t result;
	std::list<const NonTerminal *> terminals;
	std::list<const NonTerminal *>::const_iterator iter;
	std::list<const NonTerminal *>::const_iterator prev_iter;
	const NonTerminal *start = NULL;

	get_terminals(terminals);

	for (iter = terminals.begin(), prev_iter = iter; iter != terminals.end();
	     prev_iter = iter, iter++) {
		if (!start) {
			start = *prev_iter;
		} else if ((*iter)->get_id() != ((*prev_iter)->get_id() + 1)) {
			result.push_back(std::make_pair(start, *prev_iter));

			start = NULL;
		}
	}

	if (start) {
		result.push_back(std::make_pair(start, *prev_iter));
	} else {
		result.push_back(std::make_pair(*prev_iter, *prev_iter));
	}

	return result;
}

void NonTerminal::get_terminals(std::list<const NonTerminal *> &list) const
{
	std::list<NonTerminal *>::const_iterator iter;

	if (is_leaf()) {
		insert_if(list, this, std::bind2nd(std::ptr_fun(is_id_greater), this));

		return;
	}

	for (iter = _children.begin(); iter != _children.end(); iter++) {
		(*iter)->get_terminals(list);
	}
}

gint NonTerminal::get_min_height(void) const
{
	gint result = _height;
	std::list<NonTerminal *>::const_iterator iter;

	for (iter = _children.begin(); iter != _children.end(); iter++) {
		if ((*iter)->get_height() < result) {
			result = (*iter)->get_height();
		}
	}

	return result + 1;
}

void NonTerminal::set_id(gint id)
{
	_id = id;
}

void NonTerminal::set_text(const Glib::ustring &text)
{
	_text = text;
}

void NonTerminal::set_permutability(bool permutability)
{
	_is_permutable = permutability;
}

bool NonTerminal::set_parent(Node *n)
{
	if (n) {
		return set_parent(n->get_nonterminal());
	}

	if (_parent) {
		_parent_changed_signal.emit(this, _parent, false);
	}

	_parent = NULL;

	return true;
}

bool NonTerminal::set_parent(NonTerminal &n)
{
	if (!is_parent(n) && !n.is_leaf()) {
		if (_parent) {
			_parent_changed_signal.emit(this, _parent, false);
		}

		_parent = &n;

		_parent_changed_signal.emit(this, _parent, true);

		return true;
	}

	return false;
}

bool NonTerminal::add_child(Node &n)
{
	return add_child(n.get_nonterminal());
}

bool NonTerminal::add_child(NonTerminal &n)
{
	if (!is_leaf() && !is_child(n) && !is_ascendant(n)) {
		_children.push_back(&n);

		if (n.get_height() >= _height) {
			_calculate_height(n.get_height() + 1);
		}

		_child_changed_signal.emit(this, &n, true);

		return true;
	}

	return false;
}

bool NonTerminal::add_friend(Node &n)
{
	return add_friend(n.get_nonterminal());
}

bool NonTerminal::add_friend(NonTerminal &n)
{
	if (!is_friend(n)) {
		_friends.push_back(&n);

		_friend_changed_signal.emit(this, &n, true);

		return true;
	}

	return false;
}

bool NonTerminal::delete_child(const Node &n)
{
	return delete_child(n.get_nonterminal());
}

bool NonTerminal::delete_child(NonTerminal &n)
{
	std::list<NonTerminal *>::iterator iter;

	iter = std::find(_children.begin(), _children.end(), &n);

	if (iter != _children.end()) {
		_children.erase(iter);

		// Need to go over all children to see who has the deepest branch
		if (n.get_height() == (_height - 1)) {
			_calculate_height();
		}

		_child_changed_signal.emit(this, &n, false);

		return true;
	}

	return false;
}

void NonTerminal::delete_all_children(void)
{
	std::list<NonTerminal *>::iterator iter;

	for (iter = _children.begin(); iter != _children.end(); iter++) {
		_child_changed_signal.emit(this, *iter, false);
	}

	_children.clear();
	_calculate_height();
}

bool NonTerminal::delete_friend(const Node &n)
{
	return delete_friend(n.get_nonterminal());
}

bool NonTerminal::delete_friend(NonTerminal &n)
{
	std::list<NonTerminal *>::iterator iter;

	iter = std::find(_friends.begin(), _friends.end(), &n);

	if (iter != _friends.end()) {
		_friends.erase(iter);

		_friend_changed_signal.emit(this, &n, false);

		return true;
	}

	return false;
}

bool NonTerminal::is_self(const Node &n) const
{
	return is_self(n.get_nonterminal());
}

bool NonTerminal::is_self(const NonTerminal &n) const
{
	if (&n == this) {
		return true;
	}

	return false;
}

bool NonTerminal::is_parent(const Node &n) const
{
	return is_parent(n.get_nonterminal());
}

bool NonTerminal::is_parent(const NonTerminal &n) const
{
	if (&n == _parent) {
		return true;
	}

	return false;
}

bool NonTerminal::is_child(const Node &n) const
{
	return is_child(n.get_nonterminal());
}

bool NonTerminal::is_child(const NonTerminal &n) const
{
	return is_in_list(_children, &n);
}

bool NonTerminal::is_ascendant(const Node &n) const
{
	return is_ascendant(n.get_nonterminal());
}

bool NonTerminal::is_ascendant(const NonTerminal &n) const
{
	if (!get_parent()) {
		return false;
	}

	if (is_parent(n)) {
		return true;
	} else {
		return get_parent()->is_ascendant(n);
	}
}

bool NonTerminal::is_friend(const Node &n) const
{
	return is_friend(n.get_nonterminal());
}

bool NonTerminal::is_friend(const NonTerminal &n) const
{
	return is_in_list(_friends, &n);
}

void NonTerminal::_calculate_height(gint height)
{
	if (height <= _height) {
		std::for_each(_children.begin(), _children.end(),
		              std::bind2nd(std::ptr_fun(set_height_if_greater),
		                           &height));
	}

	_height = height;

	if (_parent) {
		_parent->_calculate_height(_height + 1);
	}
}
