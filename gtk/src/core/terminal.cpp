#include "terminal.hpp"
#include "language_model.hpp"

Terminal::Terminal(LanguageModel &language_model, const Glib::ustring &text) :
	NonTerminal(language_model, text),
	_trans(NULL),
	_dist(NULL),
	_ngram(NULL),
	_fert(NULL)
{

}

Terminal::~Terminal(void)
{

}

void Terminal::set_translation(const translation_entry_t &trans)
{
	_trans = List::create(trans);
}

void Terminal::set_distortion(const distortion_entry_t &dist)
{
	_dist = List::create(dist);
}

void Terminal::set_ngram(const ngram_entry_t &ngram)
{
	_ngram = List::create(ngram);
}

void Terminal::set_fertility(const fertility_entry_t &fert)
{
	_fert = List::create(fert);
}
