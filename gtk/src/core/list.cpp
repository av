#include "list.hpp"

List::List(const str_str_t &data)
{
	_create(data, _str_column, _str_value_column);
}

List::List(const str_double_t &data)
{
	_create(data, _str_column, _double_value_column);
}

List::List(const int_double_t &data)
{
	_create(data, _int_column, _double_value_column);
}

List::~List(void)
{

}

Glib::RefPtr<List> List::create(const str_str_t &data)
{
	return Glib::RefPtr<List>(new List(data));
}

Glib::RefPtr<List> List::create(const str_double_t &data)
{
	return Glib::RefPtr<List>(new List(data));
}

Glib::RefPtr<List> List::create(const int_double_t &data)
{
	return Glib::RefPtr<List>(new List(data));
}

template <typename T, typename I, typename V>
void List::_create(const T &data, I &name_column, V &value_column)
{
	Gtk::TreeModel::ColumnRecord column_record;
	typename T::const_iterator iter;

	column_record.add(name_column);
	column_record.add(value_column);

	set_column_types(column_record);

	for (iter = data.begin(); iter != data.end(); iter++) {
		Gtk::TreeModel::Row row = *(append());
		row[name_column] = iter->first;
		row[value_column] = iter->second;
	}
}
