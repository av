#ifndef JASON_SPIRIT
#define JASON_SPIRIT

/* Copyright (c) 2007 John W Wilkinson

   This source code can be used for any purpose as long as
   this comment is retained. */

// json spirit version 2.00

#pragma once

#include "json_spirit_value.h"
#include "json_spirit_reader.h"
#include "json_spirit_writer.h"

#endif
