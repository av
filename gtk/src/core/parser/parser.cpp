#include <fstream>
#include <algorithm>
#include <functional>

#include "parser.hpp"

bool is_same_name(const Pair pair, const std::string name)
{
	return pair.name_ == name;
}

Parser::Parser(void) :
	_source_size(0),
	_target_size(0)
{

}

Parser::Parser(const Glib::ustring &filename)
{
	read_file(filename);
}

Parser::~Parser(void)
{

}

void Parser::read_file(const Glib::ustring &filename)
{
	Value value;

	std::ifstream fd(filename.c_str());
	read(fd, value);
	fd.close();

	_obj = value.get_obj();
}

header_t *Parser::parse_header(void) const
{
	header_t *header = NULL;
	guint i;

	const Array *value = _find_array("header");

	if (value) {
		header = new header_t;

		for (i = 0; i < value->size(); i++) {
			const Array &v((*value)[i].get_array());

			header->push_back(std::make_pair(v[0].get_str(), v[1].get_str()));
		}
	}

	return header;
}

language_model_t *Parser::parse_source_language_model(void)
{
	language_model_t *source = NULL;
	guint i;

	source = new language_model_t;

	const Array *value = _find_array("source_sentence");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array& v((*value)[i].get_array());
			source->push_back(v[0].get_str());
		}

		_source_size = value->size();
	}

	return source;
}

language_model_t *Parser::parse_target_language_model(void)
{
	language_model_t *target = NULL;
	guint i;

	target = new language_model_t;

	const Array *value = _find_array("target_sentence");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array& v((*value)[i].get_array());
			target->push_back(v[0].get_str());
		}

		_target_size = value->size();
	}

	return target;
}

void Parser::parse_alignment(alignment_t &source, alignment_t &target) const
{
	gint index;
	guint i;

	const Array *value = _find_array("alignment");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array& v((*value)[i].get_array());
			index = v[2].get_int();

			if (index == 0) {
				source.push_back(std::make_pair(v[0].get_int(), v[1].get_int()));
			} else if (index == 1) {
				target.push_back(std::make_pair(v[0].get_int(), v[1].get_int()));
			}
		}
	}
}

translation_t *Parser::parse_translation(void) const
{
	translation_t *trans = NULL;
	gint index;
	guint i;

	if (_target_size == 0) {
		return NULL;
	}

	trans = new translation_t(_target_size);

	const Array *value = _find_array("translations");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array &v((*value)[i].get_array());

			if (v.size() == 0) {
				break;
			}

			index = v[0].get_int();

			if (index >= 0 && (guint)index < trans->size()) {
				(*trans)[index].push_back(std::make_pair(v[1].get_str(),
				                                         v[2].get_real()));
			}
		}
	}

	return trans;
}

distortion_t *Parser::parse_distortion(void) const
{
	distortion_t *dist = NULL;
	gint index;
	guint i;

	if (_target_size == 0) {
		return NULL;
	}

	dist = new distortion_t(_target_size);

	const Array *value = _find_array("distortions");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array &v((*value)[i].get_array());

			if (v.size() == 0) {
				break;
			}

			index = v[0].get_int();

			if (index >= 0 && (guint)index < dist->size()) {
				(*dist)[index].push_back(std::make_pair(v[1].get_int(),
				                                        v[4].get_real()));
			}
		}
	}

	return dist;
}

ngram_t *Parser::parse_ngram(void) const
{
	ngram_t *ngram = NULL;
	gint index;
	guint i;

	if (_target_size == 0) {
		return NULL;
	}

	ngram = new ngram_t(_target_size);

	const Array *value = _find_array("language_model");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array &v((*value)[i].get_array());

			if (v.size() == 0) {
				break;
			}

			index = v[0].get_int();

			if (index >= 0 && (guint)index < ngram->size()) {
				(*ngram)[index].push_back(std::make_pair(v[1].get_str(),
				                                         v[2].get_real()));
			}
		}
	}

	return ngram;
}

fertility_t *Parser::parse_fertility(void) const
{
	fertility_t *fert = NULL;
	gint index;
	guint i;

	if (_target_size == 0) {
		return NULL;
	}

	fert = new fertility_t(_target_size);

	const Array *value = _find_array("fertilities");

	if (value) {
		for (i = 0; i < value->size(); i++) {
			const Array &v((*value)[i].get_array());

			if (v.size() == 0) {
				break;
			}

			index = v[0].get_int();

			if (index >= 0 && (guint)index < fert->size()) {
				(*fert)[index].push_back(std::make_pair(v[1].get_int(),
				                                        v[2].get_real()));
			}
		}
	}

	return fert;
}

const Value &Parser::_find_value(const std::string &name) const
{
	Object::const_iterator iter;

	iter = std::find_if(_obj.begin(), _obj.end(),
	                    std::bind2nd(std::ptr_fun(is_same_name), name));

	if (iter == _obj.end()) {
		return Value::null;
	}

	return iter->value_;
}

const Array *Parser::_find_array(const std::string &name) const
{
	const Value &val(_find_value(name));

	if (val == Value::null) {
		return NULL;
	}

	return &val.get_array();
}

gint Parser::_find_int(const std::string &name) const
{
	return _find_value(name).get_int();
}

std::string Parser::_find_str(const std::string &name) const
{
	return _find_value(name).get_str();
}
