/**
 * \file parser.hpp
 *
 * Encapsulates the JSON parser class. It is responsible for parsing the
 * alignment data files, instead of the configuration files nor the model
 * description files.
 */

#ifndef __PARSER_HPP__
#define __PARSER_HPP__

#include <map>
#include <vector>
#include <utility>
#include <glibmm/ustring.h>

#include "utils.hpp"
#include "json_spirit.h"

using namespace json_spirit;

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The general information of the alignment file that is being parsed.
 */
typedef std::vector< std::pair<Glib::ustring, Glib::ustring> > header_t;
/**
 * \brief The language_model.
 */
typedef std::vector<Glib::ustring> language_model_t;
/**
 * \brief The alignment information. A list of index pairs from target
 * language_model terminal index to source language_model terminal index.
 */
typedef std::vector< std::pair<gint, gint> > alignment_t;
/**
 * \brief The translation probabilities for a single terminal.
 */
typedef std::vector< std::pair<Glib::ustring, gdouble> > translation_entry_t;
/**
 * \brief The translation probabilities for all the terminals in a
 * language_model.
 */
typedef std::vector<translation_entry_t> translation_t;
/**
 * \brief The distortion probabilities for a single terminal.
 */
typedef std::vector< std::pair<gint, gdouble> > distortion_entry_t;
/**
 * \brief The distortion probabilities for all the terminals in a language_model.
 */
typedef std::vector<distortion_entry_t> distortion_t;
/**
 * \brief The N-gram probabilities for a single terminal.
 */
typedef std::vector< std::pair<Glib::ustring, gdouble> > ngram_entry_t;
/**
 * \brief The N-gram probabilities for all the terminals in a language_model.
 */
typedef std::vector<ngram_entry_t> ngram_t;
/**
 * \brief The fertility probabilities for a single terminal.
 */
typedef std::vector< std::pair<gint, gdouble> > fertility_entry_t;
/**
 * \brief The fertility probabilities for all the terminals in a language_model.
 */
typedef std::vector<fertility_entry_t> fertility_t;

/**
 * \brief JSON file Parser class.
 */
class Parser
{
public:
	/**
	 * \brief Constructor.
	 */
	Parser(void);

	/**
	 * \brief Constructor with filename provided.
	 */
	Parser(const Glib::ustring &filename);

	/**
	 * \brief Destructor.
	 */
	~Parser(void);

	/**
	 * \brief Read JSON file.
	 * \param filename The filename of the JSON file.
	 */
	void read_file(const Glib::ustring &filename);

	/**
	 * \brief Parse the general information of the alignment file.
	 * \return The parsed information in a #header_t.
	 */
	header_t *parse_header(void) const;

	/**
	 * \brief Parse the source language_model.
	 * \return The parsed source language_model in a #language_model_t.
	 */
	language_model_t *parse_source_language_model(void);

	/**
	 * \brief Parse the target language_model.
	 * \return The parsed target language_model in a #language_model_t.
	 */
	language_model_t *parse_target_language_model(void);

	/**
	 * \brief Parse the alignment information.
	 * \param[out] source The alignment information for the source language_model.
	 * \param[out] target The alignment information for the target language_model.
	 */
	void parse_alignment(alignment_t &source, alignment_t &target) const;

	/**
	 * \brief Parse the translation probabilities.
	 * \return The parsed translation probabilities in a #translation_t.
	 */
	translation_t *parse_translation(void) const;

	/**
	 * \brief Parse the distortion probabilities.
	 * \return The parsed distortion probabilities in a #distortion_t.
	 */
	distortion_t *parse_distortion(void) const;

	/**
	 * \brief Parse the N-gram probabilities.
	 * \return The parsed N-gram probabilities in a #ngram_t.
	 */
	ngram_t *parse_ngram(void) const;

	/**
	 * \brief Parse the fertility probabilities.
	 * \return The parsed fertility probabilities in a #fertility_t.
	 */
	fertility_t *parse_fertility(void) const;
private:
	/**
	 * \brief Find a value in the JSON object.
	 * \param name The name of the value to find.
	 * \return The value found. Value::null if not found.
	 */
	const Value &_find_value(const std::string &name) const;

	/**
	 * \brief Find an array in the JSON object.
	 * \param name The name of the array to find.
	 * \return The array found, or <code>NULL</code> if not found.
	 */
	const Array *_find_array(const std::string &name) const;

	/**
	 * \brief Find an integer in the JSON object.
	 * \param name The name of the integer to find.
	 * \return The integer found.
	 */
	gint _find_int(const std::string &name) const;

	/**
	 * \brief Find a string in the JSON object.
	 * \param name The name of the string to find.
	 * \return The string found.
	 */
	std::string _find_str(const std::string &name) const;

	Object _obj;				//!< The JSON object.
	guint _source_size;	//!< The number of terminals in the source language_model.
	guint _target_size;	//!< The number of terminals in the target language_model.
};

/**
 * \}
 */

#endif
