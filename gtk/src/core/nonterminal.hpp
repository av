/**
 * \file nonterminal.hpp
 *
 * Encapsulates the tree node base class.
 */

#ifndef __NONTERMINAL_HPP__
#define __NONTERMINAL_HPP__

#include <list>
#include <glibmm/value.h>
#include <sigc++/signal.h>
#include <sigc++/trackable.h>

class LanguageModel;
class Node;

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The non-terminal base class. This class represents an internal node or
 * a leaf node in the tree structure.
 *
 * This is different from the Node class in the way that this class maintains
 * the actual tree structure and the correspondence information.
 */
class NonTerminal : public sigc::trackable
{
public:
	/**
	 * \brief A section of the language model which is marked by a starting
	 * position and an ending position inclusively.
	 */
	typedef std::pair<const NonTerminal *, const NonTerminal *> section_t;

	/**
	 * \brief A list of sections of the language model.
	 */
	typedef std::list<section_t> sections_t;

	/**
	 * \brief The friend changed signal type.
	 *
	 * The first argument is the non-terminal emitting this signal.
	 * The second argument is the friend added or deleted.
	 *
	 * If <code>true</code>, it means that the friend is added. Otherwise, it
	 * means that the friend is deleted.
	 */
	typedef sigc::signal<void, NonTerminal *, NonTerminal *, bool> friend_changed_signal_t;

	/**
	 * \brief The child changed signal type.
	 *
	 * The first argument is the non-terminal emitting this signal.
	 * The second argument is the child added or deleted.
	 *
	 * If <code>true</code>, it means that the child is added. Otherwise, it
	 * means that the child is deleted.
	 */
	typedef sigc::signal<void, NonTerminal *, NonTerminal *, bool> child_changed_signal_t;

	/**
	 * \brief The parent changed signal type.
	 *
	 * The first argument is the non-terminal emitting this signal.
	 * The second argument is the parent added or deleted.
	 *
	 * If <code>true</code>, it means that the parent is added. Otherwise, it
	 * means that the parent is deleted.
	 */
	typedef sigc::signal<void, NonTerminal *, NonTerminal *, bool> parent_changed_signal_t;

	/**
	 * \brief Constructor.
	 * \param language_model The language_model this non-terminal belongs to.
	 * \param text The terminal string.
	 * \param permutability <code>true</code> if the children of the non-terminal
	 * is permutable.
	 */
	NonTerminal(LanguageModel &language_model, const Glib::ustring &text = "", bool permutability = false);

	/**
	 * \brief Destructor.
	 */
	virtual ~NonTerminal(void);

	/**
	 * \brief Gets the friend changed signal.
	 *
	 * This signal is emitted whenever a friend is successfully added or
	 * deleted.
	 *
	 * \return The friend changed signal.
	 */
	virtual NonTerminal::friend_changed_signal_t get_friend_changed_signal(void) const;

	/**
	 * \brief Gets the parent changed signal.
	 *
	 * This signal is emitted whenever the parent is successfully added or
	 * deleted.
	 *
	 * \return The parent changed signal.
	 */
	virtual NonTerminal::parent_changed_signal_t get_parent_changed_signal(void) const;

	/**
	 * \brief Gets the child changed signal.
	 *
	 * This signal is emitted whenever a child is successfully added or
	 * deleted.
	 *
	 * \return The child changed signal.
	 */
	virtual NonTerminal::child_changed_signal_t get_child_changed_signal(void) const;

	/**
	 * \brief Gets the ID of the non-terminal.
	 * \return The ID of the non-terminal.
	 */
	virtual gint get_id(void) const;

	/**
	 * \brief Gets the terminal string.
	 * \return The terminal string.
	 */
	virtual const Glib::ustring &get_text(void) const;

	/**
	 * \brief Gets the language_model which the non-terminal belongs to.
	 * \return The language_model which the non-terminal belongs to.
	 */
	virtual LanguageModel &get_language_model(void);

	/**
	 * \brief Gets the permutability.
	 * \return <code>true</code> if permutable.
	 */
	virtual bool get_permutability(void) const;

	/**
	 * \brief Gets the parent non-terminal.
	 * \return The parent non-terminal, or <code>NULL</code> if it is the root
	 * non-terminal.
	 */
	virtual NonTerminal *get_parent(void) const;

	/**
	 * \brief Gets the root non-terminal.
	 * \return The root non-terminal.
	 */
	virtual NonTerminal *get_root(void);

	/**
	 * \brief Gets the Node object.
	 *
	 * If the graphical interface has not been initialized yet, or if it has
	 * been destroyed, the Node object does not exist.
	 *
	 * \return The Node object, or <code>NULL</code> if it does not exist.
	 */
	virtual Node *get_node(void) const;

	/**
	 * \brief Gets the children.
	 * \return A list of the children.
	 */
	virtual const std::list<NonTerminal *> &get_children(void) const;

	/**
	 * \brief Gets all the descendants.
	 * \param[out] l A list of all the descendants.
	 */
	virtual void get_descendants(std::list<NonTerminal *> &l);

	/**
	 * \brief Gets the friends.
	 * \return A list of friends.
	 */
	virtual const std::list<NonTerminal *> &get_friends(void) const;

	/**
	 * \brief Gets the left-most leaf of the subtree rooted at the current
	 * non-terminal.
	 * \return The left-most leaf, or itself if it is a leaf non-terminal.
	 */
	virtual const NonTerminal *get_leftmost_leaf(void) const;

	/**
	 * \brief Gets the right-most leaf of the subtree rooted at the current
	 * non-terminal.
	 * \return The right-most leaf, or itself if it is a leaf non-terminal.
	 */
	virtual const NonTerminal *get_rightmost_leaf(void) const;

	/**
	 * \brief Gets a list of contiguous leaves which are descendants of the
	 * nonterminal.
	 * \return The list.
	 */
	virtual sections_t get_sections(void) const;

	/**
	 * \brief Gets all the terminals in the subtree rooted at this nonterminal.
	 * \param[out] list The list to store the terminals.
	 *
	 * The returned list is sorted in ascending order based on the indexes of
	 * the terminals.
	 */
	virtual void get_terminals(std::list<const NonTerminal *> &list) const;

	/**
	 * \brief Gets the height of the current non-terminal.
	 * \return The height.
	 */
	virtual gint get_height(void) const;

	/**
	 * \brief Gets the height of the lowest child.
	 * \return The height.
	 */
	virtual gint get_min_height(void) const;

	/**
	 * \brief Sets the ID.
	 * \param id The ID.
	 */
	virtual void set_id(gint id);

	/**
	 * \brief Sets the terminal string.
	 * \param text The new terminal string.
	 */
	virtual void set_text(const Glib::ustring &text);

	/**
	 * \brief Sets the permutability.
	 * \param permutability <code>true</code> if the children of the
	 * non-terminal if permutable.
	 */
	virtual void set_permutability(bool permutability = false);

	/**
	 * \brief Sets the parent.
	 *
	 * This is just an easy way of calling <code>set_parent(NonTerminal
	 * &n)</code> when you are manipulating a Node object.
	 *
	 * \param n The parent Node, or <code>NULL</code> if you want to remove the
	 * current parent.
	 * \return <code>true</code> if the parent is set.
	 */
	virtual bool set_parent(Node *n);

	/**
	 * \brief Sets the parent.
	 * \param n The parent non-terminal.
	 * \return <code>true</code> if the parent is set.
	 */
	virtual bool set_parent(NonTerminal &n);

	/**
	 * \brief Sets the Node object.
	 * \param n The Node object, or <code>NULL</code> if you want to remove the
	 * current Node object.
	 */
	virtual void set_node(Node *n);

	/**
	 * \brief Adds a new child.
	 *
	 * This is just an easy way of calling <code>add_child(NonTerminal
	 * &n)</code> when you are manipulating a Node object.
	 *
	 * \param n The new child Node object.
	 * \return <code>true</code> if the child is added.
	 */
	virtual bool add_child(Node &n);

	/**
	 * \brief Adds a new child.
	 *
	 * If it is already a child, nothing will be changed.
	 *
	 * \param n The new child non-terminal.
	 * \return <code>true</code> if the child is added.
	 */
	virtual bool add_child(NonTerminal &n);

	/**
	 * \brief Adds a new friend.
	 *
	 * This is just an easy way of calling <code>add_friend(NonTerminal
	 * &n)</code> when you are manipulating a Node object.
	 *
	 * \param n The new friend.
	 * \return <code>true</code> if the friend is added.
	 */
	virtual bool add_friend(Node &n);

	/**
	 * \brief Adds a new friend.
	 *
	 * A friend is another non-terminal which has a correspondence relationship
	 * with the current non-terminal.
	 *
	 * \param n The new friend.
	 * \return <code>true</code> if the friend is added.
	 */
	virtual bool add_friend(NonTerminal &n);

	/**
	 * \brief Deletes a child.
	 *
	 * This is just an easy way of calling <code>delete_child(const NonTerminal
	 * &n)</code> when you are manipulating a Node object.
	 *
	 * \param n The child to delete.
	 * \return <code>true</code> if the child is deleted.
	 */
	virtual bool delete_child(const Node &n);

	/**
	 * \brief Deletes a child.
	 *
	 * If the non-terminal given is not a child of the current non-terminal,
	 * nothing will be changed.
	 *
	 * \param n The child to delete.
	 * \return <code>true</code> if the child is deleted.
	 */
	virtual bool delete_child(NonTerminal &n);

	/**
	 * \brief Deletes all the children.
	 */
	virtual void delete_all_children(void);

	/**
	 * \brief Deletes a friend.
	 *
	 * This is just an easy way of calling <code>delete_friend(const NonTerminal
	 * &n)</code> when you are manipulating a Node object.
	 *
	 * \param n The friend to delete.
	 * \return <code>true</code> if the friend is deleted.
	 */
	virtual bool delete_friend(const Node &n);

	/**
	 * \brief Deletes a friend.
	 *
	 * If the non-terminal given is not a friend of the current non-terminal,
	 * nothing will be changed.
	 *
	 * \param n The friend to delete.
	 * \return <code>true</code> if the friend is deleted.
	 */
	virtual bool delete_friend(NonTerminal &n);

	/**
	 * \brief Checks if the Node object is the current non-terminal.
	 *
	 * This is just an easy way of calling <code>is_self(const NonTerminal &n)
	 * const</code> when you ware manipulating a Node object.
	 *
	 * \param n The Node object to be checked.
	 * \return <code>true</code> if the Node object given is the current
	 * non-terminal.
	 */
	virtual bool is_self(const Node &n) const;

	/**
	 * \brief Checks if the non-terminal is the current non-terminal.
	 * \param n The non-terminal to be checked.
	 * \return <code>true</code> if the non-terminal given is the current
	 * non-terminal.
	 */
	virtual bool is_self(const NonTerminal &n) const;

	/**
	 * \brief Checks if the Node object is the parent.
	 *
	 * This is just an easy way of calling <code>is_parent(const NonTerminal &n)
	 * const</code> when you are manipulating a Node object.
	 *
	 * \param n The Node object to be checked.
	 * \return <code>true</code> if the Node object given is the parent.
	 */
	virtual bool is_parent(const Node &n) const;

	/**
	 * \brief Checks if the non-terminal is the parent.
	 * \param n The non-terminal to be checked.
	 * \return <code>true</code> if the non-terminal given is the parent.
	 */
	virtual bool is_parent(const NonTerminal &n) const;

	/**
	 * \brief Checks if the Node object is a child.
	 *
	 * This is just an easy way of calling <code>is_child(const NonTerminal &n)
	 * const</code> when you are manipulating a Node object.
	 *
	 * \param n The Node object to be checked.
	 * \return <code>true</code> if the Node object given is a child.
	 */
	virtual bool is_child(const Node &n) const;

	/**
	 * \brief Checks if the non-terminal is a child.
	 * \param n The non-terminal to be checked.
	 * \return <code>true</code> if the non-terminal given is a child.
	 */
	virtual bool is_child(const NonTerminal &n) const;

	/**
	 * \brief Checks if the Node object is an ascendant.
	 *
	 * This is just an easy way of calling <code>is_ascendant(const NonTerminal
	 * &n) const</code> when you are manipulating a Node object.
	 *
	 * \param n The Node object to be checked.
	 * \return <code>true</code> if the Node object given is an ascendant.
	 */
	virtual bool is_ascendant(const Node &n) const;

	/**
	 * \brief Checks if the non-terminal is an ascendant.
	 * \param n The non-terminal to be checked.
	 * \return <code>true</code> if the non-terminal given is an ascendant.
	 */
	virtual bool is_ascendant(const NonTerminal &n) const;

	/**
	 * \brief Checks if the Node object given is a friend.
	 *
	 * This is just an easy way of calling <code>is_friend(const NonTerminal &n)
	 * const</code> when you are manipulating a Node object.
	 *
	 * \param n The Node object to be checked.
	 * \return <code>true</code> if the Node object given is a friend.
	 */
	virtual bool is_friend(const Node &n) const;

	/**
	 * \brief Checks if the non-terminal is a friend.
	 * \param n The non-terminal to be checked.
	 * \return <code>true</code> if the non-terminal given is a friend.
	 */
	virtual bool is_friend(const NonTerminal &n) const;

	/**
	 * \brief Checks if the current non-terminal is a leaf node.
	 * \return <code>true</code> if the current non-terminal is a leaf node.
	 */
	virtual bool is_leaf(void) const;
private:
	/**
	 * \brief Calculates the height of the current non-terminal in the tree.
	 * \param height The reference height.
	 */
	void _calculate_height(gint height = 0);

	friend_changed_signal_t _friend_changed_signal; //!< The friend changed signal.
	parent_changed_signal_t _parent_changed_signal; //!< The parent changed signal.
	child_changed_signal_t _child_changed_signal;	//!< The child changed signal.

	LanguageModel &_language_model;	//!< The language_model this non-terminal belongs to.
	gint _id;								//!< The ID of this non-terminal.
	Glib::ustring _text;		//!< The terminal string.
	bool _is_permutable;		//!< The permutability.
	NonTerminal *_parent;		  //!< The parent.
	Node *_node;				  //!< The Node object.
	gint _height; //!< The height of the subtree rooted at the current non-terminal.
	std::list<NonTerminal *> _children; //!< The list of children.
	std::list<NonTerminal *> _friends; //!< The list of friends.
};

inline NonTerminal::friend_changed_signal_t
NonTerminal::get_friend_changed_signal(void) const
{
	return _friend_changed_signal;
}

inline NonTerminal::parent_changed_signal_t
NonTerminal::get_parent_changed_signal(void) const
{
	return _parent_changed_signal;
}

inline NonTerminal::child_changed_signal_t
NonTerminal::get_child_changed_signal(void) const
{
	return _child_changed_signal;
}

inline gint NonTerminal::get_id(void) const
{
	return _id;
}

inline const Glib::ustring &NonTerminal::get_text(void) const
{
	return _text;
}

inline LanguageModel &NonTerminal::get_language_model(void)
{
	return _language_model;
}

inline bool NonTerminal::get_permutability(void) const
{
	return _is_permutable;
}

inline NonTerminal *NonTerminal::get_parent(void) const
{
	return _parent;
}

inline Node *NonTerminal::get_node(void) const
{
	return _node;
}

inline const std::list<NonTerminal *> &NonTerminal::get_children(void) const
{
	return _children;
}

inline const std::list<NonTerminal *> &NonTerminal::get_friends(void) const
{
	return _friends;
}

inline gint NonTerminal::get_height(void) const
{
	return _height;
}

inline void NonTerminal::set_node(Node *n)
{
	_node = n;
}

inline bool NonTerminal::is_leaf(void) const
{
	return false;
}

/**
 * \}
 */

#endif
