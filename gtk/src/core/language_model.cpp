#include <algorithm>

#include "language_model.hpp"

LanguageModel::LanguageModel(language_model_t *language_model,
                             LanguageModel::language_model_type_t type) :
	_language_model(language_model),
	_type(type)
{
	language_model_t::iterator iter;

	if (!_language_model) {
		return;
	}

	for (iter = _language_model->begin(); iter != _language_model->end();
	     iter++) {
		_terminals.push_back(new Terminal(*this, *iter));
	}
}

LanguageModel::~LanguageModel(void)
{
	std::list<NonTerminal *>::iterator niter;
	std::list<Terminal *>::iterator witer;

	for (witer = _terminals.begin(); witer != _terminals.end(); witer++) {
		delete *witer;
	}

	for (niter = _nonterminals.begin(); niter != _nonterminals.end(); niter++) {
		delete *niter;
	}

	delete _language_model;
}

void LanguageModel::add(NonTerminal *n)
{
	_add<NonTerminal>(_nonterminals, n, NULL);
}

void LanguageModel::add(Terminal *terminal, Terminal *loc)
{
	_add(_terminals, terminal, loc);
}

void LanguageModel::remove(NonTerminal *n)
{
	_remove(_nonterminals, n);
}

void LanguageModel::remove(Terminal *terminal)
{
	_remove(_terminals, terminal);
}

template <typename T>
void LanguageModel::_add(std::list<T *> &l, T *val, T *loc)
{
	typename std::list<T *>::iterator iter;

	iter = std::find(l.begin(), l.end(), val);

	if (iter == l.end()) {
		iter = std::find(l.begin(), l.end(), loc);

		if (loc) {
			val->set_id(loc->get_id());
		} else {
			val->set_id(l.size());
		}
		for (; iter != l.end(); iter++) {
			(*iter)->set_id((*iter)->get_id() + 1);
		}

		l.insert(iter, val);
	}
}

template<typename T>
void LanguageModel::_remove(std::list<T *> &l, T *val)
{
	typename std::list<T *>::iterator iter;

	iter = std::find(l.begin(), l.end(), val);

	if (iter != l.end()) {
		iter = l.erase(iter);

		for (; iter != l.end(); iter++) {
			(*iter)->set_id((*iter)->get_id() - 1);
		}

		delete val;
	}
}
