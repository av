/**
 * \file terminal.hpp
 *
 * Encapsulates the Terminal class.
 */

#ifndef __TERMINAL_HPP__
#define __TERMINAL_HPP__

#include "nonterminal.hpp"
#include "list.hpp"
#include "parser.hpp"

class LanguageModel;

/**
 * \ingroup Models
 * \{
 */

/**
 * \brief The Terminal class.
 *
 * This class represents a single terminal. It is also the leaf node in a language_model.
 */
class Terminal : public NonTerminal
{
public:
	/**
	 * \brief Constructor.
	 * \param language_model The language_model the terminal belongs to.
	 * \param text The terminal string.
	 */
	Terminal(LanguageModel &language_model, const Glib::ustring &text);

	/**
	 * \brief Destructor.
	 */
	~Terminal(void);

	/**
	 * \brief Gets the translation probabilities.
	 * \return The translation probabilities contained in a List.
	 */
	const Glib::RefPtr<List> &get_translation(void) const;

	/**
	 * \brief Gets the distortion probabilities.
	 * \return The distortion probabilities contained in a List.
	 */
	const Glib::RefPtr<List> &get_distortion(void) const;

	/**
	 * \brief Gets the N-gram probabilities.
	 * \return The N-gram probabilities contained in a List.
	 */
	const Glib::RefPtr<List> &get_ngram(void) const;

	/**
	 * \brief Gets the fertility probabilities.
	 * \return The fertility probabilities contained in a List.
	 */
	const Glib::RefPtr<List> &get_fertility(void) const;

	/**
	 * \brief Sets the translation probabilities.
	 * \param trans The translation probabilities for this terminal.
	 */
	void set_translation(const translation_entry_t &trans);

	/**
	 * \brief Sets the distortion probabilities.
	 * \param dist The distortion probabilities for this terminal.
	 */
	void set_distortion(const distortion_entry_t &dist);

	/**
	 * \brief Sets the N-gram probabilities.
	 * \param ngram The N-gram probabilities for this terminal.
	 */
	void set_ngram(const ngram_entry_t &ngram);

	/**
	 * \brief Sets the fertility probabilities.
	 * \param fert The fertility probabilities for this terminal.
	 */
	void set_fertility(const fertility_entry_t &fert);

	/**
	 * \brief Checks if the current non-terminal is a leaf node.
	 * \return <code>true</code> if the current non-terminal is a leaf node.
	 */
	bool is_leaf(void) const;
private:
	Glib::RefPtr<List> _trans;	//!< The translation probabilities.
	Glib::RefPtr<List> _dist;	//!< The distortion probabilities.
	Glib::RefPtr<List> _ngram;	//!< The N-gram probabilities.
	Glib::RefPtr<List> _fert;	//!< The fertility probabilities.
};

inline const Glib::RefPtr<List> &Terminal::get_translation(void) const
{
	return _trans;
}

inline const Glib::RefPtr<List> &Terminal::get_distortion(void) const
{
	return _dist;
}

inline const Glib::RefPtr<List> &Terminal::get_ngram(void) const
{
	return _ngram;
}

inline const Glib::RefPtr<List> &Terminal::get_fertility(void) const
{
	return _fert;
}

inline bool Terminal::is_leaf(void) const
{
	return true;
}

/**
 * \}
 */

#endif
