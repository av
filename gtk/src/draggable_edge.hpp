/**
 * \file draggable_edge.hpp
 *
 * Encapsulates the base class DraggableEdge.
 */

#ifndef __DRAGGABLE_EDGE_HPP__
#define __DRAGGABLE_EDGE_HPP__

#include <utility>
#include <cairomm/context.h>

#include "node.hpp"
#include "edge.hpp"

/**
 * \ingroup Views
 * \{
 */

/**
 * \brief The base class DraggableEdge.
 *
 * This class represents an edge which can be dragged, like the correspondence
 * edge which connects two Leaf objects.
 */
class DraggableEdge : public Edge
{
public:
	/**
	 * \brief The type of the current dragging end.
	 *
	 * The type of the Node which should be placed at the dragging end of the
	 * edge.
	 */
	typedef enum {
		NONE,					//!< The edge is not being dragged.
		SOURCE,					//!< The end being dragged should be connected to a Node which belongs to a source language_model.
		TARGET,					//!< The end being dragged should be connected to a Node which belongs to a target language_model.
		LEFT,
		RIGHT,
		TOP,
		BOTTOM,
	} drag_type_t;

	/**
	 * \brief Constructor.
	 */
	DraggableEdge(const TreeModel &parent);

	/**
	 * \brief Destructor.
	 */
	~DraggableEdge(void);

	/**
	 * \brief Draws the edge.
	 * \param cr The Cairo context.
	 */
	void draw(Cairo::RefPtr<Cairo::Context> cr);

	/**
	 * \brief Drags the edge.
	 * \param x The x-coordinate of the new point.
	 * \param y The y-coordinate of the new point.
	 */
	virtual void drag(gdouble x, gdouble y);

	/**
	 * \brief Drags the edge.
	 *
	 * This method specifies the type of the dragging end. It is useful when the
	 * edge is created without specifying either end, the unspecified end can be
	 * set as the dragging end.
	 *
	 * \param type The type of the dragging end.
	 * \param x The x-coordinate of the new point.
	 * \param y The y-coordinate of the new point.
	 */
	virtual void drag(drag_type_t type, gdouble x, gdouble y);

	/**
	 * \brief Gets the type of the dragging end.
	 * \return The type of the dragging end.
	 */
	virtual drag_type_t get_drag(void) const;

	/**
	 * \brief Gets the source Node.
	 * \return The source Node, or <code>NULL</code> if there isn't one.
	 */
	virtual Node *get_source(void) const;

	/**
	 * \brief Gets the target Node.
	 * \return The target Node, or <code>NULL</code> if there isn't one.
	 */
	virtual Node *get_target(void) const;

	/**
	 * \brief Changes the opacity weight.
	 * \param weight The opacity weight.
	 */
	virtual void set_opacity_weight(gdouble weight);

	/**
	 * \brief Changes the source Node.
	 * \param source The new source Node, or <code>NULL</code> to clear the
	 * source Node.
	 * \return <code>true</code> if successful.
	 */
	virtual bool change_source(Node *source);

	/**
	 * \brief Changes the target Node.
	 * \param target The new target Node, or <code>NULL</code> to clear the
	 * target Node.
	 * \return <code>true</code> if successful.
	 */
	virtual bool change_target(Node *target);

	/**
	 * \brief Restores the edge to its original state.
	 * \return <code>true</code> if restored.
	 */
	virtual bool restore(void);

	/**
	 * \brief Changes the source end to follow the mouse.
	 * \param x The x-coordinate of the current mouse point.
	 * \param y The y-coordinate of the current mouse point.
	 */
	virtual void change_source_follow_mouse(gdouble x, gdouble y);

	/**
	 * \brief Changes the target end to follow the mouse.
	 * \param x The x-coordinate of the current mouse point.
	 * \param y The y-coordinate of the current mouse point.
	 */
	virtual void change_target_follow_mouse(gdouble x, gdouble y);

	/**
	 * \brief Deletes this edge from the two Nodes at both ends.
	 *
	 * Both Nodes will also be removed from each other's friends list.
	 */
	virtual void delete_from_nodes(void);
protected:
	/**
	 * \brief Calculates the opacity based on current state.
	 */
	void _calculate_opacity(void);

	/**
	 * \brief Adds this edge to the two Nodes at both ends.
	 *
	 * Both Nodes will also be added to each other's friends list.
	 *
	 * \return <code>true</code> if successful.
	 */
	virtual bool _add_to_nodes(void);

	/**
	 * \brief Sets the source end anchor point based on the current source Node.
	 * \param emit <code>false</code> if you do not want to emit a changed
	 * signal.
	 */
	virtual void _set_source_anchor(bool emit = true);

	/**
	 * \brief Sets the target end anchor point based on the current target Node.
	 * \param emit <code>false</code> if you do not want to emit a changed
	 * signal.
	 */
	virtual void _set_target_anchor(bool emit = true);

	/**
	 * \brief Sets the source Node.
	 * \param source The new source Node.
	 * \return <code>true</code> if successful.
	 */
	virtual bool _set_source(Node *source);

	/**
	 * \brief Sets the target Node.
	 * \param target The new target Node.
	 * \return <code>true</code> if successful.
	 */
	virtual bool _set_target(Node *target);

	/**
	 * \brief Updates the extents coordinate and size.
	 * \param emit <code>true</code> if you want to set the entity as dirty, so
	 * that it will be redrawn.
	 */
	void _update_extents(bool emit = true);

	const gdouble _SPACING; //!< The default spacing between each node of different levels in matrix model.
	const gdouble _BORDER_WIDTH; //!< The default border width of the matrix representation.

	drag_type_t _drag;						  //!< The drag type.
	gdouble _border_width;					  //!< The border width.
	gdouble _opacity_weight;				  //!< The opacity weight.

	gdouble _topleft_x, _topleft_y;			//!< The topleft corner.
	gdouble _topright_x, _topright_y;		//!< The topright corner.
	gdouble _bottomleft_x, _bottomleft_y;	//!< The bottomleft corner.
	gdouble _bottomright_x, _bottomright_y; //!< The bottomright corner.

	Node *_source;				//!< The source Node.
	Node *_target;				//!< The target Node.
};

inline DraggableEdge::drag_type_t DraggableEdge::get_drag(void) const
{
	return _drag;
}

inline Node *DraggableEdge::get_source(void) const
{
	return _source;
}

inline Node *DraggableEdge::get_target(void) const
{
	return _target;
}

/**
 * \}
 */

#endif
