#include "drawable.hpp"
#include "tree_model.hpp"

Drawable::Drawable(const TreeModel &parent) :
	_path(NULL),
	_parent(parent),
	_x(0),
	_y(0),
	_width(0),
	_height(0),
	_rect(NULL),
	_is_visible(true)
{

}

Drawable::~Drawable(void)
{
	delete _path;
	delete _rect;
}

void Drawable::set_color(Color color)
{
	_normal_color = color;
}

void Drawable::set_highlight_color(Color color)
{
	_highlight_color = color;
}

void Drawable::toggle_visibility(bool value)
{
	if (value != _is_visible) {
		_is_visible = value;

		_set_dirty();
	}
}

void Drawable::_set_x(gdouble x)
{
	_x = x;

	_set_rect();
}

void Drawable::_set_y(gdouble y)
{
	_y = y;

	_set_rect();
}

void Drawable::_set_width(gdouble width)
{
	_width = width;

	_set_rect();
}

void Drawable::_set_height(gdouble height)
{
	_height = height;

	_set_rect();
}

void Drawable::_set_rect(void)
{
	delete _rect;
	_rect = new Gdk::Rectangle(gint(_parent._c2d_x(floor(_x))),
	                           gint(_parent._c2d_y(floor(_y))),
	                           gint(_parent._c2d_w(ceil(_width))),
	                           gint(_parent._c2d_h(ceil(_height))));
}
