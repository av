// vim: set tabstop=4 shiftwidth=4 foldmethod=marker:
/*
   WT Toolkit - Web UI logic toolkit. Copyright (C) 2006 Kou Man Tong

   This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
 */

/**
  @fileoverview This file contains some classical data structures and algorithms used in WT Toolkit.
 */

// {{{ the plain old linked list
/**
  Constructs a linked list node
  @constructor
  @class The plain old link list node that every CS freshman loves and hates.
  @param {Object} data The data value of this linked list node.
 */
var ListNode = function(data)
{
	this.prevNode = null;
	this.nextNode = null;
	this.data = data;
}
/**
  Adds a linked list node holding <i>data</i> after this node.
  @param {Object} data The content for the next linked list node.
  @returns The new linked list node appended after this node.
  @type ListNode
 */
ListNode.prototype.addNext = function(data)
{
	var newNode = new ListNode(data);
	newNode.nextNode = this.nextNode;
	this.nextNode.prevNode = newNode;
	newNode.prevNode = this;
	this.nextNode = newNode;
	return newNode;
}
/**
  Adds a linked list node holding <i>data</i> before this node.
  @param {Object} data The content for the previous linked list node.
  @return The new linked list node prepended before this node.
  @type ListNode
 */
ListNode.prototype.addPrev = function(data)
{
	var newNode = new ListNode(data);
	newNode.prevNode = this.prevNode;
	this.prevNode.nextNode = newNode;
	newNode.nextNode = this;
	this.prevNode = newNode;
	return newNode;
}
/**
  Removes this node from its containing linked list.
 */
ListNode.prototype.removeSelf = function()
{
	this.prevNode.nextNode = this.nextNode;
	this.nextNode.prevNode = this.prevNode;
}
/**
  Constructs a linked list
  @constructor
  @class A doubly-linked, circular linked list. The initial node of the linked list holds a value of null
  so that you can know when to stop walking.
 */
var LinkedList = function()
{
	this.prevNode = this;
	this.nextNode = this;
}
LinkedList.prototype = new ListNode(null);
// }}}
// {{{ disjoint set with union-by-rank and path compression optimizations
/**
  Constructs a set universe.
  @constructor
  @class A set universe for containing set elements.
 */
var SetUniverse = function()
{
	this.objects = [];
	this.length = 0;
}
/**
  Adds a set element to this universe.
  @param {SetElement} obj The set element to be added.
 */
SetUniverse.prototype.add = function(obj)
{
	this.objects.push(obj);
	this.length++;
}
/**
  Clears this set universe of all set elements.
 */
SetUniverse.prototype.clear = function()
{
	while(this.objects.length)
	{
		var obj = this.objects[0];
		var realobj = obj.obj;
		if (realobj instanceof Object)
			delete realobj.wtSetElm;
		else if (isDOMNode(realobj))
			realobj.removeAttribute("wtSetElm");
		else
			realobj.wtSetElm = undefined;
		delete obj.obj;
		this.objects.splice(0, 1);
	}
	this.length = 0;
}
/**
  Makes a generator for generating set element objects from arbitary objects for this universe.
  @returns Generator function
  @type Function
 */
SetUniverse.prototype.getGenerator = function()
{
	var u = this;
	var retval = function(obj)
	{
		return SetElement(u, obj);
	}
	return retval;
}
/**
  Constructs a set element for a set universe from an arbitary object. If you want to construct
  a set element, it is recommended and more convenient to use the set universe's provided
  generator function instead.
  @constructor
  @class A set element contained in a set universe
  @param {SetUniverse} univ The container set universe
  @param {Object} An arbitary object for this set element to have equivalent relation to
  @see SetUniverse#getGenerator
 */
var SetElement = function(univ, obj)
{
	if (obj instanceof SetElement)
		return obj;
	else if (obj.wtSetElm)
		return obj.wtSetElm;
	else if (!(this instanceof SetElement))
		return new SetElement(univ, obj);

	obj.wtSetElm = this;
	this.obj = obj;
	this.parent = null;
	this.rank = 0;
	univ.add(this);
}
/**
  Unions this set element with another set element
  @param {SetElement} obj The other set element
 */
SetElement.prototype.union = function(obj)
{
	myRoot = this.find();
	theirRoot = obj.find();
	if (myRoot == theirRoot)
		return;

	if (myRoot.rank < theirRoot.rank)
		myRoot.parent = theirRoot;
	else if (myRoot.rank > theirRoot.rank)
		theirRoot.parent = myRoot;
	else
	{
		myRoot.parent = theirRoot;
		theirRoot.rank++;
	}
}
/**
  Finds the set identifier for this set element. All set elements inside the same disjoint set have the same set identifier.
  @returns Set identifier
  @type SetElement
 */
SetElement.prototype.find = function()
{
	var curNode = this;
	while(curNode.parent)
		curNode = curNode.parent;
	if (this.parent != null)
		this.parent = curNode;
	return curNode;
}
// }}}
// {{{ skip list (a simple and reasonably efficient sorted list)
// [TODO] this should solve the O(n^2) array sorting time in Internet Explorer, while at the same time being a generally useful
// data structure. But I can't really find a use for this yet, so it's not yet implemented.
// }}}
