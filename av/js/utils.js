// Capitalize the first character of the string.
function capitalize(str)
{
	if (typeof(str) == "string" ||
		(typeof(str) == "object" && typeof(str.valueOf()) == "string")) {
		return str.charAt(0).toUpperCase() + str.substr(1);
	}

	throw "capitalize(str) expects a string!";
}

// Get the absolute position of an element
function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
}