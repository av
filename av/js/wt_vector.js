// vim: set tabstop=4 shiftwidth=4 foldmethod=marker:
/*
   WT Toolkit - Web UI logic toolkit. Copyright (C) 2006 Kou Man Tong

   This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
 */

/*
 * $Log: wt_vector.js,v $
 * Revision 1.44  2007/03/28 00:29:00  francium
 * -Fixed: 3D object animation error after a 3D object is deleted
 *
 * Revision 1.43  2007/03/27 21:02:00  francium
 * -Added: 3D object animation in vector widgets demo
 *
 * Revision 1.42  2007/03/27 20:37:41  francium
 * -Fixed: DOM nodes with wtDependency are not checked by the garbage collector
 *         correctly
 * -Added: wtGraph class
 *
 * Revision 1.41  2007/03/26 04:50:24  marco
 * -Fixed: typo in wtVectorWidget::setAlpha()
 * -Added: wtVectorWidget::getStrokeWidth, wtVectorWidget::setStrokeWidth()
 * -Added: wtVectorGroup::setOpacity()
 *
 * Revision 1.40  2007/03/25 16:23:00  marco
 * -Added: wtChart::setXAxisLabel, wChart::setYAxisLabel
 *
 * Revision 1.39  2007/03/24 19:32:00  francium
 * -Fixed: "_$() is not a function" errors on page load
 * -Added: MouseOver and MouseOut signals for wtVectorGroup
 * -Added: Translucent effects for wtBar
 * -Added: wtColorGenerator::spin() operation
 * -Improved: wtChart's color selection mechanism
 * -Improved: Vector widget demo
 *
 * Revision 1.38  2007/03/24 11:45:23  marco
 * -Modified: Tuned down the saturation value of the bar color
 *
 * Revision 1.37  2007/03/24 08:23:16  francium
 * -Fixed: "wtObject not defined" error on page load
 * -Fixed: Errors being ignored within event handlers
 *
 * Revision 1.36  2007/03/24 07:44:12  francium
 * -Added: WTObjectManager::scheduleGC() which enables the programmer to schedule
 *         GC more liberally without worrying about repeated garbage collections
 * -Improved: wtGraphEdge can now let the user add/remove anchor points and
 *         change the coordinates of anchor points by mouse actions
 *
 * Revision 1.35  2007/03/23 11:03:10  francium
 * *** empty log message ***
 *
 * Revision 1.34  2007/03/22 14:12:37  francium
 * *** empty log message ***
 *
 * Revision 1.33  2007/03/22 13:20:44  francium
 * *** empty log message ***
 *
 * Revision 1.32  2007/03/22 05:36:51  marco
 * -Fixed: wtText:setText(), wtText:setFont(), wtText:setFontStyle(), wtText:setSize(), wtText:setColor() for IE
 *
 * Revision 1.31  2007/03/21 19:05:54  francium
 * -Added: wtGraphEdge and wtGraphVertex classes
 * -Added: wt2DPoint class
 *
 * Revision 1.30  2007/03/21 14:50:18  francium
 * -Fixed: Nested VML groups has incorrect coordinate scaling in Internet
 *         Explorer
 *
 * Revision 1.29  2007/03/21 14:28:44  francium
 * -Added: wtArrow class
 *
 * Revision 1.28  2007/03/21 14:17:25  marco
 * -Fixed: wtPath:curveTo()
 *
 * Revision 1.27  2007/03/21 13:33:20  marco
 * -Added: added charting widgets
 *
 * $Id: wt_vector.js,v 1.44 2007/03/28 00:29:00 francium Exp $
 */
/**
  @fileoverview This file contains all the classes related to SVG/VML vector graphics.
 */
// {{{ Math functions for converting angles.
/**
  Converts an angle in degrees to radians.
 */
Math.deg2rad = function(_ang) 
{
	return _ang/180.0*Math.PI;
}
/**
  Converts an angle in radians to degrees.
 */
Math.rad2deg = function(_rad)
{
	return _rad*180.0/Math.PI;
}
// }}}
// {{{ wtColorSpace - Package for converting between HSV, RGB and CSS color strings.
var wtColorSpace = 
{
	// HSV to RGB converter
	// reference: http://en.wikipedia.org/wiki/HSV_color_space
	// note that this function uses [0.0, 1.0] as the range for HSV input
	// and the range [0, 255] as the range for RGB output
	"hsv2rgb": function(hsv)
	{
		var H = (hsv["hue"] * 360) % 360;
		var S = hsv["saturation"];
		var V = hsv["value"];
		var Hi = parseInt((H / 60.0) % 6);
		var f = H / 60.0 - Hi;
		var p = V * (1 - S);
		var q = V * (1 - f * S);
		var t = V * (1 - (1 - f) * S);
		var retval = null;
		if (Hi == 0)
			retval = {"red":V, "green":t, "blue":p};
		else if (Hi == 1)
			retval = {"red":q, "green":V, "blue":p};
		else if (Hi == 2)
			retval = {"red":p, "green":V, "blue":t};
		else if (Hi == 3)
			retval = {"red":p, "green":q, "blue":V};
		else if (Hi == 4)
			retval = {"red":t, "green":p, "blue":V};
		else if (Hi == 5)
			retval = {"red":V, "green":p, "blue":q};
		retval.red *= 255;
		retval.green *= 255;
		retval.blue *= 255;
		return retval;
	},
	"rgb2hsv": function(rgb)
	{
		// reference: http://www.cs.rit.edu/~ncs/color/t_convert.html
		var min, max, delta, hue, saturation, value;
		min = Math.min(rgb.red, rgb.green, rgb.blue);
		max = Math.max(rgb.red, rgb.green, rgb.blue);
		delta = max - min;
		value = max;

		if (max != 0)
		{
			saturation = delta / max;
			if (rgb.red == max)
				hue = (rgb.green - rgb.blue) / delta;
			else if (rgb.green == max)
				hue = 2 + (rgb.blue - rgb.red) / delta;
			else
				hue = 4 + (rgb.red - rgb.green) / delta;
		}
		else
		{
			saturation = 0;
			hue = 0;
		}

		hue *= 60;
		if (hue < 0)
			hue += 360;
		hue /= 360;
		value /= 255;
		return {"hue": hue, "saturation": saturation, "value": value};
	},
	"cssColorTable":
	{
		"aliceblue":{"red":0xf0, "green":0xf8, "blue":0xff}, 	 
		"antiquewhite":{"red":0xfa, "green":0xeb, "blue":0xd7}, 	 
		"aqua":{"red":0x00, "green":0xff, "blue":0xff}, 	 
		"aquamarine":{"red":0x7f, "green":0xff, "blue":0xd4}, 	 
		"azure":{"red":0xf0, "green":0xff, "blue":0xff}, 	 
		"beige":{"red":0xf5, "green":0xf5, "blue":0xdc}, 	 
		"bisque":{"red":0xff, "green":0xe4, "blue":0xc4}, 	 
		"black":{"red":0x00, "green":0x00, "blue":0x00}, 	 
		"blanchedalmond":{"red":0xff, "green":0xeb, "blue":0xcd}, 	 
		"blue":{"red":0x00, "green":0x00, "blue":0xff}, 	 
		"blueviolet":{"red":0x8a, "green":0x2b, "blue":0xe2}, 	 
		"brown":{"red":0xa5, "green":0x2a, "blue":0x2a}, 	 
		"burlywood":{"red":0xde, "green":0xb8, "blue":0x87}, 	 
		"cadetblue":{"red":0x5f, "green":0x9e, "blue":0xa0}, 	 
		"chartreuse":{"red":0x7f, "green":0xff, "blue":0x00}, 	 
		"chocolate":{"red":0xd2, "green":0x69, "blue":0x1e}, 	 
		"coral":{"red":0xff, "green":0x7f, "blue":0x50}, 	 
		"cornflowerblue":{"red":0x64, "green":0x95, "blue":0xed}, 	 
		"cornsilk":{"red":0xff, "green":0xf8, "blue":0xdc}, 	 
		"crimson":{"red":0xdc, "green":0x14, "blue":0x3c}, 	 
		"cyan":{"red":0x00, "green":0xff, "blue":0xff}, 	 
		"darkblue":{"red":0x00, "green":0x00, "blue":0x8b}, 	 
		"darkcyan":{"red":0x00, "green":0x8b, "blue":0x8b}, 	 
		"darkgoldenrod":{"red":0xb8, "green":0x86, "blue":0x0b}, 	 
		"darkgray":{"red":0xa9, "green":0xa9, "blue":0xa9}, 	 
		"darkgrey":{"red":0xa9, "green":0xa9, "blue":0xa9}, 	 
		"darkgreen":{"red":0x00, "green":0x64, "blue":0x00}, 	 
		"darkkhaki":{"red":0xbd, "green":0xb7, "blue":0x6b}, 	 
		"darkmagenta":{"red":0x8b, "green":0x00, "blue":0x8b}, 	 
		"darkolivegreen":{"red":0x55, "green":0x6b, "blue":0x2f}, 	 
		"darkorange":{"red":0xff, "green":0x8c, "blue":0x00}, 	 
		"darkorchid":{"red":0x99, "green":0x32, "blue":0xcc}, 	 
		"darkred":{"red":0x8b, "green":0x00, "blue":0x00}, 	 
		"darksalmon":{"red":0xe9, "green":0x96, "blue":0x7a}, 	 
		"darkseagreen":{"red":0x8f, "green":0xbc, "blue":0x8f}, 	 
		"darkslateblue":{"red":0x48, "green":0x3d, "blue":0x8b}, 	 
		"darkslategray":{"red":0x2f, "green":0x4f, "blue":0x4f}, 	 
		"darkslategrey":{"red":0x2f, "green":0x4f, "blue":0x4f}, 	 
		"darkturquoise":{"red":0x00, "green":0xce, "blue":0xd1}, 	 
		"darkviolet":{"red":0x94, "green":0x00, "blue":0xd3}, 	 
		"deeppink":{"red":0xff, "green":0x14, "blue":0x93}, 	 
		"deepskyblue":{"red":0x00, "green":0xbf, "blue":0xff}, 	 
		"dimgray":{"red":0x69, "green":0x69, "blue":0x69}, 	 
		"dimgrey":{"red":0x69, "green":0x69, "blue":0x69}, 	 
		"dodgerblue":{"red":0x1e, "green":0x90, "blue":0xff}, 	 
		"firebrick":{"red":0xb2, "green":0x22, "blue":0x22}, 	 
		"floralwhite":{"red":0xff, "green":0xfa, "blue":0xf0}, 	 
		"forestgreen":{"red":0x22, "green":0x8b, "blue":0x22}, 	 
		"fuchsia":{"red":0xff, "green":0x00, "blue":0xff}, 	 
		"gainsboro":{"red":0xdc, "green":0xdc, "blue":0xdc}, 	 
		"ghostwhite":{"red":0xf8, "green":0xf8, "blue":0xff}, 	 
		"gold":{"red":0xff, "green":0xd7, "blue":0x00}, 	 
		"goldenrod":{"red":0xda, "green":0xa5, "blue":0x20}, 	 
		"gray":{"red":0x80, "green":0x80, "blue":0x80}, 	 
		"grey":{"red":0x80, "green":0x80, "blue":0x80}, 	 
		"green":{"red":0x00, "green":0x80, "blue":0x00}, 	 
		"greenyellow":{"red":0xad, "green":0xff, "blue":0x2f}, 	 
		"honeydew":{"red":0xf0, "green":0xff, "blue":0xf0}, 	 
		"hotpink":{"red":0xff, "green":0x69, "blue":0xb4}, 	 
		"indianred":{"red":0xcd, "green":0x5c, "blue":0x5c}, 	 
		"indigo":{"red":0x4b, "green":0x00, "blue":0x82}, 	 
		"ivory":{"red":0xff, "green":0xff, "blue":0xf0}, 	 
		"khaki":{"red":0xf0, "green":0xe6, "blue":0x8c}, 	 
		"lavender":{"red":0xe6, "green":0xe6, "blue":0xfa}, 	 
		"lavenderblush":{"red":0xff, "green":0xf0, "blue":0xf5}, 	 
		"lawngreen":{"red":0x7c, "green":0xfc, "blue":0x00}, 	 
		"lemonchiffon":{"red":0xff, "green":0xfa, "blue":0xcd}, 	 
		"lightblue":{"red":0xad, "green":0xd8, "blue":0xe6}, 	 
		"lightcoral":{"red":0xf0, "green":0x80, "blue":0x80}, 	 
		"lightcyan":{"red":0xe0, "green":0xff, "blue":0xff}, 	 
		"lightgoldenrodyellow":{"red":0xfa, "green":0xfa, "blue":0xd2}, 	 
		"lightgray":{"red":0xd3, "green":0xd3, "blue":0xd3}, 	 
		"lightgrey":{"red":0xd3, "green":0xd3, "blue":0xd3}, 	 
		"lightgreen":{"red":0x90, "green":0xee, "blue":0x90}, 	 
		"lightpink":{"red":0xff, "green":0xb6, "blue":0xc1}, 	 
		"lightsalmon":{"red":0xff, "green":0xa0, "blue":0x7a}, 	 
		"lightseagreen":{"red":0x20, "green":0xb2, "blue":0xaa}, 	 
		"lightskyblue":{"red":0x87, "green":0xce, "blue":0xfa}, 	 
		"lightslategray":{"red":0x77, "green":0x88, "blue":0x99}, 	 
		"lightslategrey":{"red":0x77, "green":0x88, "blue":0x99}, 	 
		"lightsteelblue":{"red":0xb0, "green":0xc4, "blue":0xde}, 	 
		"lightyellow":{"red":0xff, "green":0xff, "blue":0xe0}, 	 
		"lime":{"red":0x00, "green":0xff, "blue":0x00}, 	 
		"limegreen":{"red":0x32, "green":0xcd, "blue":0x32}, 	 
		"linen":{"red":0xfa, "green":0xf0, "blue":0xe6}, 	 
		"magenta":{"red":0xff, "green":0x00, "blue":0xff}, 	 
		"maroon":{"red":0x80, "green":0x00, "blue":0x00}, 	 
		"mediumaquamarine":{"red":0x66, "green":0xcd, "blue":0xaa}, 	 
		"mediumblue":{"red":0x00, "green":0x00, "blue":0xcd}, 	 
		"mediumorchid":{"red":0xba, "green":0x55, "blue":0xd3}, 	 
		"mediumpurple":{"red":0x93, "green":0x70, "blue":0xd8}, 	 
		"mediumseagreen":{"red":0x3c, "green":0xb3, "blue":0x71}, 	 
		"mediumslateblue":{"red":0x7b, "green":0x68, "blue":0xee}, 	 
		"mediumspringgreen":{"red":0x00, "green":0xfa, "blue":0x9a}, 	 
		"mediumturquoise":{"red":0x48, "green":0xd1, "blue":0xcc}, 	 
		"mediumvioletred":{"red":0xc7, "green":0x15, "blue":0x85}, 	 
		"midnightblue":{"red":0x19, "green":0x19, "blue":0x70}, 	 
		"mintcream":{"red":0xf5, "green":0xff, "blue":0xfa}, 	 
		"mistyrose":{"red":0xff, "green":0xe4, "blue":0xe1}, 	 
		"moccasin":{"red":0xff, "green":0xe4, "blue":0xb5}, 	 
		"navajowhite":{"red":0xff, "green":0xde, "blue":0xad}, 	 
		"navy":{"red":0x00, "green":0x00, "blue":0x80}, 	 
		"oldlace":{"red":0xfd, "green":0xf5, "blue":0xe6}, 	 
		"olive":{"red":0x80, "green":0x80, "blue":0x00}, 	 
		"olivedrab":{"red":0x6b, "green":0x8e, "blue":0x23}, 	 
		"orange":{"red":0xff, "green":0xa5, "blue":0x00}, 	 
		"orangered":{"red":0xff, "green":0x45, "blue":0x00}, 	 
		"orchid":{"red":0xda, "green":0x70, "blue":0xd6}, 	 
		"palegoldenrod":{"red":0xee, "green":0xe8, "blue":0xaa}, 	 
		"palegreen":{"red":0x98, "green":0xfb, "blue":0x98}, 	 
		"paleturquoise":{"red":0xaf, "green":0xee, "blue":0xee}, 	 
		"palevioletred":{"red":0xd8, "green":0x70, "blue":0x93}, 	 
		"papayawhip":{"red":0xff, "green":0xef, "blue":0xd5}, 	 
		"peachpuff":{"red":0xff, "green":0xda, "blue":0xb9}, 	 
		"peru":{"red":0xcd, "green":0x85, "blue":0x3f}, 	 
		"pink":{"red":0xff, "green":0xc0, "blue":0xcb}, 	 
		"plum":{"red":0xdd, "green":0xa0, "blue":0xdd}, 	 
		"powderblue":{"red":0xb0, "green":0xe0, "blue":0xe6}, 	 
		"purple":{"red":0x80, "green":0x00, "blue":0x80}, 	 
		"red":{"red":0xff, "green":0x00, "blue":0x00}, 	 
		"rosybrown":{"red":0xbc, "green":0x8f, "blue":0x8f}, 	 
		"royalblue":{"red":0x41, "green":0x69, "blue":0xe1}, 	 
		"saddlebrown":{"red":0x8b, "green":0x45, "blue":0x13}, 	 
		"salmon":{"red":0xfa, "green":0x80, "blue":0x72}, 	 
		"sandybrown":{"red":0xf4, "green":0xa4, "blue":0x60}, 	 
		"seagreen":{"red":0x2e, "green":0x8b, "blue":0x57}, 	 
		"seashell":{"red":0xff, "green":0xf5, "blue":0xee}, 	 
		"sienna":{"red":0xa0, "green":0x52, "blue":0x2d}, 	 
		"silver":{"red":0xc0, "green":0xc0, "blue":0xc0}, 	 
		"skyblue":{"red":0x87, "green":0xce, "blue":0xeb}, 	 
		"slateblue":{"red":0x6a, "green":0x5a, "blue":0xcd}, 	 
		"slategray":{"red":0x70, "green":0x80, "blue":0x90}, 	 
		"slategrey":{"red":0x70, "green":0x80, "blue":0x90}, 	 
		"snow":{"red":0xff, "green":0xfa, "blue":0xfa}, 	 
		"springgreen":{"red":0x00, "green":0xff, "blue":0x7f}, 	 
		"steelblue":{"red":0x46, "green":0x82, "blue":0xb4}, 	 
		"tan":{"red":0xd2, "green":0xb4, "blue":0x8c}, 	 
		"teal":{"red":0x00, "green":0x80, "blue":0x80}, 	 
		"thistle":{"red":0xd8, "green":0xbf, "blue":0xd8}, 	 
		"tomato":{"red":0xff, "green":0x63, "blue":0x47}, 	 
		"turquoise":{"red":0x40, "green":0xe0, "blue":0xd0}, 	 
		"violet":{"red":0xee, "green":0x82, "blue":0xee}, 	 
		"wheat":{"red":0xf5, "green":0xde, "blue":0xb3}, 	 
		"white":{"red":0xff, "green":0xff, "blue":0xff}, 	 
		"whitesmoke":{"red":0xf5, "green":0xf5, "blue":0xf5}, 	 
		"yellow":{"red":0xff, "green":0xff, "blue":0x00}, 	 
		"yellowgreen":{"red":0x9a, "green":0xcd, "blue":0x32}
	}
}
// }}}
// {{{ wtColor - Color that can be translated within and trasferred between RGB and HSV color spaces.
/**
  Constructs a color object.
  @class A color object, which contains RGBA channels with 8 bits per channel. It can be converted to RGB, HSV color spaces, 
  and can be converted to CSS color strings.
  @constructor
  @param {wtColor|String} colorSpace (Optional) If this is a wtColor, then a new wtColor object is copied from the given one, 
  with exactly the same RGBA channel values; if this is a CSS color name (e.g. "red", "pink", "violet", "darkblue", etc.), 
  then a new wtColor object with the corresponding color and 100% opacity is generated; if either "rgb", "hsv" or "rgba" is
  given, then the second argument <i>color</i> would be required with the corresponding format; if null, undefined, or "none" 
  is given, then a black color with 0 opacity is generated.
  @param {Object} color (Optional) This is only required if the <i>colorSpace</i> argument is specified to "rgb", "hsv" or 
  "rgba". The format of this object is the same as the output of corresponding toRGB(), toRGBA() or toHSV() operations.
 */
var wtColor = function(colorSpace, color)
{
	if (arguments.length < 1)
		return;

	this.alpha = 255;
	var matchArray;
	if (typeof(colorSpace) == "string" && 
			(matchArray = colorSpace.match(new RegExp("rgb\\s*\\(\\s*(\\d{1,3})\\s*,\\s*(\\d{1,3})\\s*,\\s*(\\d{1,3})\\s*\\)"))))
	{
		/**
		  @type Integer
		 */
		this.red = parseInt(matchArray[1]);
		/**
		  @type Integer
		 */
		this.green = parseInt(matchArray[2]);
		/**
		  @type Integer
		 */
		this.blue = parseInt(matchArray[3]);
	}
	else if (typeof(colorSpace) == "string" &&
			(matchArray = colorSpace.match(
										   new RegExp("rgba\\s*\\(\\s*(\\d{1,3})\\s*,\\s*(\\d{1,3})\\s*,\\s*(\\d{1,3})\\s*,\\s*(\\d{1,3})\\s*\\)"))))
	{
		this.red = parseInt(matchArray[1]);
		this.green = parseInt(matchArray[2]);
		this.blue = parseInt(matchArray[3]);
		/**
		  @type Integer
		 */
		this.alpha = parseInt(matchArray[4]);
	}
	else if (arguments.length == 0 || (typeof(colorSpace) == "string" && (colorSpace == "none" || colorSpace == "")) ||
			colorSpace == undefined || colorSpace == null)
	{
		this.red = this.green = this.blue = this.alpha = 0;
	}
	else if (colorSpace instanceof wtColor)
	{
		this.red = colorSpace.red;
		this.green = colorSpace.green;
		this.blue = colorSpace.blue;
		this.alpha = colorSpace.alpha;
	}
	else if (arguments.length == 1 && typeof(colorSpace) == "string")
	{
		var rgb = wtColorSpace.cssColorTable[colorSpace.toLowerCase()];
		if (!rgb)
			throw "wtColorSpace::wtColor: I can't make sense of the color name '"+colorSpace+"' that you told me!";
		this.red = rgb.red;
		this.green = rgb.green;
		this.blue = rgb.blue;
	}
	else if (colorSpace == "rgb")
	{
		this.red = parseInt(color.red);
		this.green = parseInt(color.green);
		this.blue = parseInt(color.blue);
	}
	else if (colorSpace == "hsv")
	{
		color = wtColorSpace.hsv2rgb(color);
		this.red = parseInt(color.red);
		this.green = parseInt(color.green);
		this.blue = parseInt(color.blue);
	}
	else if (colorSpace == "rgba")
	{
		this.red = parseInt(color.red);
		this.green = parseInt(color.green);
		this.blue = parseInt(color.blue);
		this.alpha = parseInt(color.alpha);
	}
	else
		throw "wtColorSpace::wtColor: colorSpace must be one of the following, rgb, hsv, rgb(#,#,#), rgba(#,#,#,#), or a CSS color name.";
}
wtColor.prototype = new Object;
/**
  Converts the color object to a CSS color string. Note that this operation ignores the alpha channel of the color.
  @returns CSS color string.
  @type String
 */
wtColor.prototype.toString = function()
{
	return "rgb($RED,$GREEN,$BLUE)".replace("$RED", this.red).replace("$GREEN", this.green).replace("$BLUE", this.blue);
}
/**
  Tells the alpha value of the color. Returned alpha value is in the range [0.0, 1.0].
  @returns Color alpha value.
  @type Number
 */
wtColor.prototype.getAlpha = function()
{
	return this.alpha / 255;
}
/**
  Sets the alpha value of the color. Input alpha value should be in the range [0.0, 1.0].
  @param {Number} alpha New alpha value.
 */
wtColor.prototype.setAlpha = function(alpha)
{
	this.alpha = parseInt(alpha * 255);
}
/**
  Tells the RGB values of the this color. RGB value outputs are in the range [0, 255].
  @returns "red": red value, "green": green value, "blue": blue value.
  @type Object
 */
wtColor.prototype.toRGB = function()
{
	return {"red": this.red, "green": this.green, "blue": this.blue};
}
/**
  Tells the HSV values of the color. HSV value outputs are in the range [0.0, 1.0].
  A value of 1.0 in the hue means 360 degrees, 0.25 means 90 degrees, etc.
  @returns "hue": hue, "saturation": saturation, "value": value.
  @type Object
 */
wtColor.prototype.toHSV = function()
{
	return wtColorSpace.rgb2hsv(this.toRGB());
}
/**
  Tells the RGBA values of the this color. RGBA value outputs are in the range [0, 255].
  @returns "red": red value, "green": green value, "blue": blue value, "alpha": alpha value.
  @type Object
 */
wtColor.prototype.toRGBA = function()
{
	return {"red": this.red, "green": this.green, "blue": this.blue, "alpha": this.alpha};
}
/**
  Generates a new color by moving from the current color's coordinates in the HSV cone.
  @param {Number} hOffset Offset in hue.
  @param {Number} sOffset Offset in saturation.
  @param {Number} vOffset Offset in value.
  @returns The new color.
  @type wtColor
 */
wtColor.prototype.translateHSV = function(hOffset, sOffset, vOffset)
{
	var hsv = this.toHSV();
	hsv.hue += hOffset;
	hsv.saturation += sOffset;
	hsv.value += vOffset;
	hsv.hue = Math.min(Math.max(0.0, hsv.hue), 1.0);
	hsv.saturation = Math.min(Math.max(0.0, hsv.saturation), 1.0);
	hsv.value = Math.min(Math.max(0.0, hsv.value), 1.0);
	var retval = new wtColor("hsv", hsv);
	retval.alpha = this.alpha;
	return retval;
}
/**
  Generates a new color by moving from the current color's coordinates in the RGB cube.
  @param {Number} rOffset Offset in red value.
  @param {Number} gOffset Offset in green value.
  @param {Number} bOffset Offset in blue value.
  @returns The new color.
  @type wtColor
 */
wtColor.prototype.translateRGB = function(rOffset, gOffset, bOffset)
{
	var rgb = this.toRGB();
	rgb.red += rOffset;
	rgb.green += gOffset;
	rgb.blue += bOffset;
	rgb.red = Math.min(Math.max(0.0, rgb.red), 1.0);
	rgb.green = Math.min(Math.max(0.0, rgb.green), 1.0);
	rgb.blue = Math.min(Math.max(0.0, rgb.blue), 1.0);
	var retval = new wtColor("rgb", rgb);
	retval.alpha = alpha;
	return retval;
}
/**
  Generates a new color by blending the RGB values of two colors by a weighted average.
  @param {wtColor} color The other color.
  @param {Number} myWeight Weight of this color used in the weighted average calculation.
  @returns The new color.
  @type wtColor
 */
wtColor.prototype.blendRGB = function(color, myWeight)
{
	if (myWeight > 1.0 || myWeight < 0.0)
		throw "wtColor::blend(): myWeight must be in the range [0.0, 1.0]!";
	myRGB = this.toRGB;
	hisRGB = color.toRGB;
	var red = myRGB.red * myWeight + hisRGB.red * (1 - myWeight);
	var green = myRGB.green * myWeight + hisRGB.green * (1 - myWeight);
	var blue = myRGB.blue * myWeight + hisRGB.blue * (1 - myWeight);
	return new wtColor("rgb", {"red":red, "green":green, "blue":blue});
}
// }}}
// {{{ wtColorGenerator - A color generator which gives out unique and distinguishable colors
/**
  Constructs a color generator which returns unique colors which tries to be visually distinct.
  @class A color generator which gives out unique and distinguishable colors.
  @param {Integer} Hsteps Number of steps to divide the uppermost and outermost hue ring of the HSV cone into.
  @param {Integer} Ssteps Number of steps to divide the uppermost saturation radius of the HSV cone into.
  @param {Integer} Vsteps Number of steps to divide the value height of the HSV cone into.
  @constructor
 */
var wtColorGenerator = function(Hsteps, Ssteps, Vsteps)
{
	this.colorArray = [];
	// this emulates a uniform distribution of color across the HSV cone model
	for(var vindex=0;vindex<Vsteps;vindex++)
	{
		for(var sindex=0;sindex<Ssteps*((vindex+1)/Vsteps);sindex++)
		{
			for(var hindex=0;hindex<Hsteps*((vindex+1)/Vsteps)*((sindex+1)/Ssteps);hindex++)
			{
				var H = (hindex + 1) / (Hsteps*((vindex+1)/Vsteps)*((sindex+1)/Ssteps));
				var S = (sindex + 1) / (Ssteps*(vindex+1)/Vsteps);
				var V = (vindex + 1) / Vsteps;
				var color = new wtColor("hsv", {"hue":Math.min(H, 1.0), "saturation":Math.min(S, 1.0), "value":Math.min(V, 1.0)});
				this.colorArray.push(color);
			}
		}
	}
	this.colorArray.push(new wtColor("rgb", {"red":0, "green":0, "blue":0}));
}
wtColorGenerator.prototype = new Object;
/**
  Tells a unique color generated from the color generator. Returns null if there are no more unique colors.
  @type wtColor
  @returns A unique color or null.
 */
wtColorGenerator.prototype.getColor = function()
{
	return this.colorArray.pop();
}
/**
  Tells the number of unique color left in the color generator.
  @type Integer
  @returns The number of unique color left in the color generator.
 */
wtColorGenerator.prototype.count = function()
{
	return this.colorArray.length;
}
/**
  Shuffles the stored colors in the color generator randomly.
 */
wtColorGenerator.prototype.randomize = function()
{
	for(var i=0;i<this.count();i++)
	{
		var swapIndex = parseInt(Math.random() * this.count());
		var tmpColor = this.colorArray[i];
		this.colorArray[i] = this.colorArray[swapIndex];
		this.colorArray[swapIndex] = tmpColor;
	}
}
/**
  Rotates the stored colors in the color generator by a number of steps.
 */
wtColorGenerator.prototype.spin = function(steps)
{
	for(var i=0;i<steps;i++)
		this.colorArray.unshift(this.colorArray.pop());
}
// }}}
// {{{ wt2DTransform - A 2D affine transformation.
/**
  Constructs a 2D affine transformation.
  @class A 2D affine transformation, which can be used to transform wt2DPoints.
  @constructor
 */
var wt2DTransform = function()
{
	if (isMSIE())
	{
		this.matrix = [
			[1,0,0],
			[0,1,0],
			[0,0,1]
				];
	}
	else
	{
		/**
		  @private
		 */
		this.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		/**
		  @private
		 */
		this.matrix = this.svg.createSVGMatrix();
	}
}
wt2DTransform.prototype = new Object;
/**
  Post-multiplies a translation matrix to the current affine transform.
  @param {Number} x The x-axis translation.
  @param {Number} y The y-axis translation.
  @returns The modified transformation.
  @type wt2DTransform
 */
wt2DTransform.prototype.translate = function(x, y)
{
	if (isMSIE())
	{
		var translateTransform = new wt2DTransform();
		translateTransform.matrix[0][2] = x;
		translateTransform.matrix[1][2] = y;
		var resultTransform = this.multiply(translateTransform);
		this.matrix = resultTransform.matrix;
	}
	else
		this.matrix = this.matrix.translate(x, y);
	return this;
}
/**
  Post-multiplies a scale matrix to the current affine transform.
  @param {Number} xScale The x-axis scaling factor.
  @param {Number} yScale (Optional) The y-axis scaling factor. If not given, then it is assumed that
  the y-axis scaling factor is the same as that of the x-axis.
  @returns The modified transformation.
  @type wt2DTransform
 */
wt2DTransform.prototype.scale = function(xScale, yScale)
{
	if (yScale == undefined)
		yScale = xScale;

	if (isMSIE())
	{
		var scaleTransform = new wt2DTransform();
		scaleTransform.matrix[0][0] = xScale;
		scaleTransform.matrix[1][1] = yScale;
		var resultTransform = this.multiply(scaleTransform);
		this.matrix = resultTransform.matrix;
	}
	else
		this.matrix = this.matrix.scale(xScale, yScale);
	return this;
}
/**
  Post-multiplies a rotational matrix to the current affine transform.
  @param {Number} degrees The number of degrees to rotate.
  @returns The modified transformation.
  @type wt2DTransform
 */
wt2DTransform.prototype.rotate = function(degrees)
{
	if (isMSIE())
	{
		var rotateTransform = new wt2DTransform();
		rotateTransform.matrix[0][0] = Math.cos(Math.deg2rad(degrees));
		rotateTransform.matrix[0][1] = -Math.sin(Math.deg2rad(degrees));
		rotateTransform.matrix[1][0] = Math.sin(Math.deg2rad(degrees));
		rotateTransform.matrix[1][1] = Math.cos(Math.deg2rad(degrees));
		var resultTransform = this.multiply(rotateTransform);
		this.matrix = resultTransform.matrix;
	}
	else
		this.matrix = this.matrix.rotate(degrees);
	return this;
}
/**
  Post-multiplies a centered rotational matrix to the current affine transform.
  @param {Number} degrees The number of degrees to rotate.
  @param {Number} x The x-axis coordinate of the rotational center.
  @param {Number} y The y-axis coordinate of the rotational center.
  @returns The modified transformation.
  @type wt2DTransform
 */
wt2DTransform.prototype.rotateAt = function(degrees, x, y)
{
	this.translate(x, y);
	this.rotate(degrees);
	return this.translate(x*-1, y*-1);
}
/**
  Generates a new 2D affine transform by post-multiplying another affine transform to the current one.
  @param {wt2DTransform} transform The other 2D affine transform.
  @returns The product.
  @type wt2DTransform
 */
wt2DTransform.prototype.multiply = function(transform)
{
	if (isMSIE())
	{
		var retval = new wt2DTransform();
		for(var i=0;i<3;i++)
		{
			for(var j=0;j<3;j++)
			{
				var Xij = this.matrix[i][0]*transform.matrix[0][j]
					+ this.matrix[i][1]*transform.matrix[1][j]
					+ this.matrix[i][2]*transform.matrix[2][j];
				retval.matrix[i][j] = Xij;
			}
		}
		return retval;
	}
	else
	{
		var resultMatrix = this.matrix.multiply(transform.matrix);
		var retval = new wt2DTransform();
		retval.matrix = resultMatrix;
		return retval;
	}
}
/**
  Generates a new 2D coordinate by transforming an input coordinate.
  @param {wt2DPoint} coordinate The input coordinate.
  @returns The transformed coordinate.
  @type wt2DPoint
 */
wt2DTransform.prototype.transform = function(coordinate)
{
	if (isMSIE())
	{
		var x = this.matrix[0][0] * coordinate.x + this.matrix[0][1] * coordinate.y + this.matrix[0][2];
		var y = this.matrix[1][0] * coordinate.x + this.matrix[1][1] * coordinate.y + this.matrix[1][2];
		return new wt2DPoint({"x":x, "y":y});
	}
	else
	{
		var point = this.svg.createSVGPoint();
		point.x = coordinate.x;
		point.y = coordinate.y;
		point = point.matrixTransform(this.matrix);
		return new wt2DPoint({"x":point.x, "y":point.y});
	}
}
/**
  Transforms an array of coordinates in situ.
  @param {Array} Array of wt2DPoint coordinates.
 */
wt2DTransform.prototype.batchTransform = function(coordArray)
{
	for(var i=0;i<coordArray.length;i++)
		coordArray[i] = this.transform(coordArray[i]);
}
/**
  Converts the affine transform matrix to a human readable form.
  @returns String representation of the affine transform matrix.
  @type String
 */
wt2DTransform.prototype.toString = function()
{
	var retval = "[A11,A12,A13]\n[A21,A22,A23]\n[A31,A32,A33]";
	if (isMSIE())
	{
		retval = retval.replace("A11", this.matrix[0][0].toPrecision(3));
		retval = retval.replace("A12", this.matrix[0][1].toPrecision(3));
		retval = retval.replace("A13", this.matrix[0][2].toPrecision(3));
		retval = retval.replace("A21", this.matrix[1][0].toPrecision(3));
		retval = retval.replace("A22", this.matrix[1][1].toPrecision(3));
		retval = retval.replace("A23", this.matrix[1][2].toPrecision(3));
		retval = retval.replace("A31", this.matrix[2][0].toPrecision(3));
		retval = retval.replace("A32", this.matrix[2][1].toPrecision(3));
		retval = retval.replace("A33", this.matrix[2][2].toPrecision(3));
	}
	else
	{
		retval = retval.replace("A11", this.matrix.a.toPrecision(3));
		retval = retval.replace("A12", this.matrix.c.toPrecision(3));
		retval = retval.replace("A13", this.matrix.e.toPrecision(3));
		retval = retval.replace("A21", this.matrix.b.toPrecision(3));
		retval = retval.replace("A22", this.matrix.d.toPrecision(3));
		retval = retval.replace("A23", this.matrix.f.toPrecision(3));
		retval = retval.replace("A31", (0).toPrecision(3));
		retval = retval.replace("A32", (0).toPrecision(3));
		retval = retval.replace("A33", (1).toPrecision(3));
	}
	return retval;
}
// }}}
// {{{ wt2DPoint - An abstract 2D coordinate.
/**
  Constructs a 2D coordinate.
  @class A 2D coordinate.
  @param {Number} x The x-coodinate.
  @param {Number} y The y-coordinate.
  @constructor
 */
var wt2DPoint = function(x, y)
{
	if ((x instanceof wt2DPoint) || (x.x != undefined))
	{
		/**
		  The x-coordinate.
		  @type Integer
		 */
		this.x = x.x;
		/**
		  The y-coordinate.
		  @type Integer
		 */
		this.y = x.y;
	}
	else
	{
		this.x = x;
		this.y = y;
	}
}
wt2DPoint.prototype = new Object;
/**
  Tells the Pythagorean distance of this coordinate from another coordinate.
  @param {wt2DPoint} pt The other point.
  @returns Pythagorean distance between two points.
  @type Number
 */
wt2DPoint.prototype.getDistance = function(pt)
{
	return Math.pow(Math.pow(this.x - pt.x, 2) + Math.pow(this.y - pt.y, 2), 0.5);
}
/**
  Tells the incline angle of two points, assuming the current coordinate is the center.
  @returns Incline angle in degrees, in the range [0.0, 360.0).
  @type Number
 */
wt2DPoint.prototype.getInclineAngle = function(pt)
{
	var dx = pt.x - this.x;
	var dy = pt.y - this.y;
	var retval = Math.rad2deg(Math.atan(dy/dx));
	if (dx < 0)
		retval += 180;
	retval += 360;
	retval %= 360;
	return retval;
}
/**
  Generates a new 2D coordinate by adding up the coordinates of two points.
  @param {wt2DPoint} .pt The other 2D coordinate.
  @returns The new 2D coordinate.
  @type wt2DPoint
 */
wt2DPoint.prototype.add = function(pt)
{
	return new wt2DPoint(this.x + pt.x, this.y + pt.y);
}
/**
  Tells the shortest distance of this point from an infinite line.
  @param {wt2DPoint} ptA First point on infinite line.
  @param {wt2DPoint} ptB Second point on infinite line.
  @returns Shortest distance of this point from given line.
  @type Number
 */
wt2DPoint.prototype.getDistanceFromLine = function(ptA, ptB)
{
	var denom = ptA.getDistance(ptB);
	var nom = (ptB.x-ptA.x)*(ptA.y-this.y)-(ptA.x-this.x)*(ptB.y-ptA.y);
	nom = Math.abs(nom);
	return nom / denom;
}
/**
  Tells the shortest distance of this point from a bounded line segment.
  @param {wt2DPoint} ptA First end of the bounded line.
  @param {wt2DPoint} ptB Second end of the bounded line.
  @returns Shortest distance of this point from the bounded line.
  @type Number
 */
wt2DPoint.prototype.getDistanceFromBoundedLine = function(ptA, ptB)
{
	var base = ptA.getDistance(ptB);
	var sideA = this.getDistance(ptA);
	var sideB = this.getDistance(ptB);
	if (sideA > sideB)
	{
		if (Math.pow(Math.pow(base, 2) + Math.pow(sideB, 2), 0.5) > sideA)
			return this.getDistanceFromLine(ptA, ptB);
		else
			return this.getDistance(ptB);
	}
	else if (sideA < sideB)
	{
		if (Math.pow(Math.pow(base, 2) + Math.pow(sideA, 2), 0.5) > sideB)
			return this.getDistanceFromLine(ptA, ptB);
		else
			return this.getDistance(ptA)
	}
	else
		return this.getDistanceFromLine(ptA, ptB);
}
/**
  Converts the 2D coordinate to a human readable form.
  @returns 2D coordiante string.
  @type String
 */
wt2DPoint.prototype.toString = function()
{
	return "("+this.x+","+this.y+")";
}
// }}}
// {{{ wtVectorView - A container for purely vector graphics.
/**
  Constructs a container for purely vector graphics.
  @constructor
  @class A container for purely vector graphics.
  @param {wtCanvas} parentWidget The parent canvas for adding this widget to.
  @param {Number} width The width of this container in pixels.
  @param {Number} height The height of this container in pixels.
 */
var wtVectorView = function(parentWidget, width, height)
{
	if (arguments.length < 3)
		return;

	var domNode = null;
	if (isMSIE())
	{
		domNode = document.createElement("v:group");
		domNode.style.width = "1px";
		domNode.style.height = "1px";
	}
	else
	{
		domNode = document.createElementNS("http://www.w3.org/2000/svg", "svg:svg");
		domNode.width.baseVal.newValueSpecifiedUnits(5, width);
		domNode.height.baseVal.newValueSpecifiedUnits(5, height);
	}
	domNode.style.position = "absolute";
	domNode.style.left = "0px";
	domNode.style.top = "0px";
	domNode.style.zIndex = 0;
	if (isMSIE())
		domNode.setAttribute("UNSELECTABLE", "on");
	else
		domNode.style.MozUserSelect = "none";
	this.startProxy(domNode);
	domNode.id = this.toString();

	if (isMSIE())
		this.set("coordSize", "1 1"); 

	parentWidget.addWidget(this);
}
wtVectorView.prototype = new wtObject;
/**
  Sets the size of this vector graphics container.
  @param {Number} width The new width in pixels.
  @param {Number} height The new height in pixels.
 */
wtVectorView.prototype.setSize = function(width, height)
{
	if (isMSIE())
		return;
	else
	{
		this.get("width").baseVal.newValueSpecifiedUnits(5, width);
		this.get("height").baseVal.newValueSpecifiedUnits(5, height);
	}
}
/**
  Adds a vector graphical widget to this container.
  @param {wtVectorWidget} widget The vector graphical widget to be added.
  @returns The added widget.
  @type wtVectorWidget
 */
wtVectorView.prototype.addWidget = function(widget)
{
	if (! (widget instanceof wtVectorWidget))
		return;

	$_(this).appendChild($_(widget));
	return widget;
}
/**
  Removes all child widgets.
 */
wtVectorView.prototype.clearAll = function()
{
	while(this.get("childNodes").length)
	{
		if (this.get("childNodes").item(0).wtObj)
			_$(this.get("childNodes").item(0)).removeSelf();
		else
			$_(this).removeChild(this.get("childNodes").item(0));
	}
}
/**
  Tells the screen position of this vector graphical widget container.
  @returns x: x-coordinate on screen, y: y-coordinate on screen
  @type Object
 */
wtVectorView.prototype.getScreenPosition = function()
{
	return _$(this.get("parentNode")).getScreenPosition();
}
// }}}
// {{{ wtCanvas - A container for drawing graphics and overlay HTML widgets.
/**
  Constructs a container for drawing vector graphics with overlay HTML widgets.
  @constructor
  @class A container for drawing vector graphics and overlay HTML widgets.
  @param {wtWidget|DOM_Node} parentWidget The parent container for adding this widget to. Can also be a DOM node or null.
  @param {Number} width The fixed width in pixels.
  @param {Number} height The fixed height in pixels.
 */
var wtCanvas = function(parentWidget, width, height)
{
	if (arguments.length < 3)
		return;

	this.base = wtView;
	this.base(parentWidget, width, height);
	$_(this).className += " wtCanvas";
	this.get("style").overflow = "hidden";
	$_(this).vectorContainer = new wtVectorView(this, width, height);
}
wtCanvas.prototype = new wtView;
/**
  @private
 */
wtCanvas.prototype._wtView_addWidget = wtView.prototype.addWidget;
/**
  Adds a wtWidget, a wtVectorWidget, or a simple string to this canvas.
  @param {wtWidget|wtVectorWidget|String} widget The widget to be added.
  @returns The added widget.
  @type wtWidget|wtVectorWidget
 */
wtCanvas.prototype.addWidget = function(widget)
{
	// add the default vector view
	if (widget instanceof wtVectorView)
		this._wtView_addWidget(widget);
	else if (widget instanceof wtVectorWidget)	// add a vector widget
		this.get("vectorContainer").addWidget(widget);
	else if (widget instanceof wtWidget)	// add a regular HTML widget or a string
	{
		this._wtView_addWidget(widget);
		if (! widget.get("style").zIndex)
			widget.setLevel(1);
	}
	else if (typeof(widget) == "string")
	{
		var s = widget;
		widget = new wtWidget(this, "div");
		widget.setAbsolutePosition(0, 0);
		widget.addWidget(s);
	}
	return widget;
}
/**
  Clears the contents of this canvas.
 */
wtCanvas.prototype.clearAll = function()
{
	var widgets = [];
	for(var i=0;i<this.get("childNodes").length;i++)
		widgets.push(_$(this.get("childNodes").item(i)));
	for(var i=0;i<widgets.length;i++)
	{
		if (! (widgets[i] instanceof wtVectorView))
			widgets[i].removeSelf();
	}
}
/**
  @private
 */
wtCanvas.prototype._wtView_setSize = wtView.prototype.setSize;
/**
  Sets the size of the canvas.
  @param {Number} width The new width in pixels.
  @param {Number} height The new height in pixels.
 */
wtCanvas.prototype.setSize = function(width, height)
{
	this._wtView_setSize(width, height);
	if (! this.get("vectorContainer"))
		return;
	this.get("vectorContainer").setSize(width, height);
}
// }}}
// {{{ wtVectorWidget - Base class of all vector graphics objects.
/**
  Constructs a vector graphics object.
  @constructor
  @class A vector graphics object. This is the base class of all vector graphics objects.
  @param {wtCanvas|wtVectorGroup} parentWidget The parent container.
  @param {String} tagName The XML tag name for this widget.
 */
var wtVectorWidget = function(parentWidget, tagName)
{
	if (arguments.length < 2)
		return;
	var dontAdd = false;
	if (arguments.length > 2)
		dontAdd = arguments[2];
	if (! ((parentWidget instanceof wtVectorGroup) || (parentWidget instanceof wtVectorView) || (parentWidget instanceof wtCanvas)))
		throw "wtVectorWidget(): parentWidget must be able to contain vector graphical widgets";
	var domNode;
	if (isMSIE())
		domNode = document.createElement(tagName);
	else
		domNode = document.createElementNS("http://www.w3.org/2000/svg",tagName);
	this.startProxy(domNode);

	this.set("id", this.toString());
	if (! dontAdd)
		parentWidget.addWidget(this);
}
wtVectorWidget.prototype = new wtObject;
/**
  Destroys the widget.
 */
wtVectorWidget.prototype.removeSelf = function()
{
	if (this.get("parentNode"))
		this.get("parentNode").removeChild($_(this));
	this.endProxy();
}
/**
  Sets the widget's visibility.
  @param {Boolean} yes Whether the widget should be visible or not.
 */
wtVectorWidget.prototype.setVisible = function(yes)
{
	var visible = yes? "visible":"hidden";
	this.get("style").visibility = visible;
}
/**
  Tells whether the widget is visible or not.
  @returns Whether the widget is visible or not.
  @type Boolean
 */
wtVectorWidget.prototype.isVisible = function()
{
	return this.get("style").visibility != "hidden";
}
/**
  Sets the widget's opacity.
  @param {Number} alpha The widget's opacity, in the range [0.0, 1.0].
 */
wtVectorWidget.prototype.setOpacity = function(alpha)
{
	this.setFillOpacity(alpha);
	this.setStrokeOpacity(alpha);
}
/**
  Sets the widget's fill opacity.
  @param {Number} alpha The widget's fill opacity, in the range [0.0, 1.0].
 */
wtVectorWidget.prototype.setFillOpacity = function(alpha)
{
	if (isMSIE())
		this.get("fill").opacity = alpha;
	else
		$_(this).setAttribute("fill-opacity", alpha);
}
/**
  Sets the widget's stroke opacity.
  @param {Number} alpha The widget's stroke opacity, in the range [0.0, 1.0].
 */
wtVectorWidget.prototype.setStrokeOpacity = function(alpha)
{
	if (isMSIE())
		this.get("stroke").opacity = alpha;
	else
		$_(this).setAttribute("stroke-opacity", alpha);
}
/**
  Tells the widget's current fill opacity.
  @returns Current fill opacity.
  @type Number
 */
wtVectorWidget.prototype.getFillOpacity = function()
{
	if (isMSIE())
		return this.get("fill").opacity;
	else
		return $_(this).getAttribute("fill-opacity");
}
/**
  Tells the widget's current stroke opacity.
  @returns Current stroke opacity.
  @type Number
 */
wtVectorWidget.prototype.getStrokeOpacity = function()
{
	if (isMSIE())
		return this.get("stroke").opacity;
	else
		return $_(this).getAttribute("stroke-opacity");
}
/**
  Sets the widget's stroke color. 
  @param {wtColor|String} color The stroke color. If a String is given here, it will be converted to wtColor automatically by 
  wtColor's constructor.
  @see wtColor
 */
wtVectorWidget.prototype.setStrokeColor = function(color)
{
	if (! (color instanceof wtColor))
		color = new wtColor(color);

	if (isMSIE())
		$_(this).setAttribute("strokecolor", color.toString());
	else
		$_(this).setAttribute("stroke", color.toString());
	this.setStrokeOpacity(color.getAlpha());
}
/**
  Sets the widget's fill color.
  @param {wtColor|String} color The fill color. If a String is given here, it will be converted to wtColor automatically by 
  wtColor's constructor.
 */
wtVectorWidget.prototype.setFillColor = function(color)
{
	if (! (color instanceof wtColor))
		color = new wtColor(color);

	if (isMSIE())
		$_(this).setAttribute("fillcolor", color.toString());
	else
		$_(this).setAttribute("fill", color.toString());
	this.setFillOpacity(color.getAlpha());
}
/**
  Sets the widget's fill and stroke color.
  @param {wtColor|String} color The new color. If a String is given here, it will be converted to wtColor automatically by 
  wtColor's constructor.
 */
wtVectorWidget.prototype.setColor = function(fillColor, strokeColor)
{
	this.setFillColor(fillColor);
	this.setStrokeColor(strokeColor);
}
/**
  Tells the widget's fill color.
  @returns The current fill color.
  @type wtColor
 */
wtVectorWidget.prototype.getFillColor = function()
{
	var rgbStr = null;
	if (isMSIE())
		rgbStr = String($_(this).getAttribute("fillcolor"));
	else
		rgbStr = $_(this).getAttribute("fill");

	var retval = new wtColor(rgbStr);
	retval.setAlpha(this.getFillOpacity());
	return retval;
}
/**
  Tells the widget's stroke color.
  @returns The current stroke color.
  @type wtColor;
 */
wtVectorWidget.prototype.getStrokeColor = function()
{
	var rgbStr = null;
	if (isMSIE())
		rgbStr = $_(this).getAttribute("strokecolor");
	else
		rgbStr = $_(this).getAttribute("stroke");

	var retval = new wtColor(rgbStr);
	retval.setAlpha(this.getStrokeOpacity());
	return retval;
}
/**
  Tells the widget's stroke width.
  @returns The current stroke width.
  @type Number
 */
wtVectorWidget.prototype.getStrokeWidth = function()
{
	var width;
	if (isMSIE())
		width = parseInt($_(this).getAttribute("strokeweight"));
	else width = $_(this).getAttribute("stroke-width");
	return width==null? 0:width;
}
/**
  Sets the widget's stroke width.
  @param {Number} strokeWidth Desired stroke width in number of pixels.
 */
wtVectorWidget.prototype.setStrokeWidth = function(strokeWidth)
{
	if (isMSIE())
		$_(this).setAttribute("strokeweight",strokeWidth);
	else $_(this).setAttribute("stroke-width",strokeWidth);
}
// }}}
// {{{ wtVectorGroup - A group of vector widgets that can undergo geometric transformations together.
/**
  Constructs a vector widget group.
  @class A vector widget group where contained widgets can undergo geometric transformations and receive mouse
  events together. <br/><br/>
  Available signals:
  <ul>
  <li>MouseUp</li>
  <li>MouseDown</li>
  <li>Click</li>
  <li>DoubleClick</li>
  <li>MouseOver</li>
  <li>MouseOut</li>
  </ul>
  @param {wtCanvas|wtVectorGroup} parentWidget The parent container
  @constructor
 */
var wtVectorGroup = function(parentWidget)
{
	if (arguments.length < 1)
		return;

	if (! ((parentWidget instanceof wtCanvas) || (parentWidget instanceof wtVectorGroup)))
		throw "wtVectorGroup: parentWidget must be a wtCanvas or wtVectorGroup";

	this.base = wtVectorWidget;
	if (isMSIE())
		this.base(parentWidget, "v:group", true);
	else
		this.base(parentWidget, "svg:g", true);

	var parentCanvas = parentWidget;
	while (! (parentCanvas instanceof wtCanvas) )
		parentCanvas = parentCanvas.get("parentCanvas");
	this.set("parentCanvas", parentCanvas);

	if (! isMSIE())
	{
		var translate = $_(this.getVectorContainer()).createSVGTransform();
		var rotate = $_(this.getVectorContainer()).createSVGTransform();
		this.get("transform").baseVal.appendItem(translate);
		this.get("transform").baseVal.appendItem(rotate);
		this.set("svgTranslate", translate);
		this.set("svgRotate", rotate);
	}
	else
	{
		var style = this.get("style");
		style.position = "absolute";
		style.left = "0px";
		style.top = "0px";
		style.width = "1px";
		style.height = "1px";
		this.set("coordSize", "1,1");
	}

	this.linkSignal("MouseUp", "onmouseup");
	this.linkSignal("MouseDown", "onmousedown");
	this.linkSignal("Click", "onclick");
	this.linkSignal("DoubleClick", "ondblclick");
	this.linkSignal("MouseOver", "onmouseover");
	this.linkSignal("MouseOut", "onmouseout");
	this.setSlot("Drag", this.dragSlot);
	this.setSlot("SetDragging", this.setDraggingSlot);
	this.setSlot("UnsetDragging", this.unsetDraggingSlot);
	
	parentWidget.addWidget(this);
}
wtVectorGroup.prototype = new wtVectorWidget;
/**
  Tells the wtVectorView object containing this vector widget group.
  @returns The wtVectorView object containing this vector widget group.
  @type wtVectorView
 */
wtVectorGroup.prototype.getVectorContainer = function()
{
	return this.get("parentCanvas").get("vectorContainer");
}
/**
  Adds a vector graphical widget to this group.
  @param {wtVectorWidget} widget The widget to be added.
  @returns The added widget.
  @type wtVectorWidget
 */
wtVectorGroup.prototype.addWidget = function(widget)
{
	$_(this).appendChild($_(widget));
	return widget;
}
/**
  Sets the pixel position of the widget group relative to the parent container's top-left corner.
  @param {Number} x The x-coordinate.
  @param {Number} y The y-coordinate.
 */
wtVectorGroup.prototype.setAbsolutePosition = function(x, y)
{
	if (isMSIE())
	{
		this.get("style").top = y;
		this.get("style").left = x;
	}
	else
		this.get("svgTranslate").setTranslate(x, y);

}
/**
  Sets the pixel position of the widget group relative to its current position.
  @param {Number} x The x-coordinate.
  @param {Number} y The y-coordinate.
 */
wtVectorGroup.prototype.setRelativePosition = function(x, y)
{
	var target = this.getAbsolutePosition();
	target.x += x;
	target.y += y;
	this.setAbsolutePosition(target.x, target.y);
}
/**
  Tells the widget group's pixel position relative to the parent container's top-left corner.
  @returns The widget group's pixel position.
  @type wt2DPoint
 */
wtVectorGroup.prototype.getAbsolutePosition = function()
{
	var retval = {"x": 0, "y": 0};
	if (isMSIE())
	{
		retval.x = parseInt(this.get("style").left);
		retval.y = parseInt(this.get("style").top);
	}
	else
	{
		// calculate the translated bounding box by finding minimum and maximum
		// translated x, y coordinates from the non-translated SVG bounding box
		if (this.get("transform").baseVal.numberOfItems > 0)
		{
			var transformMatrix = this.get("svgTranslate").matrix;
			retval.x = transformMatrix.e;
			retval.y = transformMatrix.f;
		}
	}
	return new wt2DPoint(retval);
}
/**
  Tells the widget group's pixel position relative to the browser display area's top-left corner.
  @returns The widget group's pixel position.
  @type wt2DPoint
 */
wtVectorGroup.prototype.getScreenPosition = function()
{
	var parentPos = _$(this.get("parentNode")).getScreenPosition();
	var myPos = this.getAbsolutePosition();
	return new wt2DPoint({"x": parentPos.x + myPos.x, "y": parentPos.y + myPos.y}); 
}
/**
  Sets the widget group to be draggable or not.
  @param {Boolean} yes Whether the widget group should be draggable or not.
 */
wtVectorGroup.prototype.setDraggable = function(yes)
{
	if (yes)
		this.connect("set_dragging", "MouseDown", this, "SetDragging");
	else
		this.disconnect("set_dragging", "MouseDown");
}
/**
  @private
 */
wtVectorGroup.prototype.dragSlot = function(myself, evt, source)
{
	var domNode = $_(myself);
	if (domNode.isDragging)
	{
		var myPos = myself.getAbsolutePosition();
		myself.setAbsolutePosition(myPos.x + mouse.x - domNode.prevX, myPos.y + mouse.y - domNode.prevY);
		domNode.prevX = mouse.x;
		domNode.prevY = mouse.y;
	}
}
/**
  @private
 */
wtVectorGroup.prototype.setDraggingSlot = function(myself, evt, source)
{
	var domNode = $_(myself);
	window.mouse.connect(myself.toString(), "MouseUp", myself, "UnsetDragging");
	window.mouse.connect(myself.toString(), "MouseMove", myself, "Drag");
	domNode.prevX = mouse.x;
	domNode.prevY = mouse.y;
	domNode.isDragging = true;
}
/**
  @private
 */
wtVectorGroup.prototype.unsetDraggingSlot = function(myself, evt, source)
{
	var domNode = $_(myself);
	domNode.isDragging = false;
	window.mouse.disconnect(myself.toString(), "MouseMove");
	window.mouse.disconnect(myself.toString(), "MouseUp");
}
/**
  Links a DOM object's event hook (e.g. "onclick") to a wtObject signal name
  @param {String} signalName wtObject signal name
  @param {String} eventName DOM object event hook name
 */
wtVectorGroup.prototype.linkSignal = function(signalName, eventName)
{
	var sig = $_(this).wtSignals;
	if (sig == undefined)
	{
		$_(this).wtSignals = {};
		sig = $_(this).wtSignals;
	}
	if (sig[signalName] == undefined)
		sig[signalName] = {};

	// generate the handler function for converting events into signals 
	$_(this)[eventName] = function()
	{
		// first, which event are we going to get?
		var evt;
		if (navigator.userAgent.search("MSIE") != -1)
			evt = window.event;
		else
			evt = arguments[0];
		try
		{
			if (_$(this))
				true;
		}
		catch(e){return;}
		_$(this).emit(signalName, evt);
	}
}
/**
  Sets the opacity of all the widget group's child widgets.
  @param {Number} alpha The widgets' opacity, in the range [0.0, 1.0].
 */
wtVectorGroup.prototype.setOpacity = function(alpha)
{
	for (var i=0; i<this.get("childNodes").length; i++)
	{
		var item = _$(this.get("childNodes").item(i));
		if (item.setOpacity)
			item.setOpacity(alpha);
	}
}
/**
  Tells the container canvas for this vector widget group.
  @returns The container canvas.
  @type wtCanvas.
 */
wtVectorGroup.prototype.getCanvas = function()
{
	return this.get("parentCanvas");
}
// }}}
// {{{ wtLine - A simple line.
/**
  Create a line connecting two points.
  @constructor
  @class A line connecting two points.
  @param {wtCanvas|wtVectorGroup} parentWidget The parent container
  @param {Number} x1 The x-coordinate of the first point
  @param {Number} y1 The y-coordinate of the first point
  @param {Number} x2 The x-coordinate of the second point
  @param {Number} y2 THe y-coordinate of the second point
  @param {String} color The color of the line
 */
var wtLine = function(parentWidget,x1,y1,x2,y2,color)
{
	if (arguments.length < 6)
		return;

	this.base = wtVectorWidget;

	if (isMSIE())
	{
		this.base(parentWidget,"v:line", true);
		var domNode = $_(this);
		domNode.style.position = "absolute";
		domNode.style.left = 0;
		domNode.style.top = 0;
		domNode.setAttribute("from",x1 + " " +y1);
		domNode.setAttribute("to",x2 + " " + y2);
		domNode.setAttribute("strokeweight","1px");	
	}
	else
	{
		this.base(parentWidget,"svg:line", true);
		var domNode = $_(this);
		domNode.setAttribute("style:position","absolute");
		domNode.setAttribute("x1",x1);
		domNode.setAttribute("y1",y1);
		domNode.setAttribute("x2",x2);
		domNode.setAttribute("y2",y2);
	}
	parentWidget.addWidget(this);
	this.setStrokeColor(color);
}
wtLine.prototype = new wtVectorWidget;
/**
  Tells the end point coordinates for this line.
  @returns x1:x1, y1:y1, x2:x2, y2:y2
  @type Object
 */
wtLine.prototype.getEndPoints = function()
{
	var x1,y1,x2,y2;
	if (isMSIE())
	{
		x1 = $_(this).getAttribute("from").x;
		y1 = $_(this).getAttribute("from").y;
		x2 = $_(this).getAttribute("to").x;
		y2 = $_(this).getAttribute("to").y;
	}
	else
	{
		x1 = $_(this).getAttribute("x1");
		y1 = $_(this).getAttribute("y1");
		x2 = $_(this).getAttribute("x2");
		y2 = $_(this).getAttribute("y2");
	}

	return {"x1":x1,"y1":y1,"x2":x2,"y2":y2};
}
/**
  Sets the position of the two end ponts of the line
  @param {Number} _x0 The x-coordinate of the starting point
  @param {Number} _y0 The y-coordinate of the starting point
  @param {Number} _x1 The x-coordinate of the ending point
  @param {Number} _y1 The y-coordinate of the ending point
 */
wtLine.prototype.setEndPoints = function(_x0,_y0,_x1,_y1)
{
	var domNode = $_(this);
	if (isMSIE())
	{
		domNode.setAttribute("from",_x0+" "+_y0);
		domNode.setAttribute("to",_x1+" "+_y1);
	}
	else
	{
		domNode.setAttribute("x1",_x0);
		domNode.setAttribute("y1",_y0);
		domNode.setAttribute("x2",_x1);
		domNode.setAttribute("y2",_y1);
	}
}
/**
  Returns the length of the line
  @return The length of the line
  @type {Number}
 */
wtLine.prototype.getLength = function()
{
	var pt = this.getEndPoints();
	var dx2 = Math.pow(pt.x1 - pt.x2, 2);
	var dy2 = Math.pow(pt.y1 - pt.y2, 2);
	return Math.pow(dx2 + dy2, 0.5);
}
// }}}
// {{{ wtOval - An oval.
/**
  @constructor
  @class An oval.
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the center of the oval
  @param {Number} _y The y-coordinate of the center of the oval
  @param {Number} _radius1 The lenght of the semi-major/semi-minor axis
  @param {Number} _radius2 The lenght of the semi-major/semi-minor axis
  @param {String|wtColor} _fill The fill color of the oval
  @param {String|wtColor} _stroke The stroke color of the oval
 */
var wtOval = function(_parent,_x,_y,_radius1,_radius2,_fill,_stroke) 
{
	if (arguments.length<7)
		return;
	if (_radius1<=0)
		throw "wtOval(): the value of _radius1 should be positive";
	if (_radius2<=0)
		throw "wtOval(): the value of _radius2 should be positive";

	this.base = wtVectorWidget;
	var domNode;

	if (isMSIE())
	{
		this.base(_parent,"v:oval", true);
		domNode = $_(this);
		domNode.style.position = "absolute";
		domNode.style.left = _x - _radius1;
		domNode.style.top = _y - _radius2;
		domNode.style.width = _radius1*2;
		domNode.style.height = _radius2*2;
	}
	else
	{
		this.base(_parent,"svg:ellipse", true);
		domNode = $_(this);
		domNode.setAttribute("style","position:absolute");
		domNode.setAttribute("cx",_x);
		domNode.setAttribute("cy",_y);
		domNode.setAttribute("rx",_radius1);
		domNode.setAttribute("ry",_radius2);
	}

	_parent.addWidget(this);
	this.setFillColor(_fill);
	this.setStrokeColor(_stroke);
}

wtOval.prototype = new wtVectorWidget;
/**
  Gets the center pisition of the oval to its parent container.
  @returns The position of the oval
  @ x: The x coordinate of the oval, y: The y coordinate of the oval
 */
wtOval.prototype.getCenter = function()
{
	var domNode = $_(this);
	var cx,cy;
	if (isMSIE())
	{
		cx = parseInt(domNode.style.left) + parseInt(domNode.style.width)/2;
		cy = parseInt(domNode.style.top) + parseInt(domNode.style.height)/2;
	}
	else
	{
		cx = domNode.getAttribute("cx");
		cy = domNode.getAttribute("cy");
	}
	return {"x":cx,"y":cy};
}

/**
  Sets the lenght of semi-major and semi-minor axes
  @param {Number} _rx The length of the semi-major or semi-minor axis
  @param {Number} _ry The length of the semi-major or semi-minor axis
 */
wtOval.prototype.setSize = function(_rx,_ry)
{
	if (_rx<=0)
		throw "wtOval::setSize(): the value of _rx should be positive";
	if (_ry<=0)
		throw "wtOval::setSize(): the value of _ry should be positive";

	var domNode = $_(this);
	if (isMSIE())
	{
		var center = this.getCenter();
		domNode.style.left = center.x - _rx;
		domNode.style.top = center.y -_ry;
		domNode.style.width = _rx*2;
		domNode.style.height = _ry*2;
	}
	else
	{
		domNode.setAttribute("rx",_rx);
		domNode.setAttribute("ry",_ry);
	}

}
// }}}
// {{{ wtCircle - A circle.
/**
  Constructs a circle object
  @class A circle.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of center of the circle
  @param {Number} _y The y-coordinate of center of the circle
  @param {Number} _radius The radius of the circle
  @param {String|wtColor} _fill The fill color of the circle
  @param {String|wtColor} _stroke The stroke color of the circle
 */
var wtCircle = function(_parent,_x,_y,_radius,_fill,_stroke)
{
	if (arguments.length<6)
		return;

	this.base = wtOval;
	this.base(_parent,_x,_y,_radius,_radius,_fill,_stroke);
}
wtCircle.prototype = new wtOval;

/**
  Sets the radius of the cirlce
  @param {Number} _radius The radius of the cirlce
 */
wtCircle.prototype.setRadius = function(_radius)
{
	this.setSize(_radius,_radius);
}
// }}}
// {{{ wtPath - A free path object which is capable for drawing line and curve.
/**
  Constructs a free path object.
  @class A free path object which is capable for drawing lines, curves, polygons and curved faces.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {String|wtColor} _fill The fill color of the path
  @param {string|wtColor} _stroke The stroke color of the path
 */
var wtPath = function(_parent,_fill,_stroke)
{
	if (arguments.length<3)
		return;

	this.base = wtVectorWidget;
	var domNode;

	if (isMSIE())
	{
		this.base(_parent,"v:shape",true);
		domNode = $_(this);
		domNode.style.position = "absolute";
		domNode.style.top = 0;
		domNode.style.left = 0;
		domNode.style.width = "1px";
		domNode.style.height = "1px";
		domNode.coordSize = "1,1"
			domNode.path = "";
		domNode.pathValue = "";
	}
	else
	{
		this.base(_parent,"svg:path",true);
		domNode = $_(this);
		domNode.setAttribute("style","position:absolute");
		domNode.setAttribute("d","");
	}
	this.set("commands", []);
	_parent.addWidget(this);
	this.setFillColor(_fill);
	this.setStrokeColor(_stroke);
}
wtPath.prototype = new wtVectorWidget;
/**
  @private
 */
wtPath.prototype.getLastCommandCoordinate = function()
{
	if (this.get("commands").length > 0)
	{
		var lastItem = this.get("commands")[this.get("commands").length -1];
		return lastItem.coordinate;
	}
	else
		return {"x":0, "y":0}
}
/**
  @private
 */
wtPath.prototype.addCommand = function(opName, type, x, y, argsArray)
{
	if (type == "abs")
		this.get("commands").push({"command": opName, "coordinate": {"x":x, "y":y}, "args": argsArray});
	else
	{
		var absCoord = this.getLastCommandCoordinate();
		absCoord.x += x;
		absCoord.y += y;
		this.get("commands").push({"command": opName, "coordinate": absCoord, "args": argsArray});
	}
}
/**
  @private
  moveTo static function
 */
wtPath.moveTo_ = function(_type,_x,_y)
{
	var tmp;
	var dec = isMSIE()? 0:1;
	if (_type=="abs")
	{
		tmp = isMSIE()? " m ": " M ";
	}
	else
	{
		tmp = isMSIE()? " t ": " m ";
	}
	tmp += _x.toFixed(dec) + "," + _y.toFixed(dec) + " ";
	return tmp;
}
/**
  @private
  lineTo static function
 */
wtPath.lineTo_ = function(_type,_x,_y)
{
	var tmp;
	var dec = isMSIE()? 0:1;
	if (_type=="abs")
	{
		tmp = isMSIE()? " l ": " L ";
	}
	else
	{
		tmp = isMSIE()? " r ": " l ";
	}
	tmp += _x.toFixed(dec) + "," + _y.toFixed(dec) + " ";
	return tmp;
}
/**
  @private
  curveTo static function
 */
wtPath.curveTo_ = function(_type,_x1,_y1,_x2,_y2,_x,_y)
{
	var tmp;
	var dec = isMSIE()? 0:1;
	if (_type=="abs")
	{
		tmp = isMSIE()? " c ": " C ";
	}
	else
	{
		tmp = isMSIE()? " v ": " c ";
	}
	tmp += _x1.toFixed(dec) + "," + _y1.toFixed(dec) + " " + _x2.toFixed(dec) + "," + _y2.toFixed(dec) + " " + _x.toFixed(dec) + "," + _y.toFixed(dec);
	return tmp;
}

/**
  @private
  Closes the path by drwaing a line back the starting point of the path.
 */
wtPath.closePath_ = function()
{
	var tmp = isMSIE()? " x ":" z ";
	return tmp;
}
/**
  Starts the path at given position (x,y).
  @param {String} _type The type of position. It can be absolute (abs) or relative (rel).
  @param {Number} _x The x-coordinate of the given position
  @param {Number} _y The y-coordinate of the given position
 */
wtPath.prototype.moveTo = function(_type,_x,_y)
{
	var domNode = $_(this);
	var tmp;
	var dec = isMSIE()? 0:1;

	if (_type=="abs")
		tmp = isMSIE()? " m ": " M ";
	else if (_type=="rel")
		tmp = isMSIE()? " t ": " m ";
	else
		throw "wtPath::moveTo(): _type must be abs or rel";
	this.addCommand("moveTo", _type, _x, _y);
	tmp += _x.toFixed(dec) + "," + _y.toFixed(dec) + " ";

	if (isMSIE())
	{
		domNode.pathValue = domNode.pathValue + tmp;
		domNode.path = domNode.pathValue;
	}
	else
	{
		var d = domNode.getAttribute("d");
		domNode.setAttribute("d", d + tmp);
	}
}
/**
  Draws a line from the current position to the given position
  @param {String} _type The type of posiiton. It can be absolute (abs) or relative (rel).
  @param {Number} _x The x-coordinate of the given position
  @param {Number} _y The y-coordinate of the given position
 */
wtPath.prototype.lineTo = function(_type,_x,_y)
{
	var domNode = $_(this);
	var tmp;
	var dec = isMSIE()? 0:1;

	if (_type=="abs")
		tmp = isMSIE()? " l ": " L ";
	else if (_type == "rel")
		tmp = isMSIE()? " r ": " l ";
	else
		throw "wtPath::lineTo(): _type must be abs or rel";
	this.addCommand("lineTo", _type, _x, _y);
	tmp += _x.toFixed(dec) + "," + _y.toFixed(dec) + " ";

	if (isMSIE())
	{
		domNode.pathValue = domNode.pathValue + tmp;
		domNode.path = domNode.pathValue;
	}
	else
	{
		var d = domNode.getAttribute("d");
		domNode.setAttribute("d", d + tmp);
	}
}
/**
  Draws a cubic Bezier curve from the current position to the given position (_x,_y) using (_x1,_y1) and (_x2,_y2) as control points
  @param {String} _type The type of position. It can be absolute (abs) or relative (rel).
  @param {Number} _x1 The x-coordinate of the first control point
  @param {Number} _y1 The y-coordinate of the first control point
  @param {Number} _x2 The x-coordinate of the second control point
  @param {Number} _y2 The y-coordinate of the second control point
  @param {Number} _x The x-coordinate of the target position
  @param {Number} _y THe y-coordinate of the target position
 */
wtPath.prototype.curveTo = function(_type,_x1,_y1,_x2,_y2,_x,_y)
{
	var domNode = $_(this);
	var tmp;
	var dec = isMSIE()? 0:1;
	if (_type=="abs")
		tmp = isMSIE()? " c ": " C ";
	else if (_type == "rel")
		tmp = isMSIE()? " v ": " c ";
	else
		throw "wtPath::curveTo(): _type must be abs or rel";
	var argsArray = [_x1, _y1, _x2, _y2, _x, _y];
	if (_type == "rel")
	{
		for(var i=0;i<argsArray.length;i++)
			argsArray[i] += ((i%2 == 0) ? this.getLastCommandCoordinate().x : this.getLastCommandCoordinate().y);
	}
	this.addCommand("curveTo", _type, _x, _y, argsArray);
	tmp += _x1.toFixed(dec) + "," + _y1.toFixed(dec) + " " + _x2.toFixed(dec) + "," + _y2.toFixed(dec) + " " + _x.toFixed(dec) + "," + _y.toFixed(dec);

	if (isMSIE())
	{
		domNode.pathValue = domNode.pathValue + tmp;
		domNode.path = domNode.pathValue;
	}
	else
	{
		var d = domNode.getAttribute("d");
		domNode.setAttribute("d", d + tmp);
	}
}
/**
  Closes the path by drwaing a line back the starting point of the path.
 */
wtPath.prototype.closePath = function()
{
	var domNode = $_(this);
	var tmp = isMSIE()? " x ":" z ";

	if (isMSIE())
	{
		domNode.pathValue = domNode.pathValue + tmp;
		domNode.path = domNode.pathValue;
	}
	else
	{
		var d = domNode.getAttribute("d");
		domNode.setAttribute("d", d + tmp);
	}
	this.addCommand("closePath", "abs", 0, 0);
}
/**
  Clears the path and draw a polygon according to the given pointList.
  @param {Array} pointList Array of wt2DPoints which defines a new path.
  @param {Boolean} noClose (Optional) Set this to true if you do not want to close the path.
 */
wtPath.prototype.drawPointList = function(pointList, noClose)
{
	if (!(pointList instanceof Array) || pointList.length < 2)
		throw "wtPath::setPointList(): pointList must be an array with at least 2 coordinates";
	this.clearPath();
	var startPoint = pointList[0];
	this.moveTo("abs", startPoint.x, startPoint.y);
	for(var i=1;i<pointList.length;i++)
	{
		var point = pointList[i];
		this.lineTo("abs", point.x, point.y);
	}
	if (!noClose)
		this.closePath();
}
/**
  Sets the stroke width of the path.
  @param {Number} width Path width in pixels.
 */
wtPath.prototype.setStrokeWidth = function(width)
{
	if (isMSIE())
		this.get("stroke").weight = width -1;
	else
		$_(this).setAttribute("stroke-width", width);
}
/**
  Sets the stroke style of the path.
  @param {String} style The style string of the path. Acceptable values are the following:<br/><br/>
  <ul>
  <li>solid</li>
  <li>dot</li>
  <li>dash</li>
  <li>dashdot</li>
  </ul>
 */
wtPath.prototype.setStrokeStyle = function(style)
{
	if (isMSIE())
	{
		var strokeObj = this.get("stroke");
		if (style == "solid")
			strokeObj.dashstyle = "solid";
		else if (style == "dot")
			strokeObj.dashstyle = "dot";
		else if (style == "dash")
			strokeObj.dashstyle = "dash";
		else if (style == "dashdot")
			strokeObj.dashstyle = "dashdot";
	}
	else
	{
		if (style == "solid")
			$_(this).setAttribute("stroke-dasharray", "");
		else if (style == "dot")
			$_(this).setAttribute("stroke-dasharray", "1,3");
		else if (style == "dash")
			$_(this).setAttribute("stroke-dasharray", "3,4");
		else if (style == "dashdot")
			$_(this).setAttribute("stroke-dasharray", "3,4,1,4");
	}
}
/**
  Clears the path.
 */
wtPath.prototype.clearPath = function()
{
	this.setPath("");
	this.set("commands", []);
}
/**
  @private
 */
wtPath.prototype.replayCommands = function()
{
	var commands = this.get("commands");
	this.clearPath();
	for(var i=0;i<commands.length;i++)
	{
		var c = commands[i];
		if (c.args)
		{
			var args = c.args;
			this[c.command]("abs", args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
		}
		else
			this[c.command]("abs", c.coordinate.x, c.coordinate.y);
	}
}
/**
  Transforms the path by a 2D affine transform.
  @param {wt2DTransform} tm The 2D affine transformation.
 */
wtPath.prototype.transform = function(tm)
{
	if (! (tm instanceof wt2DTransform))
		throw "wtPath::transform(): tm must be a wt2DTransform instance";
	var commands = this.get("commands");
	for(var i=0;i<commands.length;i++)
	{
		var c = commands[i];
		c.coordinate = tm.transform(c.coordinate);
		if (c.command == "curveTo")
		{
			for(var base=0;base < c.args.length;base+=2)
			{
				var pt = {"x":c.args[base], "y":c.args[base+1]};
				pt = tm.transform(pt);
				c.args[base] = pt.x;
				c.args[base+1] = pt.y;
			}
		}
	}
	this.replayCommands();
}
/**
  Sets the path.
  @private
  @param {String} _path The given path
 */
wtPath.prototype.setPath = function(_path)
{
	var domNode = $_(this);
	if (isMSIE())
	{
		domNode.pathValue = _path;
		domNode.path = _path;
	}
	else
	{
		domNode.setAttribute("d",_path);
	}
}
// }}}
// {{{ wtRectangle - A rectangle.
/**
  Constructs a rectangle object.
  @class A rectangle.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the upper left corner of the rectangle
  @param {Number} _y The y-coordinate of the upper left corner of the rectangle
  @param {Number} _width The width of the rectangle
  @param {Number} _height The height of the rectangle
  @param {String|wtColor} _fill The fill color of the rectangle
  @param {String|wtColor} _stroke The stroke color of the rectangle
 */
var wtRectangle = function(_parent,_x,_y,_width,_height,_fill,_stroke)
{
	if (arguments.length<7)
		return;
	if (_width<=0)
		throw "wtRectangle(): the value of _width should be positive";
	if (_height<=0)
		throw "wtRectangle(): the value of _height should be positive";

	this.base = wtPath;
	this.base(_parent,_fill,_stroke);

	this.set("x_",_x);
	this.set("y_",_y);
	this.set("width_",_width);
	this.set("height_",_height);
	this.set("ptList",[[_x,_y],[_width+_x,+_y],[_width+_x,_height+_y],[_x,_height+_y]]);

	this.update();
}

wtRectangle.prototype = new wtPath;
/**
  Updates the path of rectangle when the reference points of the rectangle are chnaged.
 */
wtRectangle.prototype.update = function()
{
	this.clearPath();
	this.moveTo("abs",this.get("ptList")[0][0],this.get("ptList")[0][1]);

	for (var i=1; i<this.get("ptList").length; i++)
		this.lineTo("abs",this.get("ptList")[i][0],this.get("ptList")[i][1]);
	this.closePath();
}

/**
  Skews the rectangle along x-axis and y-axis
  @param {Number} _x The angle along x-axis (measure in degree)
  @param {Number} _y The angle along y-axis (measure in degree)
 */
wtRectangle.prototype.skew = function(_x,_y)
{
	var dx = this.get("height_") * Math.tan(Math.deg2rad(_x));
	this.get("ptList")[2][0] = this.get("x_") + this.get("width_") + dx;
	this.get("ptList")[3][0] = this.get("x_") + dx;

	var dy = this.get("width_") * Math.tan(Math.deg2rad(_y));
	this.get("ptList")[1][1] = this.get("y_") + dy;
	this.get("ptList")[2][1] = this.get("y_") + this.get("height_") + dy;

	this.update();
}

/**
  Sets the width and height of the rectangle
  @param {Number} _width The width of the rectangle
  @param {Number} _height The height of the rectangle
 */
wtRectangle.prototype.setSize = function(_width,_height)
{
	if (_width<=0)
		throw "wtRectangle::setSize(): the value of _width should be positive";
	if (_height<=0)
		throw "wtRectangle::setSize(): the value of _height should be positive";

	this.set("width_",_width);
	this.set("height_",_height);
	this.get("ptList")[1][0] = this.get("x_") + _width;
	this.get("ptList")[2][0] = this.get("x_") + _width;
	this.get("ptList")[2][1] = this.get("y_") + _height;
	this.get("ptList")[3][1] = this.get("y_") + _height;

	this.update();
}
// }}}
// {{{ wtText - A graphical text block.
/**
  Constructs a graphical text block.
  @class A graphical text block. This is different from wtTextBlock or adding a string to a wtWidget in that the
  generated widget is a vector graphical widget instead of a regular HTML widget. To display non-ANSI characters
  with a wtText block reliably, you <b>must</b> set the block to use a font that contains those characters. e.g.
  To display Traditional Chinese characters reliably, you need to set the font to "MingLiu" or "Arial Unicode
  MS", for example. <br/><br/>
  An advantage of using wtText instead of adding a HTML widget to a canvas is that, the wtText object can be
  dragged along with a group of vector graphical widgets, because wtText can be added to a wtVectorGroup while
  a wtWidget cannot.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Integer} _x The x-coordinate of the text
  @param {Integer} _y The y-coordinate of the text
  @param {String} _text String to be displayed
  @param {String|wtColor} _fill The fill color of the text
 */
var wtText = function(_parent,_x,_y,_text,_fill)
{
	if (arguments.length<5)
		return;

	this.base = wtVectorWidget;
	var domNode;

	if (isMSIE())
	{
		// dummy object
		this.base(_parent,"v:rect",true);
		var domNode = $_(this);
		domNode.style.left = _x;
		domNode.style.top = _y;
		domNode.style.width = 300;
		domNode.style.height = 30;
		_parent.addWidget(this);
		domNode.fillcolor = "none";
		domNode.strokecolor = "none";
		var div = new wtWidget(null,"div");
		domNode.appendChild($_(div));
		var textNode = document.createTextNode(_text);
		$_(div).appendChild(textNode);
		this.set("divNode", div);
		div.setAbsolutePosition(0,0);
	}
	else
	{
		this.base(_parent,"svg:text",true);
		domNode = $_(this);
		domNode.setAttribute("style","position:absolute");
		domNode.setAttribute("x",_x);
		domNode.setAttribute("y",_y);
		domNode.textContent = _text;
		domNode.setAttribute("font-size","12px");
		domNode.setAttribute("font-style","normal");
		domNode.setAttribute("dominant-baseline","text-before-edge");
		_parent.addWidget(this);
	}

	this.setFillColor(_fill);
}
wtText.prototype = new wtVectorWidget;
/**
  Sets the font color of the text
  @param {String} color The color of the text
 */
wtText.prototype.setFillColor = function(color)
{
	if (! (color instanceof wtColor))
		color = new wtColor(color);

	if (isMSIE())
		this.get("divNode").get("style").color = color.toString();
	else
		$_(this).setAttribute("fill", color.toString());
	this.setFillOpacity(color.getAlpha());
}
/**
  Sets the opacity of the text
  @param {Number} alpah The value of opacity
 */
wtText.prototype.setFillOpacity = function(alpha)
{
	if (isMSIE())
		this.get("divNode").setAlpha(alpha);
	else
		$_(this).setAttribute("fill-opacity", alpha);
}
/**
  Sets the font-family of the text
  @param {String} _font The font-family
 */
wtText.prototype.setFont = function(_font)
{
	var domNode = $_(this);
	if (isMSIE())
	{
		this.get("divNode").get("style").fontFamily = _font;
	}
	else
	{
		domNode.setAttribute("font-family",_font);
	}
}

/**
  Sets the font style of the text
  @param {String} _style The font style ( normal, italic, oblique )
 */
wtText.prototype.setFontStyle = function(_style)
{
	_style = _style.toLowerCase();
	if (!(_style == "normal" || _style == "italic" || _style == "oblique"))
		throw "wtText::setFontStyle(): the font style should be 'normal','italic' or 'oblique'";

	var domNode = $_(this);
	if (isMSIE())
	{
		this.get("divNode").get("style").fontStyle = _style;
	}
	else
	{
		domNode.setAttribute("font-style",_style);
	}
}

/**
  Sets the font size of the text
  @param {Number} _size The size of the text in pixel
 */
wtText.prototype.setFontSize = function(_size)
{
	var domNode = $_(this);
	if (isMSIE())
	{
		this.get("divNode").get("style").fontSize = _size;
	}
	else
	{
		domNode.setAttribute("font-size",_size);
	}
}

/**
  Sets the content of the text
  @param {String} _text The text content
 */
wtText.prototype.setText = function(_text)
{
	var domNode = $_(this);

	if (isMSIE())
	{
		this.get("divNode").get("firstChild").nodeValue = _text;
	}
	else
	{
		domNode.textContent = _text;
	}
}
/**
  Tells the text content of this wtText block.
  @returns The text content.
  @type String
 */
wtText.prototype.getText = function()
{
	if (isMSIE())
		return this.get("divNode").get("firstChild").nodeValue;
	else return this.get("textContent");
}
/**
  Sets the anchor position of the text
  @param {String} position This can be "start", "middle" or "end".
 */
wtText.prototype.setAnchorPosition = function(position)
{
	position = position.toLowerCase();
	if (!(position == "start" || position == "middle" || position == "end"))
		throw "wtText::setAnchorPosition(): anchor position should be 'start', 'middle' or 'end'";

	if (isMSIE())
	{
		var clientWidth = parseInt(this.get("divNode").get("clientWidth"));
		var x;
		if (position=="end")
			x = -clientWidth;
		else if (position=="middle")
			x = -clientWidth/2;
		else if (position=="start")
			x = 0;
		this.get("divNode").setAbsolutePosition(x,0);
	}
	else
	{
		$_(this).setAttribute("text-anchor",position);
	}
}
// }}}
// {{{ wtBar - A 3D rectangular bar.
/**
  Constructs a 3D-like bar which is formed by three transformed rectangles.
  @class A 3D rectangluar bar.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _width The width of the bar
  @param {Number} _height The height of the bar
  @param {Number} _depth The depth of the bar
  @param {Number} _angle The degree of skewing
  @param {String} _color The color of the bar.
 */
var wtBar = function(_parent,_width,_height,_depth,_angle,_color)
{
	if (arguments.length<6)
		return;

	if (_width<=0)
		throw "wtBar(): the value of _width should be positive";
	if (_height<=0)
		throw "wtBar(): the value of _height should be positive";
	if (_depth<=0)
		throw "wtBar(): the value of _depth should be positive";
	if (_angle<=0)
		throw "wtBar(): the value of _angle should be positive";

	this.base = wtVectorGroup;
	this.base(_parent);

	_color = new wtColor(_color);

	$_(this).width_ = _width;
	$_(this).height_ = _height;
	$_(this).depth = _depth;
	$_(this).angle_ = _angle;

	var h2 = _depth * Math.sin(Math.deg2rad(_angle));
	var w2 = _depth * Math.cos(Math.deg2rad(_angle));
	var frontColor = _color;
	var sideColor = _color.translateHSV(0.0, 0.0, -0.2);
	var topColor = _color.translateHSV(0.0, 0.0, -0.1);

	var rectFront = new wtRectangle(this,0,h2,_width,_height,frontColor, frontColor);
	var rectSide = new wtRectangle(this,_width,h2,w2,_height,sideColor, sideColor);
	var rectTop = new wtRectangle(this,w2,0,_width,h2,topColor, topColor);

	rectSide.skew(0,-_angle);
	rectTop.skew(-(90-_angle),0);

	$_(this).rectangleList = [rectFront,rectSide,rectTop];

	var makeTranslucentSlot = function(myself, evt, source)
	{
		var rectList = myself.get("rectangleList");
		for(var i=0;i<rectList.length;i++)
			rectList[i].setOpacity(0.65);
	}
	var makeOpaqueSlot = function(myself, evt, source)
	{
		var rectList = myself.get("rectangleList");
		for(var i=0;i<rectList.length;i++)
			rectList[i].setOpacity(1.0);
	}
	this.setSlot("MakeTranslucent", makeTranslucentSlot);
	this.setSlot("MakeOpaque", makeOpaqueSlot);
	this.connect("make_translucent", "MouseOver", this, "MakeTranslucent");
	this.connect("make_opaque", "MouseOut", this, "MakeOpaque");
}
wtBar.prototype = new wtVectorGroup;
// }}}
// {{{ wtChart - The base class of charts for setting title, labels, legends and grid lines.
/**
  Constructs a basic chart, provides function for setting title, label, legend and grid lines.
  @class The base class of charts, which provides common features like the chart title, labels, legends and grid lines.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number} _y The y-coordinate of the chart
  @param {String} _type Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The height of the background
  @param {Number} _depth The depth of the background
  @param {Number} _angle The degree of skewing
  @param {Number} _xMin The lower bound of x-axis
  @param {Number} _xMax The upper bound of x-axis
  @param {Number} _yMin The lower bound of y-axis
  @param {Number} _yMax The upper bound of y-axis
 */
var wtChart = function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax) 
{
	if (arguments.length<12)
		return;

	if (_width<=0)
		throw "wtChart(): the value of _width should be positive";
	if (_height<=0)
		throw "wtChart(): the value of _height should be positive";
	if (_depth<=0)
		throw "wtChart(): the value of _depth should be positive";
	if (_angle<=0)
		throw "wtChart(): the value of _angle should be positive";
	if (_xMin>=_xMax)
		throw "wtChart(): _xMax should be greater than _xMin";
	if (_yMin>=_yMax)
		throw "wtChart(): _yMax should be greater than _yMin";


	this.base = wtVectorGroup;
	this.base(_parent);

	var w2 = _depth * Math.cos(Math.deg2rad(_angle));
	var h2 = _depth * Math.sin(Math.deg2rad(_angle));

	var bgRect = new wtRectangle(this,w2,0,_width,_height,"black","green");
	var verRect = new wtRectangle(this,0,h2,w2,_height,"black","maroon");
	var horRect = new wtRectangle(this,w2,_height,_width,h2,"black","purple");

	// intended for the case with negative bar value
	//var bgRectDown = new wtRectangle(this,w2,_height,_width,_height,"grey","green");
	//var verRectDown = new wtRectangle(this,0,_height+h2,w2,_height,"grey","maroon");
	//bgRectDown.setOpacity(0.1);
	//verRectDown.setOpacity(0.2);
	//verRectDown.skew(0,-_angle);

	bgRect.setOpacity(0.2);
	verRect.setOpacity(0.3);
	horRect.setOpacity(0.15);

	horRect.skew(-(90-_angle),0);
	verRect.skew(0,-_angle);

	this.set("xMin",_xMin);
	this.set("xMax", _xMax);
	this.set("yMin", _yMin);
	this.set("yMax", _yMax);
	this.set("bar_no", 0);
	this.set("type_", _type.toLowerCase());
	this.set("height_", _height);
	this.set("width_", _width);
	this.set("depth_", _depth);
	this.set("angle_", _angle);
	this.set("w2", w2);
	this.set("h2", h2);
	this.set("barWidth", 30);
	this.set("barSpace", 60);
	this.set("rectList",[bgRect,verRect,horRect]);
	this.set("titleCell", new wtText(this,w2+_width/2.0,-h2,"Chart Title","purple"));
	this.get("titleCell").setAnchorPosition("middle");
	this.set("xAxisLabel", new wtText(this,w2+_width,h2+_height,"","purple"));
	this.set("yAxisLabel", new wtText(this,-20,-h2,"","purple"));
	this.set("xList", []);
	this.set("yList", []);
	this.set("colorGen", new wtColorGenerator(9,1,1));
	this.get("colorGen").getColor();
	this.get("colorGen").spin(parseInt(this.get("colorGen").count() * Math.random()));

	var halfLength = Math.floor(this.get("colorGen").count()/2);
	for (var i=0; i<halfLength; i++) {
		if (i%2 == 0)
		{
			var tmp = this.get("colorGen").colorArray[i];
			this.get("colorGen").colorArray[i] = this.get("colorGen").colorArray[i+halfLength];
			this.get("colorGen").colorArray[i+halfLength] = tmp;
		}
	}

	for (var i=0; i<this.get("colorGen").count(); i++) {
		this.get("colorGen").colorArray[i] = this.get("colorGen").colorArray[i].translateHSV(0,-0.5,0);
	}
	this.setAbsolutePosition(_x,_y);
}

wtChart.prototype = new wtVectorGroup;

/**
  Sets the title and it color of the chart
  @param {String} _title The title of the chart
  @param {String} _color The color of the title
 */
wtChart.prototype.setTitle = function(_title,_color)
{
	this.get("titleCell").setText(_title);
	this.get("titleCell").setAnchorPosition("middle");
}
/**
  Sorting function
  @private
 */
wtChart.sort = function(a,b)
{
	return a - b;
}

/**
  Sets the grid lines for the background of the chart
  @param {Number} _division The number of division of the background
  @param {String} _axis Vertical or horizontal line ('x-axis','y-axis')
  @param {Boolean} _showLineValue Indicate to show the line value or not
 */
wtChart.prototype.setLine = function(_division,_axis,_showLineValue)
{
	if (_division<=0)
		throw "wtChart::setLine(): the value of _division should be positive";

	_axis = _axis.toLowerCase();
	if (!(_axis == "x-axis" || _axis == "y-axis"))
		throw "wtChart::setLine(): _axis should be 'x-axis' or 'y-axis'";

	if (_axis == "y-axis")
	{	
		while( this.get("yList").length>0)
		{
			var tmp = this.get("yList").pop();
			tmp.removeSelf();
		}
	}
	else
	{
		while (this.get("xList").length>0)
		{
			var tmp = this.get("xList").pop();
			tmp.removeSelf();
		}
	}
	if (_division>0)
	{
		var w2 = $_(this).w2;
		var h2 = $_(this).h2;
		var width = $_(this).width_;
		var height = $_(this).height_;
		var interval = (_axis=="y-axis"? height/_division : width/_division);
		var stepValue = 
			(_axis=="y-axis"? ($_(this).yMax-$_(this).yMin)/_division: ($_(this).xMax-$_(this).xMin)/_division);
		interval.toFixed(2);
		stepValue.toFixed(2);

		for (var i=0; i<=_division; i++) 
		{
			if (i>0 && i<_division) 
			{
				var line1,line2;
				if (_axis=="y-axis")
				{
					line1 = new wtLine(this,w2,interval*i,w2+width,interval*i,"black");
					line2 = new wtLine(this,w2,interval*i,0,interval*i+h2,"black");
					//line1.set("dir","hor");
					//line2.set("dir","ver");
					$_(this).yList.push(line1);
					$_(this).yList.push(line2);
				}
				else
				{
					line1 = new wtLine(this,interval*i+w2,height,interval*i+w2,0,"black");
					line2 = new wtLine(this,interval*i+w2,height,interval*i,height+h2,"black");
					$_(this).xList.push(line1);
					$_(this).xList.push(line2);
				}
				line1.setStrokeOpacity(0.3);
				line2.setStrokeOpacity(0.3);
			}

			if (_showLineValue)
			{
				if (_axis=="y-axis")
				{
					var value = $_(this).yMin + stepValue*(_division-i);
					var  lineValue= new wtText(this,-5,interval*i+h2-5,value,"purple");
					lineValue.setAnchorPosition("end");
					this.get("yList").push(lineValue);
				}
				else
				{
					var value = $_(this).xMin + stepValue*i;
					var lineValue= new wtText(this,interval*i,height+h2+5,value,"purple");
					lineValue.setAnchorPosition("middle");
					this.get("xList").push(lineValue);
				}
			}
		}
	}
}
/**
  Sets the label of x-axis
  @param {String} _axisLabel The label of the x-axis
 */
wtChart.prototype.setXAxisLabel = function(_axisLabel)
{
	this.get("xAxisLabel").setText(_axisLabel);
}
/**
  Sets the label of y-axis
  @param {String} _axisLabel The label of the y-axis
 */
wtChart.prototype.setYAxisLabel = function(_axisLabel)
{
	this.get("yAxisLabel").setText(_axisLabel);
}

/**
  Sets the label for a bar in the chart
  @param {Number} _index The index of the bar
  @param {String} _label The label text of the bar
 */
wtChart.prototype.setLabel = function(_index,_label)
{
	var w2 = $_(this).w2;
	var h2 = $_(this).h2;
	var width = $_(this).width_;
	var height = $_(this).height_;
	var depth = $_(this).depth_;
	var space = $_(this).barSpace;
	var offsetx = w2+10;
	if ($_(this).type_=="vertical")
	{
		var label = new wtText(this,offsetx+_index*space+5,height+25,_label,"purple");
	}
	else
	{
		var label = new wtText(this,-5,height-(offsetx+_index*space),_label,"purple");
		label.setAnchorPosition("end");
	}
}

/**
  Sets the legends for the chart
  @param {Number} _index The index of the bar,line or curve of the chart
  @param {String} _legend The legend text
 */
wtChart.prototype.setLegend = function(_index,_legend)
{
	var color = $_(this).colorGen.colorArray[_index].toString();
	var tmp = new wtRectangle(this,$_(this).width_+$_(this).w2+5,25*_index,10,10,color,"none");
	var leg = new wtText(this,$_(this).width_+$_(this).w2+20,_index*25,_legend,"black");
}
// }}}
// {{{ wtBarChart - A typical bar chart.
/**
  Constructs a bar chart object
  @class A bar chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number} _y The y-coordinate of the chart
  @param {String} _type Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The width of the background
  @param {Number} _depth  The depth of the background
  @param {Number} _angle  The angle of skewing of the chart
  @param {Number} _xMin The lower bound of x-axis
  @param {Nunber} _xMax The upper bound of x-axis
  @param {Number} _yMin The lower bound of y-axis
  @param {Number} _yMax The upper bound of y-axis
 */
var wtBarChart = function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax)
{
	_type.toLowerCase();
	if (!(_type == "vertical" || _type == "horizontal"))
		throw "wtBarChart(): _type should be 'vertical' or 'horizontal'";
	this.base = wtChart;
	this.base(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax);
	$_(this).containNegative = false;
	$_(this).barValue = [];
}
wtBarChart.prototype = new wtChart;
/**
  Sets the bar value
  @param {Number} _index The index of the bar
  @param {String} _value The bar value
 */
wtBarChart.prototype.setBarValue = function(_index,_value,_color)
{
	var domNode = $_(this);
	var w2 = domNode.w2;
	var h2 = domNode.h2;
	var width = domNode.width_;
	var height = domNode.height_;
	var depth = domNode.depth_;
	var space = domNode.barSpace;
	var _h = domNode.type_=="vertical" ? _value/domNode.yMax*height : _value/domNode.xMax*width;
	var offsetx = w2+10;
	if (domNode.type_=="vertical")
	{
		var value = new wtText(this,offsetx+_index*space+20,height-_h+offsetx-25,_value,_color);
	}
	else
	{
		var value = new wtText(this,_h,height-(offsetx+_index*space),_value,_color);
	}
}
/**
  Adds a bar to the chart
  @param {Number} _value The value of the bar
 */
wtBarChart.prototype.addBar = function(_value) 
{
	var domNode = $_(this);
	var w2 = domNode.w2;
	var h2 = domNode.h2;
	var width = domNode.width_;
	var height = domNode.height_;
	var depth = domNode.depth_;
	var angle = domNode.angle_;
	var _h = domNode.type_=="vertical" ? _value/domNode.yMax*height : _value/domNode.xMax*width;
	var space = domNode.barSpace;
	var offsetx = w2+10;
	var bar_no = domNode.bar_no;
	var color = domNode.colorGen.colorArray[bar_no].toString();

	if (!domNode.containNegative && _value<0)
	{
		// for bar with negative value
	}

	if (domNode.type_=="vertical")
	{
		var bar = new wtBar(this,domNode.barWidth,Math.abs(_h),depth,angle,color);
		var x = offsetx + bar_no * space;
		bar.setAbsolutePosition(x,height-(_h>0?_h:0));
		/*
		   if (x + domNode.barWidth + offsetx> width)
		   {
		   domNode.rectList[0].setSize(x+domNode.barWidth + offsetx, height);
		   domNode.rectList[2].setSize(x+domNode.barWidth + offsetx, h2);
		   domNode.rectList[2].skew(-(90-angle),0);
		   for (var i=0; i<domNode.yList.length; i++)
		   {
		   if (domNode.yList[i] instanceof wtLine && domNode.yList[i].get("dir") == "hor")
		   {
		   var p = domNode.yList[i].getEndPoints();
		//domNode.yList[i].setEndPoints(p.x1,p.y1,x + domNode.barWidth + offsetx + w2,p.y2);
		}
		}
		}
		 */

	}
	else
		if (domNode.type_=="horizontal")
		{
			var bar = new wtBar(this,Math.abs(_h),domNode.barWidth,depth,angle,color);
			bar.setAbsolutePosition(0,height-(offsetx+bar_no*space)-domNode.barWidth);
		}
	this.setBarValue(domNode.bar_no,_value,"black");
	domNode.barValue.push(_value);
	domNode.bar_no++;
}
/**
  Finds the sum of the bars
  @returns The sum of the bars
  @type {Number}
 */
wtBarChart.prototype.getSum = function()
{	
	var domNode = $_(this);
	var sum = 0;
	for (var i=0; i<domNode.barValue.length; i++)
		sum += domNode.barValue[i];
	return sum;
}
/**
  Finds the mean value of the bars
  @returns The mean value of the bars
  @type {Number}
 */
wtBarChart.prototype.getMean = function()
{
	var domNode = $_(this);
	return this.getSum()/domNode.barValue.length;
}
/**
  Finds the variance of the bars
  @returns The variance of the bars
  @type {Number}
 */
wtBarChart.prototype.getVariance = function()
{
	var domNode = $_(this);
	var sum = 0;
	var mean = this.getMean();
	for (var i=0; i<domNode.barValue.length; i++)
	{
		var diff = domNode.barValue[i] - mean;
		sum += diff * diff;
	}
	return sum/domNode.barValue.length;
}
/**
  Finds the maximum value of the bars
  @returns The maximum value of the bars
  @type {Number}
 */
wtBarChart.prototype.getMax = function()
{
	var domNode = $_(this);
	domNode.barValue.sort(wtChart.sort);
	return domNode.barValue[domNode.barValue.length-1];
}
/**
  Finds the minimum value of the bars
  @returns The mimimum value of the bars
  @type {Number}
 */
wtBarChart.prototype.getMin = function()
{
	var domNode = $_(this);
	domNode.barValue.sort(wtChart.sort);
	return domNode.barValue[0];
}
/**
  Finds the mdian of the bars
  @returns The median of the bars
  @type {Number}
 */
wtBarChart.prototype.getMedian = function()
{
	var domNode= $_(this);
	domNode.barValue.sort(wtChart.sort);
	if (domNode.barValue.length == 0)
		return 0;

	if (domNode.barValue.length % 2 == 0)
	{
		var index = parseInt(domNode.barValue.length/2);
		return (domNode.barValue[index-1] + domNode.barValue[index]) / 2;
	}
	else
	{
		return domNode.barValue[parseInt(domNode.barValue.length/2)];
	}
}
// }}}
// {{{ wtStackBarChart - A stacked bar chart.
/**
  Constructs a stacked bar chart.
  @class A stacked bar chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number} _y The y-coordinate of the chart
  @param {String} _type Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The height of the background
  @param {Number} _depth THe depth of the background
  @param {Number} _angle The angle of skewing of the chart
  @param {Number} _xMin The lower bound of x-axis
  @param {Number} _xMax The upper bound of x-axis
  @param {Number} _yMin The lower bound of y-axis
  @param {Number} _yMax The upper bound of y-axis
 */
var wtStackBarChart = function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax)
{
	_type.toLowerCase();
	if (!(_type == "vertical" || _type == "horizontal"))
		throw "wtStackBarChart(): _type should be 'vertical' or 'horizontal'";

	this.base = wtChart;
	this.base(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax);

	$_(this).accumSum = [];
	$_(this).accumBarNo = [];
}

wtStackBarChart.prototype = new wtChart;

/**
  Adds a bar to the chart at the given position
  @param {Number} _index The position index of the bar
  @param {String} _value The value of the bar
 */
wtStackBarChart.prototype.addBar = function(_index,_value) 
{
	if (_index<0)
		throw "wtStackBarChart::addBar(): the value of _index should be non-negative";
	if (_value<0)
		throw "wtStackBarChart::addBar(): the value of _value should be non-negative";
	var domNode = $_(this);
	var w2 = domNode.w2;
	var h2 = domNode.h2;
	var width = domNode.width_;
	var height = domNode.height_;
	var depth = domNode.depth_;
	var angle = domNode.angle_;
	var _h = domNode.type_=="vertical" ? _value/domNode.yMax*height : _value/domNode.xMax*width;
	var space = domNode.barSpace;
	var offsetx = w2+10;
	var bar_no = domNode.bar_no;

	while(_index>=domNode.accumSum.length)
	{
		domNode.accumSum.push(0);
		domNode.accumBarNo.push(0);
	}

	var color = domNode.colorGen.colorArray[domNode.accumBarNo[_index]].toString();	

	if (domNode.type_=="vertical")
	{
		var bar = new wtBar(this,domNode.barWidth,_h,depth,angle,color);
		bar.setAbsolutePosition(offsetx+_index*space,height-_h-domNode.accumSum[_index]);
	}
	else
	{
		var bar = new wtBar(this,_h,domNode.barWidth,depth,angle,color);
		bar.setAbsolutePosition(domNode.accumSum[_index],height-(offsetx+_index*space)-domNode.barWidth);
	}
	domNode.bar_no++;
	domNode.accumSum[_index] += _h;
	domNode.accumBarNo[_index]++;
}
//}}}
// {{{ wtColurmnBarChart - A multi-column bar chart.
/**
  Constructs a multi-column bar chart.
  @class A multi-column bar chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number} _y The y-coordinate of the chart
  @param {String} _type Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The height of the background
  @param {Number} _depth The depth of the background
  @param {Number} _angle The angle of skewing of the chart
  @param {Number} _xMin The lower bound of the x-axis
  @param {Number} _xMax The upper bound of the x-axis
  @param {Number} _yMin The lower bound of the y-axis
  @param {Number} _yMax The upper bound of the y-axis
  @param {Number} _groupSize The number of bars grouped in a column 
 */
var wtColumnBarChart = function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax,_groupSize)
{
	_type.toLowerCase();
	if (!(_type == "vertical" || _type == "horizontal"))
		throw "wtColumnBarChart(): _type should be 'vertical' or 'horizontal'";

	this.base = wtChart;
	this.base(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax);

	$_(this).barWidth /= _groupSize;
	$_(this).accumSum = [];
	$_(this).accumBarNo = [];
}

wtColumnBarChart.prototype = new wtChart;

/**
  Adds a bar to the given position
  @param {Number} _index The position index of the bar
  @param {String} _value The value of the bar
 */
wtColumnBarChart.prototype.addBar = function(_index,_value) 
{
	if (_index<0)
		throw "wtColumnBarChart::addBar(): the value of _index should be non-negative";

	var domNode = $_(this);
	var w2 = domNode.w2;
	var h2 = domNode.h2;
	var width = domNode.width_;
	var height = domNode.height_;
	var depth = domNode.depth_;
	var angle = domNode.angle_;
	var _h = domNode.type_=="vertical" ? _value/domNode.yMax*height : _value/domNode.xMax*width;
	var space = domNode.barSpace;
	var offsetx = w2+10;
	var bar_no = domNode.bar_no;

	while(_index>=domNode.accumSum.length)
	{
		domNode.accumSum.push(0);
		domNode.accumBarNo.push(0);
	}

	var color = domNode.colorGen.colorArray[domNode.accumBarNo[_index]].toString();

	var barWidthOffset = domNode.accumBarNo[_index]*domNode.barWidth;
	if (domNode.type_=="vertical")
	{
		var bar = new wtBar(this,domNode.barWidth,_h,depth,angle,color);
		bar.setAbsolutePosition(offsetx+_index*space+barWidthOffset,height-_h);
	}
	else
	{
		var bar = new wtBar(this,_h,domNode.barWidth,depth,angle,color);
		bar.setAbsolutePosition(0,height-(offsetx+_index*space)-domNode.barWidth-barWidthOffset);
	}
	domNode.bar_no++;
	domNode.accumSum[_index] += _h;
	domNode.accumBarNo[_index]++;
}
// }}}
// {{{ wtLineChart - A line chart.
/**
  Constructs a line chart object.
  @class A line chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number} _y The y-coordinate of the chart
  @param {String} _type  Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The height of the background
  @param {Number} _depth The depth of the background
  @param {Number} _angle The angle of skewing of the chart
  @param {Number} _xMin The lower bound of the x-axis
  @param {Number} _xMax The upper bound of the x-axis
  @param {Number} _yMin The lower bound of the y-axis
  @param {Number} _yMax The upper bound of the y-axis
 */
var wtLineChart = function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax)
{
	this.base = wtChart;
	this.base(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax);

	$_(this).path_ = [];
}

wtLineChart.prototype = new wtChart;

/**
  @private
 */
wtLineChart.prototype.add = function(_type,ptList)
{
	var domNode = $_(this);
	var color = domNode.colorGen.colorArray[domNode.path_.length].toString();
	var pathGroup = new wtVectorGroup(this);
	var path = new wtPath(pathGroup,"none",color);
	path.setStrokeWidth(2);

	for (var j=0; j<ptList.length; j++)
	{
		ptList[j].push(ptList[j][0]);
		ptList[j].push(ptList[j][1]);

		ptList[j][0] -= domNode.xMin;
		ptList[j][1] -= domNode.yMin;
		ptList[j][0] *= domNode.width_/(domNode.xMax-domNode.xMin);		
		ptList[j][1] *= -domNode.height_/(domNode.yMax-domNode.yMin);
		ptList[j][0] = Math.round(ptList[j][0]);
		ptList[j][1] = Math.round(ptList[j][1]);
	}

	for (var i = 0; i<ptList.length; i++)
	{

		var x = ptList[i][0];
		var y = ptList[i][1];
		if (i==0)
		{
			path.moveTo("abs",ptList[i][0],ptList[i][1]);
		}
		else
		{
			if (_type == "line")
			{	
				path.lineTo("abs",x,y);
			}
			else 
			{	
				var x1 = Math.round((x-ptList[i-1][0])*1/3 + ptList[i-1][0]);
				var x2 = Math.round((x-ptList[i-1][0])*2/3 + ptList[i-1][0]);
				var y1 = Math.round(ptList[i-1][1]);
				var y2 = Math.round(y);
				path.curveTo("abs",x1,y1,x2,y2,x,y);
			}
		}

		var marker = new wtCircle(this,x+domNode.w2,y+domNode.height_,4,color,"black");
	}

	pathGroup.setAbsolutePosition(domNode.w2,domNode.height_);
	domNode.path_.push(path);
}
/**
  Adds a line to the chart given a list of points
  @param {List} ptList A list of points in the form of [ [x0,y0], [x1,y1],...,[xn,yn] ]
 */
wtLineChart.prototype.addLine = function(ptList)
{
	if (ptList.length<2)
		throw "wtLineChart::addLine(): the length of ptList should be greater than or equal to 2";
	this.add("line",ptList);
}
/**
  Adds a curve to the chart given a list of points
  @param {List} ptList A list of points in the form of [ [x0,y0], [x1,y1],...,[xn,yn] ]
 */
wtLineChart.prototype.addCurve = function(ptList)
{
	if (ptList.length<3)
		throw "wtLineChart::addCurve(): the length of ptList should be greater than or equal to 3";
	this.add("curve",ptList);
}
// }}}
// {{{ wtStackLineChart - A stacked line chart.
/**
  Constructs a stacked line chart.
  @class A stacked line chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number) _y The y-coordinate of the chart
  @param {String} _type Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The height of the background
  @param {Number} _depth  The depth of the background
  @param {Number} _angle The angle of skewing of the chart
  @param {Number} _xMin The lower bound of the x-axis
  @param {Number} _xMax The upper bound of the x-axis
  @param {Number} _yMin The lower bound of the y-axis
  @param {Number} _yMax THe upper bound of the y-axis
 */
var wtStackLineChart =function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax)
{
	this.base = wtChart;
	this.base(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax);

	var domNode = $_(this);
	domNode.accumSum = [];
	domNode.dList = [];
	domNode.ptList = [];
}

wtStackLineChart.prototype = new wtChart;

/**
  @private
  Adds a line or curve to the chart
  @param {List} ptList A list of points in the form of [[x0,y0],[x1,y1],...,[xn,yn]]
 */
wtStackLineChart.prototype.add = function(_type,ptList)
{
	var domNode = $_(this);
	var d = "";
	while(domNode.accumSum.length<ptList.length)
	{
		domNode.accumSum.push(0);
	}

	for (var j=0; j<ptList.length; j++)
	{
		ptList[j][1] = Math.round(ptList[j][1]+domNode.accumSum[j]);
		domNode.accumSum[j] = ptList[j][1];
	}


	for (var j=0; j<ptList.length; j++)
	{
		ptList[j][0] -= domNode.xMin;
		ptList[j][1] -= domNode.yMin;
		ptList[j][0] *= domNode.width_/(domNode.xMax-domNode.xMin);		
		ptList[j][1] *= -domNode.height_/(domNode.yMax-domNode.yMin);

	}

	for (var i = 0; i<ptList.length; i++)
	{
		var x = Math.round(ptList[i][0]);
		var y = Math.round(ptList[i][1]);
		if (i==0)
		{
			d = wtPath.moveTo_("abs",x,y);
		}
		else
		{
			if (_type=="curve")
			{
				var x1 = Math.round((x-ptList[i-1][0])*1/3 + ptList[i-1][0]);
				var x2 = Math.round((x-ptList[i-1][0])*2/3 + ptList[i-1][0]);
				var y1 = Math.round(ptList[i-1][1]);
				var y2 = Math.round(y);
				d += wtPath.curveTo_("abs",x1,y1,x2,y2,x,y);
			} else if (_type=="line")
			{
				d += wtPath.lineTo_("abs",x,y);
			}
		}
	}

	d += wtPath.lineTo_("abs",Math.round(ptList[ptList.length-1][0]),0);
	d += wtPath.lineTo_("abs",Math.round(ptList[0][0]),0);
	d += wtPath.closePath_();

	domNode.dList.push(d);
	domNode.ptList.push(ptList);
}
/**
  Adds a line to the chart
  @param {List} ptList A list of points in the form of [[x0,y0],[x1,y1],...,[xn,yn]]
 */
wtStackLineChart.prototype.addLine = function(ptList)
{
	if (ptList.length<2)
		throw "wtStackLineChart::addLine(): the length of ptList should be greater than or equal to 2";
	this.add("line",ptList);
}
/**
  Adds a curve to the chart
  @param {List} ptList A list of points in the form of [[x0,y0],[x1,y1],...,[xn,yn]]
 */
wtStackLineChart.prototype.addCurve = function(ptList)
{
	if (ptList.length<3)
		throw "wtStackLineChart::addCurve(): the length of ptList should be greater than or equal to 3";
	this.add("curve",ptList);
}
/**
  Renders the chart
 */
wtStackLineChart.prototype.render = function()
{
	var domNode = $_(this);
	var color;

	for (var i=domNode.dList.length-1; i>=0; i--)
	{
		var pathGroup = new wtVectorGroup(this);
		color = domNode.colorGen.colorArray[i].toString();
		var path = new wtPath(pathGroup,color,color);
		path.setPath(domNode.dList[i]);
		pathGroup.setAbsolutePosition(domNode.w2,domNode.height_);
	}

	for (var i=domNode.dList.length-1; i>=0; i--)
	{
		color = domNode.colorGen.colorArray[i].translateHSV(0,-0.2,0);
		for (var j=0; j<domNode.ptList[i].length; j++)
		{
			var x = domNode.ptList[i][j][0]+domNode.w2;
			var y = domNode.ptList[i][j][1]+domNode.height_;
			var marker = new wtCircle(this,x,y,4,color,"black");
		}
	}
}
// }}}
// {{{ wtScatterChart - A scatter chart.
/**
  Constructs a scatter chart object
  @class A scatter chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _x The x-coordinate of the chart
  @param {Number} _y The y-coordinate of the chart
  @param {String} _type Vertical or horizontal
  @param {Number} _width The width of the background
  @param {Number} _height The height of the background
  @param {Number} _depth  The depth of the background
  @param {Number} _angle The angle of skewing of the chart
  @param {Number} _xMin The lower bound of the x-axis
  @param {Number} _xMax The upper bound of the x-axis
  @param {Number} _yMin The lower bound of the y-axis
  @Param {Number} _yMax The upper bound of the y-axis
 */
var wtScatterChart = function(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax)
{
	this.base = wtChart;
	this.base(_parent,_x,_y,_type,_width,_height,_depth,_angle,_xMin,_xMax,_yMin,_yMax);
	var domNode = $_(this);
	domNode.ptList = [];
	domNode.xy = [];
	domNode.xx = [];
	domNode.yy = [];
	domNode.sumX = 0;
	domNode.sumY = 0;
	domNode.sumXY = 0;
	domNode.sumXX = 0;
	domNode.sumYY = 0;
}

wtScatterChart.prototype = new wtChart;

/**
  Adds a point to the scatter chart
  @param {Number} _x The x-coordinate of the point
  @param {Number} _y The y-coordinate of the point
 */
wtScatterChart.prototype.addPoints = function(_ptList)
{
	var domNode = $_(this);
	var color = domNode.colorGen.colorArray[domNode.ptList.length].toString();
	domNode.ptList.push(_ptList);

	for (var i=0; i<_ptList.length; i++)
	{
		var _x = _ptList[i][0];
		var _y = _ptList[i][1];

		var x = _x*domNode.width_/domNode.xMax;
		var y = -_y*domNode.height_/domNode.yMax;

		var markerGroup = new wtVectorGroup(this);	
		var marker = new wtCircle(markerGroup,0,0,5,color,"black");
		markerGroup.setAbsolutePosition(x+domNode.w2,y+domNode.height_);
	}
}

/**
  Shows the linnear regression line of the chart
 */
wtScatterChart.prototype.showLinearTrendline = function()
{
	// linear regression
	var domNode = $_(this);

	for (var j=0; j<domNode.ptList.length; j++)
	{
		var list = domNode.ptList[j];
		domNode.sumX = 0;
		domNode.sumY = 0;
		domNode.sumXY = 0;
		domNode.sumXX = 0;
		domNode.sumYY = 0;
		if (list.length>=2)
		{

			for (var i=0; i<list.length; i++)
			{
				domNode.sumX += list[i][0];
				domNode.sumY += list[i][1];
				domNode.sumXY += list[i][0]*list[i][1];
				domNode.sumXX += list[i][0]*list[i][0];
				domNode.sumYY += list[i][1]*list[i][1];
			}
			var sumXSquare = domNode.sumX*domNode.sumX;
			var sumYSquare = domNode.sumY*domNode.sumY;
			var slope = (list.length*domNode.sumXY-domNode.sumX*domNode.sumY)/(list.length*domNode.sumXX-sumXSquare);
			var yIntercept = (domNode.sumY-slope*domNode.sumX)/list.length;

			var x0,x1,y0,y1;
			x0 = list[0][0];
			x1 = list[list.length-1][0];
			y0 = (slope*x0 + yIntercept);
			y1 = (slope*x1 + yIntercept);

			x0 *= domNode.width_/domNode.xMax;
			x1 *= domNode.width_/domNode.xMax;
			y0 *= -domNode.height_/domNode.yMax;
			y1 *= -domNode.height_/domNode.yMax;

			var color = domNode.colorGen.colorArray[j].toString();
			var trendlineGroup = new wtVectorGroup(this);	
			var trendline = new wtLine(trendlineGroup,x0,y0,x1,y1,color);
			trendline.setStrokeWidth(2);
			trendlineGroup.setAbsolutePosition(domNode.w2,domNode.height_);
		}
	}
}
// }}}
// {{{ wtPieChart - A pie chart.
/**
  Constructs a pie chart object
  @class A pie chart.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _cx The x-coordinate of the center of the chart
  @param {Number} _cy The y-coordinate of the center of the chart
  @param {Number} _rx The length of semi-major or semi-minor axis of the chart
  @param {Number} _ry The length of semi-major or semi-minor axis of the chart
  @param {Boolean} _flag Indicate whether the pie chart is exploded or not
 */
var wtPieChart = function(_parent,_cx,_cy,_rx,_ry,_flag) 
{
	if (arguments.length<6)
		return;
	if (_rx<=0)
		throw "wtPieChart(): the value of _rx should be positive";
	if (_ry<=0)
		throw "wtPieChart(): the value of _ry should be positive";

	this.base = wtVectorGroup;
	this.base(_parent);

	var domNode = $_(this);
	domNode.top_ = [];
	domNode.innerL = [];
	domNode.innerR = [];
	domNode.outter = [];
	domNode.extreme = [];
	domNode._cx = 0;
	domNode._cy = 0;
	domNode._rx = _rx;
	domNode._ry = _ry;
	domNode._flag = _flag;
	domNode._sum = 0;
	domNode._sector = 0;
	domNode.sectorValue = [];
	this.set("titleText", new wtText(this,0,-_ry-30,"Pie Chart","purple"));
	domNode.colorGen = new wtColorGenerator(8,1,1);
	domNode.colorGen.getColor(); // discard black color
	domNode.colorGen.spin(parseInt(domNode.colorGen.count() * Math.random()));

	var halfLength = Math.floor(domNode.colorGen.count()/2);
	for (var i=0; i<halfLength; i++) {
		if (i%2 == 0)
		{
			var tmp = domNode.colorGen.colorArray[i];
			domNode.colorGen.colorArray[i] = domNode.colorGen.colorArray[i+halfLength];
			domNode.colorGen.colorArray[i+halfLength] = tmp;
		}
	}

	for (var i=0; i<domNode.colorGen.count(); i++) {
		domNode.colorGen.colorArray[i] = domNode.colorGen.colorArray[i].translateHSV(0,-0.5,0);
	}

	domNode.sectorNo = 0;
	this.set("percentList",[]);
	this.setAbsolutePosition(_cx,_cy);
}

wtPieChart.prototype = new wtVectorGroup;

/**
  Sets the title of the pie chart
 */
wtPieChart.prototype.setTitle = function(_text)
{
	this.get("titleText").setText(_text);
}
/**
  Adds a sector to the pie chart
  @param {Number} _start_angle The starting position
  @param {Number} _delta_angle The difference in angle between the starting and ending position
  @param {Number} _color The color of the sector
 */

wtPieChart.prototype.add = function(_start_angle,_delta_angle) {
	var domNode = $_(this);
	var startx,starty,endx,endy;
	var fa = Math.abs(_delta_angle)>180? 1:0;
	var fs = _delta_angle>0? 1:0;
	var offh = 40;
	var special = false;
	if (_delta_angle>=360)
	{
		_delta_angle = 360;
		special = true;
	}

	if (_delta_angle>360) _delta_angle = 360;

	_start_angle = _start_angle % 360;
	var endAngle = (_start_angle+_delta_angle) % 360;
	if (_start_angle==endAngle) endAngle = _start_angle + 360;
	//endAngle = _start_angle + _delta_angle;
	var _cx = domNode._cx;
	var _cy = domNode._cy;
	var _rx = domNode._rx;
	var _ry = domNode._ry;
	var color = domNode.colorGen.colorArray[domNode.sectorNo];

	var middlex = parseFloat(_rx * Math.cos(Math.deg2rad(_start_angle+_delta_angle/2)) );
	var middley = parseFloat(_ry * Math.sin(Math.deg2rad(_start_angle+_delta_angle/2)) );

	this.get("percentList").push([_cx+middlex,_cy+middley,_delta_angle]);

	if (domNode._flag) 
	{
		_cx += middlex/20;
		_cy += middley/10;

		_cx = Math.round(_cx);
		_cy = Math.round(_cy);
	}

	startx = parseFloat(_rx * Math.cos(Math.deg2rad(_start_angle)) + _cx);
	starty = parseFloat(_ry * Math.sin(Math.deg2rad(_start_angle)) + _cy);
	endx = parseFloat(_rx * Math.cos(Math.deg2rad(_start_angle+_delta_angle)) + _cx);
	endy = parseFloat(_ry * Math.sin(Math.deg2rad(_start_angle+_delta_angle)) + _cy);


	var x0 = parseFloat(_rx * Math.cos(Math.deg2rad(0)) + _cx);
	var y0 = parseFloat(_ry * Math.sin(Math.deg2rad(0)) + _cy);
	var x180= parseFloat(_rx * Math.cos(Math.deg2rad(180)) + _cx);
	var y180 = parseFloat(_ry * Math.sin(Math.deg2rad(180)) + _cy);

	var starty_offh = starty + offh;
	var endy_offh = endy + offh;
	var y0_offh = y0 + offh;
	var y180_offh = y180 + offh;

	// vml would recognize num with long decimal places incorrectly
	if (isMSIE()) 
	{

		startx = Math.round(startx);
		starty = Math.round(starty);
		endx = Math.round(endx);
		endy = Math.round(endy);

		x0 = Math.round(x0);
		y0 = Math.round(y0);
		x180 = Math.round(x180);
		y180 = Math.round(y180);
		starty_offh = Math.round(starty_offh);
		endy_offh = Math.round(endy_offh);
		y0_offh = Math.round(y0_offh);
		y180_offh = Math.round(y180_offh);

	}


	var degErr = 0.5;
	var innerColor = color.translateHSV(0,0,-0.2);
	var outterColor = color.translateHSV(0,-0.1,-0.1);
	var coverColor = color.toString();

	var innerStart = {fill:innerColor, stroke:"none", level:0, path:""};
	var innerEnd = {fill:innerColor, stroke:"none", level:0, path:""};
	var outter = {fill:outterColor, stroke:"none", level:0, path:""};
	var cover = {fill:coverColor, stroke:"none", level:0, path:""};

	innerStart.path = wtPath.moveTo_("abs",_cx,_cy);
	innerStart.path += wtPath.lineTo_("rel",0,offh);
	innerStart.path += wtPath.lineTo_("abs",startx,starty_offh);
	innerStart.path += wtPath.lineTo_("rel",0,-offh);
	innerStart.path += wtPath.closePath_();

	//group.appendChild(innerStart);
	if (_start_angle>=90 && _start_angle<270) 
	{
		innerStart.level = _start_angle + degErr;
		domNode.innerL.push(innerStart);
	} 
	else 
	{
		innerStart.level = _start_angle>=0 && _start_angle<90? 90-_start_angle-degErr : 450-_start_angle -degErr;
		domNode.innerR.push(innerStart);
	}

	innerEnd.path = wtPath.moveTo_("abs",_cx,_cy);
	innerEnd.path += wtPath.lineTo_("rel",0,offh);
	innerEnd.path += wtPath.lineTo_("abs",endx,endy_offh);
	innerEnd.path += wtPath.lineTo_("rel",0,-offh);
	innerEnd.path += wtPath.closePath_();

	if (endAngle>=90 && endAngle<270) 
	{
		innerEnd.level = endAngle-degErr;
		domNode.innerL.push(innerEnd);
	} 
	else 
	{
		innerEnd.level = endAngle>=0 && endAngle<90? 90 - endAngle+degErr:450- endAngle+degErr;
		domNode.innerR.push(innerEnd);
	}

	cover.path = wtPath.moveTo_("abs",_cx,_cy);
	if (isMSIE()) 
	{
		var s = -Math.round(_start_angle * 65536);
		var e = -Math.round(_delta_angle * 65536);
		if (special)
		{
			cover.path += wtPath.moveTo_("abs",startx,starty);
		}
		cover.path += " ae " + _cx + "," + _cy + "," + _rx + "," + _ry + "," + s + "," + e ;
		cover.path += wtPath.closePath_();
	} 
	else 
	{
		if (special)
		{
			cover.path = "special";
		}
		else
		{
			cover.path += wtPath.lineTo_("abs",startx,starty);
			cover.path += " A" + _rx + "," + _ry + " 0 " + fa +"," + fs + " " + endx + "," + endy;

			cover.path += wtPath.closePath_();
		}
	}
	domNode.top_.push(cover);


	var caseA = _start_angle>=0 && _start_angle<=180;
	var caseB = endAngle >=0 && endAngle <= 180;

	var arc;

	if (caseA && caseB) {
		if (_start_angle<endAngle) 
		{
			arc = wtPath.moveTo_("abs",startx,starty_offh);
			if (isMSIE()) {
				var s = -Math.round(_start_angle * 65536);
				var e = -Math.round(_delta_angle * 65536);
				var n_cy = parseInt((_cy+offh)*100)/100;
				arc += " ae " + _cx + "," + n_cy + "," + _rx + "," + _ry + "," + s + "," + e ;
			} 
			else 
			{
				arc += " A" + _rx + "," + _ry + " 0 " + fa +"," + fs + " " + endx + "," + endy_offh;
			}
			arc += wtPath.lineTo_("abs",endx,endy);
			arc += wtPath.lineTo_("abs",startx,starty);
			arc += wtPath.closePath_();
		} else 
		{
			arc = wtPath.moveTo_("abs",startx,starty_offh);
			var nf = 1-fa;
			if (isMSIE()) {

			} 
			else 
			{
				arc += " A" + _rx + "," + _ry + " 0 " + nf +"," + fs + " " + x180 + "," + y180_offh;
			}
			arc += wtPath.lineTo_("abs",x180,y180);
			arc += wtPath.lineTo_("abs",startx,starty);
			arc += wtPath.closePath_();

			var outter2 = {fill:outterColor, stroke:"none", level:0, path:""};
			var arc2 = wtPath.moveTo_("abs",x0,y0_offh);
			if (isMSIE()) 
			{
			} 
			else 
			{
				arc2 += " A" + _rx + "," + _ry + " 0 " + nf +"," + fs + " " + endx + "," + endy_offh;
			}
			arc2 += wtPath.lineTo_("abs",endx,endy);
			arc2 += wtPath.lineTo_("abs",x0,y0);
			arc2 += wtPath.closePath_();
			outter2.path = arc2;
			domNode.outter.push(outter2);
		}
	}
	if (caseA && !caseB) 
	{
		arc = wtPath.moveTo_("abs",startx,starty_offh);
		if (_delta_angle>180) fa = 1-fa;
		if (isMSIE()) 
		{
			var s = -Math.round(_start_angle * 65536);
			var e = Math.round(-(180-_start_angle) * 65536);
			var n_cy = parseInt((_cy+offh)*100)/100;
			arc += " ae " + _cx + "," + n_cy + "," + _rx + "," + _ry + "," + s + "," + e ;
		} 
		else 
		{
			arc += " A" + _rx + "," + _ry + " 0 " + fa +"," + fs + " " + x180 + "," + y180_offh;
		}
		arc += wtPath.lineTo_("abs",x180,y180);
		arc += wtPath.lineTo_("abs",startx,starty);
		arc += wtPath.closePath_();

	}
	if (!caseA && caseB) 
	{
		arc = wtPath.moveTo_("abs",x0,y0_offh);
		if (_delta_angle>180) fa = 1-fa;
		if (isMSIE()) {
			var s = Math.round(-0 * 65536);
			var e = -Math.round(endAngle * 65536);
			var n_cy = parseInt((_cy+offh)*100)/100;
			arc += " ae " + _cx + "," + n_cy + "," + _rx + "," + _ry + "," + s + "," + e ;
		} 
		else 
		{
			arc += " A" + _rx + "," + _ry + " 0 " + fa +"," + fs + " " + endx + "," + endy_offh;
		}
		arc += wtPath.lineTo_("abs",endx,endy);
		arc += wtPath.lineTo_("abs",x0,y0);
		arc += wtPath.closePath_();
	}

	if (!caseA && !caseB && _start_angle>endAngle) {
		arc = wtPath.moveTo_("abs",x0,y0_offh);
		if (_delta_angle>180) fa = 1-fa;
		if (isMSIE()) 
		{
			var s = -Math.round(0 * 65536);
			var e = -Math.round(180 * 65536);
			var n_cy = parseInt((_cy+offh)*100)/100;
			arc += " ae " + _cx + "," + n_cy + "," + _rx + "," + _ry + "," + s + "," + e ;
		} 
		else
		{
			arc += " A" + _rx + "," + _ry + " 0 " + fa +"," + fs + " " + x180 + "," + y180_offh;
		}
		arc += wtPath.lineTo_("abs",x180,y180);
		arc += wtPath.lineTo_("abs",x0,y0);
		arc+= wtPath.closePath_();
	}

	outter.path = arc;
	domNode.outter.push(outter);
	domNode.sectorNo++;

}


/**
  Compares two level
  @param {String} a The first object
  @param {string} b The second object
  @returns Return the result of comparison ( -1 for greater than, 0 for equal, 1 for smaller than )
  @type Number
 */
wtPieChart.prototype.sortInner = function(a,b)
{
	if (a.level<b.level) return 1;
	if (a.level>b.level) return -1;
	return 0;
}
/**
  Sets the legends of the pie chart
  @param {Number} _index The index
  @param {String} _legend The legend
 */
wtPieChart.prototype.setLegend = function(_index,_legend)
{
	var domNode = $_(this);
	var color = domNode.colorGen.colorArray[_index].toString();
	var x = domNode._rx + 35;
	var y = - domNode._ry + _index * 20;
	var box = new wtRectangle(this,x,y,10,10,color,"none");
	var legend = new wtText(this,x+20,y,_legend,"black");
}
/**
  Renders the pie chart
 */
wtPieChart.prototype.render = function() 
{
	var domNode = $_(this);
	domNode.innerL.sort(this.sortInner);
	domNode.innerR.sort(this.sortInner);

	for (var i = 0; i<domNode.innerL.length; i++) 
	{
		var node = domNode.innerL[i];
		var tmp = new wtPath(this,node.fill,node.stroke);
		tmp.setPath(node.path);
	}

	for (var i = 0; i<domNode.innerR.length; i++) 
	{
		var node = domNode.innerR[i];
		var tmp = new wtPath(this,node.fill,node.stroke);
		tmp.setPath(node.path);
	}

	for (var i = 0; i<domNode.outter.length; i++) 
	{
		var node = domNode.outter[i];
		var tmp = new wtPath(this,node.fill,node.stroke);
		tmp.setPath(node.path);
	}

	for (var i = 0; i<domNode.top_.length; i++) 
	{
		var node = domNode.top_[i];
		if (node.path=="special")
		{
			var domNode = $_(this);
			var tmp = new wtOval(this,domNode._cx,domNode._cy,domNode._rx,domNode._ry,node.fill,node.stroke);
		}
		else
		{
			var tmp = new wtPath(this,node.fill,node.stroke);
			tmp.setPath(node.path);
		}
	}

	for (var i=0; i<this.get("percentList").length; i++)
	{
		var p = this.get("percentList")[i];
		var value = p[2]/3.6;
		var percentText = new wtText(this,p[0],p[1],value.toFixed(2) + "%","purple");
		percentText.setAnchorPosition("middle");
	}

}

/**
  @private
 */
wtPieChart.prototype.sum = function()
{
	var sum = 0;
	for (var i=0; i<this.get("sectorValue").length; i++)
		sum += this.get("sectorValue")[i];
	return sum;
}
/**
  @private
 */
wtPieChart.prototype.mean = function()
{
	return this.sum()/this.get("sectorValue").length;
}
// }}}
// {{{ wtAutoPieChart - A smart pie chart.
/**
  Constructs a smart pie chart object
  @class A smart pie chart that calculates the pie's angles from given data automatically.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _cx The x-coordinate of the center of the chart
  @param {Number} _cy The y-coordinate of the center of the chart
  @param {Number} _rx The length of the semi-major or semi-minor axis
  @param {Number} _ry The length of the semi-major or semi-minor axis
  @param {Boolean} _flag Indcate whether the pie is exploded or not
 */
var wtAutoPieChart = function(_parent,_cx,_cy,_rx,_ry,_flag) 
{

	this.base = wtPieChart;
	this.base(_parent,_cx,_cy,_rx,_ry,_flag);
	$_(this)._list = [];

}
wtAutoPieChart.prototype = new wtPieChart;
/**
  @private
 */
wtAutoPieChart.prototype.addOld = wtPieChart.prototype.add;
/**
  Adds a sector to the pie chart by giving a value
  @param {Number} _value The value of the sector
 */
wtAutoPieChart.prototype.add = function(_value) 
{
	if (_value>0) {
		$_(this)._sum += _value;
		$_(this)._list.push(_value);
	} else {
		throw "wtAutoPieChart::add(): the value of _value should be positive";
	}
}
/**
  @private
 */
wtAutoPieChart.prototype.renderOld = wtPieChart.prototype.render;
/**
  Renders the pie chart
 */
wtAutoPieChart.prototype.render = function() 
{
	var domNode = $_(this);
	var tmp = 0;
	for (var i=0; i<domNode._list.length; i++) {
		var delta = domNode._list[i]/domNode._sum * 360;

		this.addOld(tmp,Math.round(delta));
		tmp += delta;
	}
	this.renderOld();
}
// }}}
// {{{ wtRadarChart - A radar chart.
/**
  Constructs a radar chart object
  @class A radar chart.
  @constructor
  @param {wtWidget} _parent The paren container of the chart
  @param {Number} _x The x-coordinate of the center of the chart
  @param {Number} _y The y-coordinate of the center of the chart
  @param {Number} _size The length of each field
  @param {Number} _max The maximum value of each field
  @param {Number} _field The number of fields
 */
var wtRadarChart = function(_parent,_x,_y,_size,_max,_field)
{
	if (arguments.length<6)
		return;
	if (_size<=0)
		throw "wtRadarChart(): the value of _size should be positive";
	if (_max<=0)
		throw "wtRadarChart(): the value of _max should be positive";
	if (_field<3)
		throw "wtRadarChart(): the value of _field should be greater than or equal to 3";


	this.base = wtVectorGroup;
	this.base(_parent);

	var title = new wtText(this,0,-_size-30,"Radar Chart Title","purple");
	title.setAnchorPosition("middle");
	this.set("titleText", title);
	this.set("size", _size);
	this.set("max", _max);
	this.set("field_",_field);
	this.set("valueList", []);
	this.set("pathList", []);
	this.set("markerList", []);
	this.set("labelList", []);
	this.set("legendList",[]);
	this.set("gridList", []);
	this.set("colorGen", new wtColorGenerator(8,1,1));
	this.get("colorGen").getColor();
	this.get("colorGen").randomize();

	var division = 4;

	for (var j=0; j<division; j++)
	{
		var grid = new wtPath(this,"none","black");
		this.get("gridList").push(grid);
	}

	for (var i=0; i<_field; i++)
	{
		var angle = 360.0*i/_field;

		for (var j=1; j<=division; j++)
		{	
			var tmpx = Math.round(_size* j/division* Math.sin(Math.deg2rad(angle)));
			var tmpy = Math.round(-_size*j/division* Math.cos(Math.deg2rad(angle)));

			if (j==division)
			{
				new wtLine(this,0,0,tmpx,tmpy,"black");
				var label = new wtText(this,tmpx,tmpy,"Legend","purple");
				if (i==0 || i==_field/2)
				{
					label.setAnchorPosition("middle");
				}
				else
				{
					label.setAnchorPosition(i<_field/2?"start":"end");
				}
				this.get("labelList").push(label);
			}

			if (i==0)
			{
				this.get("gridList")[j-1].moveTo("abs",tmpx,tmpy);
			}
			else
			{
				this.get("gridList")[j-1].lineTo("abs",tmpx,tmpy);
			}

		}
	}

	for (var j=0; j<division; j++)
	{
		this.get("gridList")[j].closePath();
	}

	this.setAbsolutePosition(_x,_y);
}

wtRadarChart.prototype = new wtVectorGroup;
/**
  Sets the title of the radar chart
  @param {String} _title The title of the radar chart
 */
wtRadarChart.prototype.setTitle = function(_title)
{
	this.get("titleText").setTex(_title);
}
/**
  Adds a series to the radar chart
  @param {Array} valueList  
 */
wtRadarChart.prototype.add = function(valueList)
{
	var color = this.get("colorGen").getColor().toString();

	this.get("valueList").push(valueList);
	var path = new wtPath(this,color,"black");
	path.setOpacity(0.4);
	this.get("pathList").push(path);
	var x = this.get("size");
	var y = -this.get("size") +  20 * this.get("legendList").length;
	var box = new wtRectangle(this,x,y,10,10,color,"none");
	box.setOpacity(0.4);
	this.get("legendList").push(new wtText(this,x+15,y,"Data"+this.get("legendList").length,"purple"));
	this.update();
}
/**
  Updates the chart when the value of any field is changed
 */
wtRadarChart.prototype.update = function()
{
	for (var i=0; i<this.get("pathList").length; i++)
	{
		var path = this.get("pathList")[i];
		var value = this.get("valueList")[i];
		var interval = 360.0/this.get("field_");
		var max = this.get("max");
		var size = this.get("size");

		for (var j=0; j<value.length; j++)
		{
			var tmpx = value[j]/max * size * Math.sin(Math.deg2rad(interval*j));
			var tmpy = -value[j]/max * size *Math.cos(Math.deg2rad(interval*j));
			if (j==0)
			{
				path.clearPath();
				path.moveTo("abs",tmpx,tmpy);
			}
			else
			{
				path.lineTo("abs",tmpx,tmpy);
			}
		}
		path.closePath();
	}
}
/**
  Sets the label of a field
  @param {Number} _index The index of the field
  @param {String} _label The label of the field
 */
wtRadarChart.prototype.setLabel = function(_index,_label)
{
	if (_index<0 || _index>=this.get("labelList").length)
		throw "wtRadarChart::setLabel(): _index out of range"; 
	this.get("labelList")[_index].setText(_label);
}
/**
  Sets the legend of a data set
  @param {Number} _index The index of the data set
  @param {String} _legend The legend of the data set
 */
wtRadarChart.prototype.setLegend = function(_index,_legend)
{
	if (_index<0 || _index>=this.get("legendList").length)
		throw "wtRadarChart::setLegend(): _index out of range";
	this.get("legendList")[_index].setText(_legend);
}
// }}}
// {{{ wtArrow - An arrow line with optional arrowheads at both ends.
/**
  Constructs an arrow line.
  @class An arrow line with optional anchor points and user-defined arrowheads at both ends.
  @param {wtCanvas|wtVectorGroup} parentWidget The parent container.
  @param {wtColor|String} strokeColor The stroke color.
  @param {Array} pointList (Optional) The path of the arrow as an array of wt2DPoints.
  @constructor
 */
var wtArrow = function(parentWidget, strokeColor, pointList)
{
	if (arguments.length < 2)
		return;

	this.base = wtVectorGroup;
	this.base(parentWidget);

	this.setStrokeColor(strokeColor);
	if (pointList instanceof Array)
		this.setPointList(pointList);
	this.set("pathList", []);
	this.set("lastObjCount", 0);
}
wtArrow.prototype = new wtVectorGroup;
/**
  Sets the shape and color of the source arrowhead.
  @param {String} shape The shape name. Shapes are defined in the wtArrow::arrowShapes dictionary. Additional shapes 
  can be defined by the user by adding to the wtArrow::arrowShapes dictionary.
  @param {wtColor|String} fillColor (Optional) The fill color of the source arrowhead. If not given, the source 
  arrowhead would have a transparent fill.
 */
wtArrow.prototype.setSourceArrow = function(shape, fillColor)
{
	if (shape == "none" || (!shape))
		this.set("sourceArrowShape", null);
	else if (wtArrow.arrowShapes[shape] instanceof Object)
		this.set("sourceArrowShape", wtArrow.arrowShapes[shape]);
	else if (shape instanceof Object)
		this.set("sourceArrowShape", shape);
	else
		throw "wtArrow::setSourceArrow(): The shape argument is not valid";

	this.set("sourceArrowFillColor", new wtColor(fillColor));
	if (this.isReady())
		this.draw();
}
/**
  Sets the shape and color of the terminal arrowhead.
  @param {String} shape The shape name. Shapes are defined in the wtArrow::arrowShapes dictionary. Additional shapes 
  can be defined by the user by adding to the wtArrow::arrowShapes dictionary.
  @param {wtColor|String} fillColor (Optional) The fill color of the terminal arrowhead. If not given, the terminal
  arrowhead would have a transparent fill.
 */
wtArrow.prototype.setTerminalArrow = function(shape, fillColor)
{
	if (shape == "none" || (!shape))
		this.set("terminalArrowShape", null);
	else if (wtArrow.arrowShapes[shape] instanceof Object)
		this.set("terminalArrowShape", wtArrow.arrowShapes[shape]);
	else if (shape instanceof Object)
		this.set("terminalArrowShape", shape);
	else
		throw "wtArrow::setTerminalArrow(): The shape argument is not valid";

	this.set("terminalArrowFillColor", new wtColor(fillColor));
	if (this.isReady())
		this.draw();
}
/**
  Sets the shape of bullet points which are added to the turning points of the arrow path.
  @param {String} shape The shape name. Shapes are defined in the wtArrow::bulletShapes dictionary. Additional shapes
  can be defined by the user by adding to the wtArrow::bulletShapes dictionary.
 */
wtArrow.prototype.setBullet = function(shape)
{
	if (shape == "none" || (!shape))
		this.set("bulletShape", null);
	else if (wtArrow.bulletShapes[shape] instanceof Array)
		this.set("bulletShape", wtArrow.bulletShapes[shape]);
	else if (shape instanceof Array)
		this.set("bulletShape", shape);
	else
		throw "wtArrow::setBullet(): The shape argument is not valid";
	if (this.isReady())
		this.draw();
}
/**
  Sets the path of the arrow line.
  @param {Array} pointsList Array of wt2DPoints which defines the path of the arrow.
 */
wtArrow.prototype.setPointList = function(pointList)
{
	this.set("arrowPointList", pointList);
	if (this.isReady())
		this.draw();
}
/**
  Tells the path of the arrow line in an array of wt2DPoints.
  @returns Path of arrow line.
  @type Array
 */
wtArrow.prototype.getPointList = function()
{
	return this.get("arrowPointList");
}
/**
  Sets the stroke color of the arrow.
  @param {wtColor|String} color The stroke color.
 */
wtArrow.prototype.setStrokeColor = function(color)
{
	color = new wtColor(color);
	this.set("arrowStrokeColor", color);
	if (this.isReady())
		this.draw();
}
/**
  Sets the stroke style of the arrow.
  @param {String} style The style string. Acceptable values are the same as in wtPath::setStrokeStyle().
  @see wtPath#setStrokeStyle 
 */
wtArrow.prototype.setStrokeStyle = function(style)
{
	this.set("arrowStrokeStyle", style);
	if (this.isReady())
		this.draw();
}
/**
  Tells whether the arrow line can be drawn or not.
  @retrurns Whether the arrow line can be drawn or not.
  @type Boolean
 */
wtArrow.prototype.isReady = function()
{
	if (! ((this.get("arrowStrokeColor") instanceof wtColor) && (this.get("arrowPointList") instanceof Array)))
		return false;
	if (this.get("arrowPointList").length < 2)
		return false;
	return true;
}
/**
  Draws the arrow line.
 */
wtArrow.prototype.draw = function()
{
	// check whether the arrow is properly initialized
	// an initialized arrow must have a stroke color and a point list
	if (! ((this.get("arrowStrokeColor") instanceof wtColor) && (this.get("arrowPointList") instanceof Array)))
		throw "wtArrow::draw(): This wtArrow is not properly initialized yet. You must at least set a pointList and a strokeColor.";

	// initiate the environment
	var pointList = this.get("arrowPointList");
	if (pointList.length < 2)
		throw "wtArrow::draw(): The pointList must at least contain 2 coordinates.";
	var numExtraPaths = pointList.length;
	if (this.get("sourceArrowShape") instanceof Object)
		numExtraPaths++;
	if (this.get("terminalArrowShape") instanceof Object)
		numExtraPaths++;
	var pathList = this.get("pathList");
	if (pathList.length < numExtraPaths)
	{
		while(pathList.length < numExtraPaths)
			pathList.push(new wtPath(this, "", ""));
	}
	else if (pathList.length > numExtraPaths)
	{
		var waste = pathList.splice(0, pathList.length - numExtraPaths);
		for(var i=0;i<waste.length;i++)
			waste[i].removeSelf();
	}
	var currentExtraPathIndex = 0;

	// 1. get the source arrow's rotation
	// 2. rotate and translate the shape coordinates (deep copy needed!!)
	// 3. create the wtPath and add it to pathList
	if (this.get("sourceArrowShape") instanceof Object)
	{
		var source_dx = pointList[0].x - pointList[1].x;
		var source_dy = pointList[0].y - pointList[1].y;
		var source_rotation = Math.rad2deg(Math.atan(source_dy/source_dx));
		if (source_dx < 0)
			source_rotation += 180;

		var shape_points = this.get("sourceArrowShape").points;
		var shape_length = this.get("sourceArrowShape").length;
		var source_residue_angle = 270 - source_rotation;
		var source_points = [];
		for(var i=0;i<shape_points.length;i++)
			source_points.push({"x":shape_points[i].x, "y":shape_points[i].y});
		var source_transform = new wt2DTransform();
		source_transform.translate(pointList[0].x + Math.sin(Math.deg2rad(source_residue_angle)) * shape_length, 
				pointList[0].y + Math.cos(Math.deg2rad(source_residue_angle)) * shape_length);
		source_transform.rotate(source_rotation -90);
		source_transform.batchTransform(source_points);

		var sourceArrow = pathList[currentExtraPathIndex];
		currentExtraPathIndex++;
		sourceArrow.setFillColor(this.get("sourceArrowFillColor"));
		sourceArrow.setStrokeColor(this.get("arrowStrokeColor"));
		sourceArrow.drawPointList(source_points);
	}

	// 1. get the terminal arrow's rotation
	// 2. rotate and translate the shape coordinates (deep copy needed!!)
	// 3. create the wtPath and add it to pathList
	if (this.get("terminalArrowShape") instanceof Object)
	{
		var terminal_dx = pointList[pointList.length -1].x - pointList[pointList.length -2].x;
		var terminal_dy = pointList[pointList.length -1].y - pointList[pointList.length -2].y;
		var terminal_rotation = Math.rad2deg(Math.atan(terminal_dy/terminal_dx));
		if (terminal_dx < 0)
			terminal_rotation += 180;

		var shape_points = this.get("terminalArrowShape").points;
		var shape_length = this.get("terminalArrowShape").length;
		var terminal_residue_angle = 270 - terminal_rotation;
		var terminal_points = [];
		for(var i=0;i<shape_points.length;i++)
			terminal_points.push({"x":shape_points[i].x, "y":shape_points[i].y});
		var terminal_transform = new wt2DTransform();
		terminal_transform.translate(pointList[pointList.length -1].x + Math.sin(Math.deg2rad(terminal_residue_angle)) * shape_length, 
				pointList[pointList.length -1].y + Math.cos(Math.deg2rad(terminal_residue_angle)) * shape_length);
		terminal_transform.rotate(terminal_rotation -90);
		terminal_transform.batchTransform(terminal_points);

		var terminalArrow = pathList[currentExtraPathIndex];
		currentExtraPathIndex++;
		terminalArrow.setFillColor(this.get("terminalArrowFillColor"));
		terminalArrow.setStrokeColor(this.get("arrowStrokeColor"));
		terminalArrow.drawPointList(terminal_points);
	}

	// draw the intermediate points
	var wiringPointList = this.getWiringPointList();
	var wiringPath = null, borderPath = null;
	if (this.get("wiringPath"))
	{
		wiringPath = this.get("wiringPath");
		borderPath = this.get("borderPath");
		wiringPath.clearPath();
		borderPath.clearPath();
	}
	else
	{
		wiringPath = new wtPath(this, "", this.get("arrowStrokeColor"));
		borderPath = new wtPath(this, "", this.get("arrowStrokeColor"));
		borderPath.setStrokeOpacity(0.0);
		this.set("wiringPath", wiringPath);
		this.set("borderPath", borderPath);
	}
	wiringPath.moveTo("abs", wiringPointList[0].x, wiringPointList[0].y);
	borderPath.moveTo("abs", wiringPointList[0].x, wiringPointList[0].y);
	borderPath.setStrokeWidth(8);
	for(var i=0;i<wiringPointList.length;i++)
	{
		wiringPath.lineTo("abs", wiringPointList[i].x, wiringPointList[i].y);
		borderPath.lineTo("abs", wiringPointList[i].x, wiringPointList[i].y);
		if (i==0 && this.get("sourceArrowShape") instanceof Object)
			continue;
		if (i==wiringPointList.length -1 && this.get("terminalArrowShape") instanceof Object)
			continue;
		if (!(this.get("bulletShape") instanceof Array))
			continue;

		var bulletShape = this.get("bulletShape");
		var bulletPath = pathList[currentExtraPathIndex];
		bulletPath.setFillColor(this.get("arrowStrokeColor"));
		bulletPath.setStrokeColor(this.get("arrowStrokeColor"));
		currentExtraPathIndex++;
		bulletPath.clearPath();
		bulletPath.moveTo("abs", bulletShape[0].x + wiringPointList[i].x, bulletShape[0].y + wiringPointList[i].y);
		for(var j=0;j<bulletShape.length;j++)
			bulletPath.lineTo("abs", bulletShape[j].x + wiringPointList[i].x, bulletShape[j].y + wiringPointList[i].y);
		bulletPath.lineTo("abs", bulletShape[0].x + wiringPointList[i].x, bulletShape[0].y + wiringPointList[i].y);
	}
	if (this.get("arrowStrokeStyle"))
		wiringPath.setStrokeStyle(this.get("arrowStrokeStyle"));

	for(var i=wiringPointList.length -1;i>=0;i--)
		borderPath.lineTo("abs", wiringPointList[i].x, wiringPointList[i].y);
	borderPath.closePath();
}
/**
  Tells the path of the arrow line excluding the source and terminal arrowheads.
  @returns Path of the arrow line excluding the source and terminal arrowheads.
  @type Array
 */
wtArrow.prototype.getWiringPointList = function()
{
	var pointList = this.get("arrowPointList");
	if (! pointList)
		return [];
	var source_dx = pointList[0].x - pointList[1].x;
	var source_dy = pointList[0].y - pointList[1].y;
	var source_rotation = Math.rad2deg(Math.atan(source_dy/source_dx));
	if (source_dx < 0)
		source_rotation += 180;
	var source_residue_angle = 270 - source_rotation;
	var terminal_dx = pointList[pointList.length -1].x - pointList[pointList.length -2].x;
	var terminal_dy = pointList[pointList.length -1].y - pointList[pointList.length -2].y;
	var terminal_rotation = Math.rad2deg(Math.atan(terminal_dy/terminal_dx));
	if (terminal_dx < 0)
		terminal_rotation += 180;
	var terminal_residue_angle = 270 - terminal_rotation;
	var wiringPointList = [];
	for(var i=0;i<pointList.length;i++)
		wiringPointList.push(new wt2DPoint(pointList[i].x, pointList[i].y));
	if (this.get("sourceArrowShape") instanceof Object)
	{
		var dx = Math.sin(Math.deg2rad(source_residue_angle)) * this.get("sourceArrowShape").length;
		var dy = Math.cos(Math.deg2rad(source_residue_angle)) * this.get("sourceArrowShape").length;
		wiringPointList[0].x += dx;
		wiringPointList[0].y += dy;
	}
	if (this.get("terminalArrowShape") instanceof Object)
	{
		var dx = Math.sin(Math.deg2rad(terminal_residue_angle)) * this.get("terminalArrowShape").length;
		var dy = Math.cos(Math.deg2rad(terminal_residue_angle)) * this.get("terminalArrowShape").length;
		wiringPointList[wiringPointList.length -1].x += dx;
		wiringPointList[wiringPointList.length -1].y += dy;
	}
	return wiringPointList;
}
/**
  Dictionary of arrow shapes. Predefined shapes include the following:
  <ul>
  <li>diamond</li>
  <li>vshape</li>
  <li>simple</li>
  <li>triangle</li>
  </ul>
 */
wtArrow.arrowShapes = 
{
	"diamond": {"points": [{x:0,y:0},{x:-4,y:10},{x:0,y:20},{x:4,y:10}], "length": 20},
	"vshape": {"points": [{x:0,y:0},{x:-4,y:15},{x:0,y:0},{x:4,y:15}], "length": 15},
	"simple": {"points": [{x:0,y:0},{x:-4,y:-15},{x:0,y:0},{x:4,y:-15}], "length": 0},
	"triangle": {"points": [{x:0,y:16},{x:-4,y:0},{x:4,y:0}], "length": 16}
}
/**
  Dictionary of bullet shapes. Predefined shapes include the following:
  <ul>
  <li>square</li>
  <li>triangle</li>
  </ul>
 */
wtArrow.bulletShapes =
{
	"square": [{x:-2,y:-2},{x:2,y:-2},{x:2,y:2},{x:-2,y:2}],
	"triangle": [{x:0,y:-3},{x:-2,y:2},{x:2,y:2}]
}
// }}}
// {{{ wt3dObject - A 3D object.
/**
  Constructs a 3D object.
  @class A 3D object.
  @constructor
  @param {wtCanvas|wtVectorGroup} _parent The parent container
  @param {Number} _cx The x-coordinate of the origin of the 3D object
  @param {Number} _cy The y-coordinate of the origin of the 3D object
  @param {Number} _cz The z-coordinate of the origin of the 3D object
  @param {Number} _sx The scale in x-axis
  @param {Number} _sy The scale in y-axis
  @param {Number} _sz The scale in z-axis
 */
var wt3dObject = function(_parent,_cx,_cy,_cz,_sx,_sy,_sz)
{
	this.base = wtVectorGroup;
	this.base(_parent);

	var domNode = $_(this);
	domNode.pts = [];
	domNode.pt = [];
	domNode.path_ = [];
	domNode.animationId = null;
	domNode.signX = 1;
	domNode.signY = 1;
	domNode.signZ = 1;
	domNode.cX = _cx;
	domNode.cY = _cy;
	domNode.cZ = _cz;
	domNode.scaleX = _sx;
	domNode.scaleY = _sy;
	domNode.scaleZ = _sz;
	domNode.colorGen = new wtColorGenerator(8,3,3);
	domNode.colorGen.randomize();

	this.updatePath();
}

wt3dObject.prototype = new wtVectorGroup;

/**
  Sets the opacity of the 3D object
  @param {Number} _opac The value of opacity (0 to 1.0)
 */
wt3dObject.prototype.setOpacity = function(_opac)
{
	var domNode = $_(this);
	for (var i=0; i<domNode.path_.length; i++)
	{
		domNode.path_[i].setOpacity(_opac);
	}
}

/**
  Sets the fill color of the 3D object
  @param {String|wtColor} _fill The fill color of the 3D object
 */
wt3dObject.prototype.setFill = function(_fill)
{
	var domNode = $_(this);
	for (var i=0; i<domNode.path_.length; i++)
	{
		domNode.path_[i].setFill(_fill);
	}
}

/**
  Sets the stroke color of the 3D object
  @param {String|wtColor} _stroke The stroke color of the 3D object
 */
wt3dObject.prototype.setStrokeColor = function(_stroke)
{
	var domNode = $_(this);
	for (var i=0; i<domNode.path_.length; i++)
	{
		domNode.path_[i].setStrokeColor(_stroke);
	}
}

/**
  Adds a point to the vector space of the 3D object
  @param {Number} _x The x-coordinate of the point
  @param {Number} _y The y-coordinate of the point
  @param {Number} _z The z-coordinate of the point
 */
wt3dObject.prototype.addPoint = function(_x,_y,_z)
{
	var domNode = $_(this);
	var xtmp = (_x-domNode.cX)*domNode.scaleX;
	var ytmp = (_y-domNode.cY)*domNode.scaleY;
	var ztmp = (_z-domNode.cZ)*domNode.scaleZ;

	var tmp = {x:xtmp,y:ytmp,z:ztmp};
	domNode.pts.push(tmp);
}

/**
  Defines a surface to the 3D object
  @param {List} ptList The list of points' index
 */
wt3dObject.prototype.addSurface = function(ptList)
{
	var domNode = $_(this);
	var tmp = [];
	for (var i=0; i<ptList.length; i++)
	{
		tmp.push(ptList[i]);
	}

	domNode.pt.push(tmp);
	var color = domNode.colorGen.getColor().toString();
	var p = new wtPath(this,color,"none");
	domNode.path_.push(p);
	this.updatePath();
}

/**
  @private
 */
wt3dObject.prototype.animate = function()
{
	if (this.get("animationId"))
		return;

	var obj  = this;
	var cX = 0;
	var cY = 0;
	var cZ = 0;
	var tmp = function()
	{
		if (! $_(obj))
			return;
		cX++; cY++; cZ++;
		if (cX>100) { $_(obj).signX *= -1; cX = 0; }
		if (cY>200) { $_(obj).signY *= -1; cY = 0; }
		if (cZ>400) { $_(obj).signZ *= -1; cZ = 0; }
		obj.rotate(0,2*$_(obj).signX);
		obj.rotate(1,2*$_(obj).signY);
		obj.rotate(2,2*$_(obj).signZ);
	}
	this.set("animationId", setInterval(tmp, 25));
}

/**
  @private
 */
wt3dObject.prototype.stop = function()
{
	clearInterval(this.get("animationId"));
	this.set("animationId", null);
}

/**
  Updates the 3D object after it is transformed
 */
wt3dObject.prototype.updatePath = function()
{
	var domNode = $_(this);
	for (var i = 0; i<domNode.path_.length; i++)
	{
		var ccw = this.isCCW(domNode.pts[domNode.pt[i][0]],domNode.pts[domNode.pt[i][1]],domNode.pts[domNode.pt[i][2]]);
		if (ccw)
		{
			domNode.path_[i].setOpacity(1.0);
			var tmp = "";
			tmp += wtPath.moveTo_("abs",Math.round(domNode.pts[domNode.pt[i][0]].x),Math.round(domNode.pts[domNode.pt[i][0]].y));
			for (var j=1; j<domNode.pt[i].length; j++)
			{
				tmp += wtPath.lineTo_("abs",Math.round(domNode.pts[domNode.pt[i][j]].x),Math.round(domNode.pts[domNode.pt[i][j]].y));
			}
			tmp += wtPath.closePath_();

			domNode.path_[i].setPath(tmp);
		}
		else
		{
			domNode.path_[i].setOpacity(0.1);
			domNode.path_[i].clearPath();
		}
		//domNode.path_[i].setOpacity(0.5);
	}
}

/**
  Check if the points given form a triangle  in counter-clockwise direction
  @param {Point} _pt0 The first point
  @param {Point} _pt1 The second point
  @param {Point} _pt2 The third point
 */
wt3dObject.prototype.isCCW = function(_pt0,_pt1,_pt2)
{
	var tmp = (_pt1.x-_pt0.x)*(_pt2.y-_pt0.y)-(_pt2.x-_pt0.x)*(_pt1.y-_pt0.y);
	if (tmp>0) return true;
	return false;
	if (tmp==0) return 0;
	if (tmp<0) return -1;
}

/**
  Multiplies a point with a matrix
  @param {Matrix} _mat The given matrix
  @param {Point} _pt The given point
  @returns Returns the result of multiplication
  @type Point
 */
wt3dObject.prototype.multi = function(_mat,_pt)
{
	var tmp = {x:0,y:0,z:0};
	tmp.x = _pt.x * _mat[0][0] + _pt.y * _mat[0][1] + _pt.z * _mat[0][2] + _mat[0][3];
	tmp.y = _pt.x * _mat[1][0] + _pt.y * _mat[1][1] + _pt.z * _mat[1][2] + _mat[1][3];
	tmp.z = _pt.x * _mat[2][0] + _pt.y * _mat[2][1] + _pt.z * _mat[2][2] + _mat[2][3];
	return tmp;
}

/**
  Rotates the 3D object with respect to its origin
  @param {String} _axis The index of the axis ( 0 for x-axis, 1 for y-axis, 2 for z-axis )
  @param {Number} _deg The angle to be rotatedn (measured in degree)
 */
wt3dObject.prototype.rotate = function(_axis,_deg)
{
	var domNode = $_(this);
	var rad = Math.deg2rad(_deg);
	var cosR = Math.cos(rad);
	var sinR = Math.sin(rad);

	var matX = [[1,0,0,0],[0,cosR,-sinR,0],[0,sinR,cosR,0],[0,0,0,1]];
	var matY = [[cosR,0,sinR,0],[0,1,0,0],[-sinR,0,cosR,0],[0,0,0,1]];
	var matZ = [[cosR,-sinR,0,0],[sinR,cosR,0,0],[0,0,1,0],[0,0,0,1]];
	var mat = [matX,matY,matZ];

	for (var k=0; k<domNode.pts.length; k++) 
	{
		domNode.pts[k] = this.multi(mat[_axis],domNode.pts[k]);
	}
	this.updatePath();
}
// }}}
// {{{ wtGraphVertex - A vertex in a G=(V,E) graph.
/**
  Constructs a G=(V,E) graph vertex from a wtWidget. <br/>
  @class A G=(V,E) graph vertex. <br/><br/>
  Available signals:
  <ul>
  <li>Move</li>
  <li>DragStart</li>
  <li>DragEnd</li>
  </ul>
  @param {wtCanvas|wtGraph} parentContainer The parent container of this vertex.
  @param {wtWidget} peerWidget The widget to be associated with this graph vertex.
  @constructor
 */
var wtGraphVertex = function(parentContainer, peerWidget)
{
	if (arguments.length < 2)
		return;

	this.startProxy(this);
	if (! (((parentContainer instanceof wtCanvas) || (parentContainer instanceof wtGraph)) && (peerWidget instanceof wtWidget) ))
		throw "wtGraphVertex: parentContainer must be a wtCanvas or a wtGraph, and peerWidget must be a wtWidget";

	if (parentContainer instanceof wtCanvas )
	{
		if (! (peerWidget.get("parentNode").wtObj == parentContainer))
			throw "wtGraphVertex: peerWidget must be a direct child of parentContainer";
	}
	else
	{
		if (! (peerWidget.get("parentNode").wtObj == parentContainer.getCanvas()))
			throw "wtGraphVertex: peerWidget must be a direct child of parentContainer's canvas";
		this.set("parentGraph", parentContainer);
		parentContainer.addVertex(this);
	}

	if ( peerWidget.get("offsetWidth") == 0 )
		throw "wtGraphVertex: peerWidget must be visible when a wtGraphVertex is being constructed";

	this.set("wtDependency", peerWidget);
	this.set("anchorPoints", []);
	this.set("outEdges", []);
	this.set("inEdges", []);
	this.set("peerWidget", peerWidget);
	this.set("prevX", peerWidget.get("offsetLeft"));
	this.set("prevY", peerWidget.get("offsetTop"));
	this.generateAnchorPoints();

	var moveSlot = function(myself, evt, source)
	{
		var inEdges = myself.getInEdges();
		var outEdges = myself.getOutEdges();
		var dx = myself.getPeerWidget().get("offsetLeft") - myself.get("prevX");
		var dy = myself.getPeerWidget().get("offsetTop")- myself.get("prevY");
		myself.set("prevX", myself.getPeerWidget().get("offsetLeft"));
		myself.set("prevY", myself.getPeerWidget().get("offsetTop"));

		for(var i=0;i<outEdges.length;i++)
		{
			var anchorPoints = outEdges[i].get("anchorPoints");
			if (myself == outEdges[i].getTerminal())
			{
				for(var j=0;j<anchorPoints.length;j++)
					anchorPoints[j] = anchorPoints[j].add(new wt2DPoint(dx, dy));
			}
			wtGraphVertex.addEdgeToUpdate(outEdges[i]);
		}
		for(var i=0;i<inEdges.length;i++)
			wtGraphVertex.addEdgeToUpdate(inEdges[i]);
		myself.emit("Move", evt);
	}
	var dragStartSlot = function(myself, evt, source)
	{
		myself.emit("DragStart", evt);
	}
	var dragEndSlot = function(myself, evt, source)
	{
		myself.emit("DragEnd", evt);
	}
	this.setSlot("MoveSlot", moveSlot);
	this.setSlot("DragStartSlot", dragStartSlot);
	this.setSlot("DragEndSlot", dragEndSlot);
	this.setSignal("Move");
	this.setSignal("DragStart");
	this.setSignal("DragEnd");
	this.get("peerWidget").connect("vertex_moved", "Move", this, "MoveSlot");
	this.get("peerWidget").connect("vertex_drag_ended", "DragEnd", this, "DragEndSlot");
	this.get("peerWidget").connect("vertex_drag_started", "DragStart", this, "DragStartSlot");
}
wtGraphVertex.prototype = new wtObject;
/**
  Tells the incoming edges to this vertex in an array.
  @returns Incoming edges to this vertex.
  @type Array
 */
wtGraphVertex.prototype.getInEdges = function()
{
	var retval = [];
	for(var i=0;i<this.get("inEdges").length;i++)
		retval.push(this.get("inEdges")[i]);
	return retval;
}
/**
  Tells the outgoing edges from this vertex in an array.
  @returns Outgoing edges from this vertex.
  @type Array
 */
wtGraphVertex.prototype.getOutEdges = function()
{
	var retval = [];
	for(var i=0;i<this.get("outEdges").length;i++)
		retval.push(this.get("outEdges")[i]);
	return retval;
}
/**
  Tells the vertices connected to this vertex via incoming edges.
  @returns Vertices array.
  @type Array
 */
wtGraphVertex.prototype.getInVertices = function()
{
	var retval = this.getInEdges();
	for(var i=0;i<retval.length;i++)
		retval[i] = retval[i].get("sourceVertex");
	return retval;
}
/**
  Tells the vertices connected to this vertex via outgoing edges.
  @return Vertices array.
  @type Array
 */
wtGraphVertex.prototype.getOutVertices = function()
{
	var retval = this.getOutEdges();
	for(var i=0;i<retval.length;i++)
		retval[i] = retval[i].get("termVertex");
	return retval;
}
/**
  Tells the incoming edges from a specific vertex.
  @returns Incoming edge array.
  @type Array
 */
wtGraphVertex.prototype.getEdgesFromVertex = function(vertex)
{
	var edges = this.getInEdges();
	var retval = [];
	for(var i=0;i<edges.length;i++)
		if (edges[i].get("sourceVertex") == vertex)
			retval.push(edges[i]);
	return retval;
}
/**
  Tells the outgoing edges to a specific vertex.
  @returns Outgoing edge array.
  @type Array
 */
wtGraphVertex.prototype.getEdgesToVertex = function(vertex)
{
	var edges = this.getOutEdges();
	var retval = [];
	for(var i=0;i<edges.length;i++)
		if (edges[i].get("termVertex") == vertex)
			retval.push(edges[i]);
	return retval;
}
/**
  Generates the anchor points around the peer widget for connecting to edges. <br/>
 */
wtGraphVertex.prototype.generateAnchorPoints = function()
{
	this.clearAnchorPoints();
	var peerWidth = parseInt(this.get("peerWidget").get("offsetWidth"));
	var peerHeight = parseInt(this.get("peerWidget").get("offsetHeight"));
	var corners = [	new wt2DPoint(0, 0),
		new wt2DPoint(peerWidth, 0),
		new wt2DPoint(peerWidth, peerHeight),
		new wt2DPoint(0, peerHeight)];
	for(var i=0;i<4;i++)
	{
		var pt1 = corners[i];
		var pt2 = corners[(i+1)%4];
		var steps = 50;
		if (isMSIE())
			steps = 4;
		for(j=1;j<steps + 1;j++)
		{
			var offset = (j/steps) * pt1.getDistance(pt2);
			var inclination = pt1.getInclineAngle(pt2);
			if (inclination >= 180)
				offset *= -1;
			var anchorPoint = new wt2DPoint(pt1.x, pt1.y);
			if (inclination == 0 || inclination == 180)
				anchorPoint.x += offset;
			else
				anchorPoint.y += offset;
			this.addAnchorPoint(anchorPoint);
		}
	}
	var inEdges = this.getInEdges();
	var outEdges = this.getOutEdges();
	for(var i=0;i<outEdges.length;i++)
		wtGraphVertex.addEdgeToUpdate(outEdges[i]);
	for(var i=0;i<inEdges.length;i++)
		wtGraphVertex.addEdgeToUpdate(inEdges[i]);

}
/**
  Adds an anchor point around this vertex. The coordinate for anchor points are relative to the top-left corner
  of the peer widget.
  @param {wt2DPoint} point New anchor point for connecting to edges.
 */
wtGraphVertex.prototype.addAnchorPoint = function(point)
{
	if (! (point instanceof wt2DPoint))
		throw "wtGraphVertex::addAnchorPoint(): point must be an instance of wt2DPoint";
	this.get("anchorPoints").push(point);
}
/**
  Tells all the current anchor points associated with this vertex.
  @returns Anchor point array.
  @type Array
 */
wtGraphVertex.prototype.getAnchorPoints = function()
{
	return this.get("anchorPoints");
}
/**
  Removes one of the anchor points from the anchor point array.
  @param {Integer} idx Array index of the anchor point to be removed.
 */
wtGraphVertex.prototype.removeAnchorPoint = function(idx)
{
	return this.get("anchorPoints").splice(idx, 1);
}
/**
  Clears all the anchor points of this vertex.
 */
wtGraphVertex.prototype.clearAnchorPoints = function()
{
	this.set("anchorPoints", []);
}
/**
  Tells the center point of the peer widget relative to its parent container. <br/>
  @returns Center point coordinate of peer widget.
  @type wt2DPoint
 */
wtGraphVertex.prototype.getCenterPoint = function()
{
	var peerWidth = parseInt(this.get("peerWidget").get("offsetWidth"));
	var peerHeight = parseInt(this.get("peerWidget").get("offsetHeight"));
	return this.getAbsolutePosition().add(new wt2DPoint(peerWidth/2, peerHeight/2));
}
/**
  Tells the top-left corner position of the peer widget relative to its parent container.
  @returns Top-left corner position of peer widget.
  @type wt2DPoint
 */
wtGraphVertex.prototype.getAbsolutePosition = function()
{
	return new wt2DPoint(this.get("peerWidget").getAbsolutePosition());
}
/**
  Tells the closest anchor point to a specified point in the parent container's coordinate system.
  @returns Closest anchor point in parent container's coordinate system.
  @type wt2DPoint
 */
wtGraphVertex.prototype.getClosestAnchorPoint = function(point)
{
	if (! (point instanceof wt2DPoint))
		throw "wtGraphVertex::getClosestAnchorPoint(): point must be an instance of wt2DPoint";
	var anchorPoints = this.getAnchorPoints();
	if (anchorPoints.length < 1)
		anchorPoints = [this.getCenterPoint()];
	var absAnchorPoints = [];
	for(var i=0;i<anchorPoints.length;i++)
		absAnchorPoints.push(anchorPoints[i].add(this.getAbsolutePosition()));
	var minDistance = point.getDistance(absAnchorPoints[0]);
	var closestIndex = 0;
	for(var i=1;i<absAnchorPoints.length;i++)
	{
		if (point.getDistance(absAnchorPoints[i]) < minDistance)
		{
			closestIndex = i;
			minDistance = point.getDistance(absAnchorPoints[i]);
		}
	}
	return absAnchorPoints[closestIndex];
}
/**
  Tells the peer widget associated with this vertex.
  @returns Peer widget.
  @type wtWidget
 */
wtGraphVertex.prototype.getPeerWidget = function()
{
	return this.get("peerWidget");
}
/**
  Removes this vertex, the associated peer widget, and all edges connected to and from this vertex.
 */
wtGraphVertex.prototype.removeSelf = function()
{
	var inEdges = this.getInEdges();
	var outEdges = this.getOutEdges();
	for(var i=0;i<inEdges.length;i++)
		if ($_(inEdges[i]))
			inEdges[i].removeSelf();
	for(var i=0;i<outEdges.length;i++)
		if ($_(outEdges[i]))
			outEdges[i].removeSelf();

	if (this.get("parentGraph"))
		this.get("parentGraph").removeVertex(this);
	this.get("peerWidget").removeSelf();
	this.endProxy();
}
/**
  @private
 */
wtGraphVertex.edgesToUpdate = {};
/**
  @private
 */
wtGraphVertex.addEdgeToUpdate = function(edge)
{
	wtGraphVertex.edgesToUpdate[edge.toString()] = edge;
	if (wtGraphVertex.updateEdgeThreadID == null)
		wtGraphVertex.updateEdgeThreadID = setTimeout(wtGraphVertex.updateEdgeThread, 20);
}
/**
  @private
 */
wtGraphVertex.delEdgeToUpdate = function(edge)
{
	delete wtGraphVertex.edgesToUpdate[edge.toString()];
}
/**
  @private
 */
wtGraphVertex.updateEdgeThread = function()
{
	var edge = null;
	for(var id in wtGraphVertex.edgesToUpdate)
	{
		if ($_(wtGraphVertex.edgesToUpdate[id]))
			wtGraphVertex.edgesToUpdate[id].refresh();
	}
	wtGraphVertex.edgesToUpdate = {};
	wtGraphVertex.updateEdgeThreadID = null;
}
/**
  @private
 */
wtGraphVertex.updateEdgeThreadID = null;
// }}}
// {{{ wtGraphEdge - An edge in a G=(V,E) graph.
/**
  Constructs a graph edge.
  @class A G=(V,E) graph edge. <br/><br/>
  Available signals:
  <ul>
  <li>Refreshed</li>
  <li>Removed</li>
  <li>SourceChanged</li>
  <li>TerminalChanged</li>
  </ul>
  @param {wtCanvas} parentCanvas The container canvas.
  @param {wtColor|String} strokeColor The edge's stroke color.
  @constructor
 */
var wtGraphEdge = function(parentCanvas, strokeColor)
{
	if (arguments.length < 2)
		return;

	this.base = wtArrow;
	this.base(parentCanvas, strokeColor);
	this.set("sourceVertex", null);
	this.set("termVertex", null);
	this.set("anchorPoints", []);
	this.setBullet("square");
	this.setDraggable(true);
	this.setSlot("RemoveAnchor", this.removeAnchorSlot);
	this.connect("remove_anchor", "DoubleClick", this, "RemoveAnchor");

	this.setSignal("Refreshed");
	this.setSignal("Removed");
	this.setSignal("SourceChanged");
	this.setSignal("TerminalChanged");
}
wtGraphEdge.prototype = new wtArrow;
/**
  @private
 */
wtGraphEdge.prototype._wtArrow_removeSelf = wtArrow.prototype.removeSelf;
/**
  Removes the graph edge.
 */
wtGraphEdge.prototype.removeSelf = function()
{
	if ($_(this.get("sourceVertex")))
	{
		var outEdges = this.get("sourceVertex").get("outEdges");
		for(var i=0;i<outEdges.length;i++)
			if (outEdges[i] == this)
				outEdges.splice(i, 1);
	}
	if ($_(this.get("termVertex")))
	{
		var inEdges = this.get("termVertex").get("inEdges");
		for(var i=0;i<inEdges.length;i++)
			if (inEdges[i] == this)
				inEdges.splice(i, 1);
	}
	this.emit("Removed", {});
	this._wtArrow_removeSelf();
}
/**
  Redraws the graph edge.
 */
wtGraphEdge.prototype.refresh = function()
{
	if (!((this.get("sourceVertex") instanceof wtGraphVertex) && 
				(this.get("termVertex") instanceof wtGraphVertex)))
		throw "wtGraphEdge::refresh(): Cannot draw graph edge without linked vertices! Use the link() operation to link up two vertices first.";

	var sourceAnchor = null, termAnchor = null;
	if (this.getAnchorPoints().length < 1)
	{
		var sourceCenter = this.get("sourceVertex").getCenterPoint();
		var termCenter = this.get("termVertex").getCenterPoint();
		var center = new wt2DPoint((sourceCenter.x + termCenter.x)/2, (sourceCenter.y + termCenter.y)/2);
		sourceAnchor = this.get("sourceVertex").getClosestAnchorPoint(center);
		termAnchor = this.get("termVertex").getClosestAnchorPoint(center);
	}
	else
	{
		sourceAnchor = this.get("sourceVertex").getClosestAnchorPoint(this.getAnchorPoints()[0]);
		termAnchor = this.get("termVertex").getClosestAnchorPoint(this.getAnchorPoints()[this.getAnchorPoints().length -1]);
	}

	var pointList = [sourceAnchor];
	for(var i=0;i<this.getAnchorPoints().length;i++)
		pointList.push(this.getAnchorPoints()[i]);
	pointList.push(termAnchor);

	this.setPointList(pointList);
	this.emit("Refreshed", {});
}
/**
  Links two vertices with this graph edge.
  @param {wtGraphVertex} source The source vertex.
  @param {wtGraphVertex} destination The terminal vertex.
 */
wtGraphEdge.prototype.link = function(source, destination)
{
	this.set("sourceVertex", source);
	this.set("termVertex", destination);
	source.get("outEdges").push(this);
	destination.get("inEdges").push(this);
	this.scheduleRefresh();
	this.emit("SourceChanged", {});
	this.emit("TerminalChanged", {});
}
/**
  Sets a list of points to anchor the edge between the source and the terminal vertices.
  @param {Array} pointList Array of wt2DPoints to anchor the edge to.
 */
wtGraphEdge.prototype.setAnchorPoints = function(pointList)
{
	this.set("anchorPoints", pointList);
	this.scheduleRefresh();
}
/**
  Tells the list of anchor points between the source and the terminal vertices for this edge.
  @returns Array of anchor points.
  @type Array
 */
wtGraphEdge.prototype.getAnchorPoints = function()
{
	return this.get("anchorPoints");
}
/**
  @private
 */
wtGraphEdge.prototype._wtVectorGroup_setDraggingSlot = wtVectorGroup.prototype.setDraggingSlot;
/**
  @private
 */
wtGraphEdge.prototype.setDraggingSlot = function(myself, evt, source)
{
	var wiringPointList = myself.getWiringPointList();
	var screenPos = myself.getScreenPosition();
	var mousePoint = new wt2DPoint(mouse.x, mouse.y);
	var inRange = false, closestDistance = mousePoint.getDistanceFromBoundedLine(wiringPointList[0], wiringPointList[1]),
		closestSegmentIndex = 0;
	for(var i=0;i<wiringPointList.length;i++)
		wiringPointList[i] = wiringPointList[i].add(screenPos);
	for(var i=0;i<wiringPointList.length;i++)
	{
		var ptA = wiringPointList[i];
		var ptB = wiringPointList[(i+1)%wiringPointList.length];
		if (mousePoint.getDistanceFromBoundedLine(ptA, ptB) <= 8)
			inRange = true;
		if (mousePoint.getDistanceFromBoundedLine(ptA, ptB) < closestDistance)
		{
			closestDistance = mousePoint.getDistanceFromBoundedLine(ptA, ptB);
			closestSegmentIndex = i % wiringPointList.length;
		}
	}

	if (inRange)
	{
		myself.get("borderPath").setStrokeOpacity(0.1);
		myself.set("closestSegmentIndex", closestSegmentIndex);
		myself._wtVectorGroup_setDraggingSlot(myself, evt, source);
	}
}
/**
  @private
 */
wtGraphEdge.prototype._wtVectorGroup_unsetDraggingSlot = wtVectorGroup.prototype.unsetDraggingSlot;
/**
  @private
 */
wtGraphEdge.prototype.unsetDraggingSlot = function(myself, evt, source)
{
	myself._wtVectorGroup_unsetDraggingSlot(myself, evt, source);
	myself.get("borderPath").setStrokeOpacity(0.0);
	myself.set("dragPoint", null);
	myself.set("closestSegmentIndex", null);
}
/**
  @private
 */
wtGraphEdge.prototype.dragSlot = function(myself, evt, source)
{
	if (myself.get("isDragging"))
	{
		var relX = mouse.x - myself.getScreenPosition().x;
		var relY = mouse.y - myself.getScreenPosition().y;
		var relPoint = new wt2DPoint(relX, relY);
		var point = myself.get("dragPoint");
		for(var i=0;point == null && i<myself.getAnchorPoints().length;i++)
		{
			if (myself.getAnchorPoints()[i].getDistance(relPoint) < 20)
			{
				point = myself.getAnchorPoints()[i];
				myself.set("dragPoint", point);
			}
		}
		if (point)
		{
			point.x = relPoint.x;
			point.y = relPoint.y;
			myself.scheduleRefresh();
		}
		else
		{
			var anchors = myself.getAnchorPoints();
			var updatedAnchors = [];
			var insertIndex = myself.get("closestSegmentIndex") -1;
			if (myself.get("arrowPointList").length <= 2)
				insertIndex = -1;
			for(var i=0;i<=Math.min(insertIndex, anchors.length -1);i++)
				updatedAnchors.push(anchors[i]);
			updatedAnchors.push(relPoint);
			for(var i=insertIndex+1;i<anchors.length;i++)
				updatedAnchors.push(anchors[i]);
			myself.setAnchorPoints(updatedAnchors);
			myself.set("dragPoint", relPoint);
		}
	}
}
/**
  @private
 */
wtGraphEdge.prototype.removeAnchorSlot = function(myself, evt, source)
{
	var relPoint = new wt2DPoint(mouse.x - myself.getScreenPosition().x, mouse.y - myself.getScreenPosition().y);
	var anchorPoints = myself.getAnchorPoints();
	if (anchorPoints.length < 1)
		return;
	var closestPoint = 0, closestDistance = relPoint.getDistance(anchorPoints[0]);
	for(var i=1;i<anchorPoints.length;i++)
	{
		if (relPoint.getDistance(anchorPoints[i]) < closestDistance)
		{
			closestPoint = i;
			closestDistance = relPoint.getDistance(anchorPoints[i]);
		}
	}
	if (relPoint.getDistance(anchorPoints[closestPoint]) < 10)
	{
		anchorPoints.splice(closestPoint, 1);
		myself.scheduleRefresh();
	}
}
/**
  @private
  This is supposed to be used only by wtGraph.
 */
wtGraphEdge.prototype.attachSourceToMouse = function()
{
	this.restoreMouseAttachment();
	var tempWidget = new wtView(this.get("parentCanvas"), 1, 1);
	var tempVertex = new wtGraphVertex(this.get("parentCanvas"), tempWidget);
	tempWidget.set("wtDependency", this);
	this.set("prevSource", this.get("sourceVertex"));
	this.set("mouseSource", tempVertex);
	tempVertex.get("outEdges").push(this);
	tempWidget.setAbsolutePosition(mouse.x - this.get("parentCanvas").getScreenPosition().x, mouse.y - this.get("parentCanvas").getScreenPosition().y);
	tempWidget.setDraggable(true);
	tempWidget.setDraggingSlot(tempWidget);
	this.setSource(tempVertex);
	return tempVertex;
}
/**
  @private
  This is supposed to be sued only by wtGraph.
 */
wtGraphEdge.prototype.attachTerminalToMouse = function()
{
	this.restoreMouseAttachment();
	var tempWidget= new wtView(this.get("parentCanvas"), 1, 1);
	var tempVertex = new wtGraphVertex(this.get("parentCanvas"), tempWidget);
	tempWidget.set("wtDependency", this);
	this.set("prevTerminal", this.get("termVertex"));
	this.set("mouseTerminal", tempVertex);
	tempVertex.get("inEdges").push(this);
	tempWidget.setAbsolutePosition(mouse.x - this.get("parentCanvas").getScreenPosition().x, mouse.y - this.get("parentCanvas").getScreenPosition().y);
	tempWidget.setDraggable(true);
	tempWidget.setDraggingSlot(tempWidget);
	this.setTerminal(tempVertex);
	return tempVertex;
}
/**
  @private
  This is supposed to be sued only by wtGraph.
 */
wtGraphEdge.prototype.restoreMouseAttachment = function()
{
	if (this.get("mouseSource"))
		this.setSource(this.get("prevSource"));
	else if (this.get("mouseTerminal"))
		this.setTerminal(this.get("prevTerminal"));
}
/**
  Sets the source vertex of this edge.
  @param {wtGraphVertex} vertex The new source vertex.
 */
wtGraphEdge.prototype.setSource = function(vertex)
{
	if (this.get("sourceVertex"))
	{
		var outEdges = this.get("sourceVertex").get("outEdges");
		for(var i=0;i<outEdges.length;i++)
		{
			if (outEdges[i] == this)
			{
				outEdges.splice(i, 1);
				break;
			}
		}
	}

	if (this.get("mouseSource") != vertex)
	{
		if (this.get("mouseSource"))
		{
			this.get("mouseSource").set("outEdges", []);
			this.get("mouseSource").removeSelf();
		}
		this.set("prevSource", undefined);
		this.set("mouseSource", undefined);
		this.set("sourceVertex", undefined);
	}

	this.set("sourceVertex", vertex);
	var outEdges = vertex.get("outEdges");
	var i = 0;
	for(;i<outEdges.length;i++)
		if (outEdges[i] == this) break;
	if (i == length)
		outEdges.push(this);
	this.scheduleRefresh();
	this.emit("SourceChanged", {});
}
/**
  Sets the terminal vertex of this edge.
  @param {wtGraphVertex} vertex The new terminal vertex.
 */
wtGraphEdge.prototype.setTerminal = function(vertex)
{
	if (this.get("termVertex"))
	{
		var inEdges = this.get("termVertex").get("inEdges");
		for(var i=0;i<inEdges.length;i++)
		{
			if (inEdges[i] == this)
			{
				inEdges.splice(i, 1);
				break;
			}
		}
	}

	if (this.get("mouseTerminal") != vertex)
	{
		if (this.get("mouseTerminal"))
		{
			this.get("mouseTerminal").set("inEdges", []);
			this.get("mouseTerminal").removeSelf();
		}
		this.set("prevTerminal", undefined);
		this.set("mouseTerminal", undefined);
		this.set("termVertex", undefined);
	}

	this.set("termVertex", vertex);
	var inEdges = vertex.get("inEdges");
	var i = 0;
	for(;i<inEdges.lenght;i++)
		if (inEdges[i] == this) break;
	if (i == length)
		inEdges.push(this);
	this.scheduleRefresh(); 
	this.emit("TerminalChanged", {});
}
/**
  Tells the source vertex connected to this edge.
  @returns The source vertex.
  @type wtGraphVertex
 */
wtGraphEdge.prototype.getSource = function()
{
	return this.get("sourceVertex");
}
/**
  Tells the terminal vertex conencted to this edge.
  @returns The terminal vertex.
  @type wtGraphVertex
 */
wtGraphEdge.prototype.getTerminal = function()
{
	return this.get("termVertex");
}
/**
  Schedules a refresh of the graph edge after the current Javascript execution pass. This is the preferred
  method of refreshing an edge to calling refresh() directly because this method avoids redundant refreshes.
 */
wtGraphEdge.prototype.scheduleRefresh = function()
{
	if (this.get("refreshSchedule"))
		return;

	var me = this;
	var refreshFunc = function()
	{
		if (! $_(me))
			return;
		me.refresh(); 
		me.set("refreshSchedule", undefined);
	}
	this.set("refreshSchedule", setTimeout(refreshFunc, 30));
}
/**
  Cancels a scheduled refresh of the graph edge. If no schedule existed, this operation returns silently.
 */
wtGraphEdge.prototype.cancelRefresh = function()
{
	if (this.get("refreshSchedule"))
	{
		clearTimeout(this.get("refreshSchedule"));
		this.set("refreshSchedule", undefined);
	}
}
// }}}
// {{{ wtGraph - A collection of vertices and edges.
/**
  Constructs a G=(V,E) graph.
  @class A container for G=(V,E) vertices and their corresponding edges.
  @constructor
  @param {wtCanvas} parentCanvas The container canvas.
 */
var wtGraph = function(parentCanvas)
{
	if (arguments.length < 1)
		return;

	this.startProxy(this);
	this.set("wtDependency", parentCanvas);
	this.set("canvas", parentCanvas);
	this.set("vertices", {});
	this.setSlot("KillDrone", this.killDroneSlot);
	this.setSlot("MoveDrone", this.moveDroneSlot);
}
wtGraph.prototype = new wtObject;
/**
  Tells the container canvas of this graph.
  @returns The container canvas.
  @type wtCanvas
 */
wtGraph.prototype.getCanvas = function()
{
	return this.get("canvas");
}
/**
  Tells the vertices contained in this graph.
  @returns All graph vertices in an id:vertex dictionary.
  @type Object
 */
wtGraph.prototype.getVertices = function()
{
	return this.get("vertices");
}
/**
  Adds a vertex to this graph.
  @param {wtGraphVertex} vertex The vertex to be added.
 */
wtGraph.prototype.addVertex = function(vertex)
{
	if (vertex instanceof wtGraphVertex)
	{
		this.get("vertices")[vertex.toString()] = vertex;
		return vertex;
	}
	else if (isString(vertex))
	{
		var widget= new wtTextBlock(this.get("canvas"), vertex);
		widget.setAbsolutePosition(0 ,0);
		widget.setDraggable(true);
		vertex = new wtGraphVertex(this, widget);
		this.get("vertices")[vertex.toString()] = vertex;
		return vertex;
	}
}
/**
  Removes a vertex from this graph.
  @param {wtGraphVertex} vertex The vertex to be removed.
 */
wtGraph.prototype.removeVertex = function(vertex)
{
	delete this.get("vertices")[vertex.toString()];
}
/**
  Links two vertices with an edge from an optionally specified edge class.
  @param {wtGraphVertex} sourceVertex The source vertex.
  @param {wtGraphVertex} terminalVertex The terminal vertex.
  @param {Class} edgeClass (Optional) The edge class used for generating the
  new graph edge.
 */
wtGraph.prototype.linkVertices = function(sourceVertex, terminalVertex, 
		edgeClass)
{
	if (! edgeClass)
		edgeClass = wtGraphEdge;

	var newEdge = new edgeClass(this.getCanvas(), "black");
	newEdge.link(sourceVertex, terminalVertex);
	return newEdge;
}
/**
  Unlinks two vertices.
  @param {wtGraphVertex} sourceVertex The source vertex.
  @param {wtGraphVertex} terminalVertex The terminal vertex.
 */
wtGraph.prototype.unlinkVertices = function(sourceVertex, terminalVertex)
{
	var edges = sourceVertex.getEdgesToVertex(terminalVertex);
	for(var i=0;i<edges.length;i++)
		edges[i].removeSelf();
}
/**
  Generates an edge whose source is fixed to be one vertex and let the user select
  the terminal vertex from the same graph with his mouse.
  @param {wtGraphVertex} sourceVertex The source vertex.
  @param {Class} edgeClass (Optional) The edge class used for generating the
  new graph edge.
 */
wtGraph.prototype.linkFromMouse = function(sourceVertex, edgeClass)
{
	if (! edgeClass)
		edgeClass = wtGraphEdge;

	sourceVertex.generateAnchorPoints();

	var newEdge = new edgeClass(this.getCanvas(), "black");
	newEdge.set("notFinalized", true);
	newEdge.link(sourceVertex, sourceVertex);
	var droneVertex = newEdge.attachSourceToMouse();
	this.set("droneEdge", newEdge);
	this.set("droneVertex", droneVertex);
	this.set("droneMode", "find_source");
	droneVertex.connect("kill_drone", "DragEnd", this, "KillDrone");
	droneVertex.connect("move_drone", "Move", this, "MoveDrone");

	var highlightBox = new wtView(this.getCanvas(), 1, 1);
	highlightBox.setAbsolutePosition(0, 0);
	highlightBox.setLevel(10000);
	highlightBox.get("style").backgroundColor = "cyan";
	highlightBox.setAlpha(0.2);
	highlightBox.setDisplay(false);
	highlightBox.set("wtDependency", newEdge);
	this.set("droneHighlight", highlightBox);

	return newEdge;
}
/**
  Generates an edge whose terminal is fixed to be one vertex and let the user select
  the source vertex from the same graph with his mouse.
  @param {wtGraphVertex} terminalVertex The terminal vertex.
  @param {Class} edgeClass (Optional) The edge class used for generating the
  new graph edge.
 */
wtGraph.prototype.linkToMouse = function(termVertex, edgeClass)
{
	if (! edgeClass)
		edgeClass = wtGraphEdge;

	termVertex.generateAnchorPoints();

	var newEdge = new edgeClass(this.getCanvas(), "black");
	newEdge.set("notFinalized", true);
	newEdge.link(termVertex, termVertex);
	var droneVertex = newEdge.attachTerminalToMouse();
	this.set("droneEdge", newEdge);
	this.set("droneVertex", droneVertex);
	this.set("droneMode", "find_terminal");
	droneVertex.connect("kill_drone", "DragEnd", this, "KillDrone");
	droneVertex.connect("move_drone", "Move", this, "MoveDrone");

	var highlightBox = new wtView(this.getCanvas(), 1, 1);
	highlightBox.setAbsolutePosition(0, 0);
	highlightBox.setLevel(10000);
	highlightBox.get("style").backgroundColor = "cyan";
	highlightBox.setAlpha(0.2);
	highlightBox.setDisplay(false);
	highlightBox.set("wtDependency", newEdge);
	this.set("droneHighlight", highlightBox);

	return newEdge;
}
/**
  @private
 */
wtGraph.prototype.killDroneSlot = function(myself, evt, source)
{
	if (myself.get("overlapVertex"))
	{
		myself.get("droneEdge").set("notFinalized", undefined);;
		if (myself.get("droneMode") == "find_terminal")
			myself.get("droneEdge").setTerminal(myself.get("overlapVertex"));
		else
			myself.get("droneEdge").setSource(myself.get("overlapVertex"));
		myself.get("droneHighlight").removeSelf();
		myself.set("droneMode", undefined);
	}
	else
	{
		if (myself.get("droneMode") == "find_terminal")
			myself.get("droneEdge").setSource(myself.get("droneVertex"));
		else
			myself.get("droneEdge").setTerminal(myself.get("droneVertex"));
		myself.get("droneEdge").removeSelf();
		myself.set("droneMode", undefined);
	}
	if ($_(myself.get("droneVertex")))
	{
		window.mouse.disconnect(myself.get("droneVertex").getPeerWidget().toString(), "MouseUp");
		myself.get("droneVertex").removeSelf();
	}
}
/**
  @private
 */
wtGraph.prototype.moveDroneSlot = function(myself, evt, source)
{
	// 1. Check whether the mouse cursor is inside any of this graph's vertices
	// 2a. If found one, move the droneHighlight there to cover it, and set the droneHighlight to be displayed 
	// 	i. set the currently selected vertex as well
	// 2b. If not found, hide the droneHighlight
	//	i. clear the currently selected vertex as well

	var overlapVertex = null;
	for(var i in myself.get("vertices"))
	{
		var widget = myself.get("vertices")[i].getPeerWidget();
		if (widget.isInside(mouse.x, mouse.y))
		{
			overlapVertex = myself.get("vertices")[i];
			break;
		}
	}

	myself.get("droneEdge").setAnchorPoints([]);
	if (overlapVertex)
	{
		var pos = overlapVertex.getPeerWidget().getAbsolutePosition();
		var width = overlapVertex.getPeerWidget().get("offsetWidth");
		var height = overlapVertex.getPeerWidget().get("offsetHeight");
		myself.get("droneHighlight").setAbsolutePosition(pos.x, pos.y);
		myself.get("droneHighlight").get("style").width = dimension(width);
		myself.get("droneHighlight").get("style").height = dimension(height);
		myself.get("droneHighlight").setDisplay(true);
		myself.set("overlapVertex", overlapVertex);

		if (overlapVertex == myself.get("droneEdge").getSource() ||
				overlapVertex == myself.get("droneEdge").getTerminal())
		{
			var oTop = overlapVertex.getPeerWidget().get("offsetTop");
			var oLeft = overlapVertex.getPeerWidget().get("offsetLeft");
			var oRight = overlapVertex.getPeerWidget().get("offsetWidth") + oLeft;
			var oBottom = overlapVertex.getPeerWidget().get("offsetHeight") + oTop;

			myself.get("droneEdge").setAnchorPoints([new wt2DPoint((oLeft + oRight) /2, oBottom + 50), 
					new wt2DPoint(oRight + 50, oBottom + 50), 
					new wt2DPoint(oRight + 50, (oTop + oBottom) /2)]);
			myself.get("droneEdge").refresh();
		}

		var edge = myself.get("droneEdge");
		var points = edge.get("arrowPointList");
		if (! points)
			return;
		if (myself.get("droneMode") == "find_terminal")
		{
			var closestPoint = overlapVertex.getClosestAnchorPoint(points[points.length - 2]);
			points[points.length -1] = closestPoint;
		}
		else
		{
			var closestPoint = overlapVertex.getClosestAnchorPoint(points[1]);
			points[0] = closestPoint;
		}
		edge.draw();
		wtGraphVertex.delEdgeToUpdate(edge);
		edge.cancelRefresh();
	}
	else
	{
		myself.get("droneHighlight").setDisplay(false);
		myself.set("overlapVertex", undefined);
	}
}
// }}}
// {{{ wtGraphEdgeLabel - A row layout attached to an edge.
/**
  Constrcts a row layout attached to an edge.
  @class A row layout attached to a G=(V,E) graph edge.
  @param {wtGraphEdge} parentEdge The edge to attach this row layout to.
  @param {Number} linePos The position, within the range [0.0, 1.0], along the edge to attach this label to. 
  A position of 0 means where the edge is anchored to the source, 1 menas where the edge is anchored to the 
  terminal, 0.5 means exactly at the middle of the edge, etc.
  @param {wt2DPoint} offset The coordinate offset from the attachment point on the edge.
  @constructor
 */
var wtGraphEdgeLabel = function(parentEdge, linePos, offset)
{
	if (arguments.length < 3)
		return;

	var parentCanvas = parentEdge.getCanvas();

	this.base = wtRowLayout;
	this.base(parentCanvas, "");

	this.set("parentEdge", parentEdge);
	this.set("parentCanvas", parentCanvas);
	this.set("linePos", linePos);
	this.set("offset", offset);

	this.set("wtDependency", parentEdge);
	this.setSlot("Drag", this.dragSlot);
	this.refresh();

	var refreshMeSlot = function(myself, evt, source)
	{
		myself.refresh();
	}
	var highlightEdgeSlot = function(myself, evt, source)
	{
		myself.get("parentEdge").get("borderPath").setStrokeOpacity(0.1);
	}
	var unhighlightEdgeSlot = function(myself, evt, source)
	{
		myself.get("parentEdge").get("borderPath").setStrokeOpacity(0.0);
	}
	var removeSlot = function(myself, evt, source)
	{
		myself.removeSelf();
	}
	this.setSlot("RefreshMe", refreshMeSlot);
	this.setSlot("HighlightEdge", highlightEdgeSlot);
	this.setSlot("UnhighlightEdge", unhighlightEdgeSlot);
	this.setSlot("Remove", removeSlot);
	parentEdge.connect(this.toString(), "Refreshed", this, "RefreshMe");
	this.connect("highlight_edge", "DragStart", this, "HighlightEdge");
	this.connect("unhighlight_edge", "DragEnd", this, "UnhighlightEdge");
	parentEdge.connect(this.toString(), "Removed", this, "Remove");
}
wtGraphEdgeLabel.prototype = new wtRowLayout;
/**
  Repositions the label according to the current state of the graph edge and the displayed size
  of the row layout. <br/>
 */
wtGraphEdgeLabel.prototype.refresh = function()
{
	if (this.get("parentEdge").get("notFinalized"))
	{
		this.setDisplay(false);
		return;
	}
	this.setDisplay(true);
	var wiringPointList = this.get("parentEdge").getWiringPointList();
	if (wiringPointList.length < 2)
		return;

	if (! this.get("offsetWidth"))
		return;

	var totalLength = 0;

	for(var i=0;i<wiringPointList.length -1;i++)
	{
		var ptA = wiringPointList[i];
		var ptB = wiringPointList[i+1];
		totalLength += ptA.getDistance(ptB);
	}

	var desiredLength = totalLength * this.get("linePos");

	var currentLength = 0;
	var segmentLength = 0;
	var ptA = null;
	var ptB = null;
	for(var i=0;i<wiringPointList.length -1;i++)
	{
		ptA = wiringPointList[i];
		ptB = wiringPointList[i+1];
		segmentLength = ptA.getDistance(ptB);
		currentLength += segmentLength;
		if (currentLength >= desiredLength)
			break;
	}

	var hypotenuse = segmentLength - (currentLength - desiredLength);
	if (hypotenuse == 0)
		return;
	var inclineAngle = ptA.getInclineAngle(ptB);
	var linePoint = new wt2DPoint(ptA.x + hypotenuse * Math.cos(Math.deg2rad(inclineAngle)), 
			ptA.y + hypotenuse * Math.sin(Math.deg2rad(inclineAngle)));

	var offsetPoint = linePoint.add(this.get("offset"));
	var posPoint = offsetPoint.add(new wt2DPoint(this.get("offsetWidth") * -0.5, this.get("offsetHeight") * -0.5));

	this.setAbsolutePosition(posPoint.x, posPoint.y);
	if (! this.get("wtSignals").MouseDown.set_dragging)
		this.setDraggable(true);
}
/**
  @private
 */
wtGraphEdgeLabel.prototype.dragSlot = function(myself, evt, source)
{
	if (myself.get("dragging"))
	{
		if (isMSIE())
			myself.get("style").margin = "0px 0px 0px 0px";
		var x = mouse.x;
		var y = mouse.y;
		var dx = x - myself.get("prevX");
		var dy = y - myself.get("prevY");
		myself.set("offset", myself.get("offset").add(new wt2DPoint(dx, dy)));
		myself.set("prevX", x);
		myself.set("prevY", y);
		myself.refresh();
	}
}
// }}}
