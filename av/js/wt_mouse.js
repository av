// vim: set tabstop=4 shiftwidth=4 foldmethod=marker:
/*
    WT Toolkit - Web UI logic toolkit. Copyright (C) 2006 Kou Man Tong

    This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
*/

/** @fileoverview This file contains the global mouse event emitter. */

// {{{ wtInput and window.mouse - global mouse event signal emitter
/**
  Constructs a global mouse event signal emitter. Singleton object.
  @constructor
  @class A global mouse event signal emitter. This is a singleton class and is auto-initialized
  by WT Toolkit, so there's no need for a web developer to call the constructor. <br/><br/>
  The main use for this class is connecting its signals to your event handlers. Another use is
  for querying the current mouse cursor position relative to the browser display area.
  Available signal include:
  <ul>
  <li>MouseMove</li>
  <li>MouseUp</li>
  <li>Click</li>
  <li>MouseOver</li>
  <li>MouseOut</li>
  </ul>
  */
var wtInput = function()
{
	this.startProxy(document.documentElement, "mouse");
	document.documentElement.id = "mouse";
	this.linkSignal("MouseMove", "onmousemove");
	this.linkSignal("MouseUp", "onmouseup");
	this.linkSignal("Click", "onclick");
	this.linkSignal("MouseOver", "onmouseover");
	this.linkSignal("MouseOut", "onmouseout");
	/**
	  The mouse cursor's current x-coordinate.
	  @type Number
	  */
	this.x = 0;
	/**
	  The mouse cursor's current y-coordinate.
	  @type Number
	  */
	this.y = 0;

	var updateCoordinateSlot = function(myself, evt, source)
	{
		myself.x = evt.clientX;
		myself.y = evt.clientY;
	}
	this.setSlot("UpdateCoordinate", updateCoordinateSlot);
	this.connect("update_coordinate", "MouseMove", this, "UpdateCoordinate");
}
wtInput.prototype = new wtObject;
/**
  @private
  */
wtInput.prototype.linkSignal = function(signalName, eventName)
{
	var sig = $_(this).wtSignals;
	if (sig == undefined)
	{
		$_(this).wtSignals = {};
		sig = $_(this).wtSignals;
	}
	if (sig[signalName] == undefined)
		sig[signalName] = {};

	// generate the handler function for converting events into signals 
	$_(this)[eventName] = function()
	{
		// first, which event are we going to get?
		var evt;
		if (navigator.userAgent.search("MSIE") != -1)
			evt = window.event;
		else
			evt = arguments[0];
		try
		{
			if (_$(this))
				true;
		}
		catch(e){return;}
		_$(this).emit(signalName, evt);
	}
}
/**
  @private
  */
wtInput.prototype.isAncestorOf = function()
{
	return true;
}

window.mouse = new wtInput();

// }}}
