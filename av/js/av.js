wtIncludeCSS("styles/av.css");
wtIncludeJS("js/utils.js");

var WordNode = function(parent, group, index, width)
{
	if (arguments.length < 4)
		throw "Not enough arguments passed to WordNode constructor";

	this.base = wtTextBlock;
	this.base(parent, "");
	this.setSize(width, "");
	this.setSelectable(false);

	this.set("group", group);
	this.set("index", index);
	this.set("linked", {});

	var selectNode = function(self, event, source)
	{
		if (self.get("selected")) {
			return;
		}

		self.emit("UnselectOldWord", {"all": false});
		self.setHighlight();
		self.set("selected", true);
	}
	this.setSlot("selectNode", selectNode);
	this.connect("select_node", "Click", this, "selectNode");
	this.set("selected", false);

	var unselectNode = function(self, event, source)
	{
		self.resetHighlight();
		self.set("selected", false);
	}
	this.setSlot("unselectNode", unselectNode);
	this.setSignal("UnselectWord");
	this.connect("unselect_node", "UnselectWord", this, "unselectNode");
}
WordNode.prototype = new wtTextBlock;

WordNode.prototype.resetHighlight = function()
{
	$_(this).className = $_(this).className.replace(/\b selected\b/g, "");
}

WordNode.prototype.setHighlight = function()
{
	$_(this).className += " selected";
}

var WordEdge = function(canvas, color)
{
	if (arguments.length < 1)
		return;

	this.base = wtGraphEdge;
	if (typeof color != "undefined") {
		this.base(canvas, color);
	} else {
		this.base(canvas, "rgb(120, 120, 120)");
	}

	var menu = new wtContextMenu(this, "auto");
	var edge = this;
	var deleteHandler = function(self, event, source)
	{
		edge.removeSelf();
	}
	menu.addWidget("Delete", deleteHandler);

	var changeSourceHandler = function(self, event, source)
	{
		var vertex = edge.getTerminal();
		edge.removeSelf();
		canvas.get("graph").linkFromMouse(vertex, WordChangedEdge);
	}
	menu.addWidget("Change Source", changeSourceHandler);

	var changeTerminalHandler = function(self, event, source)
	{
		var vertex = edge.getSource();
		edge.removeSelf();
		canvas.get("graph").linkToMouse(vertex, WordChangedEdge);
	}
	menu.addWidget("Change Terminal", changeTerminalHandler);
	this.set("context_menu", menu);

	var isValid = function(self, event, source)
	{
		if (self.getSource() === self.getTerminal()) {
			alert("equals");
		}
	}
	this.setSlot("IsValid", isValid);
//	this.connect("is_source_valid", "SourceChanged", this, "IsValid");
//	this.connect("is_terminal_valid", "TerminalChanged", this, "IsValid");
}
WordEdge.prototype = new wtGraphEdge;

var WordChangedEdge = function(canvas)
{
	if (arguments.length < 1)
		throw "Not enough arguments passed to WordChangedEdge constructor";

	this.base = WordEdge;
	this.base(canvas, "orange");
}
WordChangedEdge.prototype = new WordEdge;

var Word = function(parent, group, index, pos, streams)
{
	if (arguments.length < 5)
		throw "Not enough arguments passed to Word constructor";

	var peer = new WordNode(parent.getCanvas(), group, index, "auto");
	peer.setAbsolutePosition(pos[0], pos[1]);

	this.base = wtGraphVertex;
	this.base(parent, peer);

	this.set("peer", peer);
	this.set("streams", streams);

	var menu = new wtContextMenu(peer, "auto");
	var vertex = this;
	var addEdge = function(self, event, source)
	{
		parent.linkToMouse(vertex, WordChangedEdge);
	}
	menu.addWidget("Create Edge", addEdge);

	this.generateAnchorPoints();
}
Word.prototype = new wtGraphVertex;

Word.prototype.setStream = function(index)
{
	if (arguments.length < 1)
		throw "Not enough arguments passed to Word.setStream";

	if (this.get("streams").length > index) {
		this.get("peer").setText(this.get("streams")[index]);
		return;
	}

	throw "Index out of bound for streams";
}

var Sentence = function(parent, group)
{
	if (arguments.length < 2)
		throw "Not enough arguments passed to Sentence constructor";

	this.parent = parent;
	this.group = group;
	this.words = new Array();
}

Sentence.prototype.addSentence = function(graph, y, sentence)
{
	if (arguments.length < 3)
		throw "Not enough arguments passed to Sentence.addSentence";

	var x = 10;
	for (var i = 0; i < sentence.length; i++) {
		var word = new Word(graph, this.group, i, [x, y], sentence[i]);
		try {
			word.setStream(0);
		} catch (e) {
			alert(sentence[i]);
		}

		word.get("peer").setSignal("UnselectOldWord");
		word.get("peer").connect("unselect_old_node", "UnselectOldWord",
								 this.parent, "unselectOldWord");
		x +=  $_(word.get("peer")).offsetWidth + 10;

		this.words.push(word);
	}
}

var Alignment = function(parent, menu, stat, td, nf)
{
	if (arguments.length < 5)
		throw "Not enough arguments passed to Alignment constructor";

	this.base = wtNotebook;
	this.base(parent, "100%");
	this.setRelativePosition(0, 0);

	this.buildMenu(menu);
	this.buildStat(stat);
	this.buildTD(td);
	this.buildNF(nf);

	var unselectOldWord = function(self, event, source)
	{
		var word = self.get("selected_word");
		if (word != null) {
			word.emit("UnselectWord");
		}

		if (event["all"]) {
			self.set("selected_word", null);
		} else {
			self.emit("ShowInfo", {"group": source.get("group"),
								   "index": source.get("index")});
			self.set("selected_word", source);
		}
	}
	this.setSlot("unselectOldWord", unselectOldWord);
	this.set("selected_word", null);
	this.setSignal("ShowInfo");
}
Alignment.prototype = new wtNotebook;

Alignment.prototype.buildMenu = function(parent)
{
	if (arguments.length < 1)
		throw "Not enough arguments passed to Alignment.buildMenu";

	var bar = new wtRowLayout(parent, "100%");

	var file_menu = bar.addWidget("File");
	$_(file_menu).className = "menu-item";
	bar.addSpacer("100%");

	var openHandler = function(self, event, source)
	{
		var dim = getClientDimensions();

		var wd = new wtModalWindow(document.body, 250, 60);
		wd.setTitle("Open File");
		wd.enableCloseIcon(true);
		wd.setAbsolutePosition(dim["width"] / 2 - 125, dim["height"] / 2 - 25);

		var d = new wtWidget(wd, "div");
		$_(d).innerHTML = '<form method="POST" action="index.py"' +
			'enctype="multipart/form-data"><table><tr><td>' +
			'<input type="file" name="file" /></td></tr><tr><td align="right">' +
			'<input type="submit" value="Submit" /></td></tr></table></form>';
	}

	var menu = new wtAttachedMenu(file_menu, POPUP_BOTTOM);
	menu.addWidget("Open", openHandler);
}

Alignment.prototype.buildStat = function(parent)
{
	if (arguments.length < 1)
		throw "Not enough arguments passed to Alignment.buildStat";

	var nb = new wtNotebook(parent);
	nb.setRelativePosition(0, 0);
	var p = nb.newPage("Stats");
	this.set("stat", new wtDisplay(p, "100%"));

	p.select();
}

Alignment.prototype.buildTD = function(parent)
{
	if (arguments.length < 1)
		throw "Not enough arguments passed to Alignment.buildTD";

	var nb = new wtNotebook(parent);
	nb.setRelativePosition(0, 0);
	this.set("trans", nb.newPage("Translation"));
	this.set("dist", nb.newPage("Distortion"));
	this.get("trans").select();
}

Alignment.prototype.buildNF = function(parent)
{
	if (arguments.length < 1)
		throw "Not enough arguments passed to Alignment.buildNF";

	var nb = new wtNotebook(parent);
	nb.setRelativePosition(0, 0);
	this.set("ngram", nb.newPage("N-gram"));
	this.set("fert", nb.newPage("Fertility"));
	this.get("ngram").select();
}

Alignment.prototype.loadStat = function()
{
	var sub = {"source_sentence_id": "source ID",
		"target_sentence_id": "target ID",
		"lm_ngram_length": "n-gram length"};

	for (var i = 0; i < __parsed_content["header"].length; i++) {
		var title;

		if (__parsed_content["header"][i][0] in sub) {
			title = sub[__parsed_content["header"][i][0]];
		} else {
			title = __parsed_content["header"][i][0];
		}

		var row = this.get("stat").setDisplayRow([capitalize(title),
			__parsed_content["header"][i][1]]);

		$_(row).style.whiteSpace = "nowrap";
	}
}

Alignment.prototype.loadGraph = function()
{
	var p = this.newPage("Lines");
	p.select();

	var canvas = new wtCanvas(p, 788, 286);
	$_(canvas).style.overflowX = "auto";
	canvas.set("graph", new wtGraph(canvas));

	var height = parseInt(canvas.get("offsetHeight") * 3 / 14);

	var ssentence = new Sentence(this, SOURCE);
	ssentence.addSentence(canvas.get("graph"), height,
						  __parsed_content["source_sentence"][0]);
	this.set("ssentence", ssentence);

	var tsentence = new Sentence(this, TARGET);
	tsentence.addSentence(canvas.get("graph"), height * 3,
						  __parsed_content["target_sentence"][0]);
	this.set("tsentence", tsentence);

	var linkWords = function()
	{
		var aln = __parsed_content["alignment"];
		for (var i = 0; i < aln.length; i++) {
			if (aln[i][0] < 0 || aln[i][1] < 0) {
				continue;
			}

			var sword = ssentence.words[aln[i][1]];
			var tword = tsentence.words[aln[i][0]];
			if (typeof tword.get("peer").get("linked")[aln[i][1]] != "undefined") {
				continue;
			}

			sword.get("peer").get("linked")[aln[i][0]] = true;
			tword.get("peer").get("linked")[aln[i][1]] = true;
			canvas.get("graph").linkVertices(sword, tword, WordEdge);
		}
	}
	linkWords();

	this.connect("show_info", "ShowInfo", this, "showInfo");
}

Alignment.prototype.loadMatrix = function()
{
	var p = this.newPage("Matrix");
	var display = new wtDisplay(p, "100%");

	display.setDisplayRow([""]);
}

Alignment.prototype.loadInfo = function()
{
	var subs = {"trans": ["translations", "translations"],
		"dist": ["distortions", "distortions"],
		"ngram": ["language_model", "n-gram"],
		"fert": ["fertilities", "fertilities"]};

	var showInfo = function(self, event, source)
	{
		for (var type in subs) {
			var info;
			if (event["group"] == SOURCE) {
				info = __parsed_content["inverse_" + subs[type][0]];
			} else if (event["group"] == TARGET) {
				info = __parsed_content[subs[type][0]];
			} else {
				return;
			}

			self.get(type).clearAll();

			if (info == null) {
				continue;
			}

			var display = new wtDisplay(self.get(type), "100%");
			display.setSubtitleRow([capitalize(subs[type][1]), "Probability"]);

			for (var i = 0; i < info.length; i++) {
				if (info[i][0] == event["index"]) {
					try {
						display.setDisplayRow([info[i][1], info[i][info[i].length - 1]]);
					} catch (e) {
						alert(info[i]);
					}
				}
			}
			display.setSortEnabled([0, 1]);
			// Sort in descending order.
			$_(display).colList[1] = -1;
			display.sort(1);
		}
	}
	this.setSlot("showInfo", showInfo);
}

Alignment.prototype.load = function()
{
	this.loadStat();
	this.loadInfo();
	this.loadGraph();
	this.loadMatrix();
}

function init()
{
	// Constants
	SOURCE = 0;
	TARGET = 1;

	// Get DOM objects
	var menu = document.getElementById("menu");
	var model_plane = document.getElementById("model-area");
	var stat_cell = document.getElementById("stat");
	var td_cell = document.getElementById("trans-dist");
	var nf_cell = document.getElementById("ngram-fert");

	if (!menu || !model_plane || !stat_cell || !td_cell || !nf_cell) {
		alert("This is not a valid page to use this script," +
			  "please use the original template.");
	}

	var alignment = new Alignment(model_plane, menu, stat_cell, td_cell,
		nf_cell);

	if (typeof __parsed_content != "undefined") {
		alignment.load();
	}
}