// vim: set tabstop=4 shiftwidth=4 foldmethod=marker:
/*
    WT Toolkit - Web UI logic toolkit. Copyright (C) 2006 Kou Man Tong

    This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
*/

/**
  @fileoverview This file contains the window manager class for controlling window ordering and activation.
  */

// {{{ wtWindowManager - Manages the ordering of wtWindow widgets
/**
  Constructs a window manager
  @constructor
  @class A window manager that manages the ordering and activation/deactivation of wtWindow widgets. <br/><br/>
  Available signals: <br/>
  <ul>
  <li>RaiseWindow</li>
  </ul>
  @param {wtWidget|DOM_Node} parentWidget The parent container for placing windows inside.
  */
var wtWindowManager = function(parentWidget)
{
	if (!isDOMNode(parentWidget))
	/** @private  */
	this.wtDependency = parentWidget.toString();
	this.startProxy(this);
	/** @private  */
	this.zBase = 0;
	/** @private  */
	this.parentWidget = parentWidget;
	/** @private  */
	this.windowList = new LinkedList();
	/** @private  */
	this.windowMap = {};
	this.setSignal("RaiseWindow");

	var reorderSlot = function(myself, evt, source)
	{
		// if current widget is already on the top, ignore
		if ($_(myself.windowList.prevNode.data) == source)
			return;

		// check if the current top window is modal or not
		// if yes -> ignore
		// else -> raise source widget
		if (myself.windowList.prevNode && $_(myself.windowList.prevNode.data).modal == false)
			myself.raiseWindow(source);
	}
	this.setSlot("Reorder", reorderSlot);
}
wtWindowManager.prototype = new wtObject;
/**
  Adds a wtWindow to be managed by this window manager
  @param {wtWindow} widget The window to be managed
  @param {Boolean} modal (Optional) Whether the window added should become modal or not. Leave empty to make the window modeless.
  */
wtWindowManager.prototype.addWidget = function(widget, modal)
{
	// set widget's modal property
	$_(widget).modal = (modal ? true : false);
	
	// set widget's window id
	$_(widget).windowId = "window" + genId();

	// give widget a string reference back to me so it can remove itself properly also
	$_(widget).windowManager = this.toString();
	
	// add widget to parentWidget
	if (!isDOMNode(this.parentWidget))
		this.parentWidget.addWidget(widget);
	else
		this.parentWidget.appendChild($_(widget));
	
	// add widget to linked list
	var node = this.windowList.addPrev(widget);
	
	// add widget to window map
	this.windowMap[$_(widget).windowId] = {"widget" : widget, "node" : node};
	
	// raise added window
	var manager = this;
	var timedFunc = function()
	{
		widget.connect("raise_window", "Click", manager, "Reorder");
		manager.raiseWindow(widget);
	}
	setTimeout(timedFunc, 1);
}
/**
  @private
  */
wtWindowManager.prototype.addWindow = wtWindowManager.prototype.addWidget;
/**
  Removes a window from the window manager and from the DOM tree.
  @param {wtWindow} widget The window to be removed
  */
wtWindowManager.prototype.removeWindow = function(widget)
{
	// remove widget from parentWidget
	if (!isDOMNode(this.parentWidget))
		$_(this.parentWidget).removeChild($_(widget));
	else
		this.parentWidget.removeChild($_(widget));
	if ($_(widget).coverBlock && $_($_(widget).coverBlock).parentNode)
		$_($_(widget).coverBlock).parentNode.removeChild($_($_(widget).coverBlock));
	
	// remove widget from linked list
	var node = this.windowMap[$_(widget).windowId].node;
	node.removeSelf();
	
	// remove widget from window map
	delete this.windowMap[$_(widget).windowId];
	
	// raise top window
	if (this.windowList.prevNode.data)
		this.raiseWindow(this.windowList.prevNode.data);
}
/**
  Raises an inactive window to the top, regardless of whether a modal window is currently on the top or not.
  @param {wtWindow} widget The window to be moved to the top
  */
wtWindowManager.prototype.raiseWindow = function(widget)
{
	// find node from linked list with widget's unique id from window map
	var node = this.windowMap[$_(widget).windowId].node;
	
	// delete node
	node.removeSelf();
	
	// re-add node as the last node of linked list
	this.windowMap[$_(widget).windowId].node = this.windowList.addPrev(widget);
	
	// reset the zIndex for each node in linked list
	var curNode = this.windowList.nextNode;
	var curIndex = this.zBase;
	while(curNode != this.windowList)
	{
		curNode.data.setLevel(curIndex);
		curIndex += 100;
		curNode = curNode.nextNode;
	}
	
	// disable all non-top windows, and enable the current top window
	var curNode = this.windowList.prevNode.prevNode;
	while(curNode != this.windowList)
	{
		curNode.data.setEnabled(false);
		curNode = curNode.nextNode;
	}
	if (this.windowList.prevNode != this.windowList)
		this.windowList.prevNode.data.setEnabled(true);
	
	// emit RaiseWindow signal
	this.emit("RaiseWindow", {"widget" : widget});
}
/**
  Sets the base z-axis position for the window stack.
  @param {Number} zBase The base z-axis position.
  */
wtWindowManager.prototype.setZBase = function(zBase){this.zBase = zBase;}
/**
  Tells the top window on the window stack.
  @returns The top window widget
  @type wtWindow
  */
wtWindowManager.prototype.topWindow = function()
{
	return this.windowList.prevNode.data;
}
/**
  Tells the bottom window in the window stack.
  @returns The bottom window widget
  @type wtWindow
  */
wtWindowManager.prototype.bottomWindow = function()
{
	return this.windowList.nextNode.data;
}
/**
  Removes the top window on the window stack.
  */
wtWindowManager.prototype.popWindow = function()
{
	this.removeWindow(this.topWindow());
}
/**
  Tells whether the window stack is empty or not.
  @returns Whether the window stack is empty or not.
  @type Boolean
  */
wtWindowManager.prototype.isEmpty = function()
{
	return this.windowList.nextNode == this.windowList;
}
/**
  Removes all windows from the window stack.
  */
wtWindowManager.prototype.popAll = function()
{
	while(!this.isEmpty())
		this.popWindow();
}
// }}}
