// vim: set tabstop=4 shiftwidth=4 foldmethod=marker:
/*
    WT Toolkit - Web UI logic toolkit. Copyright (C) 2006 Kou Man Tong

    This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
*/

/*
 * $Log: wt_form.js,v $
 * Revision 1.12  2007/03/08 17:57:53  francium
 * -Fixed: Disabling a wtSpreadsheet should have visual effects on the widget to
 *         indicate that it is disabled to the user
 * -Added: JSDoc entries in wt_form.js
 *
 * Revision 1.11  2007/03/08 05:23:08  francium
 * -Added: JSDoc documentation compiler under redist
 * -Added: JSDoc entries in wt_widget.js
 * -Added: JSDoc entries in wt_algor.js
 * -Added: JSDoc entries in wt_wm.js
 * -Added: JSDoc entries in wt_mouse.js
 * -Added: JSDoc entries in wt_core.js
 * -Fixed: Random MSIE() not defined errors during page load
 *
 * Revision 1.10  2007/03/07 17:35:17  francium
 * -Added: JSDoc entries in wt_widget.js
 *
 * Revision 1.9  2007/02/20 00:29:58  francium
 * -Changed: Renamed wtObject::wtDependencies to wtObject::wtDependency and the
 *         attribute is no longer an array
 * -Changed: Manual GC is now the default behavior
 * -Removed: window.disableGCThread flag since GC Thread is no longer the default
 * -Changed: Added a "?tx=ab" dummy argument after .js file references in HTML
 *         files to avoid version synchronization problems
 *
 * Revision 1.8  2007/01/11 11:54:22  francium
 * -Changed: Finished porting wt_form.js to 0.2.0 object structure
 * -Added: Unit test cases for all wt_form.js objects
 * -Changed: Generalized domDependencies into wtDependencies in garbage collector
 *         helper algorithm
 * -Fixed: wtWindow::clearAll() crashes Internet Explorer
 *
 * Revision 1.7  2007/01/07 18:25:07  francium
 * -Fixed: wtRemoteProcedureCall object cannot be created
 * -Changed: Converted wtForm, wtFormWidget and wtSingleLineTextInput to use new
 *         object system
 * -Added: prevValue and getPrevValue() to wtFormWidget
 * -Added: prevValue as an argument to wtSingleLineTextInput validator
 * -Added: ValueChanged signal to wtForm
 * -Changed: General format of arguments to ValueChanged signal
 *
 * Revision 1.6  2006/12/25 16:03:48  francium
 * -Fixed: Cross-URL memory leak in IE
 * -Fixed: Memory leaks related to observing DOM objects that has gone out of
 * 	the DOM tree and is no longer useful
 *
 * Revision 1.5  2006/12/24 16:13:42  francium
 * -Fixed: isEnabled() not working across many widgets
 *
 * Revision 1.4  2006/12/24 16:04:04  francium
 * -Fixed: Memory leakage from improper use of observer pattern
 * -Fixed: Hidden form widget not working
 *
 * Revision 1.3  2006/12/24 12:39:36  francium
 * -Fixed: Converted Javascript source files to Unix format
 *
 * Revision 1.2  2006/12/24 12:29:16  francium
 * -Fixed: wtForm::submitExclude and wtRemoteProcedureCall::submitExclude
 * 	re-enables widgets that are disabled before sending RPC request to
 * 	server
 * -Fixed: Wrongly placed copyright notice, should be near the top of the file
 *
 * $Id: wt_form.js,v 1.12 2007/03/08 17:57:53 francium Exp $
 */

/**
  @fileoverview This file contains all objects related to AJAX input forms.
  */

// {{{ wtForm - container of various input widgets, submits information to server via wtRemoteProcedureCall
/**
  Constructs a container of form input widgets, whose values can be submitted to server via wtRemoteProcedureCall. 
  @constructor
  @class A container of form input widgets, whose values can be submitted to server via wtRemoteProcedureCall.
  Submitting a wtForm is analogous to submitting a wtRemoteProcedureCall, except that the RPC value object is
  subtituted by a dictionary of &lt;form input name&gt;: &lt;form input value&gt; pairs.
  <br/><br/>
  Available signals:
  <br/>
  <ul>
  <li>SubmitSuccess</li>
  <li>SubmitFailure</li>
  <li>Submit</li>
  <li>Reset</li>
  <li>AfterSubmit</li>
  <li>ValueChanged</li>
  </ul>
  @param {String} cgi The CGI script URL for receiving the form input values
  @param {String} procName The RPC call name
  @see wtRemoteProcedureCall
  */
var wtForm = function(cgi, procName)
{
	if (arguments.length < 2)
		return;

	this.startProxy(this);
	$_(this).procArgs = {};
	$_(this).widgetMap = {};
	$_(this).RPCObject = new wtRemoteProcedureCall(cgi, procName, this.get("procArgs"));
	$_(this).submitExclude = $_($_(this).RPCObject).submitExclude;
	$_(this).submitExclude["form_widget"] = this.toString();
	$_(this).disabled =false;

	var changeValueSlot = function(myself, evt, source)
	{
		var name = $_(source).name;
		myself.get("procArgs")[name] = source.getValue();
		myself.emit("ValueChanged", {"name": name, "value": source.getValue(), "prevValue": source.getPrevValue()});
	}
	this.setSlot("ChangeValue", changeValueSlot);
	var rpcSuccessSlot = function(myself, evt, source)
	{
		myself.emit("SubmitSuccess", evt);
	}
	this.setSlot("RPCSuccess", rpcSuccessSlot);
	var rpcFailureSlot = function(myself, evt, source)
	{
		myself.emit("SubmitFailure", evt);
	}
	this.setSlot("RPCFailure", rpcFailureSlot);
	var rpcEndSlot = function(myself, evt, source)
	{
		myself.emit("AfterSubmit", evt);
	}
	this.setSlot("RPCEnd", rpcEndSlot);
	var submitSlot = function(myself, evt, source)
	{
		myself.submit();
	}
	this.setSlot("Submit", submitSlot);
	var signals = ["SubmitSuccess", "SubmitFailure", "Submit", "Reset", "AfterSubmit", "ValueChanged"];
	for(var i=0;i<signals.length;i++)
		this.setSignal(signals[i]);

	$_(this).RPCObject.connect("rpc_success", "Success", this, "RPCSuccess");
	$_(this).RPCObject.connect("rpc_failure", "Failure", this, "RPCFailure");
	$_(this).RPCObject.connect("rpc_aftersubmit", "AfterRPC", this, "RPCEnd");
}
wtForm.prototype = new wtObject;
/**
  Adds a form input widget to the form.
  @param {wtObject} Form input widget
  */
wtForm.prototype.addWidget = function(widget)
{
	$_(this).procArgs[$_(widget).name] = null;
	$_(this).widgetMap[$_(widget).name] = widget;
	widget.connect("value_changed", "ValueChanged", this, "ChangeValue");
}
/**
  Submits the form.
  */
wtForm.prototype.submit = function()
{
	this.emit("Submit", {"procArgs" : $_(this).procArgs, "widgetMap" : $_(this).widgetMap});
	$_(this).RPCObject.submit();
}
/**
  Aborts form submission.
  */
wtForm.prototype.abort = function()
{
	$_(this).RPCObject.abort();
}
/**
  Resets the form. <br/><br/>
  TODO: I think it does not currently work!
  */
wtForm.prototype.reset = function()
{
	$_(this).emit("Reset", {});
}
/**
  Sets the form to be enabled or disabled. A disabled form has all its input elements grayed out and is unable to receive user input.
  @param {Boolean} yes Whether the form should be enabled or not.
  */
wtForm.prototype.setEnabled = function(yes)
{
	$_(this).disabled = !yes;
	for(var i in $_(this).widgetMap)
		$_(this).widgetMap[i].setEnabled(yes);
}
/**
  Tells whether the form is enabled or not.
  @returns Whether the form is enabled or not.
  @type Boolean
  */
wtForm.prototype.isEnabled = function()
{
	return !($_(this).disabled);
}
/**
  Sets the form submission success handler.
  @param {Function} handler Success event handler.
  Input arguments of the handler follow the universal event handler format in WT Toolkit.
  @see wtObject#connect
  */
wtForm.prototype.setSuccessHandler = function(handler)
{
	this.setSlot("HandleSuccess", handler);
	this.connect("handle_success", "SubmitSuccess", this, "HandleSuccess");
}
/**
  Sets the form submission failure handler.
  @param {Function} handler Failure event handler.
  Input arguments of the handler follow the universal event handler format in WT Toolkit.
  @see wtObject#connect
  */
wtForm.prototype.setFailureHandler = function(handler)
{
	this.setSlot("HandleFailure", handler);
	this.connect("handle_failure", "SubmitFailure", this, "HandleFailure");
	
}
/**
  @private
  */
wtForm.prototype._wtObject_endProxy = wtObject.prototype.endProxy;
/**
  Disconnects the weak reference and removes the memory expensive object from the weak reference table.
  */
wtForm.prototype.endProxy = function()
{
	this.get("RPCObject").endProxy();
	this._wtObject_endProxy();
}
// }}}
// {{{ wtFormWidget - basic form widget element
/**
  Constructs a basic form widget element.
  @constructor
  @class A basic form widget element. This class is not meant to be used directly in web applications, it
  serves as a base class for more concrete form input classes, such as text boxes. <br/><br/>
  Available signals: <br/>
  <ul>
  <li>ValueChanged</li>
  </ul>
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} tagName The DOM tag name of the form widget.
  @param {String} inputType The input type of the form widget, only applicable if the tagName is "input".
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {String} changeEventName (Optional) The name of the DOM event hook that can be used to indicate 
  value change.
  @param {String} ieExtras (Optional) Extra arguments for widget creation in Internet Explorer, this only
  applies if the tagName is "input".
  */
var wtFormWidget = function(parentWidget, parentForm, tagName, inputType, name, changeEventName, ieExtras)
{
	if (arguments.length < 5)
		return;

	this.base = wtWidget;

	if (isMSIE() && tagName.toLowerCase() == "input")
	{
		var tagStr = "<input type=\"$type$\" name=\"$name$\" $ieExtras$>";
		tagStr = tagStr.replace("$type$", inputType);
		tagStr = tagStr.replace("$name$", name);
		tagStr = tagStr.replace("$ieExtras$", ieExtras);
		this.base(parentWidget, tagStr);
	}
	else
	{
		this.base(parentWidget, tagName);
		if (inputType)
			this.set("type", inputType);
		this.set("name", name);
	}

	if (changeEventName)
		this.linkSignal("ValueChanged", changeEventName);
	else
		this.setSignal("ValueChanged");
	if (parentForm)
	{

		this.set("parentForm", parentForm);
		this.get("parentForm").addWidget(this);
	}

	this.set("disabled", false);
}
wtFormWidget.prototype = new wtWidget;
/**
  Sets the value of the form widget, and saves down the old value. As of v0.2.2 not all form widgets
  save down the previous value on value change, so the previous value part should be treated like a
  future feature instead of something to be depended on.
  @param {Object} newval The new value
  */
wtFormWidget.prototype.setValue = function(newval)
{
	this.set("prevValue", this.getValue());
	this.set("value", newval);
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
/**
  Tells the previous value of the form widget.
  @returns The previous value
  */
wtFormWidget.prototype.getPrevValue = function()
{
	return this.get("prevValue");
}
/**
  Tells the current value of the form widget.
  @returns The current value
  */
wtFormWidget.prototype.getValue = function()
{
	return this.get("value");
}
/**
  Sets the form widget to be enabled or disabled.
  @param {Boolean} yes Whether the form widget is enabled or disabled.
  */
wtFormWidget.prototype.setEnabled = function(yes)
{
	this.set("disabled", !yes);
}
/**
  Tells whether the form widget is enabled or disabled.
  @returns true if enabled, false if disabled.
  @type Boolean
  */
wtFormWidget.prototype.isEnabled = function()
{
	return !(this.get("disabled"));
}
// }}}
// {{{ wtSingleLineTextInput - single line text input element 
/**
  Constructs a single-line text input
  @constructor
  @class A single-line text input.<br/><br/>
	Available signals:
	<ul>
	<li>Enter</li>
	</ul>
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {String} defaultVal Default value of the text box.
  @param {Integer} maxLength (Optional) Maximum length of text box in number of unicode characters.
  @param {Integer} width (Optional) Width of text box in pixels.
  */
var wtSingleLineTextInput = function(parentWidget, parentForm, name, defaultVal, maxLength, width)
{
	if (arguments.length < 3)
		return;

	this.base = wtFormWidget;
	this.base(parentWidget, parentForm, "input", "text", name);

	$_(this).className += " wtSingleLineTextInput";
	if (isString(defaultVal) || isNumber(defaultVal))
	{
		$_(this).value = defaultVal;
		this.setValue(defaultVal);
	}
	else
		this.setValue("");
	this.set("prevValue", this.getValue());
	if (isInteger(maxLength))
		this.set("maxLength", maxLength);
	if (width)
		$_(this).style.width = dimension(width);

	this.linkSignal("UserChange", "onblur");

	var validateSlot = function(myself, evt, source)
	{
		if (typeof($_(myself).validator) == "function")
		{
			if ($_(myself).validator($_(myself).value, myself.getPrevValue()))
				myself.setValue($_(myself).value);		// this updates prevValue and emits ValueChanged signal
			else
			{
				$_(myself).value = myself.getPrevValue();
				var timedFunc = function(){$_(myself).focus();}
				setTimeout(timedFunc, 0);
			}
		}
		else
		{
			myself.setValue($_(myself).value);
		}
	}
	this.setSlot("Validate", validateSlot);
	var keyPressSlot = function(myself, evt, source)
	{
		if (evt.keyCode == 13)
		{
			$_(myself).onblur();
			myself.emit("Enter", {});
		}
	}
	this.setSlot("KeyPress", keyPressSlot);
	var checkFocusSlot = function(myself, evt, source)
	{
		var d = new Date();
		if (d.getTime() - $_(myself).createTime < 500)
			return;
		var timedFunc = function()
		{
			if (!$_(myself) || !$_(myself).parentNode)
				return;
			var pos = myself.getScreenPosition();
			if (!myself.isInside(mouse.x, mouse.y))
				$_(myself).onblur();
		}
		setTimeout(timedFunc, 0);
	}
	this.setSlot("CheckFocus", checkFocusSlot);
	this.setSignal("Enter");

	this.connect("attempt_change", "UserChange", this, "Validate");
	this.connect("key_pressed", "KeyPress", this, "KeyPress");
	if (isMSIE())
	{
		var d = new Date();
		this.set("createTime", d.getTime());
		window.mouse.connect("check_focus", "Click", this, "CheckFocus");
	}
	this.emit("ValueChanged", {"prevValue": this.getPrevValue(), "value": this.getValue()});
}
wtSingleLineTextInput.prototype = new wtFormWidget;
/**
  Sets a validator function for this text box. The validator function is executed whenever the text box is defocused.
  <br/><br/>
  The validator function is expected to take two arguments: <br/>
  <i>validatorFunc(currentValue, previousValue)</i><br/><br/>
  where <i>currentValue</i> is the current value of the text box, <i>previousValue</i> is the value of the text box before
  the text box was focused. The function should return true if the current value is valid, and return false if the current
  value is invalid. WT Toolkit will automatically reset the text box's value to <i>previousValue</i> and refocus the text
  box if the validator returns false.
  @param {Function} predicate The validator function
  */
wtSingleLineTextInput.prototype.setValidator = function(predicate)
{
	this.set("validator", predicate);
}
/**
  Removes the validator function.
  */
wtSingleLineTextInput.prototype.unsetValidator = function()
{
	$_(this)["validator"] = undefined;
}
/**
  Sets the value of the text box. No validation is performed for this operation.
  @param {String} newval New string value
  */
wtSingleLineTextInput.prototype.setValue = function(newval)
{
	this.set("prevValue", this.getValue());
	this.set("currentValue", newval);
	$_(this).value = newval;
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
/**
  Tells the current value of the text box
  @returns Current value of text box
  @type String
  */
wtSingleLineTextInput.prototype.getValue = function()
{
	return this.get("currentValue");
}
wtSingleLineTextInput.prototype.focus = function()
{
	var len = this.getValue().length;
	$_(this).focus();
	this.setSelectionRange(0, len);
}
wtSingleLineTextInput.prototype.setSelectionRange = function(start, end) 
{
	if (! isMSIE()) 
	{
		$_(this).setSelectionRange(start, end);
	} 
	else 
	{
		var range = $_(this).createTextRange();
		range.collapse(true);
		range.moveStart("character", start);
		range.moveEnd("character", end - start);
		range.select();
	}
};
wtSingleLineTextInput.prototype.getSelectionStart = function()
{
	if (! isMSIE())
		return this.get("selectionStart");
	var range = document.selection.createRange();
	var isCollapsed = range.compareEndPoints("StartToEnd", range) == 0;
	if (!isCollapsed)
		range.collapse(true);
	var b = range.getBookmark();
	return b.charCodeAt(2) - 2;
};
wtSingleLineTextInput.prototype.getSelectionEnd = function() 
{
	if (! isMSIE())
		return this.get("selectionEnd");
	var range = document.selection.createRange();
	var isCollapsed = range.compareEndPoints("StartToEnd", range) == 0;
	if (!isCollapsed)
		range.collapse(false);
	var b = range.getBookmark();
	return b.charCodeAt(2) - 2;
};
// }}} 
// {{{ wtMultiLineTextInput - multiple line text input element
/**
  Constructs a multi-line text input widget.
  @constructor
  @class A multi-line text input widget.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {String} defaultVal Default value of the text box.
  @param {String} width Width of text box as a CSS length quantity.
  @param {String} height Height of text box as a CSS length quantity.
  */
var wtMultiLineTextInput = function(parentWidget, parentForm, name, defaultVal, width, height)
{
	if (arguments.length < 3)
		return;

	this.base = wtFormWidget;
	this.base(parentWidget, parentForm, "textarea", undefined, name);

	$_(this).className += " wtMultiLineTextInput";
	this.setSize(width, height);
	if (isString(defaultVal) || isNumber(defaultVal))
	{
		$_(this).value = defaultVal;
		this.setValue(defaultVal);
	}
	else
		this.setValue("");
	this.set("prevValue", this.getValue());

	this.linkSignal("UserChange", "onblur");

	var validateSlot = function(myself, evt, source)
	{
		if (typeof($_(myself).validator) == "function")
		{
			if ($_(myself).validator($_(myself).value, myself.getPrevValue()))
				myself.setValue($_(myself).value);		// this updates prevValue and emits ValueChanged signal
			else
			{
				$_(myself).value = myself.getPrevValue();
				var timedFunc = function(){$_(myself).focus();}
				setTimeout(timedFunc, 0);
			}
		}
		else
		{
			myself.setValue($_(myself).value);
		}
	}
	this.setSlot("Validate", validateSlot);

	this.connect("attempt_change", "UserChange", this, "Validate");
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});;
}
wtMultiLineTextInput.prototype = new wtFormWidget;
/**
  Sets the value of the text box. No validation is performed for this operation.
  @param {String} newval New string value
  */
wtMultiLineTextInput.prototype.setValue = function(newval)
{
	this.set("prevValue", this.getValue());
	this.set("currentValue", newval);
	$_(this).value = newval;
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
/**
  Tells the current value of the text box
  @returns Current value of text box
  @type String
  */
wtMultiLineTextInput.prototype.getValue = function()
{
	return this.get("currentValue");
}
/**
  Sets a validator function for this text box. The validator function is executed whenever the text box is defocused.
  <br/><br/>
  The validator function is expected to take two arguments: <br/>
  <i>validatorFunc(currentValue, previousValue)</i><br/><br/>
  where <i>currentValue</i> is the current value of the text box, <i>previousValue</i> is the value of the text box before
  the text box was focused. The function should return true if the current value is valid, and return false if the current
  value is invalid. WT Toolkit will automatically reset the text box's value to <i>previousValue</i> and refocus the text
  box if the validator returns false.
  @param {Function} predicate The validator function
  */
wtMultiLineTextInput.prototype.setValidator = function(predicate)
{
	this.set("validator", predicate);
}
/**
  Removes the validator function.
  */
wtMultiLineTextInput.unsetValidator = function()
{
	$_(this).validator = undefined;
}
// }}}
// {{{ wtPasswordInput - single line password input
/**
  Constructs a password input.
  @constructor
  @class A password input. Available Available signals: <br/>
  <ul>
  <li>Enter</li>
  </ul>
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {String} defaultVal Default value of the text box.
  @param {Integer} maxLength (Optional) Maximum length of text box in number of unicode characters.
  @param {Integer} width (Optional) Width of text box in pixels.
  */
var wtPasswordInput = function(parentWidget, parentForm, name, defaultVal, maxLength, width)
{
	if (arguments.length < 3)
		return;

	this.base = wtFormWidget;
	this.base(parentWidget, parentForm, "input", "password", name);
	$_(this).className += " wtPasswordInput";

	if (isString(defaultVal) || isNumber(defaultVal))
	{
		$_(this).value = defaultVal;
		this.setValue(defaultVal);
	}
	else
		this.setValue("");
	this.set("prevValue", this.getValue());
	if (isInteger(maxLength))
		this.set("maxLength", maxLength);
	if (width)
		$_(this).style.width = dimension(width);

	var validateSlot = function(myself, evt, source)
	{
		if (typeof($_(myself).validator) == "function")
		{
			if ($_(myself).validator($_(myself).value, myself.getPrevValue()))
				myself.setValue($_(myself).value);		// this updates prevValue and emits ValueChanged signal
			else
			{
				$_(myself).value = myself.getPrevValue();
				var timedFunc = function(){$_(myself).focus();}
				setTimeout(timedFunc, 0);
			}
		}
		else
		{
			myself.setValue($_(myself).value);
		}
	}
	this.setSlot("Validate", validateSlot);
	var keyPressSlot = function(myself, evt, source)
	{
		if (evt.keyCode == 13)
		{
			$_(myself).onblur();
			myself.emit("Enter", {});
		}
	}
	this.setSlot("KeyPress", keyPressSlot);
	var checkFocusSlot = function(myself, evt, source)
	{
		var d = new Date();
		if (d.getTime() - $_(myself).createTime < 500)
			return;
		var timedFunc = function()
		{
			if (!$_(myself) || !$_(myself).parentNode)
				return;
			var pos = myself.getScreenPosition();
			if (!myself.isInside(mouse.x, mouse.y))
				$_(myself).onblur();
		}
		setTimeout(timedFunc, 0);
	}
	this.setSlot("CheckFocus", checkFocusSlot);

	this.linkSignal("UserChange", "onblur");
	this.setSignal("Enter");
	this.connect("attempt_change", "UserChange", this, "Validate");
	this.connect("key_pressed", "KeyPress", this, "KeyPress");
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
wtPasswordInput.prototype = new wtFormWidget;
/**
  Sets a validator function for this text box. The validator function is executed whenever the text box is defocused.
  <br/><br/>
  The validator function is expected to take two arguments: <br/>
  <i>validatorFunc(currentValue, previousValue)</i><br/><br/>
  where <i>currentValue</i> is the current value of the text box, <i>previousValue</i> is the value of the text box before
  the text box was focused. The function should return true if the current value is valid, and return false if the current
  value is invalid. WT Toolkit will automatically reset the text box's value to <i>previousValue</i> and refocus the text
  box if the validator returns false.
  @param {Function} predicate The validator function
  */
wtPasswordInput.prototype.setValidator = function(predicate)
{
	this.set("validator", predicate);
}
/**
  Removes the validator function.
  */
wtPasswordInput.prototype.unsetValidator = function()
{
	$_(this)["validator"] = undefined;
}
/**
  Sets the value of the text box. No validation is performed for this operation.
  @param {String} newval New string value
  */
wtPasswordInput.prototype.setValue = function(newval)
{
	this.set("prevValue", this.getValue());
	this.set("currentValue", newval);
	$_(this).value = newval;
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
/**
  Tells the current value of the text box
  @returns Current value of text box
  @type String
  */
wtPasswordInput.prototype.getValue = function()
{
	return this.get("currentValue");
}
// }}}
// {{{ wtHiddenInput - hidden input
/**
  Constructs a hidden input.
  @class A hidden input.
  @constructor
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {Object} value The data value of the widget.
  */
var wtHiddenInput = function(parentForm, name, value)
{
	if (arguments.length < 3)
		return;

	// since we're not really a window object
	// we won't run wtFormWidget's constructor here

	this.startProxy(this);
	this.wtDependency = parentForm;
	this.set("wtDependency", parentForm.toString());

	this.set("value", value);
	this.set("prevValue", this.getValue());
	this.set("name", name);
	this.set("parentForm", parentForm);
	this.setSignal("ValueChanged");

	if (parentForm)
		parentForm.addWidget(this);
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
wtHiddenInput.prototype = new wtFormWidget;
// }}}
// {{{ wtCheckBoxGroup - checkbox group
/**
  Constructs a checkbox group.
  @constructor
  @class A checkbox group, which contains a set of checkboxes with the same name field in form submission.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  */
var wtCheckBoxGroup = function(parentForm, name)
{
	if (arguments.length < 2)
		return;

	// again, we won't run the parent constructor here because this is not a window object
	this.startProxy(this);
	this.set("wtDependency", parentForm.toString());

	this.set("disabled", false);
	this.set("checkBoxes", []);
	this.set("name", name);
	this.setSignal("ValueChanged");
	if (parentForm)
	{
		parentForm.addWidget(this);
		this.set("parentForm", parentForm);
	}

	var changeValueSlot = function(myself, evt, source)
	{
		myself.emit("ValueChanged", {"value": myself.getValue(), "prevValue": myself.getPrevValue()});
	}
	this.setSlot("ChangeValue", changeValueSlot);
	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
wtCheckBoxGroup.prototype = new wtFormWidget;
/**
  Tells the current value of the checkbox group. The current value is an array of the values of all the currently checked checkboxes.
  @returns Current value of the checkbox group.
  @type Array
  */
wtCheckBoxGroup.prototype.getValue = function()
{
	var retval = [];
	for(var i=0;i<this.get("checkBoxes").length;i++)
	{
		if ($_(this.get("checkBoxes")[i]).checked)
			retval.push(this.get("checkBoxes")[i].getValue());
	}
	return retval;
}
/**
  Sets the value of one of the checkboxes in the checkbox group.
  @param {Integer} idx The index of the checkbox, counted from 0 and incremented by the order of being added to this checkbox group.
  @param {Object} value The checkbox's new value.
  */
wtCheckBoxGroup.prototype.setValue = function(idx, value)
{
	this.get("checkBoxes")[idx].setValue(value);
}
/**
  Sets the state of a checkbox to be checked or not.
  @param {Integer} idx The index of the checkbox, counted from 0 and incremented by the order of being added to this checkbox group.
  @param {Boolean} checked Whether the checkbox is checked or not.
  */
wtCheckBoxGroup.prototype.setChecked = function(idx, checked)
{
	this.get("checkBoxes")[idx].checked = checked;
}
/**
  Adds a checkbox to the checkbox group.
  @param {wtCheckBox} checkbox The checkbox to be added
  */
wtCheckBoxGroup.prototype.addWidget = function(checkbox)
{
	this.get("checkBoxes").push(checkbox);
	checkbox.connect("value_changed", "ValueChanged", this, "ChangeValue");
}
/**
  @private
  */
wtCheckBoxGroup.prototype._wtFormWidget_setEnabled = wtFormWidget.prototype.setEnabled;
/**
  Sets the checkbox group to be enabled or disabled.
  @param {Boolean} yes true to enable, false to disable.
  */
wtCheckBoxGroup.prototype.setEnabled = function(yes)
{
	this._wtFormWidget_setEnabled(yes);
	if (yes)
	{
		for(var i=0;i<this.get("checkBoxes").length;i++)
			this.get("checkBoxes")[i].disabled = false;
	}
	else
	{
		for(var i=0;i<this.get("checkBoxes").length;i++)
			this.get("checkBoxes")[i].disabled = true;
	}
}
// }}}
// {{{ _wtCheckBox - a checkbox, without the labels
/**
  @private  
  @constructor
  */
var _wtCheckBox = function(parentWidget, parentGroup, value, checked)
{
	if (arguments.length < 3)
		return;

	var extras = (checked ? "checked" : "");
	this.base = wtFormWidget;
	this.base(parentWidget, undefined, "input", "checkbox", parentGroup.name, "onclick", extras);
	
	$_(this).className += " wtCheckBox";
	this.set("name", parentGroup.name);
	this.set("value", value);
	this.set("checked", checked);
	parentGroup.addWidget(this);
	this.emit("ValueChanged", {});
}
_wtCheckBox.prototype = new wtFormWidget;
// }}}
// {{{ wtCheckBox - checkbox with label
/**
  Constructs a checkbox with label.
  @constructor
  @class A checkbox with label.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtCheckBoxGroup} parentGroup The parent checkbox group for the data part of the checkbox.
  @param {Object} value The value of this checkbox.
  @param {String} label The caption of this checkbox.
  @param {Boolean} checked (Optional) Whether the checkbox starts out checked or not.
  */
var wtCheckBox = function(parentWidget, parentGroup, value, label, checked)
{
	if (arguments.length < 4)
		return;

	this.base = wtWidget;
	this.base(parentWidget, "span");
	this.set("checkbox", new _wtCheckBox(this, parentGroup, value, checked));
	this.get("checkbox").className += " wtCheckBox";
	this.set("label", new wtWidget(this , "span"));
	this.get("label").addWidget(label);
	$_(this.get("label")).className += " wtCheckBoxLabelInactive";
	var labelActivateSlot = function(myself, evt, source)
	{
		$_(myself).className = $_(myself).className.replace("wtCheckBoxLabelInactive", "wtCheckBoxLabelActive");
	}
	this.get("label").setSlot("Activate", labelActivateSlot);
	var labelDeactivateSlot = function(myself, evt, source)
	{
		$_(myself).className = $_(myself).className.replace("wtCheckBoxLabelActive", "wtCheckBoxLabelInactive");
	}
	this.get("label").setSlot("Deactivate", labelDeactivateSlot);
	var clickLabelSlot = function(myself, evt, source)
	{
		if (myself.get("checkbox").get("disabled"))
			return;
		$_(myself.get("checkbox")).checked = !$_(myself.get("checkbox")).checked;
		myself.get("checkbox").emit("ValueChanged", {});
	}
	this.setSlot("ClickLabel", clickLabelSlot);

	this.get("label").connect("mouse_activate", "MouseOver", this.get("label"), "Activate");
	this.get("label").connect("mouse_deactivate", "MouseOut", this.get("label"), "Deactivate");
	this.get("label").connect("mouse_click", "Click", this, "ClickLabel");
	this.setSelectable(false);
}
wtCheckBox.prototype = new wtWidget;
// }}}
// {{{ wtRadioGroup - radio button group
/**
  Constructs a radio button group.
  @constructor
  @class A radio button group, which contains a set of radio buttons with the same name field and at most one
  of the buttons can be checked at any time.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  */
var wtRadioGroup = function(parentForm, name)
{
	if (arguments.length < 2)
		return;

	// again, we won't run the parent constructor here
	this.startProxy(this);
	this.set("name", name);
	this.set("parentForm", parentForm);
	this.set("value", undefined);
	this.set("prevValue", undefined);
	this.set("widgetMap", {});
	this.set("disabled", false);
	this.setSignal("ValueChanged");
	if (parentForm)
	{
		this.set("wtDependency", parentForm.toString());
		parentForm.addWidget(this);
	}
	var changeValueSlot = function(myself, evt, source)
	{
		if ($_(source).checked)
			$_(myself).value = $_(source).value;
		myself.emit("ValueChanged", {"value": myself.getValue(), "prevValue": myself.getPrevValue()});
	}
	this.setSlot("ChangeValue", changeValueSlot);

	this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
}
wtRadioGroup.prototype = new wtFormWidget;
/**
  @private
  */
wtRadioGroup.prototype._wtFormWidget_setValue = wtFormWidget.prototype.setValue;
/**
  Makes the radio button with a specified data value checked.
  @param {Object} value The specified data value.
  */
wtRadioGroup.prototype.setValue = function(value)
{
	this._wtFormWidget_setValue(value);
	$_($_(this).widgetMap[value]).checked = true;
}
/**
  Adds a radio button to this radio button group.
  @param {wtRadioButton} widget The radio button to be added.
  */
wtRadioGroup.prototype.addWidget = function(widget)
{
	$_(this).widgetMap[$_(widget).value] = widget;
	if ($_(widget).checked)
	{
		$_(this).value = $_(widget).value;
		this.emit("ValueChanged", {"value": this.getValue(), "prevValue": this.getPrevValue()});
	}
	widget.connect("value_changed", "ValueChanged", this, "ChangeValue");
}
/**
  @private
  */
wtRadioGroup.prototype._wtFormWidget_setEnabled = wtFormWidget.prototype.setEnabled;
/**
  Sets the radio button group to be enabled or disabled.
  @param {Boolean} yes true to enable, false to disable.
  */
wtRadioGroup.prototype.setEnabled = function(yes)
{
	this._wtFormWidget_setEnabled(yes);
	if (yes)
	{
		for(var i in $_(this).widgetMap)
			$_(this).widgetMap[i].set("disabled", false);
	}
	else
	{
		for(var i in this.widgetMap)
			$_(this).widgetMap[i].set("disabled", true);
	}
}
// }}}
// {{{ wtRadioButton - radio button with label
/**
  Constructs a radio button with label.
  @constructor
  @class A radio button with label.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtRadioGroup} parentGroup The parent radio button group of the data part of the form widget.
  @param {Object} value The value of this radio button.
  @param {String} caption The caption of the radio button.
  @param {Boolean} checked (Optional) Whether the radio button starts out checked or not.
  */
var wtRadioButton = function(parentWidget, parentGroup, value, caption, checked)
{
	if (arguments.length < 4)
		return;

	this.base = wtWidget;
	this.base(parentWidget, "span");
	var extras = checked ? "checked" : "";
	this.set("radio", new wtFormWidget(this, undefined, "input", "radio", parentGroup.name, "onclick", extras));
	var radio = this.get("radio");
	$_(radio).className += " wtRadioButton";
	$_(radio).value = value;
	$_(radio).checked = checked;
	parentGroup.addWidget(radio);
	
	this.set("label", new wtWidget(this, "span"));
	var label = this.get("label");
	label.addWidget(caption);
	$_(label).className += " wtRadioLabelInactive";
	var labelActivateSlot = function(myself, evt, source)
	{
		$_(myself).className = $_(myself).className.replace("wtRadioLabelInactive", "wtRadioLabelActive");
	}
	label.setSlot("Activate", labelActivateSlot);
	var labelDeactivateSlot = function(myself, evt, source)
	{
		$_(myself).className = $_(myself).className.replace("wtRadioLabelActive", "wtRadioLabelInactive");
	}
	label.setSlot("Deactivate", labelDeactivateSlot);
	var clickLabelSlot = function(myself, evt, source)
	{
		if (myself.get("radio").get("disabled"))
			return;
		myself.get("radio").set("checked", true);
		myself.get("radio").emit("ValueChanged", {});
	}
	this.setSlot("ClickLabel", clickLabelSlot);

	label.connect("mouse_activate", "MouseOver", label, "Activate");
	label.connect("mouse_deactivate", "MouseOut", label, "Deactivate");
	label.connect("mouse_check", "Click", this, "ClickLabel");
	
	this.setSelectable(false);
}
wtRadioButton.prototype = new wtWidget;
// }}}
// {{{ wtSingleSelect - single selection widget
/**
  Constructs a single selection input.
  @constructor
  @class A single selection input.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {Array} valueList The array of data values of items in the selection box.
  @param {Array} labelLsit The array of labels of the items in the selection box.
  @param {Object} defaultVal (Optional) The data value of the initially selected item.
  */
var wtSingleSelect = function(parentWidget, parentForm, name, valueList, labelList, defaultVal)
{
	if (arguments.length < 5)
		return;

	if (valueList.length != labelList.length)
	{
		throw "wtSingleSelect(): valueList length not equal to labelList length";
		return;
	}

	this.base = wtFormWidget;
	this.base(parentWidget, parentForm, "select", undefined, name, "onchange", undefined);
	
	for(var i=0;i<valueList.length;i++)
	{
		var option = new wtWidget(null, "option");
		$_(option).text = labelList[i];
		$_(option).value = valueList[i];
		if (isMSIE())
			$_(this).add($_(option));
		else
			$_(this).add($_(option), null);
	}

	if (defaultVal != undefined && defaultVal != null)
		$_(this).value = defaultVal;

	this.linkSignal("UserChange", "onchange");
	var userChangeSlot = function(myself, evt, source)
	{
		myself.emit("ValueChanged", {"value": myself.getValue()});
	}
	this.setSlot("UserChange", userChangeSlot);
	this.connect("user_change", "UserChange", this, "UserChange");
	this.emit("ValueChanged", {"value": this.getValue()});
}
wtSingleSelect.prototype = new wtFormWidget;
// }}}
// {{{ wtMultiSelect - multiple selection widget
/**
  Constucts a multiple selection box.
  @constructor
  @class A multiple selection box.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {Array} valueList The array of data values of items in the selection box.
  @param {Array} labelLsit The array of labels of the items in the selection box.
  @param {Array} selectedList The array of data values of items that are initially selected.
  @param {Integer} boxSize The vertical size of the selection box in terms of number of visible items.
  */
var wtMultiSelect = function(parentWidget, parentForm, name, valueList, labelList, selectedList, boxSize)
{
	if (arguments.length < 7)
		return;

	if (valueList.length != labelList.length)
	{
		throw "wtSingleSelect(): valueList length not equal to labelList length";
		return;
	}

	this.base = wtFormWidget;
	if (isMSIE())
		this.base(parentWidget, parentForm, "<select multiple>", undefined, name, "onchange", undefined);
	else
		this.base(parentWidget, parentForm, "select", undefined, name, "onchange", undefined);
	this.set("size", boxSize);
	this.set("multiple", true);
	
	var selectedMap = {};
	if (selectedList)
	{
		for(var i=0;i<selectedList.length;i++)
			selectedMap[String(selectedList[i])] = 1;
	}
	for(var i=0;i<valueList.length;i++)
	{
		if (isMSIE())
		{
			var option = new wtWidget(null, "option");
			$_(option).text = labelList[i];
			$_(option).value = valueList[i];
			if (selectedMap[valueList[i]]) $_(option).selected = true;
			$_(this).add($_(option));
		}
		else
		{
			var option = new wtWidget(null, "option");
			$_(option).text = labelList[i];
			$_(option).value = valueList[i];
			if (selectedMap[valueList[i]]) $_(option).selected = true;
			$_(this).add($_(option), null);
		}
	}

	this.linkSignal("UserChange", "onchange");
	var userChangeSlot = function(myself, evt, source)
	{
		myself.emit("ValueChanged", {"value": myself.getValue()});
	}
	this.setSlot("UserChange", userChangeSlot);
	this.connect("user_change", "UserChange", this, "UserChange");
	this.emit("ValueChanged", {"value": this.getValue()});
}
wtMultiSelect.prototype = new wtFormWidget;
/**
  Tells the values of the currently selected items.
  @type Array
  @returns An array of values of currently selected items.
  */
wtMultiSelect.prototype.getValue = function()
{
	var retval = [];
	for(var i=0;i<$_(this).options.length;i++)
		if ($_(this).options.item(i).selected)
			retval.push($_(this).options.item(i).value);
	return retval;
}
/**
  Selects all items matching an array of data values.
  @param {Array} newValue Array of data values for matching items to be selected.
  */
wtMultiSelect.prototype.setValue = function(newValue)
{
	for(var i=0;i<$_(this).options.length;i++)
		$_(this).options.item(i).selected = false;
	for(var i=0;i<selectedList.length;i++)
		$_(this).optionMap[String(selectedList[i])].selected = true;
}
// }}}
// {{{ wtSpreadSheet - editable spreadsheet widget
/**
  Constructs an editable spreahsheet input.
  @constructor
  @class An editable spreadsheet widget. Available signals:
  <br/><ul>
  <li>ValueChanged</li>
  </ul>
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {Array} rowList A 2D array of labels displayed as the contents of the spreadsheet.
  @param {Array} labelList A 1D array of labels displayed as the subtitle row of the spreadsheet.
  @param {Array} editableList (Optional) A 1D array of booleans for indicating which columns of the spreadsheet are editable.
  If this parameter is not given then all columns of the spreadsheet are editable.
  @param {Array} validatorList (Optional) A 1D array of predicate function objects for validating the updated cell values for
  each column. The predicates here have the same semantics of the validators used in the class wtSingleLineTextInput.
  @see wtSingleLineTextInput#setValidator
  */
var wtSpreadSheet = function(parentWidget, parentForm, name, rowList, labelList, editableList, validatorList)
{
	if (arguments.length < 5)
		return;

	this.base = wtDisplay;
	this.base(parentWidget, "100%");
	$_(this).className += " wtSpreadSheet";
	this.set("name", name);
	this.set("wtDisabled", false);
	this.set("validatorList", validatorList);
	this.set("editableList", editableList);
	if (labelList)
		this.setSubtitleRow(labelList);
	for(var i=0;i<rowList.length;i++)
		this.setDisplayRow(rowList[i]);
	this.setSignal("ValueChanged");
	if (parentForm)
	{
		this.set("wtDependency", parentForm.toString());
		parentForm.addWidget(this);
	}
	var sortSlot = function(myself, evt, source)
	{
		if (myself.isEnabled() == false)
			return;
		var inputs = $_($_(myself).tbody).getElementsByTagName("textarea");
		if (inputs.length)
			return;
		inputs = $_($_(myself).tbody).getElementsByTagName("input");
		for(var i=0;i<inputs.length;i++)
			if (inputs[i].type == "text" || inputs[i].type == "password")
				return;
		$_(myself).wtSlots._wtDisplay_Sort(myself, evt, source);
	}
	this.setSlot("_wtDisplay_Sort", this.get("wtSlots").Sort);
	this.setSlot("Sort", sortSlot);
	this.emit("ValueChanged", {"value": this.getValue()});
}
wtSpreadSheet.prototype = new wtDisplay;
/**
  @private
  */
wtSpreadSheet.prototype._wtDisplay_setDisplayRow = wtDisplay.prototype.setDisplayRow;
/**
  Adds a row with a list of strings.
  @param {Array} row Array of strings to be added.
  */
wtSpreadSheet.prototype.setDisplayRow = function(row)
{
	var spreadsheet = this;
	var editStartHandler = function(myself, evt, source)
	{
		if ($_(spreadsheet).wtDisabled)
			return;
		var clientWidth = $_(myself).clientWidth;
		var editor = new wtPopupTextbox(undefined, undefined, "noname", $_(myself).cellValue, undefined, clientWidth);
		myself.setContent(editor);
		editor.emit("Click");
		$_(editor).focus();
		if ($_(spreadsheet).validatorList && typeof($_(spreadsheet).validatorList[myself.colIndex()]) == "function")
			editor.setValidator($_(spreadsheet).validatorList[myself.colIndex()]);
		editor.connect("edit_end", "ValueChanged", myself, "EditEnd");
		myself.disconnect("edit_start", "Click");
		myself.setSize(clientWidth,"auto");
	}
	var editFinishHandler = function(myself, evt, source)
	{
		if ($_(spreadsheet).wtDisabled)
			return;
		var newValue = source.getValue();
		$_(myself).innerHTML = newValue;
		myself.setValue(newValue);
		myself.connect("edit_start", "Click", myself, "EditStart");
		if ($_(spreadsheet).sortCell)
		{
			var sortIndex = $_(spreadsheet).sortCell.colIndex();
			$_(spreadsheet).colList[sortIndex] *= -1;
			spreadsheet.sort(sortIndex);
		}
		spreadsheet.emit("ValueChanged", {"value": spreadsheet.getValue()});
	}
	var activateCell = function(myself, evt, source)
	{
		if ($_(spreadsheet).wtDisabled)
			return;
		$_(myself).className = $_(myself).className.replace("wtSpreadSheetCellInactive", "wtSpreadSheetCellActive");
	}
	var deactivateCell = function(myself, evt, source)
	{
		if ($_(spreadsheet).wtDisabled)
			return;
		$_(myself).className = $_(myself).className.replace("wtSpreadSheetCellActive", "wtSpreadSheetCellInactive");
	}
	var curRow = this._wtDisplay_setDisplayRow(row);
	for(var j=0;j<$_(curRow).cells.length;j++)
	{
		var curCell = _$($_(curRow).cells.item(j));
		$_(curCell).className += " wtSpreadSheetCellInactive";
		curCell.setSlot("EditStart", editStartHandler);
		curCell.setSlot("EditEnd", editFinishHandler);
		curCell.setSlot("Activate", activateCell);
		curCell.setSlot("Deactivate", deactivateCell);
		if ($_(this).editableList == undefined || $_(this).editableList == null || $_(this).editableList[j])
		{
			curCell.connect("edit_start", "Click", curCell, "EditStart");
			curCell.connect("activate", "MouseOver", curCell, "Activate");
			curCell.connect("deactivate", "MouseOut", curCell, "Deactivate");
		}
	}
}
/**
  @private
  */
wtSpreadSheet.prototype._wtDisplay_delDisplayRow = wtDisplay.prototype.delDisplayRow;
/**
  Removes a spreadsheet row.
  @param {Integer} rowIdx The row index, counted from 0 from the top of the spreadsheet content rows.
  */
wtSpreadSheet.prototype.delDisplayRow = function(rowIdx)
{
	this._wtDisplay_delDisplayRow(rowIdx);
	this.emit("ValueChanged", {"value": this.getValue()});
}
/**
  @private
  */
wtSpreadSheet.prototype.getPrevValue = function()
{
	// not implemented yet
	return undefined;
}
/**
  Tells the values of all cells in the spreadsheet in a 2D array.
  @returns Values of all cells.
  @type Array
  */
wtSpreadSheet.prototype.getValue = function()
{
	return this.getValueArray();
}
/**
  Sets the value and string contents of one of the spreadsheet rows.
  @param {Integer} rowIdx The row index.
  @param {Array} valueList New contents of the spreadsheet row.
  */
wtSpreadSheet.prototype.setValue = function(rowIdx, valueList)
{
	var row = _$($_(this).tbody.rows.item(rowIdx));
	for(var i=0;i<valueList.length;i++)
	{
		_$($_(row).cells.item(i)).setValue(valueList[i]);
		_$($_(row).cells.item(i)).setContent(valueList[i]);
	}
	return row;
}
/**
  Sets the spreadsheet to be enabled or disabled.
  @param {Boolean} yes true to enable, false to disable.
  */
wtSpreadSheet.prototype.setEnabled = function(yes)
{
	$_(this).wtDisabled = !yes;
	if (yes)
		this.setAlpha(1.0);
	else
		this.setAlpha(0.5);
}
/**
  Tells whether the spreadsheet is enabled or not.
  @returns Whether the spreadsheet is enabled or not.
  @type Boolean
  */
wtSpreadSheet.prototype.isEnabled = function()
{
	return !($_(this).wtDisabled);
}
// }}}
// {{{ wtVariableSpreadSheet - editable spreadsheet where you can add and delete rows as well
/**
  Constructs a variable spreadsheet.
  @constructor
  @class A variable spreadsheet where a user can add and delete rows in addition to editing the contents.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {Array} rowList A 2D array of labels displayed as the contents of the spreadsheet.
  @param {Array} labelList A 1D array of labels displayed as the subtitle row of the spreadsheet.
  @param {Array} editableList (Optional) A 1D array of booleans for indicating which columns of the spreadsheet are editable.
  If this parameter is not given then all columns of the spreadsheet are editable.
  @param {Array} validatorList (Optional) A 1D array of predicate function objects for validating the updated cell values for
  each column. The predicates here have the same semantics of the validators used in the class wtSingleLineTextInput.
  @param {Array} insertValidator (Optional) A predicate function for validating new spreadsheet rows.
  @see wtSingleLineTextInput#setValidator
  */
var wtVariableSpreadSheet = function(parentWidget,
	   	parentForm, 
		name, 
		rowList, 
		labelList, 
		editableList, 
		validatorList,
		insertValidator)
{
	if (arguments.length < 5)
		return;

	labelList.push("");
	if (editableList)
		editableList.push(0);
	else
	{
		editableList = [];
		for(var i=0;i<labelList.length -1;i++)
			editableList.push(1);
		editableList.push(0);
	}

	if (validatorList)
		validatorList.push(null);

	var deleteRow = function(myself, evt, source)
	{
		var curNode = $_(myself);
		while(curNode != null && curNode.tagName && curNode.tagName.toLowerCase() != "tr")
			curNode = curNode.parentNode;
		if (curNode && curNode.tagName && curNode.tagName.toLowerCase() == "tr")
			_$(curNode).removeSelf();
	}

	this.base = wtSpreadSheet;
	this.base(parentWidget, parentForm, name, rowList, labelList, editableList, validatorList);
	_$($_($_(this).tshead).rows.item(0).cells.item($_($_(this).tshead).rows.item(0).cells.length -1)).setSize("50px", "auto");
	$_(this).className += " wtVariableSpreadSheet";
	$_(this).colList[$_(this).colList.length -1] = 0;

	$_(this).tstatus.setDisplay(true);
	if (labelList && labelList.length > 1)
	{
		$_(this).textBoxList = [];
		for(var i=0;i<labelList.length -1;i++)
			$_(this).textBoxList.push(new wtSingleLineTextInput(null, null, "", ""));
		var addButton = new wtButton(null, "Add");

		var addRow = function(myself, evt, source)
		{
			var newRow = [];
			for(var i=0;i<$_(myself).textBoxList.length;i++)
				newRow.push($_(myself).textBoxList[i].getValue());
			if ($_(myself).insertValidator && (!$_(myself).insertValidator(newRow))) return;

			for(var i=0;i<$_(myself).textBoxList.length;i++)
				$_(myself).textBoxList[i].setValue("");
			myself.setDisplayRow(newRow);
			if (myself.sortCell)
			{
				var sortIndex = myself.sortCell.colIndex();
				myself.colList[sortIndex] *= -1;
				myself.sort(sortIndex);
			}
			myself.emit("ValueChanged", {"value": myself.getValue()});
		}
		this.setSlot("AddRow", addRow);
		$_(this).insertValidator = insertValidator;
		addButton.connect("add_row", "Click", this, "AddRow");
		this.get("textBoxList").push(addButton);
		
		var appender = new wtWidget($_(this).tstatus, "tr");
		for(var i=0;i<$_(this).textBoxList.length;i++)
		{
			var cell = new wtWidget(appender, "td");
			cell.addWidget($_(this).textBoxList[i]);
			$_(cell).className += " DisplayContent";
		}
		$_(this).textBoxList.pop();
	}
}
wtVariableSpreadSheet.prototype = new wtSpreadSheet;
wtVariableSpreadSheet.prototype._wtSpreadsheet_setDisplayRow = wtSpreadSheet.prototype.setDisplayRow;
wtVariableSpreadSheet.prototype.setDisplayRow = function(rowList)
{
	var spreadsheet = this;
	var deleteRow = function(myself, evt, source)
	{
		var curNode = $_(myself);
		while(curNode != null && curNode.tagName && curNode.tagName.toLowerCase() != "tr")
			curNode = curNode.parentNode;
		if (curNode && curNode.tagName && curNode.tagName.toLowerCase() == "tr")
			_$(curNode).removeSelf();
		spreadsheet.emit("ValueChanged", {"value": spreadsheet.getValue()});
	}
	var deleteIcon = new wtImage(null, "redist/continuum/16x16/filesystems/trashcan_empty.png");
	deleteIcon.setSize(16, 16);
	$_(deleteIcon).style.cursor = "pointer";
	$_(deleteIcon).border = 0;
	deleteIcon.setSlot("DeleteRow", deleteRow);
	deleteIcon.connect("delete_row", "Click", deleteIcon, "DeleteRow");
	rowList.push(deleteIcon);
	this._wtSpreadsheet_setDisplayRow(rowList);
}
// }}}
// {{{ wtSubmit - submit button that changes to "Abort" when the form is being submitted
/**
  Constructs a submit button.
  @constructor
  @class A submit button that changes to "Abort" and allows aborting form submission when
  the form is being submitted.
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} _defaultLabel (Optional) The caption of the button in its normal state.
  @param {String} _abortLabel (Optional) The caption of the button when the form is being submitted.
  */
var wtSubmit = function(parentWidget, parentForm, _defaultLabel, _abortLabel)
{
	if (arguments.length < 2)
		return;

	var defaultLabel = _defaultLabel ? _defaultLabel : "Submit";
	var abortLabel = _abortLabel ? _abortLabel : "Abort";

	this.base = wtButton; 
	this.base(parentWidget, defaultLabel);
	this.set("defaultLabel", defaultLabel);
	this.set("abortLabel", abortLabel);
	this.set("parentForm", parentForm);
	this.set("wtDisabled", false);
	this.set("uniqueId", "submit"+genId());
	$_(parentForm).submitExclude[this.get("uniqueId")] = this.toString();

	var handler = function(myself, evt, source)
	{
		myself.get("parentForm").submit();
	}
	this.set("submitHandler", handler);
	this.setHandler(this.get("submitHandler"));
	var abortHandler = function(myself, evt, source)
	{
		myself.get("parentForm").abort();
	}
	this.set("abortHandler", abortHandler);

	var submitSlot = function(myself, evt, source)
	{
		myself.onclick();
	}
	this.setSlot("Submit", submitSlot);
}
wtSubmit.prototype = new wtButton;
/**
  Sets the button to be enabled or disabled.
  @param {Boolean} yes true to enable, false to disable.
  */
wtSubmit.prototype.setEnabled = function(yes)
{
	this.set("wtDisabled", !yes);
	if (yes)
	{
		this.setContent(this.get("defaultLabel"));
		this.setHandler(this.get("submitHandler"));
	}
	else
	{
		this.setContent(this.get("abortLabel"));
		this.setHandler(this.get("abortHandler"));
	}
}
/**
  Tells whether the button is enabled or not.
  @returns Whether the button is enabled or not.
  @type Boolean
  */
wtSubmit.prototype.isEnabled = function()
{
	return !(this.get("wtDisabled"));
}
// }}}
// {{{ wtPopupTextbox - A textbox that appears as a simple label at idle, and appears to be editable after a click
/**
  Constructs a textbox that appears as a simple label at idle, and appears to be editable after a click.
  @class A textbox that appears as a simple label at idle, and appears to be editable after a click.<br/><br/>
	Available signals:
	<ul>
	<li>EnterEditMode</li>
	<li>ExitEditMode</li>
	</ul>
  @constructor
  @param {wtWidget|DOM_Node} parentWidget The parent container of the visible part of the form widget. Can be null if you don't want to immediately add the form widget to a container.
  @param {wtForm} parentForm The parent form for the data part of the form widget. Can be null if you don't need to submit the input's data back to web server
  @param {String} name The name of the form input. This is sent to the server as the name part of the
  form widget's name:value pair when the parent form is submitted.
  @param {String} defaultVal Default value of the text box.
  @param {Integer} maxLength (Optional) Maximum length of text box in number of unicode characters.
  @param {Integer} width (Optional) Width of text box in pixels.
  */
var wtPopupTextbox = function(parentWidget, parentForm, name, defaultVal, maxLength, width)
{
	if (arguments.length < 3)
		return;

	this.base = wtSingleLineTextInput;
	this.base(parentWidget, parentForm, name, defaultVal, maxLength, width);

	$_(this).className += " wtPopupTextboxIdle";
	this.set("readOnly", true);
	this.setSelectable(false);
	this.get("style").cursor = "";

	var mouseOverSlot = function(myself, evt, source)
	{
		$_(myself).className = $_(myself).className.replace("wtPopupTextboxIdle", "wtPopupTextboxHighlighted");
	}
	var mouseOutSlot = function(myself, evt, source)
	{
		$_(myself).className = $_(myself).className.replace("wtPopupTextboxHighlighted", "wtPopupTextboxIdle");
	}
	var enterEditModeSlot = function(myself, evt, source)
	{
		if (! myself.get("readOnly"))
			return;
		myself.disconnect("enable_highlight", "MouseOver");
		myself.disconnect("disable_highlght", "MouseOut");
		window.mouse.connect(myself.toString(), "MouseUp", myself, "ExitEditMode");
		myself.connect("exit_after_enter", "Enter", myself, "ExitEditMode");
		myself.set("readOnly", false);
		$_(myself).className = $_(myself).className.replace("wtPopupTextboxHighlighted", "wtPopupTextboxIdle");
		$_(myself).className = $_(myself).className.replace("wtPopupTextboxIdle", "wtPopupTextboxActive");
		myself.setSelectable(true);
		myself.focus();
		myself.emit("EnterEditMode");
	}
	var exitEditModeSlot = function(myself, evt, source)
	{
		if (source == window.mouse && myself.isInside(mouse.x, mouse.y))
			return;
		myself.set("readOnly", true);
		myself.setSelectable(false);
		myself.get("style").cursor = "";
		$_(myself).className = $_(myself).className.replace("wtPopupTextboxActive", "wtPopupTextboxIdle");
		myself.connect("enable_highlight", "MouseOver", myself, "EnableHighlight");
		myself.connect("disable_highlght", "MouseOut", myself, "DisableHighlight");
		window.mouse.disconnect(myself.toString(), "MouseUp");
		myself.disconnect("exit_after_enter", "Enter");
		myself.emit("ExitEditMode");
	}
	this.setSignal("EnterEditMode");
	this.setSignal("ExitEditMode");
	this.setSlot("EnableHighlight", mouseOverSlot);
	this.setSlot("DisableHighlight", mouseOutSlot);
	this.setSlot("EnterEditMode", enterEditModeSlot);
	this.setSlot("ExitEditMode", exitEditModeSlot);
	this.connect("enable_highlight", "MouseOver", this, "EnableHighlight");
	this.connect("disable_highlght", "MouseOut", this, "DisableHighlight");
	this.connect("enter_edit_mode", "Click", this, "EnterEditMode");
}
wtPopupTextbox.prototype = new wtSingleLineTextInput;
// }}}
