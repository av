#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The alignment visualization tool.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA

Copyright (c) 2007 Ning Shi

@name           av
@author-name    Ning Shi
@author-email   cs_snx@stu.ust.hk
@license-name   GNU LGPL
@license-url    http://www.gnu.org/licenses/lgpl.html
"""

__author__ = "Ning Shi"
__license__ = "GNU LGPL"

import os

import simplejson
from htmltmpl import TemplateManager, TemplateProcessor
from sgmlparser import FileParser
from Error import *

# Defaults for the template file engine configurations
__TEMP_DIR__	= "template"

class AV:
    """The main alignment visualization class.

    This class is the controller.  It glues the model and views together.

    Properties:
    	__has_data__	If there is data to be sent to the client.
        __alignment__	The string containing the JSON object to be sent
    """

    def __init__(self):
        """Constructor

        @head __init__()
        """

        self.__has_data__ = 0

    def parse_file(self, data):
        """Parse alignment file uploaded from the client.

        @head parse_file(data)

        @param data The file content uploaded from the client.
        """

        if not data:
            raise FatalError("Empty file given.")

        parser = FileParser()
        parser.feed(data)
        parser.close()

        result = simplejson.dumps(parser.get_result())
        self.__alignment__ = result

        self.__has_data__ = 1

    def generate_page(self, temp_file):
        """Generate the HTML page.

        @header generate_page(temp_file)

        @param temp_file The template file to process.  If the file does not
        exist, the function will try to look for it in the default template
        directory, which is the "template" subdirectory in the current directory.

        @return Generated page as a string.

        This function will read the template files and generate the page.
        """

        if not os.path.isfile(temp_file):
            if not os.path.isfile(os.path.join(__TEMP_DIR__ + temp_file)):
                raise FatalError("File " + temp_file + "does not exist.")
            temp_file = os.path.join(__TEMP_DIR__ + temp_file)

        manager = htmltmpl.TemplateManager()
        try:
            template = manager.prepare(temp_file)
        except htmltmpl.TemplateError:
            raise FatalError("Unable to generate page from template " + \
                             temp_file)

        processor = htmltmpl.TemplateProcessor()

        # Insert data
        processor.set("has_data", self.__has_data__)
        if self.__has_data__:
            processor.set("alignment", self.__alignment__)

        return processor.process(template).decode("utf-8")
