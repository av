#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The alignment visualization tool.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA

Copyright (c) 2007 Ning Shi

@name           av
@author-name    Ning Shi
@author-email   cs_snx@stu.ust.hk
@license-name   GNU LGPL
@license-url    http://www.gnu.org/licenses/lgpl.html
"""

__author__ = "Ning Shi"
__license__ = "GNU LGPL"

import re

from sgmllib import SGMLParser

class FileParser(SGMLParser):
    """SGML alignment file parser.

    This is the class to parse alignment file in SGML format.

    Properties:
    	__tags__		Allowed tags.
    	__subs__		Regular expression to parse each section.
        __verbatim__		Treat tag as is if in verbatim mode.
    	__current_tag__		The tag currently processing.
        __current_data__	The data as a dictionary.
        __result__		The parsed results in a dictionary.
    """

    __tags__ = ("header",
                "source_sentence",
                "target_sentence",
                "reference_sentence",
                "alignment",
                "reference_alignment",
                "null_fertility",
                "fertilities",
                "distortions",
                "translations",
                "inverse_translations",
                "language_model")

    __subs__ = {"header": r"^([^:]*):\s*(.*)$"}

    def __process__(self, tag, line):
        """This method parses each line in different sections into proper forms.

        @hidden
        """

        if tag in self.__subs__:
            return re.search(self.__subs__[tag], line).groups()

        if tag in ("source_sentence", "target_sentence", "reference_sentence"):
            return tuple(tuple(i.split("|")) for i in line.split())
        elif tag in ("alignment", "reference_alignment"):
            return tuple(int(i) for i in line.split())
        elif tag == "null_fertility":
            return line
        elif tag in ("fertilities", "distortions"):
            return tuple(i.isdigit() and int(i) or i \
                         for i in line.split())
        elif tag in ("translations", "inverse_translations", "language_model"):
            i = line.split()
            return int(i[0]), i[1], i[2]

    def reset(self):
        """Reset.
        """

        self.__verbatim__ = False
        self.__current_tag__ = None
        self.__current_data__ = ""
        self.__result__ = {}
        SGMLParser.reset(self)

    def unknown_starttag(self, tag, attrs):
        """This is called every time a start tag is seen. All data following
        this tag will be saved under the same name as the tag name in the result
        dictionary until the closing tag is seen.
        """
        if (not self.__verbatim__) and (tag.lower() in self.__tags__):
            self.__current_tag__ = tag.lower()
            self.__verbatim__ = True
            return

        strattrs = "".join([' %s="%s"' % (key, value) for key, value in attrs])
        self.__current_data__ += "<%(tag)s%(strattrs)s>" % locals()

    def unknown_endtag(self, tag):
        """This is called every time an end tag is seen. It changes the mode
        back so that the data following this tag will be saved into a different
        slot.
        """

        if tag.lower() not in self.__tags__:
            if self.__verbatim__:
                self.__current_data__ += "</%(tag)s>" % locals()
            return

        stripped = self.__current_data__.strip()
        processed = tuple(self.__process__(self.__current_tag__, i) \
                          for i in stripped.split("\n"))
        self.__result__[self.__current_tag__] = processed

        self.__current_tag__ = None
        self.__current_data__ = ""
        self.__verbatim__ = False

    def handle_data(self, data):
        """This is called for every block of text.
        """

        self.__current_data__ += data

    def get_result(self):
        """This returns the parsed data as a dictionary.
        """

        return self.__result__
