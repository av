#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The alignment visualization tool.

This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2.1 of the License, or (at your option) any
later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this library; if not, write to the Free Software Foundation, Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA

Copyright (c) 2007 Ning Shi

@name           av
@author-name    Ning Shi
@author-email   cs_snx@stu.ust.hk
@license-name   GNU LGPL
@license-url    http://www.gnu.org/licenses/lgpl.html
"""

__author__ = "Ning Shi"
__license__ = "GNU LGPL"

import cgi
import cgitb; cgitb.enable()

import alignment

def main():
    av = alignment.AV();
    data = cgi.FieldStorage()

    if (data.has_key("file")):
        content = data["file"].file.read()
        av.parse_file(content)

    print "Content-Type: text/html\n"
    print av.generate_page("templates/main.tmpl")

if __name__ == '__main__':
    main()
